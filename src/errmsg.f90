SUBROUTINE ERRMSG (STR1,*)
  !======================================================================C
  Use Common_DLV
  !-st	Use Parameters
  !-st	Use Common_B1
  !-st	Use Common_B2
  !
  !	Output an error message of the form ** error ** str1
  !
  CHARACTER   :: STR1*(*),MSG*80
  CHARACTER*8 :: ERMSG
  DATA ERMSG/'ERROR - '/
  CALL ERRS (*11)
11 L=MIN0(80-LEN(ERMSG),LEN(STR1))
  MSG=STR1
  if(DLV_flag) then
     call wtext('#dlv')
     call wtext('ERROR')
     call wtext(MSG(1:L))
     call wtext('#dlv')
  endif
  CALL WTEXT (ERMSG//MSG(1:L))
  RETURN 1
END SUBROUTINE ERRMSG

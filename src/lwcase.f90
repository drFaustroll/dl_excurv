SUBROUTINE LWCASE (STRING)
  !======================================================================C
  !
  !	Convert a text string to lower case in situ
  !       STRING (I/O) Character string to be converted
  !
  CHARACTER*(*) STRING
  CHARACTER*26 LCASE,UCASE
  DATA LCASE/'abcdefghijklmnopqrstuvwxyz'/
  DATA UCASE/'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
  !
  LL=LEN(STRING)
  IF (LL.LE.0) RETURN
  DO  L=1,LL
     K=INDEX(UCASE,STRING(L:L))
     IF (K.EQ.0) cycle
     STRING(L:L)=LCASE(K:K)
  enddo
  RETURN
END SUBROUTINE LWCASE

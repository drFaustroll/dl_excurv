SUBROUTINE XROTATE (AX,AY,AZ,DIFF,CX,CY,CZ)
  !======================================================================C
  Use Common_convia
  DIMENSION RL(3),VXYZ(3),TXYZ(3),P(3,3)
  RL(1)=AX
  RL(2)=AY
  RL(3)=AZ
  VXYZ(1)=CX
  VXYZ(2)=CY
  VXYZ(3)=CZ
  TRL=RL(1)*RL(1)+RL(2)*RL(2)+RL(3)*RL(3)
  IF (TRL.EQ.0.) THEN
     TRL=1.
     RL(3)=1.
  ENDIF
  DO I=1,3
     IF (RL(I).LT.0.) THEN
        SIGN=-1.
     ELSE
        SIGN=1.
     ENDIF
     RL(I)=SQRT(RL(I)*RL(I)/TRL)*SIGN
  ENDDO
  CA=COS(DIFF)
  SA=SIN(DIFF)
  IF (DIFF/AC.EQ.90.) THEN
     CA=0.
     SA=1.
  ELSEIF (DIFF/AC.EQ.-90.) THEN
     CA=0.
     SA=-1.
  ENDIF
  P(1,1)=CA+RL(1)*RL(1)*(1.-CA)
  P(1,2)=RL(1)*RL(2)*(1.-CA)+RL(3)*SA
  P(1,3)=RL(3)*RL(1)*(1.-CA)-RL(2)*SA
  P(2,1)=RL(1)*RL(2)*(1.-CA)-RL(3)*SA
  P(2,2)=CA+RL(2)*RL(2)*(1.-CA)
  P(2,3)=RL(2)*RL(3)*(1.-CA)+RL(1)*SA
  P(3,1)=RL(3)*RL(1)*(1.-CA)+RL(2)*SA
  P(3,2)=RL(2)*RL(3)*(1.-CA)-RL(1)*SA
  P(3,3)=CA+RL(3)*RL(3)*(1.-CA)
  DO J=1,3
     TXYZ(J)=0.
     DO I=1,3
	TXYZ(J)=TXYZ(J)+VXYZ(I)*P(I,J)
     ENDDO
  ENDDO
  CX=TXYZ(1)
  CY=TXYZ(2)
  CZ=TXYZ(3)
  RETURN
END SUBROUTINE XROTATE

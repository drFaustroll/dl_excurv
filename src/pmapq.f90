SUBROUTINE PMAPQ (IOPT,MAPR,*)
  !======================================================================C
  Use definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_COMPAR
  Use Common_MAPS

  DIMENSION XVALS(0:IPARMAP-1), YVALS(0:IPARMAP-1),B(29)
  CHARACTER*60 DSN
  IF (IOPT.GE.4) THEN
10   CALL WTEXT ('Enter filename')
     CALL UZERO (AP,225)
     CALL AREAD (7,DSN,LD,*10,*260)
     CALL FILEOPEN (DSN,INFILE,'FORMATTED','dat',*260)
     IF (IOPT.NE.5) THEN
        NSKIP=INTOPT-1
        CALL RDMAP(NSKIP,*30)
        MAPR=1
     ELSE
        CALL RDTRACE (INFILE)
     ENDIF
20   CLOSE (INFILE)
     RETURN
30   CALL ERRMSGI ('Error in reading map file ',INTOPT,*20)
  ENDIF
  IF (MAPR.NE.1) GOTO 250
  CALL UZERO (C,841)
  XSTEP=(X2-X1)/(MAPPOINTS-1)
  YSTEP=(Y2-Y1)/(MAPPOINTS-1)
  DO  K=0,MAPPOINTS-1
     XVALS(K)=X1+XSTEP*K
     YVALS(K)=Y1+YSTEP*K
  enddo
  IF (IMAP.LT.1.OR.IMAP.GT.IPARNPARAMS.OR.JMAP.LT.1.OR.JMAP.GT.IPARNPARAMS) GOTO 290
  IF (X1.LT.-720..OR.X1.GT.720.OR.X1.EQ.X2) GOTO 290
  IF (Y1.LT.-720..OR.Y1.GT.720.OR.Y1.EQ.Y2) GOTO 290
  IF (MAPPOINTS.EQ.8) THEN
     DO  K=1,15,2
        CALL MINT (AP(1,K),15)
     enddo
     DO  L=1,15,2
        DO  M=1,15,2
           B(M)=AP(L,M)
        enddo
        CALL MINT (B,15)
        DO  M=2,15,2
           AP(L,M)=B(M)
        enddo
     enddo
  ENDIF
  DO  N=1,2
     DO  K=1,13,2
	IPT=15-IABS(K-7)
	DO  L=1,IPT,2
           KK=IABS(MIN0(K-7,0))+L
           LL=MAX0(0,K-7)+L
           IF (N.EQ.2) LL=16-LL
           B(L)=AP(KK,LL)
        enddo
	CALL MINT (B,IPT)
	DO  L=2,IPT,2
           KK=IABS(MIN0(K-7,0))+L
           LL=MAX0(0,K-7)+L
           IF (N.EQ.2) LL=16-LL
           IF (AP(KK,LL).NE.0.) AP(KK,LL)=(AP(KK,LL)+B(L))*.5
           IF (AP(KK,LL).EQ.0.) AP(KK,LL)=B(L)
        enddo
     enddo
  enddo
  WRITE (LOGFILE,110) AP
110 FORMAT (15F5.1)
  DO  K=1,15
     DO  L=1,15
  	C(K*2-1,L*2-1)=AP(K,L)
     enddo
  enddo
  DO  K=1,29,2
     CALL MINT (C(1,K),29)
  enddo
  DO  L=1,29,2
     DO  M=1,29,2
  	B(M)=C(L,M)
     enddo
     CALL MINT (B,29)
     DO  M=2,29,2
  	C(L,M)=B(M)
     enddo
  enddo
  DO  N=1,2
     DO  K=1,27,2
	IPT=29-IABS(K-15)
	DO  L=1,IPT,2
           KK=IABS(MIN0(K-15,0))+L
           LL=MAX0(0,K-15)+L
           IF (N.EQ.2) LL=30-LL
           B(L)=C(KK,LL)
        enddo
	CALL MINT (B,IPT)
	DO  L=2,IPT,2
           KK=IABS(MIN0(K-15,0))+L
           LL=MAX0(0,K-15)+L
           IF (N.EQ.2) LL=30-LL
           IF (C(KK,LL).NE.0.) C(KK,LL)=(C(KK,LL)+B(L))*.5
           IF (C(KK,LL).EQ.0.) C(KK,LL)=B(L)
        enddo
     enddo
  enddo
  WRITE (LOGFILE,190) C
190 FORMAT (15F5.1/14F5.1)
  FMAX=-9999.
  FMIN=9999.
  DO  K=1,15
     DO  L=1,15
	FMAX=AMAX1(AP(K,L),FMAX)
	FMIN=AMIN1(AP(K,L),FMIN)
	IF (FMIN.NE.AP(K,L)) cycle
	XM=X1+(X2-X1)/14.*real(K-1,kind=dp)
	YM=Y1+(Y2-Y1)/14.*real(L-1,kind=dp)
        !	*XM,1,YM,1,ROW(3),4)
     enddo
  enddo
  FINC=(FMAX-FMIN)/21.
  DO  K=1,20
     H(K)=FMIN-FINC*.75+real(K,kind=dp)*FINC
  enddo
  IF (IOPT.NE.2) GOTO 280
  DO  IW=OUTTERM,LOGFILE,OUTDIFF
     WRITE (IW,300) IVARS(IMAP),IVARS(JMAP),(XVALS(II),II=0,MAPPOINTS-1)
     WRITE (IW,'(A)') '________________________________________________________________'
     INC=16/MAPPOINTS
     DO  L=1,MAPPOINTS
	L8=MAPPOINTS-L
	L9=9-L
	WRITE (IW,310) YVALS(L8),(AP(KQ,L9),KQ=1,IPARMAP,INC)
     enddo
     WRITE (IW,320) FMIN,IVARS(IMAP),XM,IVARS(JMAP),YM
  enddo
  RETURN
250 CALL ERRMSG ('Map file has not been read',*260)
260 CALL ERRS (*270)
270 RETURN
280 RETURN 1
290 CALL ERRMSG ('Error in map parameters',*295)
295 WRITE (OUTTERM,*) IMAP,JMAP,X1,Y1,X2,Y2
  RETURN
  !
300 FORMAT (A4,'/',A4,F6.2,7F9.2)
310 FORMAT (F7.2,'|',F8.3,7F9.3)
320 FORMAT (' Approximate minimum is :',F10.5/' at :',A4,'= ',F8.3,',',A4,'= ',F8.3)
END SUBROUTINE PMAPQ

# Info for Portland Group Fortran/GNU C on Linux
#
CC = gcc
F90 = pgf90
#LFLAGS = -L/usr/X11R6/lib -lX11
LFLAGS = -lX11
# PRODFLAGC = -DDOUBLE
PRODFLAGC =
# PRODFLAG90 = -Mbounds
#PRODFLAG90 = -fast -tpp6
PRODFLAG90 = -fast -tp px-64
PRODFLAG77 = $(PRODFLAG90)
# DEBUFLAG = -g
DEBUFLAG =
FREEFORM = -Mfree
FIXFORM = -Mnofree

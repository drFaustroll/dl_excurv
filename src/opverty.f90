SUBROUTINE OPVERTY (APHI,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - reflection about plane //z at 90 to x
  !
  Use Common_convia
  APHI=PI-APHI
  IS=IS+1
  RETURN 1
END SUBROUTINE OPVERTY

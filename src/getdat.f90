SUBROUTINE GETDAT (CDATE,CTIME,IOUT)
  !======================================================================C
  !	Return calendar date and time of day if available
  !
  !	  CDATE       RETURNED CALENDAR DATE IN THE FORM DD/MM/YYYY
  !	              IN A CHARACTER*10 STRING. RETURN A BLANK STRING
  !	              IF NOT AVAILABLE.
  !	  CTIME       RETURNED CLOCK TIME IN THE FORM HH:MM:SS IN
  !	              A CHARACTER*8 STRING. RETURN A BLANK STRING IF
  !	              NOT AVAILABLE.
  !
  CHARACTER*10 CDATE
  character*8  CTIME
  character*10 cdate1,ctime1,czone1
  integer      date_time(8)
  !
  !	Get date and time
  !
  CALL date_and_time(cdate1,ctime1,czone1,date_time)
  WRITE (CDATE,11) date_time(3),date_time(2),date_time(1)
  WRITE (CTIME,10) date_time(5),date_time(6),date_time(7)
  IF (IOUT.NE.0) WRITE (IOUT,20) CDATE,CTIME
  RETURN
10 FORMAT(I2,':',I2,':',I2)
11 FORMAT(I2,'/',I2,'/',I4)
20 FORMAT ('EXCURVE >>>   ',A10,'    ',A8)
END SUBROUTINE GETDAT

SUBROUTINE RDPARM (*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_SETKW
  Use Common_MTR
  Use Common_PMTCH
  Use Common_VAR
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_POT
  Use Common_UPU
  Use Common_COMPAR
  Use Common_IP
  Use Index_INDS
  Use Common_DISTANCE
  Use Common_LSQ
  Use Common_RCBUF
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  !
  !	Read a parameter data file
  !
  LOGICAL ERR,PAR,DISTANCE,WEIGHTING,PARLIST,BONDLENGTH,GOTMTR
  CHARACTER UNAMES(IPARNUNITS)*12,CHARADD*3
  DATA UNAMES &
       /'UNAME1','UNAME2','UNAME3','UNAME4','UNAME5','UNAME6',     &
       'UNAME1','UNAME8','UNAME9','UNAME10','UNAME11','UNAME12',   &
       'UNAME13','UNAME14','UNAME15','UNAME16','UNAME17','UNAME18',&
       'UNAME19','UNAME14','UNAME21','UNAME22','UNAME23','UNAME24',&
       'UNAME25'/,CHARADD/'ADD'/
  !
  !	Initialisations
  !
  !-st	call eqv_convia
  CALL GETTIM (1,CPU1,ELAP)
  PAR=.FALSE.
  PARLIST=.FALSE.
  GOTMTR=.FALSE.
  ICODE=-1
  NHEAD=0
  IADD=0
  MADD=0
  ICHARLEN=NCSTR(CHAROPT)
  IF (CHAROPT(1:ICHARLEN).EQ.CHARADD(1:ICHARLEN)) IADD=NS+1
  IF (CHAROPT(1:ICHARLEN).EQ.CHARADD(1:ICHARLEN)) MADD=MG
  !
  !	Read next record
  !
10 READ(INFILE,320,ERR=300,END=310)BUF
  IF (BUF(1:1).EQ.'!') GOTO 10
  IF (BUF(1:1).EQ.' ') THEN
     M=2
  ELSE
     M=1
  ENDIF
  IF (BUF(M:M).EQ.'&') then
     IF (BUF(M+1:M+1).EQ.'P'.AND.BUF(M+2:M+2).EQ.'A')THEN
        GOTO 20
     ELSEIF (BUF(M+1:M+1).EQ.'E'.AND.BUF(M+2:M+2).EQ.'R')THEN
        DO I=1,IPARNVARS
           IVCH(I)=' '
           E(I)=0.
        ENDDO
        INUMVARS=0
        GOTO 220
     ELSEIF (BUF(M+1:M+1).EQ.'S'.AND.BUF(M+2:M+2).EQ.'E') THEN
        GOTO 150
     ENDIF
  ENDIF
  !
  !	Write header record if required
  !
  IF (NHEAD.LT.10) CALL WTEXT(BUF)
  NHEAD=NHEAD+1
  IF (.NOT.PARLIST) GOTO 10
  GO TO 310
  !
  !	Parameter records found
  !
20 PARLIST=.TRUE.
  CALL IUZERO (NAT,IPARNCLUS)
  JSTART=1
30 READ (INFILE,320,ERR=300,END=240) BUF
  IF (BUF(1:1).EQ.'!') GOTO 30
  IF (BUF(1:1).EQ.' ') THEN
     M=2
  ELSE
     M=1
  ENDIF
  IF (BUF(M:M).EQ.'&') GOTO 10
  !
  !	Interpret fields of the record
  !
  M1=0
  DO I=1,81
     IF (BUF(I:I).EQ.' '.OR.BUF(I:I).EQ.'='.OR.I.EQ.81) THEN
        IF (M1.NE.0) THEN
           M2=I-1
           MC=M2-M1+1
           IF (BUF(M1:M1).LT.'A'.AND.BUF(M1:M1).NE.'$') THEN
	      X=FPVAL(BUF(M1:M2),ERR)
	      IF (ERR) GOTO 40
	      IF (PAR) THEN
                 JADD=0
                 IF (IADD.NE.0) THEN
                    IF (IP.GE.inds(INDN).AND.IP.LE.inds(INDROT)+IPARNS.OR.IP.GE.inds(INDTWST).AND.IP.LE.inds(INDCLUS)+IPARNS) THEN
                       JADD=IADD
                    ELSEIF(IP.GE.inds(INDPIV).AND.IP.LE.inds(INDUOC)+IPARNS) THEN
                       JADD=MADD
                    ENDIF
                 ENDIF
                 IF (IP+JADD.LE.IPARNPARAMS) THEN
                    PA1(IP+JADD)=X
                    IF (IP.EQ.inds(INDNS)) PA1(IP)=PA1(IP)+IADD
                    IF (IP.GE.inds(INDUN).AND.IP.LE.inds(INDUN)+IPARNS) PA1(IP)=PA1(IP)+MADD
                 ENDIF
                 !	      LSTAT=IOR(LSTAT,LSTATS(IP+JADD))
              ELSEIF (DISTANCE) THEN
                 DISTANCES(IP,JP)=X
              ELSEIF (WEIGHTING) THEN
                 WEIGHTINGS(IP,JP)=X
              ELSEIF (BONDLENGTH) THEN
                 BONDS(IP,JP)=X
                 IF (JP.NE.0) BONDS(JP,IP)=X
              ENDIF
              PAR=.FALSE.
              DISTANCE=.FALSE.
              WEIGHTING=.FALSE.
              BONDLENGTH=.FALSE.
              GOTO 140
           ENDIF
40         GOTO (50,80,100) JSTART
50         DO IP=1,IPARNPARAMS
              DO II=1,MC
                 IF (BUF(II+M1-1:II+M1-1).NE.IVARS(IP)(II:II)) GOTO 60
              ENDDO
              !	  IF(BUF(M1:M2).NE.IVARS(IP))cycle
              PAR=.TRUE.
              JSTART=1
              IF (IP.GE.inds(INDR)      .AND.IP.LT.inds(INDR)+IPARNS+1)THEN
                 ICODE=0
              ELSEIF (IP.GE.inds(INDX)  .AND.IP.LT.inds(INDX)+IPARNS+1)THEN
                 ICODE=1
              ELSEIF (IP.GE.inds(INDMTR).AND.IP.LT.inds(INDMTR)+IPARNP)THEN
                 GOTMTR=.TRUE.
              ENDIF
              GO TO 140
60            CONTINUE
           enddo
           DO IP=1,IPARNUNITS
              IF (BUF(M1:M2).EQ.UNAMES(IP)) WRITE (6,*) UNAMES(IP)
           enddo
70         CONTINUE
           !
           IF (JSTART.EQ.2) GOTO 130
80         DO  IP=1,IPARNS
              DO  JP=0,IP-1
                 DO II=1,MC
                    IF (BUF(II+M1-1:II+M1-1).NE.DISTVARS(IP,JP)(II:II)) GOTO 90
                 ENDDO
                 !	  IF(BUF(M1:M2).NE.DISTVARS(IP,JP))GO TO 70
                 DISTANCE=.TRUE.
                 JSTART=2
                 GO TO 140
90               CONTINUE
              enddo
           enddo
           IF (JSTART.EQ.3) GOTO 130
100        DO  IP=1,IPARNS
              DO  JP=0,IP-1
                 DO II=1,MC
                    IF (BUF(II+M1-1:II+M1-1).NE.WEIGHTVARS(IP,JP)(II:II)) GOTO 110
                 ENDDO
                 !	    IF(BUF(M1:M2).NE.WEIGHTVARS(IP,JP))GO TO 110
                 WEIGHTING=.TRUE.
                 JSTART=3
                 GO TO 140
110              CONTINUE
              enddo
           enddo
           DO  IP=1,IPARNS
              DO  JP=0,IP-1
                 DO II=1,MC
                    IF (BUF(II+M1-1:II+M1-1).NE.BONDVARS(IP,JP)(ii:ii)) GOTO 120
                 ENDDO
                 !	    IF(BUF(M1:M2).NE.BONDVARS(IP,JP))GO TO 120
                 BONDLENGTH=.TRUE.
                 JSTART=3
                 GO TO 140
120              CONTINUE
              enddo
           enddo
           IF (JSTART.EQ.1) GOTO 130
           GOTO 50
           !
           !	Invalid parameter - print warning
           !
130        PAR=.FALSE.
           CALL WTEXT('Warning - invalid parameter ignored: '//BUF(M1:M2))
140        M1=0
        ENDIF
     ELSEIF (M1.EQ.0) THEN
        M1=I
     ENDIF
  ENDDO
  GOTO 30
  !
  !	End of file or parameters
  !
150 READ(INFILE,320,ERR=300,END=240)BUF
  IF (BUF(1:1).EQ.'!') GOTO 150
  IL=0
160 CALL NXFLD1(BUF,IL,M1,M2,*160,*150)
  IF(BUF(M1:M2).EQ.'&END')GO TO 10
  DO  J=1,IPARSETOPTS
     IF (BUF(M1:M2).EQ.SETKW(J)) GOTO 190
  enddo
  IF (BUF(M1:M2).EQ.'TITLE') THEN
     CALL NEXFLD(BUF,IL,M1,M2,*150)
     IF (BUF(M1:M1).EQ.'''') M1=M1+1
     IF (BUF(M2:M2).EQ.'''') M2=M2-1
     GTITLE=BUF(M1:M2)
     GOTO 190
  ENDIF
180 WRITE (OUTTERM,*) 'Option ',BUF(M1:M2),' not found.'
  GOTO 150
190 CALL NXFLD1(BUF,IL,M1,M2,*160,*150)
  DO I=1,ISETMAX(J)
     IF (BUF(M1:M2).EQ.SETKWO(I,J)) GOTO 210
  enddo
  GOTO 180
210 ISET(J)=I-MINUS(J)
  GOTO 150
220 READ(INFILE,320,ERR=300,END=240)BUF
  IF (BUF(1:1).EQ.'!') GOTO 220
  IL=0
230 CALL NXFLD1(BUF,IL,M1,M2,*230,*220)
  IF(BUF(M1:M2).EQ.'&END')GO TO 10
  !	DO J=1,IPARNPARAMS
  !	IF (BUF(M1:M2).EQ.IVARS(J)) GOTO 191
  !       enddo
  IF (INUMVARS.LT.IPARNVARS) INUMVARS=INUMVARS+1
  IVCH(INUMVARS)=BUF(M1:M2)
  CALL NXFLD1(BUF,IL,M1,M2,*230,*220)
  MC=M2-M1+1
  X=FPVAL(BUF(M1:M2),ERR)
  E(INUMVARS)=X
  GOTO 230
240 IF (.NOT.GOTMTR) CALL SETMTR
  !	THEN
  !	  DO J=1,IPARNP
  !	  IDJ=ATOM(J)
  !	  IF (IDJ.LE.0) THEN
  !	    RMTR(J)=.1
  !	  ELSE
  !
  !	Set default MTR according to ION
  !
  !	    IF (RION(J)) 250,260,270
  ! 250	    RMTR(J)=DEFMTRM1(IDJ)
  !	    GO TO 280
  ! 260	    RMTR(J)=DEFMTR(IDJ)
  !	    GO TO 280
  ! 270	    RMTR(J)=DEFMTRP1(IDJ)
  !	  ENDIF
  ! 280	  CONTINUE
  !	  ENDDO
  !	ENDIF
  !	DO 290 I=1,IPARNPARAMS
  !	PA2(I)=PA1(I)*CONVIA(ICF(I))
  !       call all_IPA
  ! 	IPA(I)=PA2(I)
  ! 290   call IPA_all
  IF (ICODE.NE.-1) THEN
     IF (ICODE.EQ.1) THEN
        NS=PA1(IPARNS)
        DO I=0,NS
           CALL CARTOPOLD (UX(I),UY(I),UZ(I),R(I),TH(I),PHI(I))
        ENDDO
     ENDIF
     !	  CALL FLIM (*281)
     CALL UZERO (ROT,IPARNS+1)
     !	  CALL UZERO (TRANSX,3)
     TRANSX=0
     TRANSY=0
     TRANSZ=0
     CALL UZERO (SDX,IPARNP)
     CALL UZERO (SDY,IPARNP)
     CALL UZERO (SDZ,IPARNP)
     SURROT=0
  ENDIF
  !	CALL IUZERO (IDSITE,IPARNS+1)
  CALL FLIM (*291)
291 CALL GETTIM (1,CPU2,ELAP)
  WRITE (LOGFILE,*) 'Total time in RDPARM',CPU2-CPU1
  RETURN
  !
  !	Error conditions
  !
300 CALL ERRMSG ('Error in reading parameter file',*125)
125 RETURN 1
310 IF (PARLIST) GOTO 240
  CALL ERRMSG ('No parameters found',*126)
126 RETURN 1
  !
320 FORMAT(A)
END SUBROUTINE RDPARM

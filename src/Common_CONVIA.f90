MODULE Common_CONVIA
  ! contains constants and conversion parameters from bdat.f file
  USE Definition
  implicit none
  real(dp),            public  :: convia(0:10)
  real(dp), parameter, public  :: ONE = 1.0_dp
  real(dp), parameter, public  :: EC  = 0.036748982_dp
  real(dp), parameter, public  :: DC  = 1.889726342_dp
  real(dp), parameter, public  :: DWC = 3.571065648_dp
  real(dp), parameter, public  :: AC  = 0.017453292_dp
  real(dp), parameter, public  :: RKC = 0.529177150_dp
  real(dp), parameter, public  :: PI  = 3.141592653589790_dp
  real(dp), parameter, public  :: PI2 = PI*2.d0    !6.283185307179580_dp
  real(dp), parameter, public  :: PID2= PI/2.d0    !1.570796326794897_dp
  real(dp), parameter, public  :: S3C = 0.6748336823_dp
  real(dp), parameter, public  :: S4C = 0.1275250986_dp
contains 
  subroutine eqv_convia
    convia( 0) = one
    convia( 1) = ec
    convia( 2) = dc
    convia( 3) = dwc
    convia( 4) = ac
    convia( 5) = rkc
    convia( 6) = pi
    convia( 7) = pi2
    convia( 8) = pid2
    convia( 9) = s3c
    convia(10) = s4c
  end subroutine eqv_convia
END MODULE Common_convia

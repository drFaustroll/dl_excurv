   MODULE DEFINITION
   implicit none
!  *********************************************************
!  Standard definitionsf of data types 
!  *********************************************************
   INTEGER,  PARAMETER     :: I4B    = SELECTED_INT_KIND(8)
!   INTEGER,  PARAMETER     :: I2B    = SELECTED_INT_KIND(4)
   INTEGER,  PARAMETER     :: I1B    = SELECTED_INT_KIND(2)
   INTEGER,  PARAMETER     :: float  = KIND(1.D0)
   INTEGER,  PARAMETER     :: dp     = KIND(1.E0)
   INTEGER,  PARAMETER     :: sp     = KIND(1.E0)
   INTEGER,  PARAMETER     :: imag   = KIND((0.0_sp,0.0_sp))
   INTEGER,  PARAMETER     :: imag2  = KIND((0.0_float,0.0_float))
   INTEGER,  PARAMETER     :: LG     = KIND(.true.)
!  *********************************************************
!   REAL(dp),parameter      :: small   =   1.E-34_dp
   COMPLEX(imag),parameter :: CZERO1  = (0.0_sp,0.0_sp)
   COMPLEX(imag),parameter :: RUNITY  = (1.0_dp,0.0_dp)
   COMPLEX(imag),parameter :: CUNITY  = (0.0_dp,1.0_dp)
   END MODULE DEFINITION

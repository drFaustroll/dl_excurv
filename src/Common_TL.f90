MODULE Common_TL

  !      COMMON /TL/ ITL(20),MSTL(IPARNS+2)
  !      CHARACTER ITL*48, MSTL*39

  USE Definition
  Use Parameters
  implicit none
  private
  character*48, dimension(20), public :: ITL = (/ &
       'EXAFS(exp)                    ', &
       'EXAFS(theory)                 ', &
       'EXAFS(exp)                    ', &
       'EXAFS(theory)                 ', &
       'FT (experiment)               ', &
       'FT (theory)                   ', &
       'SINE TRANSFORM (experiment)   ', &
       'SINE TRANSFORM (theory)       ', &
       'Backscattering Magnitude      ', &
       'Backscattering Phase          ', &
       'Phase of Backscattering Factor', &
       'Difference                    ', &
       'Difference (FT)               ', &
       'Window                        ', &
       'Atomic Potential              ', &
       'Superposed Potential          ', &
       'Muffin-tin Potential          ', &
       'Atomic Charge Density         ', &
       'Superposed Charge             ', &
       'Central Atom Phase            '/)
  character*49, dimension(IPARNS+2), public :: MSTL
end MODULE Common_TL

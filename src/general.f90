SUBROUTINE GENERAL (I,TPHI,TTHETA,*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Include_PA2

  TTHETA=RADTH(I)
  IF (ABS(TTHETA).LT.0.001) THEN
     TPHI=0.
  ELSE
     TPHI=RADPHI(I)
  ENDIF
  RETURN
END SUBROUTINE GENERAL

MODULE Common_AIL

  !      COMMON /AIL/NALIAS,IALLEN(IPARAL),ALID(IPARAL),ALSTRING(IPARAL)
  !      CHARACTER ALID*8,ALSTRING*80
  !      data NALIAS/0/
  Use Parameters
  Use Definition
  implicit none
  private

  integer(i4b), public :: NALIAS = 0
  integer(i4b), dimension(IPARAL), public :: IALLEN
  character*8,  dimension(IPARAL), public :: ALID
  character*80, dimension(IPARAL), public :: ALSTRING
end MODULE Common_AIL

SUBROUTINE SHM4 (ATH,APHI,I,MAXL)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_YLM
  !
  !	Standard routine for calculating spherical harmonics
  !
  DIMENSION FAC1(0:IPARDLMAX*2)
  dimension FAC2(0:IPARDLMAX*2,-IPARDLMAX*2:IPARDLMAX*2)
  dimension FAC3(0:IPARDLMAX*2)
  COMPLEX CT,ST,CF,SF,SA
  real(float) FAC1,FAC2,FAC3,A,B
  !	REMTH(I)=ATH
  !	REMPHI(I)=APHI
  CT=CMPLX(COS(ATH),0.)
  ST=CMPLX(SIN(ATH),0.)
  CF=CMPLX(COS(APHI),SIN(APHI))
  CL=0.
  A=1.
  B=1.
  !	IF (CABS(CT).LT.1.E-6) CT=0.
  IF (ABS(REAL(CT)).LT.1.E-6) CT=0.
  ASG=1.
  !	MAXL=IDLMAX+IDLMAX
  DO  L=0,MAXL
     FAC1(L)=ASG*SQRT((2.*CL+1.)*A/(4.*PI*B*B))
     FAC3(L)=SQRT(2.*CL)
     CM=-CL
     DO  M=-L,L
	FAC2(L,M)=SQRT((CL+1.+CM)*(CL+1.-CM)/((2.*CL+3.)*(2.*CL+1.)))
   	CM=CM+1.
     enddo
     CL=CL+1.
     CL2=CL+CL
     A=A*CL2*(CL2-1.)/4.
     B=B*CL
     ASG=-ASG
  enddo
  CL=1.
  ASG=-1.
  SF=CF
  SA=1.
  YLM(I,0,0)=CMPLX(FAC1(0),0.0D0)
  DO  L=1,MAXL
     YLM(I,L,L)  = CMPLX(FAC1(L),0.0D0)*SA*SF*ST
     YLM(I,L,-L) = CMPLX(FAC1(L),0.0D0)*CMPLX(ASG,0.)
     YLM(I,L,-L) = YLM(I,L,-L)*SA*ST/SF
     YLM(I,L,L-1)= CMPLX(FAC3(L),0.0D0)*CMPLX(FAC1(L),0.0D0)
     YLM(I,L,L-1)=-YLM(I,L,L-1)*SA*SF*CT/CF
     YLM(I,L,1-L)=-CMPLX(ASG,0.)*YLM(I,L,L-1)*CF*CF/(SF*SF)
     SA=ST*SA
     SF=SF*CF
     CL=CL+1.
     ASG=-ASG
  enddo
  DO  L=2,MAXL
     DO  M=2-L,L-2
	YLM(I,L,M)=-(CMPLX(FAC2(L-2,M),0.0D0)*YLM(I,L-2,M)-CT*YLM(I,L-1,M))
	YLM(I,L,M)=YLM(I,L,M)/CMPLX(FAC2(L-1,M),0.0D0)
     enddo
  enddo
  DO  J=0,MAXL
     DO  N1=-J,J
	IF (ABS(YLM(I,J,N1)).LT.1.E-20) YLM(I,J,N1)=0
     enddo
  enddo
  RETURN
END SUBROUTINE SHM4

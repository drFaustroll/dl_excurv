FUNCTION NUMCHAR (STRING,INDEX)
  !======================================================================C
  !
  !	Converts characters in STRING to a number 
  !       until STRING(INDEX) is not a numeral.
  !
  CHARACTER*(*) STRING
  NUMCHAR=0
  MAXIND=LEN(STRING)
  IF (MAXIND.LT.1) RETURN
  DO  INDEX=1,MAXIND
     J=ICHAR(STRING(INDEX:INDEX))-48
     IF (J.LT.0.OR.J.GT.9) RETURN
     NUMCHAR=NUMCHAR*10+J
  enddo
  RETURN
END FUNCTION NUMCHAR

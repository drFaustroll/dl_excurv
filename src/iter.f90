SUBROUTINE ITER
  !======================================================================C
  Use Definition
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Common_COMPAR
  Use Common_IP
  Use Common_PRDSN
  Use Include_PA1
  Use Include_XY

  DIMENSION    :: Y(IPARNVARS)
  CHARACTER*12 :: IP1,IP2,IP3
  CHARACTER*6  :: OPT(18)
  CHARACTER*1  :: FITOPT
  DATA OPT/ &
       'GRID','TABLE',' ',  'AE', 'ANE','ANRE', 'APRE','ARE',   'LSR',  &
       'NE',  'NRE',  'PE', 'PRE','RE', 'STATS','XYE', 'RESUME','RESET'/
  CALL FINDOPT (18,OPT,IOPT,9,'Step Parameter',*50)
  CALL ITERQ (IOPT,DSTEP,Y,*50)
  !
  !	WRITE (6,*) 'IOPT',IOPT
  !
  IF (IOPT.GE.3) THEN
     CALL LSR (DSTEP)
  ELSE
     IF (INTOPT.LT.2) INTOPT=2
     JL=INTOPT**NCALL-1
     FITOPT=' '
     IF (IOPT.EQ.2) THEN
        CALL OUTNAM (11,PRDSN,' ',*50)
        CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*50)
        WRITE (OUTFILE,*) NCALL,INTOPT,NPT(IEXP),(U(I),I=1,NCALL),(Y(I),I=1,NCALL)
        WRITE (OUTFILE,60) (RK(I,IEXP),I=JS(IEXP),JF(IEXP))
        FITOPT='Q'
     ENDIF
     DO I=0,JL
        DO J=1,NCALL
           KJ=INTOPT**J/INTOPT
           UU=U(J)+real(MOD(I/KJ,INTOPT),kind=dp)*(Y(J)-U(J))/real(INTOPT-1,kind=dp)
           IF (MOD(I,KJ).EQ.0) CALL PCHANGE (IVCH(J),UU)
           LF=2
        enddo
        IF (UU.GE.1..OR.IVCH(J)(1:1).NE.'R') THEN
           CALL FIT (FITOPT,iset(IWEIGHT),*20)
        ELSE
           CALL UZERO (XABT(JS(IEXP),1),NPT(IEXP))
        ENDIF
20      IF (IOPT.EQ.2) THEN
           WRITE (OUTFILE,60) (XABT(II,1),II=JS(IEXP),JF(IEXP))
        ELSE
           CALL WTEXT ('Select: PLOT or RETURN to skip')
           CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*40,*50)
           CALL GPLOT (2,0)
           CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*30,*50)
30         CALL ALPHAM
        ENDIF
40      CONTINUE
     enddo
  ENDIF
50 ID=0
  IF (IOPT.EQ.2) CALL EFILE ('Table for Monte-Carlo Program')
  RETURN
60 FORMAT (7F11.4)
END SUBROUTINE ITER

SUBROUTINE C3J
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_CL
  Use Common_FACTORIALS
  !
  !	Calculate (2L1+1)(2L2+1)*CGC(L1,L2,L3) for L and M edges
  !
  !	L3 is the final state angular momentum
  !
  real(float)  :: A,B
  LMAX=25
  DO L3=2,3
     DO L2=0,LMAX
	II=0
	DO L1=L2-L3,L2+L3,2
           II=II+1
           L=L1+L2+L3
           IF (L1.GE.0.AND.MOD(L,2).EQ.0.AND.L1+L2.GE.L3) THEN
              A=FAC(L1+L2-L3)*FAC(L1+L3-L2)*FAC(L2+L3-L1)
              A=A/FAC(L1+L2+L3+1)
              B=FAC(L/2)/(FAC(L/2-L1)*FAC(L/2-L2)*FAC(L/2-L3))
              A=A*B**2
              B=A*(2*L2+1)*(2*L1+1)
              IF(L3.EQ.3.AND.L1.EQ.L2+L3) B=(2*L2+1)-CCL3(L2,1)-CCL3(L2,2)-CCL3(L2,3)
              IF (L3.EQ.2) THEN
                 CCL2(L2,II)=B
              ELSEIF (L3.EQ.3) THEN
                 CCL3(L2,II)=B
              ENDIF
           ELSE
              IF (L3.EQ.2) THEN
                 CCL2(L2,II)=1.
              ELSE
                 CCL3(L2,II)=1.
              ENDIF
              B=1.
           ENDIF
           !	WRITE (6,'(3I3,F10.6,F8.4,I3)') L1,L2,L3,B,A,2*L2+1
	ENDDO
     ENDDO
  ENDDO
  !
  !	Calculate CG coefs and D matrices for polarisation dependence
  !	Terms for L0+/-1 for L0=0 to 2
  !
  DO L=0,3
     DO M=-L-1,L+1
	IF (L.LT.2.AND.IABS(M).LT.3) THEN
           A=(L+M)*(L+M+1)
           B=(2*L+1)*(2*L+2)
           IF (A/B.GT.0.) CGC(L,M-1,L+1,M)=SQRT(A/B)
           A=(L-M+1)*(L+M+1)
           B=(2*L+1)*(L+1)
           IF (A/B.GT.0.) CGC(L,M,L+1,M)=SQRT(A/B)
           A=(L-M)*(L-M+1)
           B=(2*L+1)*(2*L+2)
           IF (A/B.GT.0.) CGC(L,M+1,L+1,M)=SQRT(A/B)
	ENDIF
	IF (L.NE.0.AND.IABS(M).LT.3) THEN
           A=(L-M)*(L-M+1)
           B=2*L*(2*L+1)
           IF (A/B.GT.0.) CGC(L,M-1,L-1,M)=SQRT(A/B)
           A=(L-M)*(L+M)
           B=L*(2*L+1)
           IF (A/B.GT.0.) CGC(L,M,L-1,M)=-SQRT(A/B)
           A=(L+M+1)*(L+M)
           B=2*L*(2*L+1)
           IF (A/B.GT.0.) CGC(L,M+1,L-1,M)=SQRT(A/B)
	ENDIF
     ENDDO
  ENDDO
  DO L0=0,2
     DO LF=L0-1,L0+1,2
	IF (LF.GE.0) THEN
           DO M0=-L0,L0
              DO MF=-LF,LF
                 M2=M0-MF
                 IF (IABS(M2).GT.1) THEN
                    DM(L0,LF,M0,MF)=0.
                 ELSE
                    A=(2*LF+1)*3
                    B=6*(2*L0+1)
                    DM(L0,LF,M0,MF)=SQRT(A/B)*CGC(LF,MF,L0,M0)*CGC(LF,0,L0,0)
                 ENDIF
              ENDDO
           ENDDO
	ENDIF
     ENDDO
  ENDDO
  IF (DEBUG) THEN
     WRITE (7,*)
     WRITE (7,*) 'K,L1,M1-EDGE'
     WRITE (7,*)
     WRITE (7,'(7F9.5)') (DM(0,1,0,J),J=-1,1)
     WRITE (7,*)
     WRITE (7,*) 'L2,L3,M2,M3-EDGE'
     WRITE (7,*)
     DO I=-1,1
        WRITE (7,'(7F9.5)') DM(1,0,I,0)
     ENDDO
     WRITE (7,*)
     DO I=-1,1
        WRITE (7,'(7F9.5)') (DM(1,2,I,J),J=-2,2)
     ENDDO
     WRITE (7,*)
     WRITE (7,*) 'M4,M5-EDGE'
     WRITE (7,*)
     DO I=-2,2
        WRITE (7,'(7F9.5)') (DM(2,1,I,J),J=-1,1)
     ENDDO
     WRITE (7,*)
     DO I=-2,2
        WRITE (7,'(7F9.5)') (DM(2,3,I,J),J=-3,3)
     ENDDO
     WRITE (7,*)
  ENDIF
END SUBROUTINE C3J

SUBROUTINE FOURS (JKLS)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_FACTORIALS
  Use Common_PMTX
  Use Common_MATEL
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2
  Use Include_XY
  Use Include_FT
  !
  !	Organises Fourier transforms of theory and experiment, using
  !	phaseshift calculated by EXCAL and window function
  !	JKLS: 1 Exp+Theory 2 Exp 3 Theory 4 Difference
  !
  DIMENSION W(IPARNPOINTS),WT(IPARNPOINTS),AA(8),RI(2)
  DIMENSION FFA(2048),FFI(2048),TABLE(513),RKI(2048)
  DATA AA/.40217,-.49703,.09392,-.00183,.35875,-.48829,.14128,-.01168/
  NA=IPARNA
  STEP=(RMAX2-RMIN2)/(real(NA-1,kind=dp))
  NOFFSET=NINT(RMIN2/STEP)
  DO JA=1,NA
     !
     !	Save previous OFT for P FT 2, CP 2 etc/
     !
     OFT(JA+NA)=OFT(JA)
     !
     !	Save the previous theory FT for P FT 1, CP 1 etc.
     !
     OFT(JA)=FT(JA,1)
     AF(JA)=RMIN2+real(JA-1,kind=dp)*STEP
  enddo
  IW=IFTSET(3)
  CALL COSQT (TABLE)
  DO IEXP=1,NSPEC
     DIFF=RK(JF(IEXP),IEXP)-RK(JS(IEXP),IEXP)
     IF (IFTSET(5).EQ.1) CENT=(RK(JS(IEXP),IEXP)+RK(JF(IEXP),IEXP))*.5
     !
     !	Window functions are 1: B-H 2: Gaussian 3: Hanning 4: K-B
     !
     GOTO (20,40,60,80),IW
20   K=1
     IF (WP2.GT.1.5) K=5
     DO I=JS(IEXP),JF(IEXP)
	W(I)=AA(K)
	DO J=1,3
           W(I)=W(I)+COS(real(J*2,kind=dp)*PI*(RK(I,IEXP)-RK(JS(IEXP),IEXP))/DIFF)*AA(J+K)
        enddo
     enddo
     GOTO 110
40   DO I=JS(IEXP),JF(IEXP)
   	W(I)=EXP(-WP2*(RK(I,IEXP)-CENT)*(RK(I,IEXP)-CENT))
     enddo
     GOTO 110
60   DO I=JS(IEXP),JF(IEXP)
   	W(I)=(COS((RK(I,IEXP)-CENT)*PI/DIFF))**WP2
     enddo
     GOTO 110
80   DO  I=JS(IEXP),JF(IEXP)
	TAA=(2.*(RK(I,IEXP)-CENT)/DIFF)
	DO  J=1,2
           RI(J)=1.
           U=WP2*PI*AMIN1(1.,SQRT(1.-TAA*TAA)+real(J-1,kind=dp))
           DO  L=1,14
              RI(J)=RI(J)+U**(L*2)/(FAC(L)*2.**L)**2.
           enddo
        enddo
        W(I)=RI(1)/RI(2)
     enddo
     !
     !	Select weighting scheme
     !
110  IF (IFTSET(2).NE.4) THEN
        DO I=JS(IEXP),JF(IEXP)
           WT(I)=RK(I,IEXP)**IFTSET(2)*W(I)
        enddo
     ELSEIF (IFTSET(2).EQ.4) THEN
        DO  I=JS(IEXP),JF(IEXP)
           WT(I)=RK(I,IEXP)/FPIM(I,IEXP)*W(I)
        enddo
     ENDIF
     !
     !	FT of experiment (JK=1) and theory (JK=2)
     !
     DO  JK=1,2
	IF (JK.EQ.4-JKLS) cycle
	DO  I=JS(IEXP),JF(IEXP)
           IF (JKLS.EQ.4) THEN
              PFT(I)=(XABS(I,IEXP)-XABT(I,IEXP))*WT(I)
           ELSE
              IF (iset(IATABS).EQ.1) THEN
                 IF (JK.EQ.1) THEN
                    GROT=CG1(I,IEXP)
                 ELSE
                    GROT=CG3(I,IEXP)
                 ENDIF
              ELSE
                 IF (JK.EQ.1) THEN
                    GROT=XABS(I,IEXP)
                 ELSE
                    GROT=XABT(I,IEXP)
                 ENDIF
              ENDIF
              PFT(I)=GROT*WT(I)
           ENDIF
        enddo
	CALL UZERO (FFA,2048)
	CALL UZERO (FFI,2048)
        !
        !	Interpolate onto regular k-grid using equal area interpolation.
        !
	DO I=1,NPT(IEXP)
           IE=I+JS(IEXP)-1
           FFA(I)=PFT(IE)*COS(FPIP(IE,IEXP))
           FFI(I)=-PFT(IE)*SIN(FPIP(IE,IEXP))
	ENDDO
	SSTEP=PI/(2048.*STEP)
	NPN=MIN(NINT(RK(JF(IEXP),IEXP)/SSTEP)+1,2048)
        !	WRITE (6,*) 'STEP',STEP,SSTEP,NA,NPN,NOFFSET
	DO I=1,NPN
           RKI(I)=(I-1)*SSTEP
	ENDDO
	CALL SPLINT1 (NPT(IEXP),TRK(JS(IEXP),IEXP),FFA,NPN,RKI,0,.FALSE.,*200)
	CALL SPLINT1 (NPT(IEXP),TRK(JS(IEXP),IEXP),FFI,NPN,RKI,0,.FALSE.,*200)
	IF (NPN.LT.2048) THEN
           CALL UZERO (FFA(NPN+1),2048-NPN)
           CALL UZERO (FFI(NPN+1),2048-NPN)
	ENDIF
	DO I=1,NPN
           IF (RKI(I).LT.TRK(JS(IEXP),IEXP)) THEN
              FFA(I)=0.
              FFI(I)=0.
           ENDIF
	ENDDO
	CALL FFT (FFA,FFI,TABLE,-1)
	DO  I=1,NA
           J=I+NOFFSET
           EFE(I,IEXP,JK*2-1)=SQRT(FFA(J)**2+FFI(J)**2)
           EFE(I,IEXP,JK*2)=FFI(J)
           IF (IFTSET(4).NE.2) cycle
           RMPF=AF(I)*AF(I)
           EFE(I,IEXP,JK*2)=EFE(I,IEXP,JK*2)*RMPF
           EFE(I,IEXP,JK*2-1)=EFE(I,IEXP,JK*2-1)*RMPF
        enddo
     enddo
  ENDDO
  LF=0
200 RETURN
END SUBROUTINE FOURS

SUBROUTINE OPMIR (ATH,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - reflection about horiz plane
  !
  Use Common_convia
  ATH=PI-ATH
  IS=IS+1
  RETURN 1
END SUBROUTINE OPMIR

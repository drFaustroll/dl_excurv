SUBROUTINE TPOS (KX,KY)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Index_IGF
  Use Common_PMTX
  Use Include_PA1
  !
  !	 Set the cursor position
  !
  !	 Parameters
  !
  !	KX (I)   X position (across screen left to right) 0 to 1023
  !	KY (I)   Y position (up screen bottom to top) 0 to 1023
  !
  !	Returns without taking action if IGF(2)=3
  !
  CHARACTER*1 POS(0:6)
  !      INTEGER*2 IH,IV
  !      IX=KX/8
  !      IY=MAX((760-IY)/20,0)
  !
  !      PC only next line
  !
  !      CALL SET_CURSOR_POS@ (IX,IY)
  !      RETURN
  IX=KX
  IY=KY
  !
  !	No positioning in X-windows or non-terminal options
  !
  IF (IGF(2).NE.1.OR.IGF(ITNUM).EQ.5) RETURN
  IF (IX.LT.0) IX=0
  IF (IX.GT.1023) IX=1023
  IF (IY.LT.0) IY=0
  IF (IY.GT.1023) IY=1023
  POS(0)=CHAR(31)
  POS(1)=CHAR(29)
  POS(6)=CHAR(31)
  JXH=IX/32
  JYH=IY/32
  POS(4)=CHAR(JXH+32)
  POS(2)=CHAR(JYH+32)
  POS(5)=CHAR(IX-32*JXH+64)
  POS(3)=CHAR(IY-32*JYH+96)
  WRITE (OUTTERM,10) POS
  RETURN
10 FORMAT (6A1,$)
END SUBROUTINE TPOS

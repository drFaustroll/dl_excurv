Module Common_RSC
  !      PARAMETER (IPART1=(IPARTLMAX+1)*(IPARTLMAX+1),IPART2=(IPARLMAX+1)*
  !     1(IPARLMAX+1))
  !      COMMON /RSC/ RSC(2,158067),IRSC(0:IPARDLMAX),JRSC(0:IPART1,0:IPART
  !     12)
  Use Definition
  Use Parameters
  implicit none
  private
  integer(i4b), parameter, private :: IPART1=(IPARTLMAX+1)*(IPARTLMAX+1)
  integer(i4b), parameter, private :: IPART2=(IPARLMAX+1) *(IPARLMAX+1)
  real(dp),     dimension(:,:), public ::  RSC(2,158067)
  integer(i4b), dimension(:),   public :: IRSC(0:IPARDLMAX)
  integer(i4b), dimension(:,:), public :: JRSC(0:IPART1,0:IPART2)
end Module Common_RSC

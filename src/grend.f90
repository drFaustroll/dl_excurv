SUBROUTINE GREND
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PLFLAG
  Use Index_IGF
  Use Common_PMTX
  Use Common_PLTDSN

  CHARACTER*100 COMMAND
  !      IF (DEBUG) WRITE (6,*) 'IPLOTTR',IGF(IPLOTTR)
  !      CALL CLOSE_PLOTTER@
  !      IER=0
  !      IF (IGF(IPLOTTR).EQ.1) THEN
  !        CALL CISSUE ('HPGLOFF',IER)
  !      ENDIF
  IF (DEBUG) CALL WTEXT ('Plotter closed')
  IF (IGF(2).EQ.2) THEN
     CALL FCLEAR
     CALL FVEXIT
     PLFLAG=.FALSE.
     IF (IGF(IPLOTTR).EQ.1.OR.IGF(IPLOTTR).EQ.2) THEN
        COMMAND='lpr '//PLTDSN(1:LNBLNK(PLTDSN))
        I=LNBLNK(COMMAND)+1
        COMMAND(I:I)=CHAR(0)
        !-st          ISTAT=SYSTEM (COMMAND)
        IF (ISTAT.EQ.0) THEN
           CALL WTEXT ('Output submitted to printer')
        ELSEIF (ISTAT.EQ.-1) THEN
           CALL WTEXT ('Unable to spawn fork')
        ELSE
           CALL WTEXT ('Unable to submit printer output')
        ENDIF
     ENDIF
     CALL WTEXT ('Plot retained in '//PLTDSN)
  ENDIF
  RETURN
END SUBROUTINE GREND

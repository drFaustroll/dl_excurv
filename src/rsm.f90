SUBROUTINE RSM (JLMIN,JLMAX,LLMAX)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_RSC
  !
  !	Calculates r and s coefficients of pendry.
  !
  !	Function BLM returns the integral of three spherical harmonics.
  !	Terms are calculated in the same order as required by EXCAL/MSCAL
  !
  IC=0
  IC1=0
  DO  L1=JLMIN,JLMAX
     IRSC(L1)=IC
     DO  M1=-L1,L1
	IC1=IC1+1
	IC2=0
	DO  L2=0,LLMAX
           DO  M2=-L2,L2
              IC2=IC2+1
              JRSC(IC1,IC2)=IC
              DO  L3=0,L1+L2
                 IF (MOD(L1+L2+L3,2).NE.0) cycle
                 IF (IABS(L2-L1).GT.L3) cycle
                 DO  M3=-L3,L3
                    IF (M1+M3-M2.NE.0) cycle
                    IC=IC+1
                    IVR=(L1-L3-L2+2*M2-2*M3)/2
                    IVS=(L2-L3-L1+2*M3+2*M1)/2
                    AR=(-1.)**IVR*4.*PI
                    AS=(-1.)**IVS*2.*PI
                    !
                    !     Function BLM is a standard Gaunt integral routine.
                    !
                    RSC(1,IC)=AR*BLM(L3,M3,L1,M1,L2,-M2,LLMAX)
                    RSC(2,IC)=AS*BLM(L3,-M3,L2,M2,L1,-M1,LLMAX)
                 enddo
              enddo
           enddo
        enddo
     enddo
  enddo
  write (7,*) 'icmax',ic,llmax,ic1,ic2
  RETURN
END SUBROUTINE RSM

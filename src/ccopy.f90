SUBROUTINE CCOPY (A,B,N)
  !======================================================================C
  !
  !	Copy a number of words from one array to another (real or integer)
  !
  !	A (I) Array from which words are to be copied
  !	B (O) Array into which words are to be copied
  !	N (I) The number of words to be copied
  !
  COMPLEX :: A(*),B(*)
  IF (N.LE.1) RETURN
  DO I=1,N/2
     B(I)=A(I)
  enddo
  RETURN
END SUBROUTINE CCOPY

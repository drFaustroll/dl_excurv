SUBROUTINE RDSPEC (IUN,NREC,TITLE,A1,N1,A2,N2,IOPT,*)
  !======================================================================C
  !
  !	Subroutine used to read one or two columns of data from a
  !	file. Used, in particular, to read spectra
  !
  !	    IUN (I)   UNIT NUMBER FOR READING FILE
  !	   NREC (I/O) NREC IS INPUT AS THE MAXIMUM NUMBER OF DATA RECORDS
  !	              TO BE READ (0=ALL) AND RETURNS THE ACTUAL NUMBER
  !	              OF DATA RECORDS READ.
  !	  TITLE (O)   THIS CHARACTER VARIABLE WILL BE FILLED WITH ANY
  !	              HEADER INFORMATION FOUND. IF IT IS SHORTER THAN
  !	              THE AMOUNT OF HEADER INFORMATION PRESENT, THE
  !	              HEADER INFORMATION RETURNED WILL BE TRUNCATED
  !	     A1 (O)   ARRAY TO CONTAIN DATA READ FROM COLUMN N1. THIS
  !	              ARRAY WILL BE CHECKED TO ENSURE THAT THE POINTS
  !	              INCREASE IN VALUE THROUGHOUT.
  !	     N1 (I)   THE COLUMN NUMBER OF THE DATA TO BE READ INTO
  !	              A1 (<= 9). IF N1 IS 0 THEN NO DATA WILL BE READ
  !	              INTO A1.
  !	     A2 (O)   ARRAY TO CONTAIN DATA READ FROM COLUMN N2. NO
  !	              CHECKING OF THE ORDER OF DATA POINTS IS DONE FOR
  !	              THIS INPUT
  !	     N2 (I)   THE COLUMN NUMBER OF THE DATA TO BE READ INTO
  !	              A2 (<= 9). IF N2 IS 0 THEN NO DATA WILL BE READ INTO
  !	              INTO A2.
  !	   IOPT (I)   THE POINT FREQUENCY (OR MINUS THE POINT FREQUENCY
  !	              IF HEADERS ARE TO BE LISTED AS THEY ARE READ). IF
  !	              IOPT=0 ALL POINTS WILL BE READ AND NO HEADERS WILL
  !	              BE LISTED
  !
  !	Header records are taken as the records found before the first
  !	valid numeric record. A valid numeric record must contain valid
  !	numbers only and must contain at least as many values as the
  !	maximum of n1 and n2. Alphanumeric records may also be embedded
  !	in between the numeric records. These are identified as records
  !	containing characters other than 0-9,+,-,. and e. If header records
  !	are to be listed then these alphanumeric records are also listed
  !
  !	Return 1  error in reading file
  !
  Use Common_RCBUF
  LOGICAL ERR
  DIMENSION A1(*),A2(*),X(9)
  CHARACTER DIG*14,TITLE*(*)
  DATA DIG/'E0123456789.+-'/
  IF (N1.GT.9.OR.N2.GT.9) THEN
     CALL ERRMSG ('More than 9 columns requested in rdspec',*150)
  ENDIF
  NC=MAX0(N1,N2)
  IF (NC.LE.0) CALL ERRMSG ('No data requested in rdspec',*150)
  NR=0
  IR=0
  NHEAD=0
  IP=MAX(IOPT,1)
  LT=LEN(TITLE)-80
  L1=1
  TITLE=' '
  IT=1
  !
  !	Read header records and first numeric record
  !
10 READ (IUN,160,END=110,ERR=120) BUF
  IF (BUF.EQ.' ') GOTO 10
  IL=0
  DO  N=1,NC
     CALL NEXFLD (BUF,IL,M1,M2,*40)
     X(N)=FPVAL (BUF(M1:M2),ERR)
     IF (ERR) GOTO 40
  enddo
30 CALL NEXFLD (BUF,IL,M1,M2,*50)
  XX=FPVAL(BUF(M1:M2),ERR)
  IF (ERR) GOTO 40
  GOTO 30
  !
  !	Process header record
  !
40 NHEAD=NHEAD+1
  IF (NHEAD.LT.10) CALL WTEXT (BUF)
  LH=NCSTR(BUF)
  IF (L1.LE.LT) TITLE(L1:)=BUF
  L1=L1+80
  GOTO 10
  !
  !	Process numeric records
  !
50 IR=1
  NR=1
  GOTO 90
60 READ (IUN,160,END=110,ERR=120) BUF
  IF (BUF.EQ.' ') GOTO 60
  IR=IR+1
  IF (MOD(IR-1,IP).NE.0) GOTO 60
  IL=0
  DO N=1,NC
     CALL NEXFLD(BUF,IL,M1,M2,*130)
     X(N)=FPVAL(BUF(M1:M2),ERR)
     IF (ERR) THEN
        IF (BUF(M1:M1).GT.'9') GOTO 100
        DO  M=M1,M2
           IF (INDEX(DIG,BUF(M:M)).EQ.0) GOTO 100
        enddo
        GOTO 130
     ENDIF
  enddo
  NR=NR+1
  IF (NREC.GT.0.AND.NR.GT.NREC) THEN
     NR=NREC
     GOTO 110
  ENDIF
  !
  !	Store values
  !
90 IF (N1.GT.0) THEN
     A1 (NR)=X(N1)
     IF (NR.GT.1) THEN
        IF (A1(NR).LE.A1(NR-1)) GOTO 140
     ENDIF
  ENDIF
  IF (N2.GT.0) A2(NR)=X(N2)
  GOTO 60
  !
  !	Alphanumeric record within numerics
  !
100 IF (NHEAD.LT.10) CALL WTEXT (BUF)
  IR=IR-1
  GOTO 60
  !
  !	End of file reached
  !
110 NREC=NR
  CALL WTEXTI ('Number of points read: ',NREC)
  RETURN
  !
  !	Error conditions
  !
120 CALL ERRMSG ('error in reading file in rdspec',*150)
130 CALL ERRMSG ('Invalid data record:',*131)
131 CALL ERRMSG (BUF,*150)
140 CALL ERRMSG ('Non-increasing a1 value in buffer:',*141)
141 CALL ERRMSG (BUF,*150)
150 NREC=0
  RETURN 1
  !
160 FORMAT(A)
END SUBROUTINE RDSPEC

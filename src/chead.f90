SUBROUTINE CHEAD (CDATE,CTIME)
  !======================================================================C
  Use Common_PMTCH
  Use Common_PMTX
  CHARACTER :: CDATE*11,CTIME*10
  IF (IGF(14).EQ.2) RETURN
  CALL PLACE (1,1)
  CALL TYPECS (CDATE//'  '//CTIME//'  ')
  CALL TYPECS (GTITLE)
  RETURN
END SUBROUTINE CHEAD

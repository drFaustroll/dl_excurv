SUBROUTINE LARGIT (Y,X,XI,N)
  !======================================================================C
  DIMENSION Y(10), X(10)
  !
  !	N-point Lagrangian interpolator: given arrays Y(I),X(I),(I=1,N)
  !	LARGIT finds the y-value corresponding to XI (XI should lie roughly
  !	in the centre of X(I)). The interpolated value of y is returned in XI.
  !
  YI=0.
  DO  K=1,N
     FNUM=1.
     FDEN=1.
     DO I=1,N
	IF (I.EQ.K) cycle
	FNUM=FNUM*(XI-X(I))
	FDEN=FDEN*(X(K)-X(I))
     enddo
     YI=YI+FNUM*Y(K)/FDEN
  enddo
  XI=YI
  RETURN
END SUBROUTINE LARGIT

COMPLEX FUNCTION P3D (X,Y,Z)
  !======================================================================C
  Use Common_VIEW
  !
  !	Return projection of point XYZ onto a plane
  !
  TT1=VVX*VVX+VVY*VVY
  IF (TT1.LT.1.E-6) THEN
     P3D=CMPLX(X,Y)
     RETURN
  ENDIF
  TT2=TT1+VVZ*VVZ
  S1=SQRT(TT1)
  S2=SQRT(TT2)
  O=S1*(TT2-X*VVX-Y*VVY-Z*VVZ)
  IF (O.EQ.0.) O=1.E-6
  TX=TT2*(Y*VVX-X*VVY)/O
  TY=S2*(Z*TT1-VVZ*(X*VVX+Y*VVY))/O
  P3D=CMPLX(TX,TY)
  RETURN
END FUNCTION P3D

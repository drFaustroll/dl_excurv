SUBROUTINE PERFOP1 (DN,OP)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  DIMENSION DN(36,*)
  CHARACTER*(*) OP
  IF (DN(1,1).LT..01) RETURN
  IF (DEBUG) THEN
     OLDTH=DN(1,2)/AC
     OLDPHI=DN(1,3)/AC
  ENDIF
  IS=1
  DO I=1,10
5    IF (OP(IS:IS).EQ.' ') GOTO 10
     IF (OP(IS:IS).EQ.'T') CALL OCTRIAD (DN,IS,*5)
     IF (OP(IS:IS).EQ.'C') CALL OCROT (DN,ICHAR(OP(IS+1:IS+1))-48,IS,*5)
     IF (OP(IS:IS).EQ.'M') CALL OCMIR (DN,IS,*5)
     IF (OP(IS:IS).EQ.'I') CALL OCCENT (DN,IS,*5)
     IF (OP(IS:IS).EQ.'D') CALL OCDIAD (DN,IS,*5)
     IF (OP(IS:IS).EQ.'W') CALL OCDIADXY (DN,IS,*5)
     IF (OP(IS:IS).EQ.'V') CALL OCVERTX (DN,IS,*5)
     IF (OP(IS:IS).EQ.'Y') CALL OCVERTY (DN,IS,*5)
     IF (OP(IS:IS).EQ.'Z') CALL OCVERTXY (DN,IS,*5)
  ENDDO
10 IF (ABS(DN(1,2)).LT..001) THEN
     DN(1,2)=0.
     DN(1,3)=0.
     !-st	ELSEIF (ABS(DN(1,2)-SNGL(PI)).LT..001) THEN
  ELSEIF (ABS(DN(1,2)-real(PI,kind=sp)).LT..001) THEN
     DN(1,2)=PI
     DN(1,3)=0.
  ELSEIF (ABS(DN(1,3)-PI2).LT..001) THEN
     DN(1,3)=0.
  ELSEIF (ABS(DN(1,3)).LT..001) THEN
     DN(1,3)=0.
  ELSEIF (DN(1,3).GE.PI2) THEN
     DN(1,3)=DN(1,3)-PI2
  ELSEIF (DN(1,3).LT.0.) THEN
     DN(1,3)=DN(1,3)+PI2
  ENDIF
  IF (DEBUG) THEN
     !-st	  CALL POLTOCAR (SNGL(DN(1,1)/DC),DN(1,2),DN(1,3),AX,AY,AZ)
     CALL POLTOCAR(real(DN(1,1)/DC,kind=sp),DN(1,2),DN(1,3),AX,AY,AZ)
     WRITE (7,'(2A,3F7.2,F9.2,F7.2,F6.2,2F5.2)') 'Performed1 ',OP,DN(1,1)/DC,OLDTH,OLDPHI,DN(1,2)/AC,DN(1,3)/AC,AX,AY,AZ
  ENDIF
  RETURN
END SUBROUTINE PERFOP1

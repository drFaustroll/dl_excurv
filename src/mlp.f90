SUBROUTINE MLP (TH,LX,F)
  !======================================================================
  Use Definition
  real(dp) F(16,16)
  !       
  !	Calculates modified Legendre polynomials for fast multiple scattering.
  !       
  LY=LX-2
  CU=COS(TH)
  SU=SIN(TH)
  F(1,1)=1.
  DO I1=2,LX
     F(I1,I1)=SU**(I1-1)
     IF(ABS(F(I1,I1)).LT.0.1E-20) F(I1,I1)=0.0
  enddo
  DO I2=2,LX
     I3=I2-1
     F(I3,I2)=-2.0*real(I3,kind=dp)*CU
     IF(I3.NE.1) F(I3,I2)=F(I3,I2)*SU**(I3-1)
     IF(ABS(F(I3,I2)).LT.0.1E-20) F(I3,I2)=0.0
  enddo
  DO I4=1,LY
     I5=I4+2
     DO I6=I5,LX
        CC1=-real(2*(I6-1)*(2*I6-3),kind=dp)/real(I6+I4-2,kind=dp)
        CC2=-real(4*(I6-1)*(I6-2)*(I6-I4-1),kind=dp)/real(I6+I4-2,kind=dp)
        F(I4,I6)=CC1*CU*F(I4,(I6-1))+CC2*F(I4,(I6-2))
     enddo
  enddo
  DO I=1,LX
     F(1,I)=F(1,I)*.707106781
  enddo
  RETURN
END SUBROUTINE MLP

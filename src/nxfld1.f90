SUBROUTINE NXFLD1 (BUF,IL,M1,M2,*,*)
  !======================================================================C
  !
  !	Get next field from a parameter file record.
  !	Separators are spaces, =signs, commas. J.W.Campbell
  !
  !	    BUF (I)   CHARACTER TEXT BUFFER
  !	    IL (I/O)  POSITION OF LAST CHARACTER PROCESSED
  !	    M1 (O)    START POSITION OF NEXT FIELD
  !	    M2 (O)    END POSITION OF NEXT FIELD
  !         RETURN 1  BLANK FIELD
  !         RETURN 2  END OF STRING
  !
  CHARACTER*(*) BUF
  LOGICAL BRACKETS
  !
  !	Get next field
  !
  NBUF=LEN(BUF)
  M1=0
  M2=0
  BRACKETS=.FALSE.
10 IL=IL+1
  IF (IL.GT.NBUF)GOTO 60
  IF (BUF(IL:IL).EQ.' ') GOTO 10
  !	IF (INDEX(',=',BUF(IL:IL)).GT.0)RETURN 1
  IF (INDEX(',=',BUF(IL:IL)).GT.0)GOTO 50
  M1=IL
20 M2=IL
  IL=IL+1
  IF (IL.GT.NBUF) RETURN
  IF (BUF(IL:IL).EQ.'[') THEN
     BRACKETS=.TRUE.
     GOTO 20
  ELSEIF (BUF(IL:IL).EQ.']') THEN
     BRACKETS=.FALSE.
     GOTO 20
  ENDIF
  IF (BRACKETS) GOTO 20
  J=INDEX(' ,=',BUF(IL:IL))
  IF (J.EQ.0) GOTO 20
  IF (J.NE.1) RETURN
30 IL=IL+1
  IF (IL.GT.NBUF) RETURN
  J=INDEX(' =,',BUF(IL:IL))
  IF (J.EQ.1) GOTO 30
  IF (J.EQ.0)IL=IL-1
  RETURN
50 RETURN 1
60 RETURN 2
END SUBROUTINE NXFLD1

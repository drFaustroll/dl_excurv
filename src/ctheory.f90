SUBROUTINE CTHEORY (KEXP,IGEN,ISPEC)
  !======================================================================C
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Include_IPA
  !	Use Common_IPA
  Use Include_XY

  CHARACTER :: POWER(6)*5,THTITLE*25
  DATA POWER /'*k','*k^2','*k^3','*k^4','*k^5','*k^6'/
  IF (ISPEC.LT.IPARNSP*4) ISPEC=ISPEC+1
  IF (IGEN.EQ.0) THEN
     I1=iset(IEXTYP)
     I2=iset(ISING)
     I3=iset(IMSC)
  ELSEIF (IGEN.EQ.1) THEN
     I1=LASTEXTYP
     I2=LASTSING
     I3=LASTMSC
  ELSE
     I1=IPREVEXTYP
     I2=IPREVSING
     I3=IPREVMSC
  ENDIF
  PLOTTITLE=THTITLE(I1,I2,I3)
  ITLEN=NCSTR(PLOTTITLE)
  IF (NSPEC.NE.1) THEN
     PLOTTITLE(ITLEN+1:)=' '//CHAR(48+KEXP)
     ITLEN=ITLEN+2
  ENDIF
  JQ=IGF(5)-1
  IF (JQ.NE.0) THEN
     IF (ITLEN.GT.1.AND.JQ.LE.6) PLOTTITLE(ITLEN+1:)=POWER(JQ)
  ENDIF
  RETURN
END SUBROUTINE CTHEORY

SUBROUTINE SETUP (*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_VTIME
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_COMPAR
  Use Common_IP
  Use Common_MATEL
  Use Common_P
  Use Common_PATHS
  Use Common_ATMAT
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  Use Include_XA
  !
  !	SETUP initialises parameters for, and calls exafs theory routines
  !	EXCAL, CALCPATH, MSC
  !
  !	RETURN 1 indicates an error condition
  !
  CHARACTER*1 IP1,IP2,IP3
  LOGICAL CENTRAL
  DATA ITHISEXTYP/0/,ITHISSING/0/,ITHISMSC/0/
  !
  !	iset(IEXTYPE) = 1 curved wave
  !	          2 small atom
  !	          3 test
  !	iset(ISING) =   0 Amorphous
  !	          1 single crystal
  !	          2 exact polarisation
  !	iset(IMSC)  =   0 No multiple scattering
  !	          1 Multiple scattering between shells, in units
  !	          2 All multiple scattering
  !
  !	If ITERATE is being executed, change the variables being refined.
  !
  IF (ID.NE.0.AND.NCALL.GT.0) THEN
     DO  II=1,NCALL
        U(II)=XP(II)*SF(II)
        NUMFLAG=2
        CALL PCHANGE (IVCH(II),U(II))
     enddo
     IF (IAND(LC,16_i4b).NE.0) THEN
        IS=1
        IF (iset(IATABS).EQ.1) IS=0
        CENTRAL=.TRUE.
        CALL CALCPOT (.TRUE.,IS,NPS,CENTRAL)
        CENTRAL=.TRUE.
        CALL CALCPHASE (.TRUE.,IS,NPS,CENTRAL)
        LC=IAND(LC,-1_i4b-16_i4b)
     ENDIF
  ENDIF
  IF (LC.EQ.0) RETURN
  CALL UZERO (VTIME,12)
  CALL GETTIM (1,CPU1,ELAP)
  !
  !	If the coefficient of linear expansion and temperature are defined,
  !	calculate the assymetric terms in the DW factor by the anharmonic
  !	oscillator model.
  !
  IF (CLE.GT.0.) THEN
     DO I=1,NS
        B(I)=A(I)*CLE*R(I)*TEMPQ*1.E-6*10.
        B2(I)=B(I)*S3C
        IF (IOUTPUT.GT.0) WRITE (7,*) 'C3',I,A(I),B(I),B2(I)
     ENDDO
     LC=IOR(LC,2_i4b)
  ENDIF
  !
  !	Save old spectrum for plot 1 etc.
  !
  IF (ID.NE.1) THEN
     DO IEXP=1,NSPEC
        NPD2=IPARNPOINTS/2
        IF (NP.LE.NPD2) CALL UCOPY (XABO(1,IEXP),XABO(NPD2+1,IEXP),NP)
        CALL UCOPY (XABT(1,IEXP),XABO(1,IEXP),NP)
     ENDDO
  ENDIF
  IF (DEBUG) WRITE (LOGFILE,'(A,3I5)') &
       '   IEXTYP,   ISING,   IMSC',iset(IEXTYP),iset(ISING),iset(IMSC), &
       'LASTEXTYP,LASTSING,LASTMSC', LASTEXTYP, LASTSING, LASTMSC,       &
       'PREVEXTYP,PREVSING,PREVMSC',IPREVEXTYP,IPREVSING,IPREVMSC
  IPREVEXTYP=LASTEXTYP
  IPREVSING =LASTSING
  IPREVMSC  =LASTMSC
  LASTEXTYP =ITHISEXTYP
  LASTSING  =ITHISSING
  LASTMSC   =ITHISMSC
  ITHISEXTYP=iset(IEXTYP)
  ITHISSING =iset(ISING)
  ITHISMSC  =iset(IMSC)
  IF (DEBUG) WRITE (LOGFILE,'(A,3I5)') &
       '   IEXTYP,   ISING,   IMSC',iset(IEXTYP),iset(ISING),iset(IMSC), &
       'LASTEXTYP,LASTSING,LASTMSC', LASTEXTYP, LASTSING, LASTMSC,       &
       'PREVEXTYP,PREVSING,PREVMSC',IPREVEXTYP,IPREVSING,IPREVMSC
  !
  !	Only calculate T-matrix if LC = .....1..
  !
  IF (DEBUG) WRITE (LOGFILE,'(A,2I3)') 'LC =',LC,iset(IMSC)
  IF (IAND(LC,65_i4b).NE.0) CALL UZERO (XDT,4*IPARNSP*IPARNPOINTS)
  DO  IEXP=1,NSPEC
     !	write (6,*) 'iexp,ipol(iexp)',iexp,ipol(iexp)
     IF (IPOL(IEXP).GT.1) cycle
     JSPEC=1
     JJS=JS(IEXP)
     JJF=JF(IEXP)
     IF (IEXP.NE.NSPEC.AND.IPOL(IEXP).EQ.1) THEN
        DO J=IEXP+1,NSPEC
           IF (IPOL(J).LE.IPOL(IEXP)) GOTO 110
           JSPEC=JSPEC+1
           JJS=MIN(JJS,JS(J))
           JJF=MAX(JJF,JF(J))
        ENDDO
110     CONTINUE
     ENDIF
     JPT=JJF-JJS+1
     !
     !	Strictly NSPEC can be more than 1 if there is only one polarisatio n-group
     !
     KCLUS=1
     DO JEXP=1,NSPEC
	IF (NCLUS(JEXP).GT.0) THEN
           DO I=1,NCLUS(JEXP)
              KEXP(KCLUS)=JEXP
              KCLUS=KCLUS+1
           ENDDO
	ELSEIF (KCLUS.GT.1) THEN
           DO I=1,KCLUS-1
              KEXP(I)=IEXP
           ENDDO
	ENDIF
     ENDDO
     IF (IAND(LC,4_i4b).NE.0.OR.NSPEC.GT.1) THEN
        CALL GETTIM (1,CPU2,ELAP)
        CALL TMATR (*30)
        CALL GETTIM (1,CPU3,ELAP)
        VTIME(7)=VTIME(7)+CPU3-CPU2
     ENDIF
     !
     !	Calculate multiple scattering if required
     !
     IF (iset(IMSC).EQ.2.OR.iset(ISING).NE.0) THEN
        IF (NAT(1).LT.1) THEN
           KEYWORD=' '
           CALL SYMMETRY (.TRUE.,.TRUE.,*30)
        ENDIF
        IF (NAT(1).LT.1) RETURN 1
     ENDIF
     CALL UZERO (XABT(1,IEXP),IPARNPOINTS*JSPEC)
     !
     !	Zero XD,XT,X4 and X5
     !
     II=0
     DO  IEDGE=LINITIAL(IEXP)-1,LINITIAL(IEXP)+1,2
	II=II+1
	IF (IEDGE.LT.0.OR.(iset(NFIN).GT.0.AND.II.NE.iset(NFIN))) cycle
	IF (IOUTPUT.GT.0) CALL WTEXTI ('Calculating contribution for Lfinal=',IEDGE)
        !
        !	POLAR sets up the angular part of the dipole operator
        !	for exact polarisation dependent case. Depends on
        !	BETH, BEPHI and BEW only.
        !
	IF (iset(ISING).NE.0) CALL POLAR (*30)
        !	CALL UZERO (XDT,4*IPARNSP*IPARNPOINTS)
	IF (iset(IMSC).NE.0.AND.IAND(LC,64_i4b).NE.0) CALL CALCPATH (*30,II)
	CALL EXCAL(II)
	IF (iset(IMSC).NE.0) THEN
           IF (.NOT.BACKGROUND.AND.IOUTPUT.GT.0) THEN
              WRITE (LOGFILE,*) 'Path magnitudes:'
              IMAX=MIN(IPARPATH,NUMPATHS)
              DO I=1,IMAX
                 JIND=PATHINDEX(I)
                 WRITE (LOGFILE,40) I,PATHINDEX(I),(IPATHINDS(K,I),K=1,5),PATHPL(I),PATHANG(I),PATHMAX(JIND,1)
              ENDDO
              WRITE (LOGFILE,*) ' '
           ELSE
              !	    CALL WRSPEC (88)
           ENDIF
	ENDIF
     enddo
  enddo
  LC=0
  CALL GETTIM (1,CPU4,ELAP)
  VTIME(1)=VTIME(1)+CPU4-CPU1
  RETURN
  !
  !	ERROR condition
  !
30 RETURN 1
40 FORMAT (I5,F10.2,5I4,F10.3,F8.1,2E12.4)
END SUBROUTINE SETUP

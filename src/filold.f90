LOGICAL FUNCTION FILOLD (DSN)
  !======================================================================C
  !
  !	Find out whether a file exists. (see subroutine 'fname'
  !	for further details of file nameing)
  !
  !
  !	DSN (I)   Character variable holding the filename.
  !	          Max of 60 characters long. may contain trailing spaces.
  !
  CHARACTER*(*) DSN
  LOGICAL EXS
  INQUIRE (FILE=DSN,EXIST=EXS)
  FILOLD=EXS
  RETURN
END FUNCTION FILOLD

REAL FUNCTION FFQ2 (WP,WPSQ,QSQ,QSQ2)
  !======================================================================C
  WQ =SQRT(WPSQ+(4./3.)*QSQ+QSQ*QSQ)
  WQ2=SQRT(WPSQ+(4./3.)*QSQ2+QSQ2*QSQ2)
  AWP=(2./3.)/WP
  FFQ2=(WP+WQ)/QSQ+AWP
  FFQ3=(WP+WQ2)/QSQ2+AWP
  !
  !	check prefactor (wp/4xk) to see if units are correct.
  !
  FFQ2=(FFQ2/FFQ3)
  RETURN
END FUNCTION FFQ2

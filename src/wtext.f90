SUBROUTINE WTEXT (STR)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_DLV
  !
  !	Subroutine to output a text string to the terminal. suppress
  !	 Output if a command file is in use (lmdsn > 0 )
  !	 Unless IPR=1 when output unconditionally.
  !
  !	   STR (I)    TEXT STRING TO BE OUTPUT (TYPE = CHARACTER)
  !
  CHARACTER*(*) :: STR
  LOGICAL       :: CRET
  IF (IPR.EQ.1) GOTO 10
  IF (LMDSN.GT.0) RETURN
10 LK=LEN(STR)
  LL=NCSTR(STR)
  IF (STR(1:1).EQ.'$') THEN
     LSTART=2
     CRET=.FALSE.
     LL=LK
     WRITE (OUTTERM,*)
  ELSE
     LSTART=1
     CRET=.TRUE.
  ENDIF
  K=MIN0(LL,80)
  IF (LOGFILE.NE.OUTTERM) THEN
     IF (CRET.or.DLV_flag) THEN
        WRITE (OUTTERM,20) STR(LSTART:K)
        WRITE (LOGFILE,20) STR(LSTART:K)
        if (DLV_flag) then
           call myflush(OUTTERM)
        endif
     ELSE
        WRITE (OUTTERM,30) STR(LSTART:K)
        WRITE (LOGFILE,30) STR(LSTART:K)
     ENDIF
  ELSE
     IF (CRET.or.DLV_flag) THEN
        WRITE (OUTTERM,20) STR(LSTART:K)
        if (DLV_flag) then
           call myflush(OUTTERM)
        endif
     ELSE
        WRITE (OUTTERM,30) STR(LSTART:K)
     ENDIF
  ENDIF
  RETURN
20 FORMAT(A)
30 FORMAT(A,$)
END SUBROUTINE WTEXT

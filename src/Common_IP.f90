MODULE Common_IP

  !      COMMON /IP/NCALL,ICALL,XP(IPARNVARS),SF(IPARNVARS),U(IPARNVARS),
  !     1RU(IPARNVARS),IVNUMS(IPARNVARS),FSMIN,REME(IPARNPARAMS)
  !      COMMON /IVCH/IVCH(IPARNVARS)
  !      CHARACTER*64 IVCH

  Use Definition
  Use Parameters
  implicit none
  private
  integer(i4b), public :: NCALL
  integer(i4b), public :: ICALL
  real(dp), dimension(:), public :: XP(IPARNVARS)
  real(dp), dimension(:), public :: SF(IPARNVARS)
  real(dp), dimension(:), public ::  U(IPARNVARS)
  real(dp), dimension(:), public :: RU(IPARNVARS)
  integer(i4b), dimension(:), public :: IVNUMS(IPARNVARS)
  real(dp), public :: FSMIN
  real(dp), dimension(:), public :: REME(IPARNPARAMS)
  character*64, dimension(:), public :: IVCH(IPARNVARS)
end MODULE Common_IP



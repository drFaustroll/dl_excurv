Module Common_ATMAT
!	Calculate the t-matrix
!
!	Arrays in common /ATMAT/
!
!	CAP (IPARNPOINTS,2)              Central atom phaseshifts (complex)
!	TMAT (LMAX+1,IPARNP,IPARNPOINTS) T-matrix (complex)
!
!      COMMON /ATMAT/ EREFP(IPARNPOINTS,IPARNP),CAP(IPARNPOINTS,IPARNCLUS
!     1,2),TMAT(IPARNPOINTS,0:IPARLMAX,IPARNP),LMAXVALS(IPARNPOINTS,IPARN
!     2P)
!      COMPLEX EREFP,CAP,TMAT
Use Definition
Use Parameters
implicit none
private
complex(imag),dimension(:,:),  public ::    EREFP(IPARNPOINTS,IPARNP)
complex(imag),dimension(:,:,:),public ::      CAP(IPARNPOINTS,IPARNCLUS,2)
complex(imag),dimension(:,:,:),public ::     TMAT(IPARNPOINTS,0:IPARLMAX,IPARNP)
integer(i4b), dimension(:,:),  public :: LMAXVALS(IPARNPOINTS,IPARNP)
end Module Common_ATMAT

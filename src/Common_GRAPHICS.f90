Module Common_GRAPHICS
!	COMMON /GRAPHICS/  IXMIN,IXMAX,IYMIN,IYMAX,IXDIFF,IYDIFF,GXMIN,GXMAX,
!     &  GYMIN,GYMAX,XDIFF,YDIFF,FVIEW(4),XCUR,YCUR,IPLOTSCALE,CSPIX,
!     &  JXMAX,JYMAX,FJXMAX,FJYMAX,JUST,XMININ,XMAXIN,YMININ,YMAXIN,
!     &  XDIFFIN,YDIFFIN,IGRCOL,ORIENT
Use Definition
implicit none
private
integer(i4b), public :: IXMIN,IXMAX,IYMIN,IYMAX,IXDIFF,IYDIFF
real(dp),     public :: GXMIN,GXMAX,GYMIN,GYMAX, XDIFF, YDIFF
real(dp),dimension(:),public :: FVIEW(4)
real(dp),     public :: XCUR,YCUR
integer(i4b), public :: IPLOTSCALE
real(dp),     public :: CSPIX
integer(i4b), public ::  JXMAX, JYMAX
real(dp),     public :: FJXMAX,FJYMAX
integer(i4b), public :: JUST
real(dp),     public :: XMININ,XMAXIN,YMININ,YMAXIN,XDIFFIN,YDIFFIN
integer(i4b), public :: IGRCOL
real(dp),     public :: ORIENT
end Module Common_GRAPHICS


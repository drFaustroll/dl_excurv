MODULE Common_CDT

  !C      Parameter IPARMXL =0 for LMAX=6,     1 for LMAX=12
  !C      Parameter IPARMXL1=0 for no fast MS, 1 for fast MS
  !      PARAMETER (IPARMXL=1,IPARMXL1=1)
  !      COMMON /CDT/ CFA(71725*IPARMXL1+1),CFC((77040+1615514*IPARMX
  !     2L)*IPARMXL1+1),DSI(0:IPARDLMAX,0:IPARDLMAX),TSI(0:IPARTLMAX,0:IPAR
  !     3DLMAX,0:IPARDLMAX)
  !      INTEGER DSI,TSI

  Use Definition
  Use Parameters
  implicit none
  private
  integer(i4b), parameter :: IPARMXL  = 1
  integer(i4b), parameter :: IPARMXL1 = 1
  real(dp),     dimension(:),     public :: CFA(71725*IPARMXL1+1)
  real(dp),     dimension(:),     public :: CFC((77040+1615514*IPARMXL)*IPARMXL1+1)
  integer(i4b), dimension(:,:),   public :: DSI(0:IPARDLMAX,0:IPARDLMAX)
  integer(i4b), dimension(:,:,:), public :: TSI(0:IPARTLMAX,0:IPARDLMAX,0:IPARDLMAX)
end MODULE Common_CDT

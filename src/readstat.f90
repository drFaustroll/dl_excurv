SUBROUTINE READSTAT (*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Include_IPA
  Use Include_XY
  !
  !	READ READS INPUT FILES ON ENTRY TO THE PROGRAM AND WHEN REQUIRED
  !	BY COMMANDS RESTART AND COMPARE. INITIALISATION OF TIMERS
  !	AND OTHER VARIABLES ON ENTRY.
  !
  DIMENSION DUMMY(IPARNPOINTS)
  CHARACTER*60 WSPACE
  CHARACTER*12 KP1,KP2,KP3
  GOTO 30
  !
  !	ERROR IN READING EXPERIMENTAL FILE
  !
10 CLOSE (INFILE)
  !
20 CALL ERRS(*30)
  !
  !	READ FROM EXPERIMENTAL DATA SET
  !
30 CALL WTEXT ('Filename for sigma ?')
  CALL AREAD (1,WSPACE,LD,*30,*80)
  CALL FILEOPEN (WSPACE,INFILE,'FORMATTED','dat',*20)
  WRITE (OUTTERM,90) WSPACE
40 CALL WTEXT ('POINT FREQUENCY ?')
  CALL CREAD (KP1,KP2,KP3,ISP,IC,V,*40,*80)
  IF (ISP.GT.0.AND.ISP.LT.50) GOTO 50
  CALL ERRMSGI ('Invalid point frequency: ',ISP,*40)
50 CALL WTEXT ('COLUMN COMBINATION ?')
  CALL CREAD (KP1,KP2,KP3,ICOL,IC,V,*50,*80)
  IF (ICOL.GE.12.AND.ICOL.LT.100.AND.MOD(ICOL,11).NE.0) GOTO 60
  CALL ERRMSGI ('Invalid column combination: ',ICOL,*50)
60 ICODE=ISP
  !	IF (LHEAD.NE.0) ICODE=-ISP
  NSIG=IPARNPOINTS
  CALL RDSPEC (INFILE,NSIG,WSPACE,DUMMY,ICOL/10,SIGMA,MOD(ICOL,10),ICODE,*10)
  CLOSE (INFILE)
  IF (NSIG.GE.10) RETURN
  !
  !   ERROR CONDITION
  !
  CALL ERRS(*70)
70 CALL ERRMSG ('Less than ten points in spectrum',*80)
80 RETURN
90 FORMAT ('Sigma in ',A)
END SUBROUTINE READSTAT

SUBROUTINE POLTOCAR (R,THETA,PHI,X,Y,Z)
  !======================================================================C
  Use Common_convia
  IF ( ABS(THETA-PID2).LT.1.d-6) THEN
     SINTH=1.d0
     COSTH=0.d0
  ELSE
     SINTH=SIN(THETA)
     COSTH=COS(THETA)
  ENDIF
  IF (ABS(PHI).LT.1.d-6) THEN
     SINPHI=0.d0
     COSPHI=1.d0
  ELSEIF ( ABS(PHI-PI).LT.1.d-6) THEN
     SINPHI= 0.d0
     COSPHI=-1.d0
  ELSE
     SINPHI=SIN(PHI)
     COSPHI=COS(PHI)
  ENDIF
  X=R*SINTH*COSPHI
  Y=R*SINTH*SINPHI
  Z=R*COSTH
  RETURN
END SUBROUTINE POLTOCAR

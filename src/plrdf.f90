SUBROUTINE PLRDF (IN1)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_TL
  Use Common_PMTX
  Use Include_IPA
  Use Include_PA2
  Use Include_XY
  Use Include_FT
  !	REAL*4 E1,E2,VE
  DIMENSION RDF(IPARNA,IPARNS+1)
  !
  !     Here for plot rdf
  !
  NS1=NS+1
  CALL UZERO (RDF,NS1*IPARNA)
  RQ(1)=0.
  RQ(2)=0.
  PLOTTITLE='Radial Distribution'
  DO  I=1,NS
     AI=A2(I)
     BI1=ABS(D2(I))
     BI12=BI1*2.
     IF (AI.LE.0..AND.BI1.EQ.0.) THEN
        CALL ERRMSGI ('Peak height infinite for shell ',I,*60)
     ENDIF
     RGAM=1.3333333*B2(I)/A2(I)**3
     SQUIG=2./A2(I)**4*(C2(I)/3.-2.*B2(I)**2/A2(I))
     DELTA=32.*5.*B2(I)**2/(72.*A2(I)**5)
     !-st	DIV=DSQRT(A2(I)*PI)*(1.+.75*A2(I)**2*(SQUIG+DELTA))
     DIV= SQRT(A2(I)*PI)*(1.+.75*A2(I)**2*(SQUIG+DELTA))
     V1=SQRT(AI)
     !-st	SPI=DSQRT(PI)
     SPI=SQRT(PI)
     IF (BI12.NE.0.) THEN
        V2=AI/(BI12*BI12)
        V4=V1/BI12
        V5=SPI/BI12
     ENDIF
     IF (AI.NE.0.) V6=-R2(I)/V1
     DO  J=1,IPARNA
	V3=AF(J)-R2(I)
	IF (D2(I).LT.0.) V3=-V3
	IF (BI1.EQ.0.) THEN
           IF (J.EQ.1) WRITE (LOGFILE,*) 'BLOCK1'
           V=-V3*V3/AI
           IF (V.GT.50.) V=50.
           TOP=1.+RGAM*V3**3+SQUIG*V3**4
           V=EXP(V)/DIV*TOP
	ELSEIF (AI.EQ.0.) THEN
           IF (J.EQ.1) WRITE (LOGFILE,*) 'BLOCK2'
           V=0.
           IF (V3.GE.0.) V=EXP(-V3/BI1)/BI1
	ELSE
           IF (J.EQ.1) WRITE (LOGFILE,*) 'BLOCK3'
           E1=V3/V1-V4
           E2=V6-V4
           V=0.
           !-st	  VE=ERF(E1)-ERF(E2)
           VE=erf_fun(E1)-erf_fun(E2)
           WRITE (LOGFILE,*) VE,E1,E2
           IF (VE.EQ.0.) GOTO 10
           V=VE
           ARG=V2-V3/BI1+ALOG(V)
           IF (ARG.GT.50.) ARG=50.
           V=EXP(ARG)*V5
10         WRITE (LOGFILE,*) V,V1,V2,V3,V4,V5,V6,E1,E2,ARG
	ENDIF
	V=V*RN2(I)
        !	IF (IFTSET(6).NE.2.AND.IFTSET(6).NE.4) GOTO 20
	ITT=IT(I)
	IF (IATOM(ITT).EQ.0) THEN
           CALL ERRMSGI ('Atom not defined for shell',I,*60)
	ENDIF
	V=V*(ATOM2(ITT)-RION2(ITT))
        !  20  IF (IFTSET(6).GT.2) V=V/AF(J)/AF(J)
	RDF(J,NS1)=RDF(J,NS1)+V
	RDF(J,I)=V
	IF (RDF(J,NS1).GT.RQ(2)) RQ(2)=RDF(J,NS1)
     enddo
  enddo
  PLOTXSCALE=DC
  IF (DEBUG) WRITE (LOGFILE,*) AF(1),AF(IPARNA),IPARNA,R2(I),I
  CALL PLT (AF,RDF(1,NS1),1,IPARNA,1,*60)
  IF (IN1.NE.1) GOTO 50
  DO I=1,NS
     PLOTXSCALE=DC
     CALL PLT (AF,RDF(1,I),1,IPARNA,0,*60)
     !
     !	Ensure all separate shells are same colour by reducing MPL
     !
     MPL=MPL-1
  enddo
50 CALL PLIST
60 RETURN
END SUBROUTINE PLRDF

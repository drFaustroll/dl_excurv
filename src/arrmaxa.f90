SUBROUTINE ARRMAXA (ARRAY,I_LEN,ARRMAX)
  !======================================================================C
  !       
  !	Return maximum element ARRMAX in ARRAY length ILEN
  !	ARRMAX is not zeroed here, so max. of several arrays can be found
  !       
  DIMENSION :: ARRAY(*)
  DO I=1,I_LEN
     IF (ABS(ARRAY(I)).GT.ARRMAX) ARRMAX=ABS(ARRAY(I))
  ENDDO
  RETURN
END SUBROUTINE ARRMAXA

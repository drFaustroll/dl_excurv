SUBROUTINE XANES (*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Common_P
  Use Common_PRDSN
  Use Common_SYMOPS
  Use Include_IPA
  Use Include_PA1

  DIMENSION NATOM(256),NRI(256)
  CHARACTER*12 IP3
  LOGICAL ONECOM
  IF (KEYWORD.NE.' ') THEN
     ONECOM=.TRUE.
     GOTO 20
  ENDIF
  ONECOM=.FALSE.
10 CALL WTEXT (' ')
  CALL WTEXT ('Enter : DISPLAY, DRAW, LIST, PLOT, RUN, SUBMIT, WRITE or END')
  CHAROPT=' '
  CALL CREAD (KEYWORD,CHAROPT,IP3,IDUM,IC,V,*10,*30)
20 IF (KEYWORD(1:1).NE.'P'.AND.KEYWORD(1:1).NE.'E'.AND.NAT(1).EQ.0)THEN
     IP3=KEYWORD
     KEYWORD=CHAROPT
     CALL SYMMETRY (.FALSE.,.TRUE.,*40)
     KEYWORD=IP3
  ENDIF
  IF (KEYWORD(1:2).EQ.'DI') THEN
     CALL DISPLAY (0)
     goto 10
  ELSEIF (KEYWORD(1:2).EQ.'DR') THEN
     CALL DRAWQ (*10)
     CALL TPOS (10,75)
  ELSEIF (KEYWORD(1:1).EQ.'E') THEN
     RETURN
  ELSEIF (KEYWORD(1:1).EQ.'L') THEN
     CALL GETSHELL (0,*10,*40)
  ELSEIF (KEYWORD(1:1).EQ.'S'.OR.KEYWORD(1:1).EQ.'W') THEN
     CALL XNSUB (KEYWORD,*40)
  ELSEIF (KEYWORD(1:1).EQ.'P') THEN
     CALL XNPLOT (CHAROPT,*40)
  ELSEIF (KEYWORD(1:1).EQ.'R') THEN
     IF (CHAROPT.EQ.' ') THEN
        CALL XNSUB (KEYWORD,*40)
     ELSE
        IPP=ICHAR(CHAROPT(1:1))
        IPP=IPP+32
        CHAROPT(1:1)=CHAR(IPP)
        PRDSN='exanes'//CHAROPT(1:2)//'.cont'
     ENDIF
     LPRDSN=NCROOT(PRDSN)
     !	  ISTAT=SYSTEM('/mnt7/sjc/excurve/runxanes '//PRDSN(1:LPRDSN))
     !-st      ISTAT=SYSTEM('/user1/sjc/bin/rundxanes '//PRDSN(1:LPRDSN)//char(0))
  ENDIF
  IF (.NOT.ONECOM) GOTO 10
30 RETURN
40 CALL ERRS (*41)
41 RETURN 1
END SUBROUTINE XANES

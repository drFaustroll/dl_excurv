SUBROUTINE EXITQ
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PLFLAG

  CALL GETTIM (1,CPU,ELAPS)
  DO IW=OUTTERM,LOGFILE,OUTDIFF
     WRITE (IW,60) CPU,ELAPS
  enddo
  !
  !	Close any open graphics devices
  !
  IF (PLFLAG.AND.PLCMD) CALL GREND
  RETURN
60 FORMAT (/'Total cpu time used =',F10.2,' Elapsed time =',F10.2)
END SUBROUTINE EXITQ

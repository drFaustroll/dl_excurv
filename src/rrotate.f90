SUBROUTINE RROTATE (JSHELL,DIFF,JVECA,IJVECB)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  !
  !	JVECA=0,n JVECB=0 - centre or atom n + pivotal axis
  !	JVECA=0,n JVECB<0 - centre or atom n + relative direction
  !	JVECA=0,n JVECB=m - centre or atom n + atom m
  !
  DIMENSION RL(3),VXYZ(3),TXYZ(3),P(3,3)
  IF (DEBUG.AND.JSHELL.LE.NS) WRITE (LOGFILE,*) 'Rotating',JSHELL,'about',IJVECB,' by',DIFF
  IF (IJVECB.EQ.0) THEN
     IG=NU(JSHELL)
     IF (IG.LE.0) CALL ERRMSG ('VECB undefined - set it to a shell number',*10)
     JVECB=NR(IG)
  ELSE
     JVECB=IJVECB
  ENDIF
  IF (JVECB.LE.0) THEN
     JAXIS=JVECA
  ELSE
     JAXIS=JVECB
  ENDIF
  IF (JAXIS.LE.0) THEN
     QX=0
     QY=0
     QZ=0
  ELSE
     QX=UX2(JAXIS)
     QY=UY2(JAXIS)
     QZ=UZ2(JAXIS)
  ENDIF
  IF (JVECB.EQ.-1) THEN
     QX=QX+1.
  ELSEIF (JVECB.EQ.-2) THEN
     QY=QY+1.
  ELSEIF (JVECB.EQ.-3) THEN
     QZ=QZ+1.
  ELSEIF (JVECB.EQ.-4) THEN
     TQ=QX
     QX=QY
     QY=TQ
     !
     !	Normal to JVECB
     !
  ELSEIF (JVECB.EQ.-5) THEN
     IF (JVECA.LE.0) THEN
        WRITE (OUTTERM,*) 'VECA undefined'
        RETURN
     ELSEIF (UX2(JVECA).EQ.0.) THEN
        WRITE (OUTTERM,*) 'X is zero'
        RETURN
     ENDIF
     QX=UX2(JVECA)+1
     QY=(UX2(JVECA)-UY2(JVECA)/UX2(JVECA))*QX
     QZ=UZ2(JVECA)
  ENDIF
  VXYZ(1)=UX2(JSHELL)-UX2(JVECA)
  VXYZ(2)=UY2(JSHELL)-UY2(JVECA)
  VXYZ(3)=UZ2(JSHELL)-UZ2(JVECA)
  RL(1)=QX-UX2(JVECA)
  RL(2)=QY-UY2(JVECA)
  RL(3)=QZ-UZ2(JVECA)
  TRL=RL(1)*RL(1)+RL(2)*RL(2)+RL(3)*RL(3)
  IF (TRL.EQ.0.) THEN
     TRL=1.
     RL(3)=1.
  ENDIF
  DO I=1,3
     IF (RL(I).LT.0.) THEN
        SIGN=-1.
     ELSE
        SIGN=1.
     ENDIF
     RL(I)=SQRT(RL(I)*RL(I)/TRL)*SIGN
  ENDDO
  CA=COS(DIFF*AC)
  SA=SIN(DIFF*AC)
  IF (DIFF.EQ.90.) THEN
     CA=0.
     SA=1.
  ELSEIF (DIFF.EQ.-90.) THEN
     CA=0.
     SA=-1.
  ENDIF
  P(1,1)=CA+RL(1)*RL(1)*(1.-CA)
  P(1,2)=RL(1)*RL(2)*(1.-CA)+RL(3)*SA
  P(1,3)=RL(3)*RL(1)*(1.-CA)-RL(2)*SA
  P(2,1)=RL(1)*RL(2)*(1.-CA)-RL(3)*SA
  P(2,2)=CA+RL(2)*RL(2)*(1.-CA)
  P(2,3)=RL(2)*RL(3)*(1.-CA)+RL(1)*SA
  P(3,1)=RL(3)*RL(1)*(1.-CA)+RL(2)*SA
  P(3,2)=RL(2)*RL(3)*(1.-CA)-RL(1)*SA
  P(3,3)=CA+RL(3)*RL(3)*(1.-CA)
  DO J=1,3
     TXYZ(J)=0.
     DO I=1,3
	TXYZ(J)=TXYZ(J)+VXYZ(I)*P(I,J)
     ENDDO
  ENDDO
  IF (JVECA.NE.0) THEN
     TXYZ(1)=TXYZ(1)+UX2(JVECA)
     TXYZ(2)=TXYZ(2)+UY2(JVECA)
     TXYZ(3)=TXYZ(3)+UZ2(JVECA)
  ENDIF
  CALL CARTOPOL (TXYZ(1),TXYZ(2),TXYZ(3),R2(JSHELL),TH2(JSHELL),PHI2(JSHELL))
  R(JSHELL)=R2(JSHELL)/DC
  TH(JSHELL)=TH2(JSHELL)/AC
  PHI(JSHELL)=PHI2(JSHELL)/AC
  UX2(JSHELL)=TXYZ(1)
  UY2(JSHELL)=TXYZ(2)
  UZ2(JSHELL)=TXYZ(3)
  UX(JSHELL)=UX2(JSHELL)/DC
  UY(JSHELL)=UY2(JSHELL)/DC
  UZ(JSHELL)=UZ2(JSHELL)/DC
10 RETURN
END SUBROUTINE RROTATE

SUBROUTINE DK56 (I,JSYM,*)
  Use Parameters
  Use Common_convia
  Use Common_UPU
  !
  !	Decide between horizontal mirror plane or diad and principal axis.
  !
  IF (ABS(RADTH(I)).LT..001.OR.ABS(RADTH(I)-PI).LT..001) THEN
     JSYM=6
     RETURN 1
  ELSE
     RETURN
  ENDIF
END SUBROUTINE DK56

SUBROUTINE PCOR (J,OLD,OLD2,JTN,MS,JCLUS)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_UPU
  Use Index_INDS
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2

  real(float) :: AA,BB,CC,DD,EE,FF,ROOT1,ROOT2,WR2
  real(float) :: YROOT1,YROOT2,SIGN1,SIGN2,CALCDSQRT,ROOTS(2),YROOTS(2)
  LOGICAL     :: LERR
  !
  !	NR(I) is the shell index for the nearest atom in unit I
  !	NU(I) is the unit number for shell I ( zero no unit)
  !	JT indicates the type of variable - 3 for r, 4 for alpha etc.
  !	MS is the shell index of variable J
  !	IG is the unit number for variable J
  !	NF is NR(IG)
  !
  IF (DEBUG) THEN
     DO IW=OUTTERM,LOGFILE,OUTDIFF
        WRITE (IW,*) 'pcor: icor,jtn,ms,old',IVARS(J),iset(ICOR),JTN,MS,OLD
     ENDDO
  ENDIF
  !
  !	iset(ICOR)=2 (Correlation='All') is for X,Y,Z and ROT only
  !
  IF (iset(ICOR).EQ.2.AND.(JTN.LT.inds(INDX).OR.JTN.GT.inds(INDROT))) RETURN
  IG=NU(MS)
  NF=NR(IG)
  JRAD=IRAD(JCLUS)
  DIFF=PA1(J)-OLD
  DIFF2=PA2(J)-OLD2
  TX=UX(NF)-UX(JRAD)
  TY=UY(NF)-UY(JRAD)
  TZ=UZ(NF)-UZ(JRAD)
  !
  !
  !   1    2    3    4    5    6    7    8    9   10   11   12   13   14
  !   N    T    R    A    B    C    D   UN  ANG   TH  PHI   UX   UY   UZ
  !
  !  15   16   17   18   19   20   21   22   23   24   25   26   27
  ! ROT  PIV  PLA  PLB TORA TORB TORC  PAT PANG  UOC TWST TILT CLUS
  !
  !	GOTO (150,150,10,150,150,150,150,150,50,30,60,80,80,80,120,150,150, &
  !             150,150,150,150,150,150,150,100,100,150),JT
  !
  !	Here for R
  !
  !	Nearest Atom has been Changed - Calculate the Vector to be added.
  !
  IF (JTN.EQ.inds(INDR)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR: Changing R')
     AA=TX*TX+TY*TY+TZ*TZ
     BB=2.*(UX(JRAD)*TX+UY(JRAD)*TY+UZ(JRAD)*TZ)
     CC=R(JRAD)*R(JRAD)-R(NF)*R(NF)
     CALL QUAD (AA,BB,CC,ROOT1,ROOT2,NROOTS,*160)
     IF (ROOT1.GT.0.) THEN
        DIFFR=ROOT1
     ELSE
        DIFFR=ROOT2
     ENDIF
     DIFFR=DSQRT(AA)*(DIFFR-1.)
     CALL POLTOCAR (DIFFR,RADTH(NF),RADPHI(NF),QX,QY,QZ)
     DO I=0,IPARNS
        IF (NU(I).EQ.IG.AND.I.NE.JRAD) THEN
           UX(I)=UX(I)+QX
           UY(I)=UY(I)+QY
           UZ(I)=UZ(I)+QZ
        ENDIF
     enddo
     GOTO 130
  ELSEIF (JTN.EQ.inds(INDTH)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR: Changing TH')
     !
     !	Here for THETA
     !
     EE=UX(JRAD)**2+UY(JRAD)**2
     IF (DABS(EE).GE.1.E-10) THEN
        CALL WTEXT ('Cannot rotate to given THETA value while the central atom X and Y coords are non-zero')
        GOTO 160
     ENDIF
     WR2=TX*TX+TY*TY+TZ*TZ
     DO JJ=1,2
        IF (JJ.EQ.1) THEN
           THETA=TH2(NF)
        ELSE
           THETA=OLD2
        ENDIF
        U=COS(THETA)
        V=-SIN(THETA)
        DD=-UZ(JRAD)*V
        AA=V*V+U*U
        BB=-2.*DD*U
        CC=DD*DD-V*V*WR2
        CALL QUAD (AA,BB,CC,ROOT1,ROOT2,NROOTS,*160)
        YROOT1=CALCDSQRT(WR2-ROOT1*ROOT1,'YROOT1   ',LERR)
        IF (NROOTS.EQ.1) THEN
           YROOT2=-YROOT1
        ELSE
           YROOT2=CALCDSQRT(WR2-ROOT2*ROOT2,'YROOT1   ',LERR)
           SIGN1=(-U*ROOT1+DD)/V
           SIGN2=(-U*ROOT2+DD)/V
           YROOT1=DSIGN(YROOT1,SIGN1)
           YROOT2=DSIGN(YROOT2,SIGN2)
        ENDIF
        IF (ABS(THETA-PI).LT.1.E-6) THEN
           IF (YROOT2.LT.0.) THEN
	      ROOTS(JJ)=ROOT2
	      YROOTS(JJ)=YROOT2
           ELSE
	      ROOTS(JJ)=ROOT1
	      YROOTS(JJ)=YROOT1
           ENDIF
        ELSE
           IF (ROOT2.GT.0.) THEN
	      ROOTS(JJ)=ROOT2
	      YROOTS(JJ)=YROOT2
           ELSE
	      ROOTS(JJ)=ROOT1
	      YROOTS(JJ)=YROOT1
           ENDIF
        ENDIF
     ENDDO
     ANGLE=(ROOTS(1)*ROOTS(2)+YROOTS(1)*YROOTS(2))/WR2
     ANGLE=-CALCACOS(ANGLE,'ANGLE   ',LERR)
     TY=YROOTS(1)*ROOTS(2)-ROOTS(1)*YROOTS(2)
     IF (TY.EQ.0.) TY=1.
     !	  WRITE (6,'(A,4F9.3)')'AXIS,ANGLE,ROT',TY,ANGLE/AC,RADPHI(NF)/AC
     DO I=0,IPARNS
        IF (NU(I).NE.IG.OR.I.EQ.JRAD) cycle
        UX(I)=UX(I)-UX(JRAD)
        UY(I)=UY(I)-UY(JRAD)
        UZ(I)=UZ(I)-UZ(JRAD)
        CALL XROTATE (0.,0.,1.,-RADPHI(NF),UX(I),UY(I),UZ(I))
        CALL XROTATE (0.,TY,0.,ANGLE,UX(I),UY(I),UZ(I))
        CALL XROTATE (0.,0.,1.,+RADPHI(NF),UX(I),UY(I),UZ(I))
        UX(I)=UX(I)+UX(JRAD)
        UY(I)=UY(I)+UY(JRAD)
        UZ(I)=UZ(I)+UZ(JRAD)
     enddo
     I=NF
     !
     !	This outputs the inchanged values
     !
     WRITE (6,'(A,4F9.3)') 'xyz           ',UX2(I)/DC,UY2(I)/DC,(UZ2(I)+UZ2(JRAD))/DC
     GOTO 130
  ELSEIF (JTN.EQ.inds(INDANG)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR: Changing ANG')
     !
     !	ANG(NF) must always be zero
     !
50   ANG(NF)=0.
     !	  ANG2(NF)=0.
     RETURN
  ELSEIF (JTN.EQ.inds(INDPHI)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR: Changing PHI')
     !
     !	Here for PHI
     !
60   WR2=TX*TX+TY*TY
     IF (ABS(SIN(PHI2(NF))).LT..7071) THEN
        U1=-TAN(PHI2(NF))
        V1=1.
     ELSE
        U1=-1.
        !-st	    V1=TAN(SNGL(PID2-PHI2(NF)))
        V1=TAN(real(PID2-PHI2(NF),kind = sp))
     ENDIF
     DD=-UX(JRAD)*U1-UY(JRAD)*V1
     AA=V1*V1+U1*U1
     BB=-2.*DD*U1
     CC=DD*DD-WR2*V1*V1
     CALL QUAD (AA,BB,CC,ROOT1,ROOT2,NROOTS,*160)
     IF (ABS(V1).LT.1.E-6) THEN
        SIGN1=SIN(PHI2(NF))
        SIGN2=SIN(PHI2(NF))
     ELSE
        SIGN1=(DD-U1*ROOT1)/V1
        SIGN2=(DD-U1*ROOT2)/V1
     ENDIF
     YROOT1=CALCDSQRT(WR2-ROOT1*ROOT1,'yroot1  ',LERR)
     YROOT2=CALCDSQRT(WR2-ROOT2*ROOT2,'yroot2  ',LERR)
     YROOT1=DSIGN(YROOT1,SIGN1)
     YROOT2=DSIGN(YROOT2,SIGN2)
     IF (COS(PHI2(NF)).GE.0.) THEN
        ANGLE=(TX*ROOT1+TY*YROOT1)/WR2
        AXIS1=DSIGN(1.0D0,TX*YROOT1-ROOT1*TY)
     ELSE
        ANGLE=(TX*ROOT2+TY*YROOT2)/WR2
        AXIS1=DSIGN(1.0D0,TX*YROOT2-ROOT2*TY)
     ENDIF
     ANGLE=CALCACOS (ANGLE,'angle   ',LERR)
     !	WRITE (7,*) 'tx,ty',TX,TY
     !	WRITE (7,*) 'root1,yroot1',ROOT1,YROOT1
     !	WRITE (7,*) 'root2,yroot2',ROOT2,YROOT2
     !	WRITE (7,'(A,4f8.3)') 'ANGLE:',ANGLE/AC
     DO  I=0,IPARNS
        IF (NU(I).NE.IG.OR.I.EQ.JRAD) cycle
        !	PHI2(I)=PHI2(I)+DIFF2
        UX(I)=UX(I)-UX(JRAD)
        UY(I)=UY(I)-UY(JRAD)
        UZ(I)=UZ(I)-UZ(JRAD)
        CALL XROTATE (0.,0.,AXIS1,ANGLE,UX(I),UY(I),UZ(I))
        UX(I)=UX(I)+UX(JRAD)
        UY(I)=UY(I)+UY(JRAD)
        UZ(I)=UZ(I)+UZ(JRAD)
     enddo
     GOTO 130
  ELSEIF (JTN.EQ.inds(INDX).OR.JTN.EQ.inds(INDY).OR.JTN.EQ.inds(INDZ))THEN
     IF (DEBUG) CALL WTEXT ('PCOR: Changing XYZ')
     !
     !	Here for X,Y and Z
     !
     !  80	  IOFF=(IPARNS+1)*(JT-11)
80   DO  I=0,IPARNS
        IF (iset(ICOR).EQ.2) THEN
           IF (ICLUS(I).NE.JCLUS) cycle
           !	    IF (UX(I+JTN-inds(INDX)).EQ.0.) cycle
        ELSE
           IF (I.EQ.MS.OR.NU(I).NE.IG.OR.I.EQ.JRAD) cycle
        ENDIF
        IF (DEBUG) WRITE (LOGFILE,*) 'PCOR: I,JTN,DIFF,UX(I+JTN)',I,JTN,DIFF,UX(I+JTN-inds(INDX))
        UX(I+JTN-inds(INDX))=UX(I+JTN-inds(INDX))+DIFF
     enddo
     GOTO 130
  ELSEIF (JTN.EQ.inds(INDTILT).OR.JTN.EQ.inds(INDTWST)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR: Changing TILT/TWST')
     !
     !	Here for TILT or TWST
     !
100  CALL GETAXIS (MS,JTN,NF,IG,RX,RY,RZ,FANG,*160)
     DO  I=0,IPARNS
        IF ((iset(ICOR).NE.2.AND.NU(I).NE.IG).OR.I.EQ.JRAD) cycle
        CX=UX(I)-UX(JRAD)
        CY=UY(I)-UY(JRAD)
        CZ=UZ(I)-UZ(JRAD)
        CALL XROTATE (RX,RY,RZ,DIFF2,CX,CY,CZ)
        UX(I)=CX+UX(JRAD)
        UY(I)=CY+UY(JRAD)
        UZ(I)=CZ+UZ(JRAD)
     enddo
     GOTO 130
  ELSEIF (JTN.EQ.inds(INDROT)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR: Changing ROT')
     !
     !	Here for ROT
     !
120  JVECA=IVECA
     JVECB=IVECB
     DO I=0,IPARNS
        IF((iset(ICOR).EQ.2.AND.ICLUS(I).EQ.JCLUS).OR.NU(I).EQ.IG.AND.I.NE.JRAD) THEN
           CALL RROTATE (I,DIFF,JVECA,JVECB)
        ENDIF
     ENDDO
     RETURN
  ELSE
     GOTO 150
  ENDIF
  !
  !	Final unit conversion - shouldnt be neccessary ?
  !
  ! 130	DO 140 I=0,IPARNS
  !	IF ((iset(ICOR).NE.2.AND.NU(I).NE.IG).OR.I.EQ.JRAD) GOTO 140
  !	UX2(I)=UX(I)*DC
  !	UY2(I)=UY(I)*DC
  !	UZ2(I)=UZ(I)*DC
  ! 140	CONTINUE
130 RETURN
150 RETURN
  !
  !	Restore old values after error
  !
160 PA1(J)=OLD
  !	PA2(J)=OLD2
  !       call all_IPA
  !	IPA(J)=PA1(J)
  !       call IPA_all
  RETURN
END SUBROUTINE PCOR

SUBROUTINE XADD (IST,NCOL,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_TL
  Use Common_PMTX
  Use Common_PRDSN
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2
  Use Include_XY
  Use Include_XA
  !
  !	XADD COMMAND / ADD EXTERNAL SPECTRUM TO THEORY
  !
  !	OPTIONS ARE : ON     - MARK ADDITION STATUS AS ON IF A SUITABLE
  !	                       FILE HAS BEEN READ
  !	              OFF    - MARK ADDITION STATUS AS OFF
  !	              READ   - READ A FILE. USUALLY OUTPUT FROM THE
  !	                       FROM MULTIPLE SCATTERING PROGRAM EXFIT)
  !	              WRITE  - CREATE A FILE FROM CURRENT THEORY
  !	              STATUS - DISPLAY STATUS (ON/OF, WHETHER A FILE
  !	                       HAS BEEN SUCCESSFULLY READ
  !	              ADD    - ADD ANOTHER XADD SPECTRUM TO EXISTING ONE
  !	              MULT   - MULTIPLY EXISTING SPECTRUM BY FUNCTION
  !
  !	When status is on, the spectrum in array xa in common /xa/ is
  !	added to theory calculations throughout the program, provided
  !	XE1, XE2 and NX correspond to EMIN, EMAX and NPT.
  !	Checking is performed by subroutine CXKA.
  !
  DIMENSION WSPACE(IPARNPOINTS*2)
  CHARACTER*60 WKDSN
  CHARACTER*80 XTITL
  CHARACTER*12 IP0,IP2,IP3
  CHARACTER*10 CDATE
  CHARACTER*8 CTIME
  CHARACTER*6 OPTS(7)
  CHARACTER*4 IST,IPT
  REAL ROW(3)
  DATA OPTS/'OFF','ON','READ','WRITE','STATUS','ADD','MULT'/
  IEXP=1
  CALL FINDOPT (7,OPTS,J,0,'Column number - default 2',*270)
  GOTO (10,10,90,200,240,90,10),J
  !
  !	Here for OFF, ON and MULT options
  !
10 IF (IXA/2.NE.1) GOTO 250
  IF (J.NE.7) GOTO 80
  !	IF (LC.LT.3) LC=3
20 CALL WTEXT ('Enter constant')
  CALL CREAD (IP0,IP2,IP3,IM,IC,V,*20,*260)
  IF (V.EQ.0.) GOTO 20
  DO I=1,NX
     XA(I)=XA(I)*V
  enddo
40 CALL WTEXT ('Enter alpha for term exp(alpha*k**2)')
  CALL CREAD (IP0,IP2,IP3,IM,IC,V,*40,*260)
  IF (IC.EQ.0) GOTO 40
  DO I=1,NX
     XA(I)=XA(I)*EXP(V*RK(I,IEXP)*RK(I,IEXP))
  enddo
60 CALL WTEXT ('ENTER BETA FOR TERM EXP(BETA/K)')
  CALL CREAD (IP0,IP2,IP3,IM,IC,V,*60,*260)
  IF (IC.EQ.0) GOTO 60
  DO I=1,NX
     XA(I)=XA(I)*EXP(V/RK(I,IEXP))
  enddo
  GOTO 240
80 IXA=2
  IF (J.EQ.2) IXA=3
  LC=IOR(LC,4_i4b)
  LF=2
  GOTO 240
  !
  !	Here for read option and add option
  !
90 IF (NCOL.EQ.-1) THEN
     LD=NCSTR(WKDSN)
  ELSE
100  CALL WTEXT ('Enter filename')
     CALL AREAD (1,WKDSN,LD,*100,*260)
  ENDIF
  CALL FILEOPEN (WKDSN,INFILE,'FORMATTED','dat',*270)
  !
  !	Save old spectrum if add option
  !
  IF (J.NE.6) GOTO 110
  IF (IXA.LT.2.OR.NX.LE.10) CALL ERRMSG ('ADD options invalid',*250)
  CALL UCOPY (XA,WSPACE,NX)
  NNX=NX
110 IF (NCOL.LT.2) NCOL=2
  NX=IPARNPOINTS
  JP=1
  !	IF (LHEAD.NE.0) JP=-JP
  CALL RDSPEC (INFILE,NX,XTITL,WSPACE(IPARNPOINTS+1),1,XA,NCOL,JP,*220)
  REWIND INFILE
  XAE1=WSPACE(IPARNPOINTS+1)*EC
  XAE2=WSPACE(IPARNPOINTS+NX)*EC
  IF (NX.LT.10) GOTO 220
  IXA=3
  LC=IOR(LC,3_i4b)
  LF=MAX(LF,2)
  IF (J.NE.6) GOTO 130
  IF (NNX.NE.NX) GOTO 220
  DO K=1,NX
     XA(K)=XA(K)+WSPACE(K)
  enddo
  CALL WTEXT ('Spectrum added to original')
  GOTO 150
  !
  !	Get title records if present after setting whole of MSTL to blanks
  !
130 DO KQ=1,IPARNS+2
     MSTL(KQ)=' '
  enddo
150 DO  I=1,1000
     READ (INFILE,280,END=230) IPT
     IF (IPT.EQ.'&PT ') GOTO 170
  enddo
  GOTO 230
170 DO I=1,IPARNS+2
     IF (MSTL(I)(1:4).NE.'    ') cycle
     READ (INFILE,280,END=230) MSTL(I)
     IF (MSTL(I)(1:4).EQ.'&END') GOTO 190
  enddo
  GOTO 230
190 MSTL(I)(1:4)='    '
  GOTO 230
  !
  !	Here for write option
  !
200 DO IEXP=1,NSPEC
     CALL OUTNAM (12,PRDSN,' ',*270)
     CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*270)
     CALL GETDAT (CDATE,CTIME,OUTFILE)
     CALL FIT ('F',iset(IWEIGHT),*270)
     WRITE (OUTFILE,*) 'Energy       Total       Single      Double     Triple         Quad       Quint'
     DO IE=1,NP
	IF (IE.LT.JS(IEXP).OR.IE.GT.JF(IEXP)) THEN
           WRITE (OUTFILE,'(F8.3,6G12.4)') ENER(IE)/EC,0.,0.,0.,0.,0.,0.
	ELSE
           IEP=IE-JS(IEXP)+1
           SUM=XD(IEP,IEXP)+XT(IEP,IEXP)+X4(IEP,IEXP)+X5(IEP,IEXP)
           IF (DEBUG) WRITE (7,*) 'XADD Sum for iep=',IEP,' iexp=',IEXP,SUM,XD(IEP,IEXP),XT(IEP,IEXP),X4(IEP,IEXP)
           WRITE (OUTFILE,'(F8.3,6G12.4)') ENER(IE)/EC,XABT(IE,IEXP), &
                XABT(IE,IEXP)-SUM,XD(IEP,IEXP),XT(IEP,IEXP),X4(IEP,IEXP),X5(IEP,IEXP)
	ENDIF
     enddo
     CALL EFILE ('Spectrum')
  ENDDO
  RETURN
220 CALL ERRMSG ('Error in reading XADD file',*221)
221 IXA=1
230 CLOSE (INFILE)
  !
  !	Here for status option, or status display after all options
  !
240 WRITE (OUTTERM,290) OPTS(MAX0(1,IXA-1))
250 IF (IXA.LT.2) THEN
     CALL WTEXT ('No XADD file has been read')
  ELSE
     CALL WTEXT ('An XADD file has been read')
  ENDIF
260 RETURN
270 RETURN 1
  !
280 FORMAT (1X,A)
290 FORMAT ('Addition flag : ',A3)
END SUBROUTINE XADD

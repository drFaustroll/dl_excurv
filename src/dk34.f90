SUBROUTINE DK34 (I,JSYM,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_SYMOPS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2
  !
  !	Called where there is ambiguity as to whether atoms lie on a diad,
  !	one of which must be at theta=90, phi=0, or a vertical mirror
  !	plane. Dnd groups only
  !
  IF (RADPHI(I).NE.0) THEN
     TP=PI/real(JAXIS/2,kind=dp)
     TF=AMOD(RADPHI(I),TP)
     IF (ABS(TF-TP*.5).LT..0001) THEN
        JSYM=4
        RETURN 1
     ELSE
        IF (ABS(TF).LT..0001.OR.ABS(TF-TP).LT..0001) RETURN
        CALL WTEXTI ('Shell: ',I)
        CALL WTEXT  ('PHI value invalid for special position.')
     ENDIF
  ENDIF
  RETURN
END SUBROUTINE DK34

SUBROUTINE ARRMINMAX (ARRAY,I_LEN,ARRMIN,ARRMAX)
  !======================================================================C
  !       
  !	Return maximum + minumum elements in ARRAY length I_LEN
  !	ARRMAX/ARRMIN not zeroed here, so several arrays can be processed
  !       
  DIMENSION ARRAY(*)
  DO I=1,I_LEN
     IF (ARRAY(I).GT.ARRMAX) THEN
        ARRMAX=ARRAY(I)
     ELSEIF (ARRAY(I).LT.ARRMIN) THEN
        ARRMIN=ARRAY(I)
     ENDIF
  ENDDO
  RETURN
END SUBROUTINE ARRMINMAX

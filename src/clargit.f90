SUBROUTINE CLARGIT (Y,X,XI,N,YI)
  !======================================================================C
  !       
  !	Complex interpolation routine
  !       
  DIMENSION :: X(*)
  COMPLEX   :: Y(*),YI
  YI=0.0
  DO K=1,N
     FNUM=1.0
     FDEN=1.0
     DO I=1,N
        IF (I.EQ.K) GOTO 10
        FNUM=FNUM*(XI-X(I))
        FDEN=FDEN*(X(K)-X(I))
10      CONTINUE
     enddo
     YI=YI+FNUM*Y(K)/FDEN
     IF (FDEN.EQ.0.) WRITE (6,*) 'fden=0.'
  enddo
  RETURN
END SUBROUTINE CLARGIT

SUBROUTINE GETCOORD1 (JCLUS,I,IA1,DN)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Include_IPA
  !	Use Common_IPA

  DIMENSION DN(0:5,0:5,6)
  DN(0,I,1)=RAD(IA1)
  DN(0,I,2)=RADTH(IA1)
  DN(0,I,3)=RADPHI(IA1)
  DN(0,I,4)=RADX(IA1)
  DN(0,I,5)=RADY(IA1)
  DN(0,I,6)=RADZ(IA1)
  DN(I,0,1)=RAD(IA1)
  DN(I,0,2)=PI-RADTH(IA1)
  DN(I,0,3)=RADPHI(IA1)+PI
  DN(I,0,4)=-RADX(IA1)
  DN(I,0,5)=-RADY(IA1)
  DN(I,0,6)=-RADZ(IA1)
  RETURN
END SUBROUTINE GETCOORD1

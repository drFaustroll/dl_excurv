SUBROUTINE SYSCMD (TEXT)
  !======================================================================C
  Use Parameters
  Use Common_B1
  !
  !	carry out system command TEXT within excurve
  !
  CHARACTER*(*) TEXT
  INTEGER SYSTEM
  ITLEN=NCSTR(TEXT)
  !-st        ISTAT=SYSTEM(TEXT(1:NCSTR(TEXT)))
  IF (DEBUG) WRITE (6,'(Z8)') 'ISTAT=',ISTAT
  RETURN
END SUBROUTINE SYSCMD

SUBROUTINE CROUT (MR,NR,NCC,A,ZMCH,DT,IERR,LP)
  !======================================================================C
  Use Definition
  !
  !     CROUT (1) operates on a coefficient matrix to solve a system of
  !	simultaneous equations or to compute an inverse and (2)
  !	calculates a determinant.
  !	CROUT reduces the original matrix and right-hand sides until
  !	upon completion the reduced matrix replaces the original
  !	matrix and the solutions replace the right-hand sides.
  !
  IMPLICIT real(float) (A-H,O-Z)
  DIMENSION A(*),LP(*)
  IF (NR.GT.MR) GOTO 210
  MTX=MR*NR
  MRA=MR+1
  MRS=MR-1
  MDN=MR-NR
  MTR=MTX-MDN
  MTRA=MTX+1
  DT=1.
  IERR=0
  NRS=NR-1
  DO I=1,NR
     LP(I)=I
  enddo
  IF (NCC) 210,805,1001
805 NTC=NR+NR
  MTT=MR*NTC-MDN
  J=MTRA
  DO K=MTRA,MTT,MR
     JJF=K+NRS
     DO KX=K,JJF
	A(KX)=0.
     enddo
     A(J)=1.
     J=J+MRA
  enddo
  GOTO 1
1001 NTC=NR+NCC
  MTT=MR*NTC-MDN
1 IF (NTC.LE.NR) GOTO 210
  DO I=1,NR
     IS=I-1
     II=MR*IS+I
     IISB=II-1
     IIAD=II+MR
     ICF=MR*I-MDN
     ICS=ICF-NRS
     IIA=II+1
     TEMP=0.
     DO J=II,MTT,MR
	IF (I.EQ.1) GOTO 33
	KF=J-1
	KS=J-I+1
	KX=I
	DO K=KS,KF
           A(J)=A(J)-A(KX)*A(K)
           KX=KX+MR
        enddo
33      IF(J.GT.MTR) GOTO 31
        IF (ABS(A(J)).LE.TEMP) GOTO 31
        TEMP=ABS(A(J))
        NX=J/MR+1
31      CONTINUE
     enddo
     IF (I.EQ.NR) GOTO 35
     IF (NX.EQ.I) GOTO 35
     ITEMP=LP(NX)
     LP(NX)=LP(I)
     LP(I)=ITEMP
     LPIS=MR*NX-MRS
     DO  K=ICS,ICF
        TEMP=A(K)
        A(K)=A(LPIS)
        A(LPIS)=TEMP
        LPIS=LPIS+1
     enddo
     DT=-DT
35   DT=DT*A(II)
     IF (ZMCH -ABS(A(II))) 45,45,220
45   DO J=IIAD,MTT,MR
        A(J)=A(J)/A(II)
     enddo
     IF (I.EQ.1) GOTO 70
     IF (I.EQ.NR) GOTO 78
     DO M=IIA,ICF
        KX=M-ICS+1
        DO KY=ICS,IISB
           A(M)=A(M)-A(KX)*A(KY)
           KX=KX+MR
        enddo
     enddo
70   CONTINUE
  enddo
78 NRTAD=MTX+NR
  DO  I=1,NRS
     IREV=NRTAD-I
     KRS=IREV-MR*I
     DO  IRCNT=IREV,MTT,MR
        KCS=IRCNT+1
        DO K=KRS,MTR,MR
           A(IRCNT)=A(IRCNT)-A(KCS)*A(K)
           KCS=KCS+1
        enddo
     enddo
  enddo
  DO I=1,NRS
9    IF (LP(I).EQ.I) GOTO 6
     NX=LP(I)
     LP(I)=LP(NX)
     LP(NX)=NX
     IXS=MTX+I
     IY=MTX+NX
     DO IX=IXS,MTT,MR
        TEMP=A(IX)
        A(IX)=A(IY)
        A(IY)=TEMP
        IY=IY+MR
     enddo
     GOTO 9
6    CONTINUE
  enddo
  RETURN
210 IERR=2
  NTC=NR
  DT=9E+10
  RETURN
220 IERR=1
  RETURN
END SUBROUTINE CROUT

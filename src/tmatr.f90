SUBROUTINE TMATR (*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_POT
  Use Common_UPU
  Use Common_MATEL
  Use Common_P
  Use Common_ATMAT
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  Use Include_FT
  !
  !	Calculate the t-matrix
  !
  !	Arrays in common /ATMAT/
  !
  !	CAP (IPARNPOINTS,2)              Central atom phaseshifts (complex)
  !	TMAT (LMAX+1,IPARNP,IPARNPOINTS) T-matrix (complex)
  !
  DIMENSION WT(2),ISTATUS(IPARNP)
  COMPLEX CTEMP,PH1,PH2,PSTEP
  IF (DEBUG) WRITE (LOGFILE,'(A)') 'Calculating TMATRIX:'
  IF (NPS.GT.1) THEN
     IG=1
     DO I=2,NPS
        IF (IATOM(I).GT.1) THEN
           IF (LASTHOLE(I).NE.-99) IG=IG+1
           IATGROUP(I)=IG
        ENDIF
     ENDDO
  ENDIF
  RENORM=1.
  IF (NSITES.GT.0) THEN
     RENORM=0.
     TCLUS=0.
     DO  I=1,MAXCLUS
        IF (KEXP(I).NE.IEXP) cycle
        IQA=IATOM(IT(IRAD(I)))
        IZD=IZED(KEXP(I))
        IF (IQA.GE.0.) THEN
           IF (IQA.EQ.IZD) THEN
	      RENORM=RENORM+1.
	      TCLUS=TCLUS+1.
           ENDIF
        ELSE
           IF (ITYPEA(-IQA).EQ.IZD) THEN
	      RENORM=RENORM+PERCA(-IQA)
	      TCLUS=TCLUS+1.
           ELSEIF (ITYPEB(-IQA).EQ.IZD) THEN
	      RENORM=RENORM+PERCB(-IQA)
	      TCLUS=TCLUS+1.
           ELSEIF (ITYPEC(-IQA).EQ.IZD) THEN
	      RENORM=RENORM+PERCC(-IQA)
	      TCLUS=TCLUS+1.
           ENDIF
        ENDIF
     enddo
     RENORM1=1./RENORM
     RENORM=TCLUS/RENORM
     IF (DEBUG) Write (6,*) 'iexp,renorm',iexp,renorm
     DO I=1,NSITES
        JTYPE(I,1)=0.
        JTYPE(I,2)=0.
        JTYPE(I,3)=0.
        IGOTONE=0
        JEXP=1
        DO J=1,IPARNP
           IF (J.NE.1.AND.LASTHOLE(J).NE.-99) JEXP=JEXP+1
           IF (IATOM(J).NE.0) THEN
              IF((LASTHOLE(J).EQ.-99.AND.JEXP.EQ.IEXP).OR.IGOTONE.EQ.0)THEN
                 IF (ITYPEA(I).EQ.IATOM(J)) JTYPE(I,1)=J
                 IF (ITYPEB(I).EQ.IATOM(J)) JTYPE(I,2)=J
                 IF (ITYPEC(I).EQ.IATOM(J)) JTYPE(I,3)=J
                 IF (LASTHOLE(J).NE.-99) IGOTONE=1
              ENDIF
           ENDIF
        ENDDO
     ENDDO
  ENDIF
  CALL IUZERO (ISTATUS,IPARNP)
  ISTATUS(1)=1
  DO  I=0,NS
     KI=IT(I)
     IF (NPH(KI).LT.2.AND.IATOM(KI).GE.0) THEN
        CALL WTEXTI ('Phaseshifts undefined for atom type ',KI)
        RETURN 1
     ENDIF
     IF (IATOM(KI).GE.0) ISTATUS(KI)=4
  enddo
  IF (NSITES.GT.0) THEN
     DO I=1,NSITES
        DO K=1,3
           IF (JTYPE(I,K).GT.0) ISTATUS(JTYPE(I,K))=4
        ENDDO
     ENDDO
  ENDIF
  CALL CZERO (TMAT,IPARNPOINTS*(IPARLMAX+1)*MIN(NPS+1,IPARNP)*2)
  IF(iset(IATABS).EQ.1.OR.(iset(NFIN).EQ.0.AND.LINITIAL(IEXP).NE.0))THEN
     IF (NME.LT.3) CALL ERRMSG ('No atomic absorption available',*270)
     !	  WRITE (6,*) 'Calculating ratios'
     KE=1
     NPHC=NME-1
     XEX=XE2(1)+EF02(0)+EF02(IEXP)
     DO  IE=JJS,JJF
        IEP=IE-JJS+1
        EE=ENER(IE)+XEX
29      IF (XME(KE+1).GE.EE) GOTO 39
        KE=KE+1
        IF (KE.LE.NPHC) GOTO 29
        KE=KE-1
39      IF (iset(IEXTYP).EQ.9) THEN
           IF (KE.LT.3) KE=3
           IF (KE.GT.NPHC-1) KE=NPHC-1
        ENDIF
        KE1=KE+1
        ES=1./(XME(KE1)-XME(KE))
        F=(EE-XME(KE))*ES
        DO IFINAL=1,2
           IF (iset(IEXTYP).EQ.9) THEN
              CALL LARGIT (RME(KE-2,IFINAL),XME(KE-2),EE,5)
           ELSE
              RH1=RME(KE1,IFINAL)
              RH2=RME(KE,IFINAL)
              RSTEP=RH1-RH2
              TEMP=RME(KE,IFINAL)+F*(RME(KE1,IFINAL)-RME(KE,IFINAL))
           ENDIF
           ATABS(IEP,IFINAL)=TEMP
           !	  if (ie.le.3) write (6,*) ifinal,ie,temp
        ENDDO
        IF (iset(NFIN).EQ.0.AND.LINITIAL(IEXP).NE.0) THEN
           !	    WT(1)=(LINITIAL(IEXP)-1)*2+1
           !	    WT(2)=(LINITIAL(IEXP)+1)*2+1
           WT(1)=1.
           WT(2)=2.
           SUM=ATABS(IEP,1)*WT(1)+ATABS(IEP,2)*WT(2)
           DO IFINAL=1,2
              IF (SUM.NE.0.) THEN
                 RATIO(IEP,IFINAL)=ATABS(IEP,IFINAL)*WT(IFINAL)/SUM
              ELSE
                 RATIO(IEP,IFINAL)=0.
              ENDIF
           ENDDO
        ELSE
           RATIO(IEP,1)=0.
           RATIO(IEP,2)=1.
        ENDIF
     enddo
  ELSE
     DO IE=1,JPT
        RATIO(IE,1)=0.
        RATIO(IE,2)=1.
     ENDDO
  ENDIF
  !
  !	Loop through atom types
  !
  DO JATOM=1,NPS
     IF (DEBUG) WRITE (7,'(A,2I3)') 'Jatom:',JATOM,ISTATUS(JATOM)
     IF (ISTATUS(JATOM).EQ.0) cycle
     LMN=0
     LMX=LMAX
     !	IF (ISTATUS(JATOM).LT.4) THEN
     !	  LMX=LINITIAL(IEXP)+1
     !	  LMN=LINITIAL(IEXP)+1
     !	ENDIF
     KE=1
     NPHC=NPH(JATOM)-1
     XEX=XE2(JATOM)+EF02(0)+EF02(IEXP)
     VPM=2.*(VPI02(0)+VPI02(IEXP))
     DO  IE=JJS,JJF
	IEP=IE-JJS+1
	EE=ENER(IE)+XEX
30	IF (EN(KE+1,JATOM).GE.EE) GOTO 40
	KE=KE+1
	IF (KE.LE.NPHC) GOTO 30
	KE=KE-1
40	IF (iset(IEXTYP).EQ.9) THEN
           IF (KE.LT.3) KE=3
           IF (KE.GT.NPHC-1) KE=NPHC-1
	ENDIF
	KE1=KE+1
	ES=1.0/(EN(KE1,JATOM)-EN(KE,JATOM))
	F=(EE-EN(KE,JATOM))*ES
        !
        !	Loop through 'L' values
        !
	DO  L=LMN,LMX
           LL=L+1
           IF (iset(IEXTYP).EQ.9) THEN
              CALL CLARGIT (PHS(KE-2,JATOM,LL),EN(KE-2,JATOM),EE,5,CTEMP)
           ELSE
              PH1=PHS(KE1,JATOM,LL)
              IF (L.GT.LINITIAL(IEXP)+1.AND.REAL(PH1).LT.0.0001) GOTO 60
              PH2=PHS(KE,JATOM,LL)
              PSTEP=PH1-PH2
              CTEMP=PH2+F*PSTEP
           ENDIF
           PHR=MIN(-2.*AIMAG(CTEMP),99.)
           PHIM=2.*REAL(CTEMP)
           XXS=EXP(PHR)
           XXC=XXS*COS(PHIM)
           XXS=XXS*SIN(PHIM)
           XXR=.5*(XXC-1.0)
           XXI=.5*XXS
           TMAT(IEP,L,JATOM)=CMPLX(XXR,XXI)
           !	IF (ISPARE8.NE.-1) THEN
           !	  IF (L.NE.ISPARE8) TMAT(IEP,L,JATOM)=0
           !	ENDIF
           !	IF (ISPARE11.EQ.98) TMAT(IEP,L,JATOM)=1.
           IF (DEBUG.AND.IE.EQ.1) WRITE (7,'(A,2I3)') 'L,LINITIAL(IEXP)+1',L,LINITIAL(IEXP)+1
           IF (L.EQ.LINITIAL(IEXP)-1.OR.L.EQ.LINITIAL(IEXP)+1) THEN
              DO JCLUS=1,MAXCLUS
                 IF (KEXP(JCLUS).NE.IEXP) cycle
                 ITRQ=ITAD(JCLUS)
                 !	IF (IEP.EQ.1) WRITE (6,*) 'IEXP,JCLUS,ITRQ',IEXP,JCLUS,ITRQ
                 ITRQ1=IT(IRAD(JCLUS))
                 IF (JATOM.EQ.ITRQ) THEN
                    !
                    !	Why do I need all this RITRQ stuff ?
                    !
                    RITRQ=RENORM
                    IZD=IZED(KEXP(JCLUS))
                    ITAT=IATOM(ITRQ1)
                    IF (ITAT.LT.0) THEN
                       IF (ITYPEA(-ITAT).EQ.IZD) THEN
                          RITRQ=PERCA(-ITAT)*RENORM
                       ELSEIF (ITYPEB(-ITAT).EQ.IZD) THEN
                          RITRQ=PERCB(-ITAT)*RENORM
                       ELSEIF (ITYPEC(-ITAT).EQ.IZD) THEN
                          RITRQ=PERCC(-ITAT)*RENORM
                       ENDIF
                    ENDIF
                    IF (L.EQ.LINITIAL(IEXP)-1) THEN
                       II=1
                    ELSE
                       II=2
                    ENDIF
                    FCAP(IEP+JJS-1)=PHIM
                    CAP(IEP,JCLUS,II)=CMPLX(XXC,XXS)*RITRQ
                    !	    IF (ISPARE11.EQ.98) CAP(IEP,JCLUS)=1.
                 ENDIF
              enddo
           ENDIF
           EREFP(IEP,JATOM)=EREF(KE,JATOM)+F*(EREF(KE1,JATOM)-EREF(KE,JATOM))-XE2(JATOM)
           IF (JATOM.EQ.ITRQ) THEN
              DO JEXP=IEXP,IEXP+JSPEC-1
                 TT=ENER(IE)+EF02(0)+EF02(JEXP)-EREFP(IEP,JATOM)
                 IF (TT.GT.1.E-10) THEN
                    RK(IE,JEXP)=SQRT(2.*TT)
                    TRK(IE,JEXP)=RK(IE,JEXP)
                 ELSE
                    RK(IE,JEXP)=1.E-5
                    IF (TT.EQ.0.) THEN
                       TRK(IE,JEXP)=0.
                    ELSEIF (TT.GT.0.) THEN
                       TRK(IE,JEXP)=SQRT(2.)*1.E-5
                    ELSE
                       TRK(IE,JEXP)=-SQRT(-2.*TT)
                    ENDIF
                 ENDIF
              ENDDO
           ENDIF
        enddo
        !
        !	End of energy points loop
        !
        L=LMX+1
60      LMAXVALS(IEP,JATOM)=L-1
     enddo
     IF (L-1.LT.IPARLMAX) THEN
        DO I=L,IPARLMAX
           TMAT(IEP,L,JATOM)=0.
        ENDDO
     ENDIF
     !	WRITE (7,*) 'LMV:',ENER(IE)/EC,LMAXVALS(IEP,JATOM)
     !
     !	End of atom types loop
     !
  enddo
  DO I=1,NPS
     IF (DEBUG) WRITE (7,'(A,4G12.3)') 'tm:',TMAT(1,0,I),TMAT(1,1,I)
  ENDDO
  IF (NSITES.LE.0) RETURN
  DO  NPS1=1,IPARNP
     J=IATOM(NPS1)
     IF (J.GE.0) cycle
     DO IEP=1,NPT(IEXP)
        DO L=0,LMAX
           TMAT(IEP,L,NPS1)=0.
        ENDDO
        EREFP(IEP,NPS1)=0.
        LMAXVALS(IEP,NPS1)=0
        K=JTYPE(-J,1)
        IF (K.GT.0.AND.PERCA(-J).GT.0) THEN
           IF (IEP.EQ.1.AND.DEBUG) WRITE (6,*) 'Adding ',PERCA(-J),' of ',K,' to TMAT ',NPS1
           DO L=0,LMAX
              TMAT(IEP,L,NPS1)=TMAT(IEP,L,NPS1)+TMAT(IEP,L,K)*PERCA(-J)
           ENDDO
           EREFP(IEP,NPS1)=EREFP(IEP,NPS1)+EREFP(IEP,K)*PERCA(-J)*RENORM1
           LMAXVALS(IEP,NPS1)=MAX(LMAXVALS(IEP,NPS1),LMAXVALS(IEP,K))
        ENDIF
        K=JTYPE(-J,2)
        IF (K.GT.0.AND.PERCB(-J).GT.0.) THEN
           DO L=0,LMAX
              TMAT(IEP,L,NPS1)=TMAT(IEP,L,NPS1)+TMAT(IEP,L,K)*PERCB(-J)
           ENDDO
           EREFP(IEP,NPS1)=EREFP(IEP,NPS1)+EREFP(IEP,K)*PERCB(-J)*RENORM1
           LMAXVALS(IEP,NPS1)=MAX(LMAXVALS(IEP,NPS1),LMAXVALS(IEP,K))
        ENDIF
        K=JTYPE(-J,3)
        IF (K.GT.0.AND.PERCC(-J).GT.0.) THEN
           DO L=0,LMAX
              TMAT(IEP,L,NPS1)=TMAT(IEP,L,NPS1)+TMAT(IEP,L,K)*PERCC(-J)
           ENDDO
           EREFP(IEP,NPS1)=EREFP(IEP,NPS1)+EREFP(IEP,K)*PERCC(-J)*RENORM1
           LMAXVALS(IEP,NPS1)=MAX(LMAXVALS(IEP,NPS1),LMAXVALS(IEP,K))
        ENDIF
     ENDDO
  enddo
  RETURN
270 RETURN
END SUBROUTINE TMATR

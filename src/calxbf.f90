SUBROUTINE CALXBF (Z,CCOSZ,CSINZ,L,BJ,BN)
  !======================================================================C
  Use Definition
  !       
  !	Spherical Bessel (BJ) and Neumann (BN) Functions for Angular
  !	Momentum L and (complex) Argument Z.
  !	CCOSZ = CCOS(Z)/Z
  !	CSINZ = CSIN(Z)/Z
  !	Function ABSQ(CX) returns CX**2 (CX complex)
  !       
  COMPLEX Z,BJ,BN, BF,BF1,BF2,BFP,BFS,BFSX,BFLX,CCOSZ,CSINZ
  !       
  !	First the L=0 case
  !       
  IF (L.EQ.0) THEN
     BJ= CSINZ
     BN=-CCOSZ
     RETURN
  ENDIF
  !       
  !	L > 0
  !       
  IF (real(L**2,kind=dp)*1.21.LE.CABS(Z)**2) THEN
     !       
     !	High kr algorithm
     !       
     BF2=CSINZ
     IF (Z.EQ.0.) WRITE (6,*) 'Z=0'
     BF1=BF2/Z-CCOSZ
     BFJ=3.0
     IF (L.GT.1) THEN
        DO J=2,L
           BFS=BF1
           BF1=BFJ*BF1/Z-BF2
           BF2=BFS
           BFJ=BFJ+2.0
        enddo
     ENDIF
     BJ=BF1
  ELSE
     !       
     !	Low kr algorithm
     !       
     BFP=1.
     BF=0.
     IBFLL=2*L+3
     IBFSR=1
     BFSX=0.5*Z*Z
20   BF=BF+BFP
     BFL=IBFSR*IBFLL
     IF (BFL.EQ.0.) WRITE (6,*) 'bfl=0'
     BFP=-BFP*BFSX/BFL
     IBFSR=IBFSR+1
     IBFLL=IBFLL+2
     !-st  IF (ABSQ(BFP).GT.ABSQ(BF)*1.0E-12) GOTO 20
     IF (CABS(BFP)**2.GT.(CABS(BF)**2)*1.0E-12) GOTO 20
     BFLX=1.
     DO  J=1,L
        BFLX=Z*BFLX
     enddo
     BFZ=1
     BFY=2*L+1
     JBBMAX=BFY+1
     DO JBB=1,JBBMAX,2
        BFZ=BFZ*BFY
        BFY=BFY-2
     enddo
     IF (BFZ.EQ.0.) WRITE (6,*) 'BFZ',BFZ
     BJ=BFLX*BF/BFZ
  ENDIF
  !       
  !	Calculate BN
  !       
  BF2=-CCOSZ
  BF1=BF2/Z-CSINZ
  BFJ=3.0
  IF (L.GT.1) THEN
     DO J=2,L
        BFS=BF1
        BF1=BFJ*BF1/Z-BF2
        BF2=BFS
        BFJ=BFJ+2.0
     enddo
  ENDIF
  BN=BF1
  RETURN
END SUBROUTINE CALXBF

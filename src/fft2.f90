SUBROUTINE FFT2 (SPECT,JJMIN,JJMAX,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_TL
  Use Common_F
  Use Common_PMTCH
  Use Common_PMTX
  Use Common_ICF
  Use Common_PRDSN
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  Use Include_FT

  !      *** INCLUDES CALLS TO GHOST LIBRABRY ROUTINES ***

  DIMENSION FFA(2048),FFI(2048),TABLE(513),VMM(2,10),RKI(500)
  dimension F1(500),F2(500),FFA1(2048),FFA2(2048),EVI(500),FFTEMP(2048)
  DIMENSION SPECT(*)
  real(float) STEP
  CHARACTER*12 K2,K3,K4
  CALL COSQT (TABLE)
  !
  !	MNM is number of points in output spectrum and also determines
  !	the rspace maximum. Derived from input argument in (0,1,2 or 3)
  !
  NPN=300
  IEXP=1
  IFTW=IFTSET(2)
  CALL UZERO (FFA,2048)
  CALL UZERO (FFI,2048)
  !
  !	Interpolate onto regular k-grid using equal area interpolation.
  !	This method ensures noise does not generate spurious components
  !	although the curve generated is not particularly smooth.
  !
  DO I=1,NPT(IEXP)
     FFA(I)=SPECT(I)*COS(FPIP(JS(IEXP)+I-1,IEXP))
     DO J=1,IFTW
	FFA(I)=FFA(I)*RK(JS(IEXP)+I-1,IEXP)
     ENDDO
  ENDDO
  STEP=(RK(JF(IEXP),IEXP)-RK(JS(IEXP),IEXP))/real(NPN-1,kind=dp)
  RKI(1)=RK(JS(IEXP),IEXP)
  DO I=2,NPN
     RKI(I)=RKI(1)+(I-1)*STEP
  ENDDO
  CALL SPLINT1 (NPT(IEXP),RK(JS(IEXP),IEXP),FFA,NPN,RKI,0,.FALSE.,*80)
  CALL UZERO (FFA(NPN+1),2048-NPN)
10 DO I=1,NPT(IEXP)
     FFI(I)=SPECT(I)*SIN(FPIP(JS(IEXP)+I-1,IEXP))
     DO J=1,IFTW
	FFI(I)=FFI(I)*RK(JS(IEXP)+I-1,IEXP)
     ENDDO
  ENDDO
  CALL SPLINT1 (NPT(IEXP),RK(JS(IEXP),IEXP),FFI,NPN,RKI,0,.FALSE.,*80)
  CALL UZERO (FFI(NPN+1),2048-NPN)
  !
  !	Find plotting limits and save for final spectrum
  !
20 RQ(2)=0
  DO I=1,NPN
     RQ(2)=AMAX1(RQ(2),ABS(FFA(I)))
  enddo
  IF (RQ(2).EQ.0.) RQ(2)=1.
  RQ(1)=-RQ(2)
  JJ=JJ+1
  IF (JJ.GT.JJMAX) THEN
     IF (IGF(2).EQ.1) CALL CREAD (K2,K3,K4,INT,IC,V,*40,*80)
40   CALL GRINIT
     JJ=JJMIN
  ENDIF
  !
  !	Transform to R-space
  !
  CALL FFT (FFA,FFI,TABLE,-1)
  !
  !	Determine RSTEP by Nyquist relation
  !
  RSTEP=PI/((RKI(2)-RKI(1))*2048.)
  !
  !	Construct grid in R-space for plots
  !
  F1(1)=0.
  DO I=2,NPN
     F1(I)=F1(I-1)+RSTEP
  enddo
  !
  !	Calc modulus for plotting
  !
  DO I=1,NPN
     F2(I)=FFA(I)*FFA(I)+FFI(I)*FFI(I)
     F2(I)=SQRT(F2(I))
  enddo
  !
  !	Calc FT plotting limits
  !
  FRAN=0.
  DO I=1,NPN
     FRAN=AMAX1(FRAN,F2(I))
  enddo
  RQ(1)=0
  RQ(2)=FRAN
  PLOTTITLE=ITL(5)
  CALL PLT (F1,F2,1,NPN,1,*80)
  RETURN
80 RETURN 1
END SUBROUTINE FFT2

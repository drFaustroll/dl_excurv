SUBROUTINE CARTOPOL (X,Y,Z,R,THETA,PHI)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_CONVIA
  Use Common_B1
  Use Common_B2
  real(float)  :: R2,R1,XOR,ZOR
  IENT=0
  GOTO 10
  ENTRY CARTOPOLD (X,Y,Z,R,THETA,PHI)
  IENT=1
10 R1=X*X+Y*Y
  IF (ABS(R1).GT.1.E-6_float) THEN
     R2=SQRT(DBLE(R1+Z*Z))
     R1=SQRT(R1)
  ELSE
     R1=0.
     R2=ABS(Z)
  ENDIF
  R=R2
  IF (R2.EQ.0.) THEN
     THETA=0.
     PHI=0.
  ELSE
     ZOR=Z/R2
     IF (ABS(ZOR).GT.1.) THEN
        IF (ABS(ZOR).GT.1.001) CALL WTEXT ('In CARTOPOL, Z/R > 1.')
        ZOR=SIGN(1._float,ZOR)
     ENDIF
     IF (IENT.EQ.0) THEN
        THETA=ACOS(ZOR)
     ELSE
        THETA=ACOS(ZOR)*(180./PI)
     ENDIF
     IF (ABS(R1).LT.1.E-6_float) THEN
        PHI=0.
     ELSEIF (ABS(Y).LT.1.E-6) THEN
        IF (X.GE.0.) THEN
           PHI=0.
        ELSE
           IF (IENT.EQ.0) THEN
              PHI=PI
           ELSE
              PHI=180.
           ENDIF
        ENDIF
     ELSE
        XOR=X/R1
        IF (ABS(XOR).GT.1.) THEN
           IF (ABS(XOR).GT.1.001) CALL WTEXT ('In CARTOPOL, X/R > 1.')
           XOR=SIGN(1._float,XOR)
        ENDIF
        IF (IENT.EQ.0) THEN
           PHI=ACOS(XOR)
        ELSE
           PHI=ACOS(XOR)*(180./PI)
        ENDIF
        IF (Y.LT.0.) THEN
           IF (IENT.EQ.0) THEN
              PHI=PI2-PHI
           ELSE
              PHI=360.-PHI
           ENDIF
        ENDIF
     ENDIF
  ENDIF
  !	IF (DEBUG) WRITE (LOGFILE,'(A,4F10.5,2F10.4)') 'CARTOPOL:',X,Y,Z,R,THETA,PHI
  RETURN
END SUBROUTINE CARTOPOL

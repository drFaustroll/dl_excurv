SUBROUTINE FNAME (TEXT,DSN,LD,*)
  !======================================================================C
  !
  !	Checks the syntax of a file name.
  !
  !	If required the subroutine may supply default parts of the file name
  !
  !	   TEXT (I)   CHARACTER SUBSTRING HOLDING FILE NAME AS READ IN
  !	    DSN (O)   RETURNED FILE NAME TO BE USED BY THE
  !	              FILOPN SUBROUTINE TO OPEN THE FILE (TYPE=CHARACTER)
  !	              MAX OF 60 CHARACTERS ALLOWED.
  !	     LD (O)   NUMBER OF CHARACTERS IN DSN UP TO THE LAST NON-BLANK
  !	              CHARACTER. IF LD=0 THEN DSN=' '.
  !
  CHARACTER*(*) TEXT,DSN
  CHARACTER*60 RDSN
  SAVE RDSN
  LD=0
  !
  !	Extract the file name
  !
  DSN=' '
  IL=0
  CALL NEXFLD(TEXT,IL,M1,M2,*30)
  DSN=TEXT(M1:M2)
  !
  !	See if extension only given, if so form file name using RDSN
  !
  IF (M2.GT.M1.AND.TEXT(M1:M1).EQ.'.') THEN
     IF (TEXT(M1+1:M1+1).NE.'.'.AND.TEXT(M1+1:M1+1).NE.'/') THEN
        IF (RDSN.EQ.' ') GOTO 40
        N=NCSTR(RDSN)
        IF (INDEX(RDSN,'.').GT.0) THEN
           DO N=60,1,-1
	      IF (RDSN(N:N).EQ.'.') GOTO 20
           enddo
20         N=N-1
        ENDIF
        DSN=RDSN(1:N)
        DSN(N+1:)=TEXT(M1:M2)
        GOTO 30
     ENDIF
  ENDIF
  !
  !	System dependent code for UNIX machines
  !
  !       See if file name is of special unix form needing to be expanded
  !
  IF (TEXT(M1:M1).EQ.'.'.OR.TEXT(M1:M1).EQ.'~') THEN
     CALL FILEXT (TEXT(M1:M2),DSN,*40)
     IL=0
     CALL NEXFLD(DSN,IL,M1,M2,*40)
  ENDIF
30 RDSN=DSN
  LD=NCSTR(DSN)
  RETURN
  !
  !	Error conditions
  !
40 CALL ERRMSG ('No default name set up',*50)
50 LD=0
  DSN=' '
  RDSN=' '
  RETURN 1
END SUBROUTINE FNAME

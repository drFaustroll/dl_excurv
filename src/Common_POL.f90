Module Common_POL
  !      COMMON /POL/ CCANG1(-2:2,-
  !     13:3,IPARNSP),CCANG2(-3:3,-2:2,IPARNSP)
  !      COMPLEX CCANG1,CCANG2
  Use definition
  Use Parameters
  implicit none
  private
  complex(imag), dimension(:,:,:), public :: CCANG1(-2:2,-3:3,IPARNSP)
  complex(imag), dimension(:,:,:), public :: CCANG2(-3:3,-2:2,IPARNSP)
end Module Common_POL

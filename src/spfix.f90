SUBROUTINE SPFIX
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_PATHS

  DIMENSION  TPATHINDEX(IPARPATH),ITPATHINDS(5,IPARPATH)
  dimension TPATHPL(IPARPATH),TPATHANG(IPARPATH),ITPATHOCC(IPARPATH)
  dimension TPATHMAX(IPARPATH),TSPECTRA(IPARPATH)
  CHARACTER*6 LABEL
  IF (NUMPATHS.LT.2) RETURN
  NUMP=MIN(NUMPATHS,IPARPATH)
  DO  I=2,NUMP
     IF (IPATHOCC(I).EQ.0) cycle
     DO  J=1,I-1
	IF (IPATHOCC(J).NE.0) THEN
           DO K=1,5
              IF (IPATHINDS(K,I).NE.IPATHINDS(K,J)) GOTO 10
           ENDDO
           GOTO 20
10         DO K=1,5
              IF (IPATHINDS(6-K,I).NE.IPATHINDS(K,J)) GOTO 30
           ENDDO
20         PATH=(PATHINDEX(I)-int(PATHINDEX(I)))*100.
           IPATH=NINT(PATH)
           PATH=(PATHINDEX(J)-int(PATHINDEX(J)))*100.
           JPATH=NINT(PATH)
           IF (IPATH.EQ.32) IPATH=31
           IF (IPATH.EQ.33) IPATH=31
           IF (IPATH.EQ.52) IPATH=51
           IF (JPATH.EQ.32) JPATH=31
           IF (JPATH.EQ.33) JPATH=31
           IF (JPATH.EQ.52) JPATH=51
           IF (IPATH.NE.JPATH) GOTO 30
           PDIFF=ABS(PATHPL(J)-PATHPL(I))
           IF (PDIFF.GT.1.E-3) GOTO 30
           PDIFF=ABS(PATHANG(J)-PATHANG(I))
           IF (PDIFF.GT.1.E-3) GOTO 30
           DO K=1,IPARNPOINTS
              SPECTRA (K,J,1)=SPECTRA(K,J,1)+SPECTRA(K,I,1)
              SPECTRA (K,I,1)=0.
           ENDDO
           PATHMAX(J,1)=PATHMAX(J,1)+PATHMAX(I,1)
           PATHMAX(I,1)=-1.
           PATHMAX(J,1)=PATHMAX(J,1)+PATHMAX(I,1)
           PATHMAX(I,1)=-1.
           IPATHOCC(J)=IPATHOCC(J)+IPATHOCC(I)
           IPATHOCC(I)=0
           GOTO 40
	ENDIF
30	CONTINUE
     enddo
40   CONTINUE
  enddo
  !
  !	Now compress spectra
  !
  CALL UCOPY (PATHINDEX,TPATHINDEX,IPARPATH*(IPARNPOINTS+11))
  JIND=0
  DO  I=1,NUMP
     IF (ITPATHOCC(I).NE.0) THEN
        JIND=JIND+1
        PATHINDEX(JIND)=TPATHINDEX(I)
        DO K=1,5
           IPATHINDS(K,JIND)=ITPATHINDS(K,I)
        ENDDO
        PATHPL(JIND)=TPATHPL(I)
        PATHANG(JIND)=TPATHANG(I)
        IPATHOCC(JIND)=ITPATHOCC(I)
        PATHMAX(JIND,1)=TPATHMAX(I)
        DO K=1,IPARNPOINTS
           SPECTRA(K,JIND,1)=TSPECTRA(I)
        ENDDO
     ENDIF
  enddo
  WRITE (LABEL,'(I6)') NUMPATHS
  CALL WTEXTI ('Number of paths reduced from'//LABEL//' to ',JIND)
  NUMPATHS=JIND
  CALL SORTSP
  RETURN
END SUBROUTINE SPFIX

SUBROUTINE ZSUM (ZS,QQ,OO,CEXAFS,IPART,LLMAX,QUIET)
  !======================================================================C
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Include_IPA
  Use Include_XY

  COMPLEX OO(IPART,7),QQ(7,IPART),ZS(-3:3,-3:3),CEXAFS(IPARNSP)
  LOGICAL QUIET
  !
  !	Calculate Z matrix from component product matrices QQ and OO
  !
  IJ1=0
  DO J1=-IEDGE,IEDGE
     IJ1=IJ1+1
     IJ2=0
     DO J2=-IEDGE,IEDGE
	IJ2=IJ2+1
	ZS(J1,J2)=0.
	I2=0
	DO L2=0,LLMAX
           DO M2=-L2,L2
              I2=I2+1
              ZS(J1,J2)=ZS(J1,J2)+QQ(IJ1,I2)*OO(I2,IJ2)
           enddo
        enddo
     enddo
  enddo
  !	DO IEXP=1,NSPEC
  DO JEXP=IEXP,IEXP+JSPEC-1
     CALL ZPOL (ZS,CEXAFS(JEXP),JEXP,QUIET)
     IF(.NOT.QUIET) WRITE (7,'(A,I3,4E10.4)')'ISING,CEXAFS',iset(ISING),CEXAFS(JEXP),ZS(0,0)
  ENDDO
  RETURN
END SUBROUTINE ZSUM

SUBROUTINE CALCRL (BETA,LMAXIN,MUMAX,RLM,EA,MEE,MEG,npt)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_FACTORIALS
  Use Common_ATMAT

  COMPLEX*16  :: RLM (-2:2,-2:2,0:IPARLMAX),EA(-2:2,-2:2)
  real(float) :: XLM(-IPARLMAX:IPARLMAX,-IPARLMAX:IPARLMAX,0:IPARLMAX)
  real(float) :: YUM,DUM,SUM,BETA2,SB2,CB2
  LOGICAL     :: FIRST,MEE,MEG
  SAVE XLM,FIRST
  DATA FIRST/.TRUE./
  IF (FIRST) THEN
     DO L=0,IPARLMAX
        DO M1=-2,2
           DO M2=-2,2
              XLM(M1,M2,L)=0
              IF (L+M1.LT.0.OR.L-M1.LT.0.OR.L+M2.LT.0.OR.L-M2.LT.0) GOTO 15
              XLM(M1,M2,L)=FAC(L+M1)*FAC(L-M1)*(FAC(L+M2)*FAC(L-M2))
              XLM(M1,M2,L)=SQRT(XLM(M1,M2,L))
15            CONTINUE
           enddo
        ENDDO
     ENDDO
     FIRST=.FALSE.
  ENDIF
  IF (DEBUG) WRITE (7,*) 'BETA',BETA/AC
  BETA2=BETA*.5
  SB2=SIN(BETA2)
  CB2=COS(BETA2)
  !	IF (SB2.EQ.0.) SB2=1.E-20
  !	IF (CB2.EQ.0.) CB2=1.E-20
  IF (ABS(CB2).LT.1.E-6) THEN
     CALL CDZERO (reshape(RLM,(/25 * (iparlmax+1)/)),25*(LMAXIN+1)*4)
     MEE=.TRUE.
     DO L=0,LMAXIN
        DO M1=-MUMAX,MUMAX
           IF (L+M1.GE.0.AND.L-M1.GE.0) THEN
              RLM(M1,-M1,L)=(-1)**(L+M1)
              !	    WRITE (7,'(a,3I3,2F9.6,2F9.3,E14.4)') 'rl',M1,-M1,L,RLM(M1,-M1,L),BETA/AC
           ENDIF
        ENDDO
     ENDDO
  ELSEIF (ABS(SB2).LT.1.E-6) THEN
     CALL CDZERO (reshape(RLM,(/25 * (iparlmax+1)/)),25*(LMAXIN+1)*4)
     MEG=.TRUE.
     DO L=0,LMAXIN
        DO M1=-MUMAX,MUMAX
           IF (L+M1.GE.0.AND.L-M1.GE.0) THEN
              RLM(M1,M1,L)=1.
              !	    WRITE (7,'(a,3I3,2F9.6,2F9.3,E14.4)') 'rl',M1,M1,L,RLM(M1,M1,L),BETA/AC
           ENDIF
        ENDDO
     ENDDO
  ELSE
     DO L=0,LMAXIN
        DO M1=-MUMAX,MUMAX
           DO M2=-MUMAX,MUMAX
              RLM(M1,M2,L)=0
              IF (L+M1.LT.0.OR.L-M1.LT.0.OR.L+M2.LT.0.OR.L-M2.LT.0) GOTO 10
              SUM=0
              DUM=0
              YUM=0
              DO I=0,LMAXIN*2
                 IF (M2-M1+I.LT.0.OR.L+M1-I.LT.0.OR.L-M2-I.LT.0) GOTO 20
                 DUM=(-1)**(M2-M1+I)*CB2**(2*L+M1-M2-2*I)*SB2**(M2-M1+2*I)
                 YUM=FAC(I)*FAC(M2-M1+I)*FAC(L+M1-I)*FAC(L-M2-I)
                 SUM=SUM+DUM/YUM
20               CONTINUE
              enddo
              RLM(M1,M2,L)=XLM(M1,M2,L)*SUM*EA(M1,M2)
              !	  WRITE (7,'(A,3I3,2F9.6,2F9.3,E14.4)') 'RL',M1,M2,L,RLM(M1,M2,L),BETA/AC,TMAT(NPT,L,2)
10            CONTINUE
           enddo
        ENDDO
     ENDDO
  ENDIF
  RETURN
END SUBROUTINE CALCRL

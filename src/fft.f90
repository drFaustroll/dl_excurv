SUBROUTINE FFT (X,Y,TABLE,ISN)
  !======================================================================C
  !
  !	FFT Is in place dft computation using Sande algorithm
  !	and Markel pruning modification.
  ! 	X  =  array of length 2**M used to hold real part of complex input
  !	Y  =  array lf length 2**M used to hold imaginary part of input
  !
  !	Table=array of length n/4+1 (n=2**m9 contains quarter length
  !	Cosine table.
  !	M=integer power of two which determines size of fft to be
  !	performed. (Bit reverse table is set for a max of n=2**12)
  !	LL=integer power of 2 which determines 2**LL actual data pts
  !	and thus no. of stages in which no pruning is allowable
  !	ISN=-1 for forward FFT
  !	ISN=+1 for reverse FFT
  !	X&Y are real and complex part of output
  !
  DIMENSION X(2048), Y(2048), TABLE(513), L(12)
  !  EQUIVALENCE  &
  !       (L12,L(1)),(L11,L(2)),(L10,L(3)),(L9,L(4)),(L8,L(5)),(L7,L(6)), &
  !       (L6,L(7)),(L5,L(8)),(L4,L(9)), (L3,L(10)), (L2,L(11))
  !-st above EQUIVALENCE is in the scope of only this subroutine
  INTEGER IPOW(12)
  REAL FN
  DATA IPOW/1,2,4,8,16,32,64,128,256,512,1024,2048/
  data N/2048/,M/11/,ND4/512/,ND4P1/513/,ND4P2/514/,ND2P2/1026/
  DATA FN/2048./
  DO  LO=1,M
     LMX=IPOW(12-LO)
     LIX=2*LMX
     ISCL=N/LIX
     DO  LM=1,LMX
	IARG=(LM-1)*ISCL+1
	IF (IARG.LE.ND4P1) GOTO 10
	K1=ND2P2-IARG
	C=-TABLE(K1)
	K3=IARG-ND4
	S=ISN*TABLE(K3)
	GOTO 20
10	C=TABLE(IARG)
	K2=ND4P2-IARG
	S=ISN*TABLE(K2)
20	DO  LI=LIX,N,LIX
           J1=LI-LIX+LM
           J2=J1+LMX
           TT1=X(J1)-X(J2)
           TT2=Y(J1)-Y(J2)
           X(J1)=X(J1)+X(J2)
           Y(J1)=Y(J1)+Y(J2)
           X(J2)=C*TT1-S*TT2
           Y(J2)=C*TT2+S*TT1
        enddo
     enddo
  enddo
  !
  !	Perform bit reversal
  !
  DO  J=1,11
     L(J)=IPOW(M+2-J)
  enddo
  L12=L(1)
  L11=L(2)
  L10=L(3)
  L9 =L(4)
  L8 =L(5)
  L7 =L(6)
  L6 =L(7)
  L5 =L(8)
  L4 =L(9)
  L3 =L(10)
  L2 =L(11)
  JN=1
  DO  J2=1,L2
     DO  J3=J2,L3,L2
	DO  J4=J3,L4,L3
           DO  J5=J4,L5,L4
              DO  J6=J5,L6,L5
                 DO  J7=J6,L7,L6
                    DO  J8=J7,L8,L7
                       DO  J9=J8,L9,L8
                          DO  J10=J9,L10,L9
                             DO  J11=J10,L11,L10
                                DO  JR=J11,L12,L11
                                   IF (JN-JR) 50,50,60
50                                 R=X(JN)
                                   X(JN)=X(JR)
                                   X(JR)=R
                                   FI=Y(JN)
                                   Y(JN)=Y(JR)
                                   Y(JR)=FI
60                                 IF (ISN) 80,80,70
70                                 X(JR)=X(JR)/FN
                                   Y(JR)=Y(JR)/FN
80                                 JN=JN+1
                                enddo
                             enddo
                          enddo
                       enddo
                    enddo
                 enddo
              enddo
           enddo
        enddo
     enddo
  enddo
  RETURN
END SUBROUTINE FFT

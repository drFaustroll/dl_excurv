SUBROUTINE ADDSHIFT (TITLE,XSHIFT)
  !======================================================================C
  Use Parameters
  !       
  !	Add an X-shift message to existing label TITLE
  !       
  CHARACTER :: TITLE*(*),CTEMP*5
  IF (XSHIFT.NE.0) THEN
     WRITE (CTEMP,'(F5.1)') XSHIFT
     TITLE(NCSTR(TITLE)+1:)=' X-shift='//CTEMP
  ENDIF
  RETURN
END SUBROUTINE ADDSHIFT

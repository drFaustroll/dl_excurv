SUBROUTINE READBASE (IDEN,ISHELL,IDEALIN,*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_MTR
  Use Common_VAR
  Use Common_UPU
  Use Common_COMPAR
  Use Common_ICF
  Use Index_INDS
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2

  REAL*8 CORRECT
  CHARACTER*(*) IDEN
  CHARACTER CN2*4,CN3*4,CN4*1,AT(99)*4
  character FILE*6
  LOGICAL IDEALISE,IDEALIN,LERR
  DIMENSION &
       X(99),Y(99),Z(99),ITYPE(99),IATOMTYPES(20),CN2(99),XX(99),YY(99),ZZ(99), &
       KTYPE(99),MAIN(5),RTOR(0:1),CORRECTS(-5:61),TORIDS(-5:61)
  DATA IATOMTYPES/0,6,7,8,16,0,26,15,12*0/
  !	DATA (X(I),I=1,4)/1.20134,0.,-1.25029,-2.18525/
  !	DATA (Y(I),I=1,4)/0.84658,0.,0.88107,0.66029/
  !	DATA (Z(I),I=1,4)/0.,0.,0.,-0.78409/
  DATA (X(I),I=1,4)/1.20134,0.,-1.25029,-2.18525/
  DATA (Y(I),I=1,4)/0.84658,0.,0.88107,0.66029/
  DATA (Z(I),I=1,4)/0.,0.,0.,-0.78409/
  DATA (AT(I),I=1,4)/'N','CA','C','O'/
  DATA (ITYPE(I),I=1,4)/2,1,1,3/
  call eqv_convia
  IF (IDEN.EQ.'?') WRITE (OUTTERM,170)
  FILE='ideals'
  CALL FFOPENQ (INFILE,FILE,3,1,*41)
  JRAD=IRAD(IABS(ICLUS(ISHELL)))
  IDEALISE=IDEALIN
  DO I=-5,61
     CORRECTS(I)=120.
     TORIDS(I)=180.
  ENDDO
  CORRECTS(4)=134.
  CORRECTS(9)=125.6
  CORRECTS(19)=128.
  TORIDS(19)=170.
  IF (IDEN(1:1).EQ.'$') THEN
     IDEALISE=.TRUE.
     IDEN=IDEN(2:)
  ENDIF
  IF (IDEALISE) THEN
     JUNIT=NU(ISHELL)
     IF (JUNIT.LE.0) RETURN 1
     CALL IUZERO (MAIN,4)
     DO I=1,NS
        IF (NU(I).EQ.JUNIT) THEN
           DO J=1,4
              IF (ATLABEL(I).EQ.AT(J)) MAIN(J)=I
           ENDDO
           IF (IDEN.EQ.'CO3') THEN
	      IF (ATLABEL(I).EQ.'O1') MAIN(5)=I
           ELSE
	      IF (ATLABEL(I).EQ.'CB') MAIN(5)=I
           ENDIF
        ENDIF
     ENDDO
     RTOR(0)=TORA(JUNIT)
     RTOR(1)=TORB(JUNIT)
  ELSE
     DO JUNIT=1,IPARNUNITS
        DO I=1,NS
           IF (I.NE.ISHELL.AND.UN(I).EQ.JUNIT) GOTO 10
        ENDDO
        GOTO 20
10      CONTINUE
     enddo
20   IF (NS.LT.ISHELL) NS=ISHELL
  ENDIF
  DO I=1,100000
     KSTART=1
     !	READ (INFILE,120,END=30) I1,I2,I3,CORRECT,TORID,IN1,IN2,CN3,CN4
     READ (INFILE,120,END=30) IN1,IN2,CN3,CN4
     IF (CN3.EQ.'END') GOTO 30
     !	CORRECT=CORRECT*AC
     IF (IDEN.EQ.'?') THEN
        IF (MOD(I-1,10).NE.9) THEN
           WRITE (6,140) CN3
        ELSE
           WRITE (6,150) CN3
        ENDIF
     ELSEIF (IDEN.EQ.CN3) THEN
        WRITE (6,160) CN3,JUNIT
     ENDIF
     RSQ=-1.
     K=4
     IF (CN3.EQ.'MAIN') K=0
     INDEX=1
     !	IF (I3.GT.0) THEN
     DO J=1,1000
        K=K+1
        READ (INFILE,130,END=30) X(K),Y(K),Z(K),ITYPE(K),IN1,IN2,AT(K)
        IF (IN1.NE.0) THEN
           K=K-1
           BACKSPACE INFILE
           GOTO 25
        ENDIF
        IF (CN3.EQ.IDEN.AND.AT(K)(1:2).EQ.'C') KSTART=5
        RR=X(K)*X(K)+Y(K)*Y(K)+Z(K)*Z(K)
        IF (RR.GT.RSQ.AND.ITYPE(K).NE.1) THEN
           RSQ=RR
           INDEX=K
        ENDIF
     ENDDO
     !	ENDIF
25   IF (CN3.EQ.IDEN) GOTO 30
  ENDDO
30 CLOSE (INFILE)
  CORRECT=CORRECTS(I)*AC
  TORID=TORIDS(I)
  IF (CN3.NE.IDEN) THEN
     IF (IDEN.EQ.'?') WRITE (6,'(//)')
40   RETURN 1
  ENDIF
  IF (IDEN.EQ.'CO3'.OR.IDEALISE) INDEX=KSTART+1
  IF (IDEN.EQ.'HEM') THEN
     INDEX=47
     KSTART=5
  ENDIF
  KMAX=1
  XX(1)=0.
  YY(1)=0.
  ZZ(1)=0.
  KTYPE(1)=ITYPE(INDEX)
  CN2(1)=AT(INDEX)
  DO  I=KSTART,K
     IF (I.EQ.INDEX) cycle
     KMAX=KMAX+1
     XX(KMAX)=X(INDEX)-X(I)
     YY(KMAX)=Y(INDEX)-Y(I)
     ZZ(KMAX)=Z(INDEX)-Z(I)
     KTYPE(KMAX)=ITYPE(I)
     CN2(KMAX)=AT(I)
  enddo
  I1=K-1
  I2=K
  IF (IDEN.EQ.'HIS') THEN
     I1=10
     I2=9
  ELSEIF (IDEN.EQ.'TYR') THEN
     I1=8
  ENDIF
  IF (I1.NE.0.AND.I2.NE.0.AND.IDEN.NE.'HEM') THEN
     IF (.NOT.IDEALISE) THEN
        !
        !	Align unit with X-Y plane
        !
        XN=YY(I1)*ZZ(I2)-ZZ(I1)*YY(I2)
        YN=ZZ(I1)*XX(I2)-XX(I1)*ZZ(I2)
        ZN=XX(I1)*YY(I2)-YY(I1)*XX(I2)
        CALL CALCAX (0.,0.,1.,XN,YN,ZN,X3,Y3,Z3,ANGL,ASIGN)
        DO I=1,KMAX
           CALL XROTATE (X3,Y3,Z3,-ANGL,XX(I),YY(I),ZZ(I))
        ENDDO
        X1=-1.
        Y1=0.
        Z1=0.
     ELSEIF (IDEALISE) THEN
        CORRECT=0.
        IF (IDEN.EQ.'CO3') THEN
           I1=2
           MAIN(2)=MAIN(3)
           CALL CTOR (JRAD,IPIV(JUNIT),IPLA(JUNIT),IPLB(JUNIT),TORID,TORID2)
        ELSE
           I1=5
           CALL CTOR (JRAD,MAIN(3),MAIN(2),MAIN(5),TORID,TORID2)
        ENDIF
        X1=UX(MAIN(5))-UX(MAIN(2))
        Y1=UY(MAIN(5))-UY(MAIN(2))
        Z1=UZ(MAIN(5))-UZ(MAIN(2))
     ENDIF
     CALL CALCAX (X1,Y1,Z1,XX(I1),YY(I1),ZZ(I1),XN,YN,ZN,ANGL2,ASIGN)
     IF (ASIGN.LT.0..AND..NOT.IDEALISE) THEN
        ANGL2=PI2-ANGL2
        XN=-XN
        YN=-YN
        ZN=-ZN
     ENDIF
     ANGL2=CORRECT-ANGL2
     DO I=1,KMAX
        CALL XROTATE (XN,YN,ZN,ANGL2,XX(I),YY(I),ZZ(I))
     ENDDO
  ENDIF
  IF (IDEALISE) THEN
     TX=UX(MAIN(2))
     TY=UY(MAIN(2))
     TZ=UZ(MAIN(2))
     DO I=1,KMAX
        DO J=1,4
           IF (CN2(I).EQ.AT(J)) GOTO 60
        ENDDO
        DO J=1,NS
           IF (NU(J).EQ.JUNIT.AND.ATLABEL(J).EQ.CN2(I)) THEN
              UX(J)=XX(I)+TX
              UY(J)=YY(I)+TY
              UZ(J)=ZZ(I)+TZ
              GOTO 60
           ENDIF
        ENDDO
60      CONTINUE
     enddo
  ELSE
     IF (KSTART.EQ.5) THEN
        DO I=1,4
           NTORA(I,JUNIT)=0
           NTORB(I,JUNIT)=0
        ENDDO
     ELSE
        NTORA(1,JUNIT)=ISHELL+1
        NTORA(2,JUNIT)=ISHELL+2
        NTORA(3,JUNIT)=ISHELL+5
        NTORA(4,JUNIT)=ISHELL+6
        DO I=2,4
           NTORB(I-1,JUNIT)=NTORA(I,JUNIT)
        ENDDO
        NTORB(4,JUNIT)=NTORA(4,JUNIT)+1
     ENDIF
     NS=NS-1
     RX=0.
     RY=+UZ(ISHELL)
     RZ=-UY(ISHELL)
     ANGLE=-CALCACOS (UX(ISHELL)/R(ISHELL),'angle',LERR)
     IF (IDEN.EQ.'HEM') THEN
        R(ISHELL)=0.
        ANGLE=0.
        RN(ISHELL)=1.
     ENDIF
     DO I=1,KMAX
        IF (KTYPE(I).EQ.6) cycle
        NS=NS+1
        UX(NS)=XX(I)+R(ISHELL)
        UY(NS)=YY(I)
        UZ(NS)=ZZ(I)
        CALL XROTATE (RX,RY,RZ,ANGLE,UX(NS),UY(NS),UZ(NS))
        ATLABEL(NS)=CN2(I)
        RN(NS)=RN(ISHELL)
        UN(NS)=JUNIT
        ITT=KTYPE(I)+1
        DO K=2,IPARNP
           IF (IATOM(K).EQ.IATOMTYPES(ITT)) THEN
              ITT=K
              GOTO 70
           ENDIF
        ENDDO
        ATOM(ITT)=IATOMTYPES(ITT)
        RMTR(ITT)=DEFMTR(IATOMTYPES(ITT))
70      CONTINUE
        T(NS)=ITT
     enddo
     RNS=NS
     PIV(JUNIT)=ISHELL
     UNAME(JUNIT)=IDEN
     PLA(JUNIT)=I1+ISHELL-1
     PLB(JUNIT)=I2+ISHELL-1
     IF (NS.LT.IPARNS) THEN
        DO I=NS+1,IPARNS
           RN(I)=0.
           UN(I)=0.
        ENDDO
     ENDIF
  ENDIF
  DO  I=1,IPARNPARAMS
     PA2(I)=PA1(I)*CONVIA(ICF(I))
     !	call all_IPA
     IPA(I)=PA2(I)
     !        call IPA_all
  enddo
  CALL UTAB (1)
  IF (TORID.NE.-999.) THEN
     IF (IDEALISE) THEN
        ISTART=1
        IMISS=MAIN(4)
        IMISS2=MAIN(1)
        IF (IDEN.EQ.'CO3') THEN
           JVECA=IPIV(JUNIT)
           JVECB=IPLA(JUNIT)
        ELSE
           JVECA=MAIN(3)
           JVECB=MAIN(2)
           CALL CTOR (JRAD,MAIN(3),MAIN(2),MAIN(5),TORC(JUNIT),TORC2(JUNIT))
        ENDIF
     ELSE
        JVECA=NR(JUNIT)
        JVECB=IPLA(JUNIT)
        ISTART=ISHELL
        IMISS=JRAD
        IMISS2=JRAD
     ENDIF
     IF (JVECA.GT.0.AND.JVECB.GT.0) THEN
        DO  I=ISTART,NS
           IF (NU(I).NE.JUNIT.OR.I.EQ.JRAD.OR.I.EQ.IMISS.OR.I.EQ.IMISS2) cycle
           CALL RROTATE (I,TORID-TORC(JUNIT),JVECA,JVECB)
        enddo
        CALL UTAB (1)
     ENDIF
  ENDIF
  IF (IDEALISE) THEN
     DO K=0,1
        IF (K.EQ.0) THEN
           JVECA=NTORA(2,JUNIT)
           JVECB=NTORA(3,JUNIT)
        ELSE
           JVECA=NTORB(2,JUNIT)
           JVECB=NTORB(3,JUNIT)
        ENDIF
        DO  I=1,NS
           IF (NU(I).NE.JUNIT.OR.I.EQ.JRAD) cycle
           DO J=1,4
              IF (ATLABEL(I).EQ.AT(J)) GOTO 110
           ENDDO
           CALL RROTATE (I,RTOR(K)-TORA(JUNIT+K*(IPARNS+1)),JVECA,JVECB)
110        CONTINUE
        enddo
     ENDDO
     CALL UTAB (1)
  ENDIF
  IF (.NOT.IDEALISE) THEN
     DO I=1,NS
        IF (NU(I).EQ.JUNIT) THEN
           IF (RAD(I).LT.RAD(ISHELL)+.01) THEN
	      A(I)=A(ISHELL)
           ELSE
	      A(I)=A(ISHELL)*1.5
           ENDIF
           A2(I)=A(I)*DWC
           CLUS(I)=CLUS(ISHELL)
           CLUS2(I)=CLUS2(ISHELL)
           ICLUS(I)=ICLUS(ISHELL)
        ENDIF
     ENDDO
  ENDIF
  FPOPT=T(ISHELL)
  RETURN
41 RETURN
  ! 120	FORMAT (2X,I2,2X,I2,I2,2F10.5,15X,2I5,10X,A4,A1)
120 FORMAT (45X,2I5,10X,A4,A1)
130 FORMAT (3F10.5,10X,3I5,20X,A4)
140 FORMAT (1X,A4,$)
150 FORMAT (1X,A4)
160 FORMAT (1X,A4,' (unit=',I2,')'/)
170 FORMAT (//'Options available are: '/)
END SUBROUTINE READBASE

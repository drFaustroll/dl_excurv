SUBROUTINE SWC (CKR,CQ,NUMAXIN,LMAXIN)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  COMPLEX CKR,CQ(0:IPARLMAX,0:IPARLMAX),CI,Z,TLM2(25)
  DIMENSION TLM1(25)
  !	LOGICAL QUIET
  DATA TLM1/1.,3.,5.,7.,9.,11.,13.,15.,17.,19.,21.,23.,25.,27.,29.,31.,33.,35.,37.,39.,41.,43.,45.,47.,49./
  CI=(0.,1.)
  Z=1./(CKR*CI)
  !	IF (DEBUG.AND..NOT.QUIET) WRITE (7,*) 'SWC for kr=',ckr,' z=',Z,'i/kr=',CI/CKR
  CQ(0,0)=1.
  CQ(1,0)=1.-Z
  DO L=2,LMAXIN
     TLM2(L)=TLM1(L)*Z
     CQ(L,0)=CQ(L-2,0)-TLM2(L)*CQ(L-1,0)
  ENDDO
  DO M=1,NUMAXIN
     DO L=0,1
	CQ(L,M)=0.
     ENDDO
  ENDDO
  CQ(1,1)=-Z
  DO M=1,NUMAXIN
     DO L=2,LMAXIN
	CQ(L,M)=CQ(L-2,M)-TLM2(L)*(CQ(L-1,M)+CQ(L-1,M-1))
     ENDDO
  ENDDO
  !	IF (DEBUG.AND..NOT.QUIET) THEN
  !	  WRITE (7,*) 'CQ(l,m)'
  !	  DO M=0,NUMAXIN
  !	  WRITE (7,'(7(F7.2,F6.2))') (CQ(L,M),L=0,MIN(6,LMAXIN))
  !	  ENDDO
  !	ENDIF
  RETURN
END SUBROUTINE SWC

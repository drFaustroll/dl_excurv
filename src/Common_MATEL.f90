MODULE Common_MATEL

  !      COMMON /MATEL/NME,MZ,LL0,LL1,XME(IPARPHPOINTS),RME(IPARPHPOINTS,2)
  !     1,ATABS(IPARNPOINTS,2),RATIO(IPARNPOINTS,2)

  Use Definition
  Use Parameters
  implicit none
  private
  integer(i4b), public :: NME,MZ,LL0,LL1
  real(dp), dimension(:),   public ::  XME(IPARPHPOINTS)
  real(dp), dimension(:,:), public ::  RME(IPARPHPOINTS,2)
  real(dp), dimension(:,:), public :: ATABS(IPARNPOINTS,2)
  real(dp), dimension(:,:), public :: RATIO(IPARNPOINTS,2)
end MODULE Common_MATEL

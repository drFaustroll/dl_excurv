SUBROUTINE CHDEN (PSQ,IZ,ION,INCD,JWS,IHC,IWM,GRID1,WSPACE1,WSPACE2,NENDS,XSTART,*)
  !======================================================================
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  real(float)   :: Y1,FNUM,Y77  ! double prescision in old code
  real(dp)      :: PSQ(*)
  real(dp)      :: WSPACE1(*)
  real(dp)      :: WSPACE2(*)
  real(dp)      :: grid1(*)
  real(dp)      :: XSTART
  integer(i4b)  :: NENDS(2)
  !       
  !	Generate radial grid for rho (log grid starts at -8.8 with step .05)
  !       
  NPOINT=251
  DO I=1,NPOINT
     RI=real(I-177,kind=dp)*.05
     WSPACE1(I)=EXP(RI)
  enddo
  CALL RDDEN (IZ,ION,IHC,IWM,WSPACE2,*80)
  !       
  !	Log grid for potentials starts at -8.80487526
  !       
  IF (DEBUG) WRITE (7,'(A,I3,6E12.5)') 'chden',INCD,(WSPACE2(I),I=1,251,50)
  IS=1
  DO K=1,IPARGRID
     DO I=IS,NPOINT
        IF (WSPACE1(I).GT.GRID1(K)) GOTO 30
     enddo
     I=NPOINT
30   IND=MIN0(MAX0(I-2,1),NPOINT-4)
     IND2=IND+4
     IS=I
     X=GRID1(K)
     Y1=0.
     Y77=0.
     DO  K1=IND,IND2
        FNUM=1.
        DO K2=IND,IND2
           IF (K1.EQ.K2) GOTO 40
           FNUM=FNUM*(X-WSPACE1(K2))/(WSPACE1(K1)-WSPACE1(K2))
40         CONTINUE
        enddo
        Y1=Y1+FNUM*WSPACE2(K1)
        !    Y77=Y77+FNUM*XCOL(K1)
     enddo
     IF (Y1.LT.0.) Y1=0.
     PSQ(K)=Y1
     !	  COL(K)=Y77
     !    IF (K.LE.JWS) GOTO 60
     IF (Y1.LT.-1.E10.OR.GRID1(K).GT.WSPACE1(NPOINT)) GOTO 70
60   CONTINUE
  enddo
  K=IPARGRID
70 NENDS(INCD)=K
  IF (K.LT.IPARGRID) THEN
     DO I=K+1,IPARGRID
        PSQ(I)=0.
     ENDDO
  ENDIF
  IF (DEBUG) WRITE (LOGFILE,'(A,2I5)') 'nends',NENDS
  RETURN
80 RETURN 1
END SUBROUTINE CHDEN

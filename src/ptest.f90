LOGICAL FUNCTION PTEST (P,RMSANG,IORD,I,IEXP)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PATHS
  Use Include_IPA
  Use Include_PA2
  !
  !	Test whether a specific MS path should be calculated
  !	P pathlength, RMSANG min. scattering angle (RMINANG2 is a bond
  !	angle), IORD order of scattering.
  !	Called only by CALCPATH
  !
  IF (FILTER) THEN
     PTEST=PATHFLAG(I)
     WRITE (7,*) 'FILT',I,PATHFLAG(I)
  ELSE
     PTEST=P.GE.PLMIN2.AND.P.LE.PLMAX2.AND.PI-RMSANG.GE.RMINANG2-.001.AND.IORD.GE.IOMIN.AND.IORD.LE.IOMAX
     IF (I.LE.IPARPATH) THEN
        IF (PTEST) PTEST=PTEST.AND.(PATHMAX(I,IEXP).EQ.0..OR.PATHMAX(I,IEXP).GE.RMINMAG2)
     ENDIF
  ENDIF
  RETURN
END FUNCTION PTEST

function erf_fun(X)
  Use Definition
  implicit none
  !***********************************************************************
  !   ERROR FUNCTION,   X = ARGUMENT
  !***********************************************************************
  real(dp) :: X,Z,T,POM,erf_fun
  Z=ABS(X)                                                          
  T=1.0_dp/(1.0_dp + 0.5_dp*Z)                                                   
  POM =T*EXP(-Z*Z - 1.26551223_dp + &
       T*( 1.00002368_dp+T*( 0.37409196_dp+T*( 0.09678418_dp + &
       T*(-0.18628806_dp+T*( 0.27886807_dp+T*(-1.13520398_dp + &
       T*( 1.48851587_dp+T*(-0.82215223_dp+T*0.17087277_dp)))))))))
  IF (X.LT.0.0_dp) POM=2.0_dp - POM                                           
  erf_fun=1.0_dp - POM                                                         
  RETURN                                                            
END function erf_fun

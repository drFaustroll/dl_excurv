SUBROUTINE NEWSTAT
  !======================================================================C
  Use Definition
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Common_SPARS
  Use Index_INDS
  Use Common_LSQ
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  !
  !	Perform statistical test for significance of shells as proposed
  !	by Joyner, Martin and Meehan, J.PHYS.C. 20 (1987) 4005.
  !
  !	    NSN (I)   CURRENT NUMBER OF SHELLS
  !	   NPTS (I)   CURRENT NUMBER OF POINTS
  !	  FSMIN (I)   CURRENT FIT INDEX
  !	   FIMP (I)   PREVIOUS FIT INDEX
  !
  CHARACTER*4 IP1,IP2,IP3
  DIMENSION FITIN(2),NSHL(2),NPTS(2),RFS(2),RCHISQU(2)
  CHARACTER*60 WSPACE
  CALL UCOPY (PA1,RJM,IPARNPARAMS)
  IF (NSAVE.GT.0.AND.LRIS.EQ.0) THEN
     CALL WTEXTI ('Number of parameter sets saved: ',NSAVE)
     DO I=1,NSAVE
        NI=0
        NJ=SPARS(10,I)
        DO J=1,NJ
           IF (SPARS(inds(INDN)+J-1,I).NE.0.) NI=NI+1
        ENDDO
        !	  WRITE (OUTTERM,130) I,ifix(SPARS(10,I)),SPARS(IPARNPARAMS+1,I),
        WRITE (OUTTERM,*) I,ifix(SPARS(10,I)),SPARS(IPARNPARAMS+1,I),SPARS(IPARNPARAMS+2,I),NI
     ENDDO
  ENDIF
  DO  J=1,2
     IF (LRIS+LMDSN.EQ.0) THEN
        IF (J.EQ.1) THEN
           CALL WTEXT (' First set: ')
        ELSE
           CALL WTEXT (' Second set:')
        ENDIF
     ENDIF
10   IF (LRIS+LMDSN.EQ.0) THEN
        CALL WTEXT (' Enter 0 for current parameters')
        CALL WTEXT ('       number for saved parameters')
        CALL WTEXT ('       F to read parameters from a file')
        CALL WTEXT ('    or M to read a multiple scattering file')
     ENDIF
     CALL CREAD (IP1,IP2,IP3,INT,IC,V,*10,*80)
     IF (IC.NE.0.AND.INT.EQ.0) THEN
        CALL FIT ('Q',iset(IWEIGHT),*80)
        FITIN(J)=FITINDEX
        RFS(J)=RFAC
        RCHISQU(J)=CHISQU
        NSHL(J)=0.
        DO  K=1,NS
           IF (RN(K).NE.0.) NSHL(J)=NSHL(J)+1
        enddo
        NPTS(J)=NPT(IEXP)
     ELSEIF (INT.GT.0.AND.INT.LE.NSAVE) THEN
        FITIN(J)=SPARS(IPARNPARAMS+1,INT)
        RFS(J)=SPARS(IPARNPARAMS+2,INT)
        RCHISQU(J)=SPARS(IPARNPARAMS+6,INT)
        NSHL(J)=0
        DO  K=1,ifix(SPARS(INDNS,INT))
           IF (SPARS(inds(INDNS)+K,INT).NE.0.) NSHL(J)=NSHL(J)+1
        enddo
        NPTS(J)=SPARS(IPARNPARAMS+5,INT)
     ELSEIF (IP1(1:1).EQ.'F'.OR.IP1(1:1).EQ.'M') THEN
        GOTO 50
40      CLOSE (INFILE)
50      IF (LRIS+LMDSN.EQ.0) CALL WTEXT ('Filename for parameters ?')
        CALL AREAD (2,WSPACE,LD,*50,*80)
        CALL FILEOPEN (WSPACE,INFILE,'FORMATTED','dat',*50)
        CALL RDPARM (*40)
        CLOSE (INFILE)
        IF (IP1(1:1).EQ.'M') CALL XADD ('READ',-1,*90)
        CALL FLIM (*51)
51      CALL UTAB (0)
        CALL FIT ('Q',iset(IWEIGHT),*90)
        FITIN(J)=FITINDEX
        RFS(J)=RFAC
        NSHL(J)=0
        RCHISQU(J)=CHISQU
        DO  K=1,NS
           IF (RN(K).NE.0.) NSHL(J)=NSHL(J)+1
        enddo
        NPTS(J)=NPT(IEXP)
        CALL UCOPY (RJM,PA1,IPARNPARAMS)
        CALL FLIM (*61)
61      CALL UTAB (0)
     ELSE
        GOTO 10
     ENDIF
  enddo
  DO J=1,2
     WRITE (OUTTERM,100) J,NSHL(J),NPTS(J),FITIN(J),RFS(J),RCHISQU(J)
     WRITE (LOGFILE,100) J,NSHL(J),NPTS(J),FITIN(J),RFS(J),RCHISQU(J)
  ENDDO
  IF (NPTS(1).NE.NPTS(2)) THEN
     CALL ERRMSG ('Number of energy points is not the same for the two parameter sets.',*99)
  ENDIF
  IF (NSHL(1)-NSHL(2).EQ.1) THEN
     FSMIN=FITIN(1)
     FIMP=FITIN(2)
     NSN=NSHL(1)
  ELSEIF (NSHL(1)-NSHL(2).EQ.-1) THEN
     FSMIN=FITIN(2)
     FIMP=FITIN(1)
     NSN=NSHL(2)
  ELSE
     CALL ERRMSG ('The number of shells does not differ by one between parameter sets.',*99)
  ENDIF
  DQQ=NPT(IEXP)-(3*NSN+3)
  RQQ=DQQ*(FIMP-FSMIN)/(3*FSMIN)
  WRITE (LOGFILE,*) 'RQQ ',RQQ
  IF (DQQ.GT.50) THEN
     IF (RQQ.GE.3.95) THEN
        WRITE (OUTTERM,110)
        WRITE (LOGFILE,110)
     ELSE IF (RQQ.GE.2.7.AND.RQQ.LT.3.95) THEN
        WRITE (OUTTERM,120)
        WRITE (LOGFILE,120)
     ELSE IF (RQQ.LT.2.7) THEN
        WRITE (OUTTERM,130)0.95*FIMP
        WRITE (LOGFILE,130)0.95*FIMP
     ENDIF
  ELSE
     WRITE (OUTTERM,140) 51+ifix(DQQ)
     WRITE (LOGFILE,140) 51+ifix(DQQ)
  ENDIF
80 LC=IOR(LC,4_i4b)
  RETURN
90 CALL UCOPY (RJM,PA1,IPARNPARAMS)
  CALL FLIM (*98)
98 CALL UTAB (0)
99 RETURN
  !
100 FORMAT (I3,': Shells:',I3,' Points:',I5,' Fit index:',F8.4,' R:',F8.2,' CHI^2:',E12.4)
110 FORMAT(/' Additional shell is significant at 1% probability level')
120 FORMAT(/' Additional shell is significant at 5% probability level')
130 FORMAT(/' Additional shell is not significant at 5% probability level'/' Fit index of <',F9.4,' required.')
140 FORMAT(' Number of data points <',I3)
END SUBROUTINE NEWSTAT

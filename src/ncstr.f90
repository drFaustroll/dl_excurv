FUNCTION NCSTR (STRING)
  !======================================================================C
  !
  !	Returns the number of non-blank leading characters in STRING (1 if null)
  !
  CHARACTER*(*) STRING
  LL=LEN(STRING)
  DO  J=LL,1,-1
     IF (STRING(J:J).NE.' ') GOTO 20
  enddo
  J=1
20 NCSTR=J
  RETURN
END FUNCTION NCSTR

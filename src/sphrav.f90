SUBROUTINE SPHRAV (R,G,RG,F,WS,NR)
  !======================================================================C
  !	IMPLICIT REAL*8(A-H,O-Z)
  DIMENSION R(NR),G(NR),F(NR),WS(NR)
  DATA HALF/0.5E0/
  !
  !	****************************************************************
  !	*                                                              *
  !	*  SPHRAV FORMS THE SPHERICAL AVERAGE OF A SPHERICALLY         *
  !	*  SYMMETRICAL FUNCTION G(R) CENTRED AT R=RG. G(R) IS ASSUMED  *
  !	*  TO BE ZERO FOR R >= R(NR).                                  *
  !	*                                                              *
  !	****************************************************************
  !	*                                                              *
  !	*  INPUT :                                                     *
  !	*                                                              *
  !	*  R(I),I=1,NR  : REAL SPACE GRID.                             *
  !	*  G(I),I=1,NR  : REAL FUNCTION TO BE SPHERICALLY AVERAGED.    *
  !	*  RG           : RADIUS AT WHICH G IS CENTRED.                *
  !	*                                                              *
  !	****************************************************************
  !	*                                                              *
  !	*  OUTPUT :                                                    *
  !	*                                                              *
  !	*  F(I),I=1,NR  : SPHERICAL AVERAGE OF G(|R-RG|).              *
  !	*  (WS(I),I=1,NR IS TEMPORARY WORKSPACE).                      *
  !	*                                                              *
  !	****************************************************************
  !
  !
  !	Load integrand into workspace array and initialise F(I).
  !
  DO  I=1,NR
     WS(I)=R(I)*G(I)
     F(I)=0.0E0
  enddo
  !
  !	IF RG IS LARGER THAN 2*R(NR) THEN F(R) IS ZERO FOR ALL R,
  !	SINCE G(R)=0 FOR R>R(NR).
  !
  S=R(NR)
  IF (RG.GE.2.0E0*S) RETURN
  !
  !	Perform indefinite integral and load result into workspace.
  !
  CALL SIMPSN(R,WS,NR,1,F)
  DO  I=1,NR
     WS(I)=F(I)
  enddo
  !
  !	For each R, find upper and lower limits of definite integral, and
  !	calculate spherical average.
  !
  DO  I=1,NR
     ARG=RG+R(I)
     TOP=MIN(ARG,S)
     ARG=ABS(RG-R(I))
     BOT=MIN(ARG,S)
     F(I)=(HALF/(R(I)*RG))*(FLAGQ(TOP,R,WS,NR,5,NEXT)-FLAGQ(BOT,R,WS,NR,5,NEXT))
  enddo
  RETURN
END SUBROUTINE SPHRAV

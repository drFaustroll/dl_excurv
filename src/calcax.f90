SUBROUTINE CALCAX (X1,Y1,Z1,X2,Y2,Z2,AX,AY,AZ,ANGLE,ASIGN)
  !======================================================================C
  Use Common_convia
  !
  !	Calculate normal to two vectors and angle between them
  !
  LOGICAL :: LERR
  AX=Y1*Z2-Z1*Y2
  AY=Z1*X2-X1*Z2
  AZ=X1*Y2-Y1*X2
  U1=SQRT(X1*X1+Y1*Y1+Z1*Z1)
  U2=SQRT(X2*X2+Y2*Y2+Z2*Z2)
  ASIGN=AX+AY+AZ
  IF (U1.EQ.0..OR.U2.EQ.0.) THEN
     ANGLE=0.
     IF (ASIGN.LT.0.) ANGLE=PI
  ELSE
     ANGLE=(X1*X2+Y1*Y2+Z1*Z2)/(U1*U2)
     ANGLE=CALCACOS(ANGLE,'angle',LERR)
  ENDIF
  RETURN
END SUBROUTINE CALCAX

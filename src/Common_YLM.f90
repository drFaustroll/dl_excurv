Module Common_YLM
  !      COMMON /YLM/ YLM(25,0:IPARLMAX+3,-IPARLMAX-3:IPARLMAX+3)
  !      COMPLEX YLM
  Use Definition
  Use Parameters
  implicit none
  complex(imag), dimension(:,:,:), public :: YLM(25,0:IPARLMAX+3,-IPARLMAX-3:IPARLMAX+3)
end Module Common_YLM

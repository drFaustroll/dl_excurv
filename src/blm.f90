FUNCTION BLM (L1,M1,L2,M2,L3,M3,LMAX)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_FACTORIALS
  !
  !	Function BLM provides the integral of the product
  !	of three spherical harmonics, each of which can be
  !	expressed as a prefactor times a legendre function.
  !	the three prefactors are lumped together as factor
  !	'c'; and the integral of the three legendre functions
  !	follows Gaunt's summation scheme set out by Slater:
  !	atomic structure, vol1, 309,310
  !
  real(float) A,B,C,SIGN,BN,CN
  NN=4*LMAX+1
  IF (M1+M2+M3) 80,10,80
10 IF (L1-LMAX-LMAX) 20,20,80
20 IF (L2-LMAX) 30,30,80
30 IF (L3-LMAX) 40,40,80
40 IF (L1-IABS(M1)) 80,50,50
50 IF (L2-IABS(M2)) 80,60,60
60 IF (L3-IABS(M3)) 80,70,70
70 IF (MOD(L1+L2+L3,2)) 80,90,80
80 BLM=0.0
  WRITE (OUTTERM,190) L1,L2,M2,L3,M3
  RETURN
90 NL1=L1
  NL2=L2
  NL3=L3
  NM1=IABS(M1)
  NM2=IABS(M2)
  NM3=IABS(M3)
  IC=(NM1+NM2+NM3)/2
  IF (MAX0(NM1,NM2,NM3)-NM1) 130,130,100
100 IF (MAX0(NM2,NM3)-NM2) 110,110,120
110 NL1=L2
  NL2=L1
  NM1=NM2
  NM2=IABS(M1)
  GOTO 130
120 NL1=L3
  NL3=L1
  NM1=NM3
  NM3=IABS(M1)
130 IF (NL2-NL3) 140,150,150
140 NTEMP=NL2
  NL2=NL3
  NL3=NTEMP
  NTEMP=NM2
  NM2=NM3
  NM3=NTEMP
150 IF (NL3-IABS(NL2-NL1)) 160,170,170
160 BLM=0.0
  RETURN
  !
  !	 CALCULATION OF FACTOR 'A'
  !
170 IS=(NL1+NL2+NL3)/2
  IA1=IS-NL2-NM3
  IA2=NL2+NM2
  IA3=NL2-NM2
  IA4=NL3+NM3
  IA5=NL1+NL2-NL3
  IA6=IS-NL1
  IA7=IS-NL2
  IA8=IS-NL3
  IA9=NL1+NL2+NL3+1
  A=((-1.0)**IA1)*FAC(IA2)/FAC(IA3)
  A=A*FAC(IA4)/FAC(IA6)
  A=A*FAC(IA5)/FAC(IA7)
  A=A*FAC(IS)/FAC(IA8)
  A=A/FAC(IA9)
  !
  !	 CALCULATION OF SUM 'B'
  !
  IB1=NL1+NM1
  IB2=NL2+NL3-NM1
  IB3=NL1-NM1
  IB4=NL2-NL3+NM1
  IB5=NL3-NM3
  IT1=MAX0(0,-IB4)+1
  IT2=MIN0(IB2,IB3,IB5)+1
  B=0.
  SIGN=(-1.0)**(IT1)
  IB1=IB1+IT1-2
  IB2=IB2-IT1+2
  IB3=IB3-IT1+2
  IB4=IB4+IT1-2
  IB5=IB5-IT1+2
  DO IT=IT1,IT2
     SIGN=-SIGN
     IB1=IB1+1
     IB2=IB2-1
     IB3=IB3-1
     IB4=IB4+1
     IB5=IB5-1
     BN=SIGN*FAC(IB1)/FAC(IB3)
     BN=BN*FAC(IB2)/(FAC(IT-1)*FAC(IB4))
     BN=BN/FAC(IB5)
     B=B+BN
  enddo
  !
  !	 CALCULATION OF FACTOR 'C'
  !
  IC1=NL1-NM1
  IC2=NL1+NM1
  IC3=NL2-NM2
  IC4=NL2+NM2
  IC5=NL3-NM3
  IC6=NL3+NM3
  CN=real((2*NL1+1)*(2*NL2+1)*(2*NL3+1),kind=float)
  CN=CN*FAC(IC1)/FAC(IC2)
  CN=CN*FAC(IC3)/FAC(IC4)
  CN=CN*FAC(IC5)/FAC(IC6)
  C=CN/PI
  C=(SQRT(C))/2.
  BLM=((-1.0)**IC)*A*B*C
  RETURN
190 FORMAT ('Invalid arguments for BLM: ',5(I2,','))
END FUNCTION BLM

SUBROUTINE EXTIME (NSEC,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_IP
  !
  !	Timing for EXFIT. Entry TCHECK decides whether there is enough
  !	time for the next step.
  !
  SAVE RICP,RMIN

  CALL GETTIM (1,RICP,ELAPS)
  WRITE (LOGFILE,30) NSEC
  WRITE (LOGFILE,40) RICP
  RMIN=real(NSEC,kind=dp)-RICP-2.0
  CALL GETTIM (0,CPU,ELAPS)
  RETURN
  ENTRY TCHECK(*)
  !
  !	Check time used. terminate if insufficient left for next step
  !
  CALL GETTIM (1,RICT,ELAPS)
  RICP=RICP+RICT
  TMIN=AMOD(RICP,60.)
  MR=RICP/60.
  CALL GETTIM (0,CPU,ELAPS)
  IF (ICALL.EQ.1) WRITE (LOGFILE,50) ICALL,MR,TMIN
  IF (ICALL.EQ.999) THEN
     !
     !     GIVE TOTAL TIME USED IF LAST CALL
     !
     DO IW=PLOTFILE,OUTTERM,OUTTERM-PLOTFILE
        WRITE (IW,60) MR,TMIN
     enddo
     RETURN
  ENDIF
  !	WRITE (LOGFILE,*) 'RICP,RICT,RMIN: ',RICP,RICT,RMIN
  IF (RICP+RICT*2..LT.RMIN) RETURN
  !	DO 20 IW=PLOTFILE,OUTTERM,OUTTERM-PLOTFILE
  !  20	WRITE (IW,70) MR,TMIN
  !	RETURN 1
  RETURN
  !
30 FORMAT (' Time requested ',I5,' seconds')
40 FORMAT (' Time before first call ',F6.2,' seconds')
50 FORMAT (' Time after',I4,' calls : ',I3,' minutes',F6.2,' seconds'/)
60 FORMAT (' Total cpu time used :',I3,' minutes',F6.2,' seconds'/)
70 FORMAT (' Terminating due to time limit : '/'time used: ',I3,' MINUTES',F6.2,' SECONDS.')
END SUBROUTINE EXTIME

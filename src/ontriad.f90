SUBROUTINE ONTRIAD (I,TPHI,TTHETA,JCLUS,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_SYMOPS
  Use Include_IPA
  Use Include_PA2

  real(float) ATANS2
  TPHI=PI/4.
  ATANS2=DATAN(DSQRT(2.D0))
  IF (N(I).EQ.4) THEN
     IF (ABS(RADTH(I)-ATANS2).LT.0.00001) THEN
        INT=1
     ELSEIF (ABS(RADTH(I)-(PI-ATANS2)).LT.0.00001) THEN
        INT=-1
     ELSE
        WRITE (OUTTERM,10) 'Shell: ',I,'Theta invalid - must be', ATANS2/AC,' or ',(PI-ATANS2)/AC
        RETURN 1
     ENDIF
  ELSE
     INT=1
  ENDIF
  TTHETA=ATANS2
  IF (INT.LT.0) TTHETA=PI-ATANS2
  JTRIAD=1
  IF (PGROUP(JCLUS)(1:2).EQ.'Oh') JCX=1
  IF (PGROUP(JCLUS)(1:2).EQ.'Td') JSX=1
  RETURN
10 FORMAT (A,I3,A,F5.0,A,F5.0)
END SUBROUTINE ONTRIAD

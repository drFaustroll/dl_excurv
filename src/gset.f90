SUBROUTINE GSET (IP1,K4,INT,*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_SETKW
  Use Common_PMTCH
  Use Common_PLFLAG
  Use Common_PMTX
  !-st	Use Common_COMPAR
  !
  !	Set graphics options
  !
  CHARACTER*(*) K4
  INTEGER MAX(IPARGSET)
  CHARACTER*12 IP1,K3,KP3
  CHARACTER*12 CHARRESET,CHARLIST,CHARQUIT
  LOGICAL IEXIT
  DATA MAX/2,2,4,2,5,3,3,5,4,3,2,2,3,1,4/
  DATA CHARRESET/'RESET'/,CHARLIST/'LIST'/,CHARQUIT/'QUIT'/
  !
  !	Set graphics options
  !
  IEXIT=IP1.NE.' '.AND.K4.NE.' '
  IF (IP1.NE.' ') GOTO 80
  GOTO 60
10 DO  I=1,IPARGSET
     IGF(I)=1
  enddo
  RETURN 1
30 CALL WTEXT  ('GRAPHICS OPTIONS-------- ')
  CALL WTEXT  ('KEWORD    OPTIONS')
  CALL WTEXT  ('          1        2         3        4     5  NOW')
  CALL WTEXTI ('AXES      ON       OFF       BOTTOM_ONLY         ',IGF(6))
  CALL WTEXTI ('DEVICE    TERMINAL PLOTTER                       ',IGF(2))
  CALL WTEXTI ('FRAMES    FIXED    SELECT                        ',IGF(1))
  CALL WTEXTI ('HEADER    ON       OFF       LARGE               ',IGF(14))
  CALL WTEXTI ('LABELS    ON       OFF       LARGE               ',IGF(13))
  CALL WTEXTI ('LINE      NORMAL   SYMBOL    THICK               ',IGF(10))
  CALL WTEXTI ('MAXOUTPUT OFF      ON                            ',IGF(11))
  CALL WTEXTI ('PLOTTER   HP       PS        HP_FILE  PS_FILE    ',IGF(15))
  CALL WTEXTI ('PROMPT    ON       OFF                           ',IGF(12))
  CALL WTEXTI ('PWEIGHT   NONE     K         K2       K3    K4   ',IGF(5))
  CALL WTEXTI ('RANGE     EMIN/MAX SELECT    YMIN/MAX            ',IGF(7))
  CALL WTEXTI ('SPECTRUM  ALL      EXPER     THEORY   DIFFERENCE ',IGF(3))
  CALL WTEXTI ('TABLE     LONG     MEDIUM    SHORT    NONE       ',IGF(9))
  CALL WTEXTI ('TERMINAL  SELANAR  PERICOM   T4010    XT    XWIN ',IGF(8))
  CALL WTEXTI ('X-AXIS    K        ENERGY                        ',IGF(4))
  GOTO 60
40 CALL ERRMSG ('Invalid keyword '//IP1(1:NIP1),*60)
  !
  !	Next line required by FTN77
  !
50 K3=K4
  CALL ERRMSG ('Invalid option '//K3(1:NK4),*60)
60 IF (IEXIT) RETURN
  IF (LRIS.GT.0) GOTO 70
  CALL WTEXT ('Enter keyword + option number, LIST, RESET or QUIT ')
70 CALL CREAD (IP1,K4,KP3,INT,IC,V,*60,*60)
80 NK4=NCSTR(K4)
  NIP1=NCSTR(IP1)
  IF (IP1(1:NIP1).EQ.CHARRESET(1:NIP1)) GOTO 10
  IF (IP1(1:NIP1).EQ.CHARLIST(1:NIP1).OR.IP1(1:1).EQ.'?') GOTO 30
  IF (IP1(1:NIP1).EQ.CHARQUIT(1:NIP1)) RETURN 1
  DO  J=1,IPARGSET
     IF (IP1(1:NIP1).EQ.IAL(J)(1:NIP1)) GOTO 100
  enddo
  CALL ERRS (*40)
100 IF (K4.EQ.' '.AND.INT.EQ.0) THEN
110  CALL WTEXT ('Option ?')
     CALL CREAD (K4,K3,KP3,INT,IC,V,*110,*60)
     NK4=NCSTR(K4)
  ENDIF
  IF (INT.NE.0) THEN
     IF (INT.LT.1.OR.INT.GT.MAX(J).AND.J.NE.5) CALL ERRS (*50)
  ELSE
     DO  INT=1,MAX(J)
        IF (K4(1:NK4).EQ.GSETKWO(INT,J)(1:NK4)) GOTO 130
     enddo
     INT=0
     GOTO 50
130  CONTINUE
  ENDIF
  IF (LMDSN.EQ.0)  WRITE (OUTTERM,140) IAL(J),GSETKWO(INT,J)
  IF (J.EQ.2.OR.J.EQ.8) THEN
     IF (PLFLAG) THEN
        CALL FVEXIT
        IF (IGF(8).EQ.5) CALL FQQVEXIT
     ENDIF
     PLFLAG=.FALSE.
  endif
  IGF(J)=INT
  !
  if ( j.eq.3 ) lf = 2
  !
  GOTO 60
140 FORMAT(' Keyword: ',A,' Option: ',A)
END SUBROUTINE GSET

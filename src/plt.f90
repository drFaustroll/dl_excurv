SUBROUTINE PLT (X,Y,JJS,JJF,NN,*)
  !======================================================================C
  Use Definition	
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PMTCH
  Use Common_PLFLAG
  Use Index_IGF                   ! because comented last line only
  Use Common_PMTX
  Use Common_UPU
  Use Common_PATHS
  Use Common_PRDSN
  Use Include_PA1
  Use Common_TXY
  !
  !	Curve plotting routine
  !
  !	Plot Y vs. X from point JJS to JJF.
  !
  !	IRR =  1 Experiment
  !	IRR =  2 Theory
  !	IRR =  3 FT exp
  !	IRR =  4 FT th
  !	IRR =  5 Sine transform exp
  !	IRR =  6 Sine transform th
  !	IRR =  7 BS mag
  !	IRR =  8 BS phase
  !	IRR =  9 Phase of Fpi
  !	IRR = 10 Difference (exafs)
  !	IRR = 11 Difference (FT)
  !	IRR = 12 Window
  !	IRR = 13 Atomic potential
  !	IRR = 14 Superposed potential
  !	IRR = 15 Muffin Tin Potential
  !	IRR = 16 Atomic charge density
  !	IRR = 17 Superposed charge density
  !	IRR = 18 Almost anything - user defined
  !	IRR = 19 Not used, but IAL(19) used for 2nd title in compare
  !	IRR = 20 Central Atom Phase
  !
  !	NN determines type of mapping :
  !	    -1  New map defined, but no new axes
  !	     0  Same map
  !	     1  New map, new axes
  !	     2  Special case of 1, reduce frame size slightly
  !
  CHARACTER PATHLAB*30
  real(sp)  X(JJF)
  real(sp)  Y(JJF)  ! must to be (sp) due to vogle ?
  REAL S1(0:19),S2(0:19),S3(0:19),S4(0:19)
  real UXY(4)
  !  real TXY(4)
  CHARACTER*4 KP1,KP2,KP3,ILB(4)
  !  SAVE TXY
  !  EQUIVALENCE (UXMIN,UXY(1)),(UXMAX,UXY(2)),(UYMIN,UXY(3)),(UYMAX,UXY(4))
  !  equivalence (TXMIN,TXY(1)),(TXMAX,TXY(2)),(TYMIN,TXY(3)),(TYMAX,TXY(4))
  !-st     above EQUIVALENCE is in the scope of only this subroutine
  DATA S1/.07,2*.07,2*.55,5*.05,5*.4,5*.75/
  DATA S2/.98,2*.5,2*.98,5*.35,5*.7,5*1.05/
  DATA S3/.07,.51,.07,.51,.07,.77,.59,.41,.23,.05,.77,.59,.41,.23,.05,.77,.59,.41,.23,.05/
  DATA S4/2*.95,.51,.95,.51,.95,.77,.59,.41,.23,.95,.77,.59,.41,.23,.95,.77,.59,.41,.23/
  DATA ILB/'XMIN','XMAX','YMIN','YMAX'/
  !
  !	TABLE OF IGF OPTIONS :
  !	1   FRAMES           2    DEVICE            3    SPECTRUM
  !	4   X-AXIS           5    WEIGHTING         6    ANOTATION
  !	7   NPOINTS          8    TRANSFORM         9    PTABLE
  !	10  GTYPE            11   VFORMAT
  !
  UXY(1)= UXMIN
  UXY(2)= UXMAX
  UXY(3)= UYMIN
  UXY(4)= UYMAX

  TXMIN=TXY(1)
  TXMAX=TXY(2)
  TYMIN=TXY(3)
  TYMAX=TXY(4)
  IF (DEBUG) THEN
     write (logfile,*)'*AW in plt*jjs,2nd line is col',jjs,y(jjs)
     WRITE (LOGFILE,*) 'X(',JJS,') =',X(JJS),' X(',JJF,')= ',X(JJF)
     WRITE (LOGFILE,*) 'Y(',JJS,') =',Y(JJS),' Y(',JJF,')= ',Y(JJF)
  ENDIF
  IF (JJF-JJS.LT.3) THEN
     CALL ERRMSG ('Less than 3 points to be plotted',*190)
  ENDIF
  !	NPLTS=0
  IF (DEBUG) WRITE (LOGFILE,*) 'NN=',NN,' JJ=',JJ
  IF (NN) 30,100,10
10 MPL=0
  IF (IGF(1).EQ.2) THEN
20   IF (LMDSN.EQ.0.AND.LRIS.EQ.0) THEN
        IF (IGF(2).NE.3) THEN
           CALL TPOS (0,740)
        ENDIF
        CALL WTEXT ('$Select Frame (0, 1-4 or 5-19) ')
     ENDIF
     CALL CREAD (KP1,KP2,KP3,JJ,IC,V,*20,*190)
     IF (JJ.LT.0.OR.JJ.GT.19) CALL ERRS (*20)
  ENDIF
  IF (JJ.LE.1.OR.JJ.EQ.5) CALL GRINIT
  !
  !	Determine XMIN/MAX, YMIN/MAX from first and last points, and
  !	from RQ(1), RQ(2) ( spectrum min and max).
  !
30 IF (RQ(1).EQ.RQ(2)) THEN
     RQ(1)=RQ(1)-1.
     RQ(2)=RQ(2)+1.
  ENDIF
  TXMIN=X(JJS)
  TXMAX=X(JJF)
  TYMIN=RQ(1)
  TYMAX=RQ(2)
  IF (DEBUG) THEN
     WRITE (LOGFILE,*) 'TXMIN,TXMAX,TYMIN,TYMAX',TXMIN,TXMAX,TYMIN,TYMAX
     WRITE (LOGFILE,*) 'PLOTXSCALE,PLOTYSCALE',PLOTXSCALE,PLOTYSCALE
  ENDIF
  !	DO I=JJS,JJF
  !	WRITE (LOGFILE,*) X(I),Y(I)
  !	ENDDO
  IF (TYMIN.LT.0.) TYMIN=TYMIN*1.2
  IF (TYMAX.GT.0.) TYMAX=TYMAX*1.2
  IF (NN.LT.0) GOTO 100
  UXMIN=TXMIN/PLOTXSCALE
  UXMAX=TXMAX/PLOTXSCALE
  UXY(1)= UXMIN
  UXY(2)= UXMAX
  IF (IGF(7)-2) 40,50,80
40 UYMIN=TYMIN/PLOTYSCALE
  UYMAX=TYMAX/PLOTYSCALE
  UXY(3)= UYMIN
  UXY(4)= UYMAX
  GOTO 100
50 DO  I=1,4
60   WRITE (OUTTERM,'(''Enter '',A4)') ILB(I)
     CALL CREAD (KP1,KP2,KP3,IN1,IC,UXY(I),*60,*190)
     IF (IC.EQ.0) CALL ERRS (*60)
     IF (I.EQ.3) UXY(I)=-ABS(UXY(I))
  enddo
  UXMIN = UXY(1)
  UXMAX = UXY(2)
  UYMIN = UXY(3)
  UYMAX = UXY(4)
  IF (UXMAX-UXMIN.LE.0.OR.UYMAX-UYMIN.LE.0) CALL ERRS(*50)
  write(6,*)'CHECK 1',uxmax,uxmin,plotxscale
  TXMIN=UXMIN*PLOTXSCALE
  TXMAX=UXMAX*PLOTXSCALE
  GOTO 90
80 UYMIN=YMIN
  UYMAX=YMAX
  UXY(3) =  UYMIN
  UXY(4) =  UYMAX
90 TYMIN=UYMIN*PLOTYSCALE
  TYMAX=UYMAX*PLOTYSCALE
100 MPL=MPL+1
  IF (DEBUG) WRITE (7,*) 'TXY',TXMIN,TXMAX,TYMIN,TYMAX
  IGF13=IGF(13)
  IF (IGF(11).EQ.2.AND.IGF(2).EQ.2) IGF13=1
  IF (NN) 140,150,130
130 TT=S4(JJ)
  TT2=S2(JJ)
  IF (NN.EQ.2) TT=TT*.93
  IF (IGF(2).EQ.2.AND.TT2.GT.1.) TT2=1.
  CALL PSPACE (S1(JJ)+CENTRED,TT2+CENTRED,S3(JJ),TT)
  USED3=JJ.EQ.3.OR.JJ.GT.4
  USED4=JJ.GE.4
  IF (TXMAX.LE.TXMIN.OR.TYMAX.LE.TYMIN) THEN
     WRITE (OUTTERM,*) TXMIN,TXMAX,TYMIN,TYMAX
     CALL ERRMSG ('Error in plotting coordinates',*190)
  ENDIF
  CALL MAP (UXMIN,UXMAX,UYMIN,UYMAX)
  KK2=4
  CALL WHITEPEN
  CALL BORDER
  IF (IGF(6).NE.2) CALL SCALES
140 CALL MAP (TXMIN,TXMAX,TYMIN,TYMAX)
150 MNPL=MPL
  IF (NPL.EQ.2.OR.NPL.EQ.3) MNPL=MNPL-(NPL-1)*2
  IF (MNPL.EQ.1) THEN
     CALL GRNPEN
  ELSE
     CALL MAGPEN
     CALL BROKEN
     !	  NPLTS=1
  ENDIF
160 CALL PTJOIN (X,Y,JJS,JJF,1)
  CALL FULL
  IF(IGF(10).EQ.2.AND.MNPL.EQ.1)THEN
     DO  I=JJS,JJF
        CALL PLOTC (X(I),Y(I),'X')
     enddo
  ENDIF
  !
  !	No labels if IGF(13)=2
  !
  XMM=(TXMAX-TXMIN)*CSIZE
  YMM=(TYMAX-TYMIN)*CSIZE
  XPL=TXMIN+XMM
  YPL=TYMAX-real(MPL,kind=dp)*YMM
  !
  !      PC value
  !
  YPL2=TYMIN+YMM
  !
  !      PC value
  !
  !      YPL2=TYMIN+YMM*3.
  CALL WHITEPEN
  CALL FTEXTJUSTIFY (2)
  IF(IGF(9).EQ.5.AND.IGF13.NE.2) THEN
     CALL PLOTNF (XPL,YPL,PATHINDEX(IPINDEX),2)
     JI=IRAD(IABS(int(CLUS(IPATHINDS(1,IPINDEX)))))
     CALL ADDP (PATHLAB,IPLAB,IPATHINDS(1,IPINDEX),JPATHINDS(1,IPINDEX),JI)
     CALL FGETGP2 (XCUR,YCUR)
     CALL PLOTCSX (XCUR,YPL,' '//PATHLAB(1:IPLAB)//' ')
     CALL FGETGP2 (XCUR,YCUR)
     CALL PLOTNF (XCUR,YPL,PATHPL(IPINDEX),2)
     CALL PLOTNF (XPL,YPL2,PATHANG(IPINDEX),0)
     CALL FGETGP2 (XCUR,YCUR)
     NIN=1
     IF (IPATHOCC(IPINDEX).GT.9) NIN=2
     CALL PLOTNI (XCUR+XMM,YPL2,IPATHOCC(IPINDEX),NIN)
     CALL FGETGP2 (XCUR,YCUR)
     CALL PLOTNE (XCUR+XMM,YPL2,PATHMAX(INT(PATHINDEX(IPINDEX)),1))
     TXY(1)= TXMIN
     TXY(2)= TXMAX
     TXY(3)= TYMIN
     TXY(4)= TYMAX
     RETURN
  ENDIF
  IF (IGF13.EQ.2.OR.PLOTTITLE.EQ.' '.OR.MPL.GT.10) then
     TXY(1)= TXMIN
     TXY(2)= TXMAX
     TXY(3)= TYMIN
     TXY(4)= TYMAX
     RETURN
  end IF
  INUM=NCSTR(PLOTTITLE)
  XPP=15.
  !
  !      PC value
  !      XPP=23.
  !
  IF (INUM.GT.23) XPP=real(INUM,kind=dp)*17./23.
  !
  !      PC value
  !
  CALL PLOTCSX (XPL,YPL,PLOTTITLE(1:INUM))
  CALL FTEXTJUSTIFY (0)
  XPP=XMM*XPP+TXMIN
  MNPL=MPL
  IF (NPL.EQ.2.OR.NPL.EQ.3) MNPL=MNPL-(NPL-1)*2
  IF (MNPL.EQ.1) THEN
     CALL GRNPEN
  ELSE
     CALL MAGPEN
     CALL BROKEN
  ENDIF
  CALL POSITN (XPP,YPL)
  CALL JOIN (XPP+(TXMAX-TXMIN)*.15,YPL)
  CALL FULL
  IF (MNPL.EQ.1.AND.IGF(10).EQ.2) THEN
     DROW=(TXMAX-TXMIN)*.15/9.
     ROW=XPP
     DO  I=1,10
        CALL PLOTC (ROW,YPL,'X')
        ROW=ROW+DROW
     enddo
  ENDIF
  !	IF (IGF(2).EQ.1.AND.IGF(ITNUM).EQ.5) CALL FGFLUSH
  TXY(1)= TXMIN
  TXY(2)= TXMAX
  TXY(3)= TYMIN
  TXY(4)= TYMAX
  RETURN
  TXY(1)= TXMIN
  TXY(2)= TXMAX
  TXY(3)= TYMIN
  TXY(4)= TYMAX
190 RETURN 1
END SUBROUTINE PLT

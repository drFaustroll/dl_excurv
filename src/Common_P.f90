MODULE Common_P

  !      COMMON /P/ PHS(IPARPHPOINTS,IPARNP,IPARLMAX+1),EREF(IPARPHPOINTS,I
  !     1PARNP),EN(IPARPHPOINTS,IPARNP),NPH(IPARNP)
  !      COMPLEX*8 EREF,PHS
  !      real(sp) EN

  Use Definition
  Use Parameters
  implicit none
  private
  complex(imag), dimension(:,:,:), public ::  PHS(IPARPHPOINTS,IPARNP,IPARLMAX+1)
  complex(imag), dimension(:,:),   public :: EREF(IPARPHPOINTS,IPARNP)
  real(sp),      dimension(:,:),   public ::   EN(IPARPHPOINTS,IPARNP)
  integer(i4b),  dimension(:),     public ::  NPH(IPARNP)
end MODULE Common_P


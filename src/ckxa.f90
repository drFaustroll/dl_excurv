SUBROUTINE CKXA (*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_TL
  Use Include_XY
  Use Include_XA

  IF (NX.NE.NP) GOTO 20
  IF (ABS(ENER(1)-XAE1).GT.0.005.OR.ABS(ENER(NP)-XAE2).GT.0.005) GOTO 20
  DO I=1,IPARNS
     IF (I.NE.1.AND.MSTL(I)(3:3).NE.':') GOTO 10
     CALL WTEXT ('XADD - exc'//MSTL(I))
10   CONTINUE
  enddo
  RETURN
20 WRITE (OUTTERM,30) NX,XAE1,XAE2
  WRITE (OUTTERM,30) np,ener(1),ener(np)
30 FORMAT(' Error: energy ranges are incompatible',/,' ',I4,' points in XADD file from',F9.4,' to',F9.4)
END SUBROUTINE CKXA

SUBROUTINE FFILTER
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_LETTERS
  Use Common_TL
  Use Common_F
  Use Common_PMTCH
  Use Common_PMTX
  Use Common_COMPAR
  Use Common_ICF
  Use Index_INDS
  Use Common_ATMAT
  Use Common_PRDSN
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  !
  !	Fourier filtering subroutine
  !
  DIMENSION FFA(2048),FFI(2048),TABLE(513),VMM(2,10),RKI(500),F1(500),F2(500),FFA1(2048),FFA2(2048),EVI(2048)
  REAL*8 STEP
  CHARACTER*8 CDATE*10,CTIME*8,OPT(3)*7,RMM(2)*4,K1*1,K2*1,K3*1
  LOGICAL PRINTED
  DATA  RMM/'RMIN','RMAX'/,OPT/'SHORT','MEDIUM','LONG'/
  CALL FINDOPT (3,OPT,IOPT,1,'Spectrum number (-ve for fullpage mode)',*420)
  JEXP=MAX(1,MIN(IABS(INTOPT),NSPEC))
  !
  !	Ensure the wave-vector is updated
  !
  CALL FIT ('Q',iset(IWEIGHT),*420)
  CALL COSQT (TABLE)
  !
  !	NPN is number of points in output spectrum and also determines
  !	the rspace maximum. Derived from input argument in (0,1,2 or 3)
  !
  NPN=MIN0(100+IOPT*100,500)
  !
  !	Interpolate onto regular k-grid using equal area interpolation.
  !	This method ensures noise does not generate spurious components
  !	although the curve generated is not particularly smooth.
  !
  IFTW=IFTSET(2)
  !
  IF (IFTW.GT.3) CALL ERRMSG ('FT weighting option k/fpim may not be used with Fourier filtering',*410)
  STEP=RK(JF(JEXP),JEXP)/real(NPN-1,kind=dp)
  DO I=1,NPN
     RKI(I)=(I-1)*STEP
  ENDDO
  DO I=1,NPT(JEXP)
     FFA(I)=XABS(I+JS(JEXP)-1,JEXP)
     EVI(I)=ENER(I+JS(JEXP)-1)
     !
     !	Apply k**n weighting to spectrum before interpolation
     !
     DO J=1,IFTW
	FFA(I)=FFA(I)*RK(I+JS(JEXP)-1,JEXP)
     enddo
  enddo
  CALL SPLINT1 (NPT(JEXP),TRK(JS(JEXP),JEXP),FFA,NPN,RKI,0,.FALSE.,*420)
  CALL SPLINT1 (NPT(JEXP),TRK(JS(JEXP),JEXP),EVI,NPN,RKI,0,.FALSE.,*420)
  WRITE (7,*) 'EV RK FFA'
  DO I=1,NPN
     WRITE (7,*) EVI(I),RKI(I),FFA(I)
  ENDDO
  CALL UZERO (FFA(NPN+1),2048-NPN)
  CALL UZERO (FFI,2048)
  ISTART=1
  DO I=1,NPN
     IF (RKI(I).LT.TRK(JS(JEXP),JEXP)) THEN
        FFA(I)=0.
        ISTART=I+1
     ENDIF
  ENDDO
  !
  !	Find plotting limits and save for final spectrum
  !
  CALL GRINIT
  RQ(2)=0
  DO I=1,NPN
     RQ(2)=AMAX1(RQ(2),ABS(FFA(I)))
  enddo
  RRR=RQ(2)
  RQ(1)=-RQ(2)
  JJ=1
  !
  !	Keyword = 'N' is the newpage option - new plot to be initialised
  !
  IF (INTOPT.LT.0) JJ=0
  !
  !	Plot interpolated spectrum
  !
  PLOTTITLE=ITL(1)
  PLOTXSCALE=RKC
  PLOTYSCALE=RKC**IFTW
  PLOTXLAB='k/Angstroms^-1'
  PLOTYLAB='k^'//NUMBERS(IFTW)//'^ chi(k)'
  CALL PLT (RKI,FFA,1,NPN,1,*55)
  !
  !	Transform to R-space
  !
55 CALL FFT (FFA,FFI,TABLE,-1)
  !
  !	Determine rstep by Nyquist relation
  !
  RSTEP=PI/(STEP*2048.)
  !
  !	Construct grid in r-space for plots
  !
  F1(1)=0.
  DO I=2,NPN
     F1(I)=F1(I-1)+RSTEP
  enddo
  !
  !	Calc modulus for plotting and FT plotting limits
  !
  FRAN=0.
  DO I=1,NPN
     F2(I)=FFA(I)*FFA(I)+FFI(I)*FFI(I)
     F2(I)=SQRT(F2(I))
     FRAN=MAX(FRAN,F2(I))
  enddo

  RQ(1)=0
  RQ(2)=FRAN
  JJ=3
  IF (INTOPT.LT.0) THEN
     JJ=0
     CALL CREAD (K1,K2,K3,INT,IC,V,*70,*410)
70   CALL GRINIT
  ENDIF
  PLOTTITLE=ITL(5)
  PLOTXSCALE=DC
  PLOTYSCALE=1
  CALL PLT (F1,F2,1,NPN,1,*90)
  !
  !	Print spectrum name on plot and/or save it for background plot
  !
  TT=FRAN*.05
  FRAN=FRAN+2.*TT
  DO J=1,NSPEC
     CALL PLOTCSX (0.,FRAN,' '//LFS(J))
     FRAN=FRAN-TT
  ENDDO
  !
  !	Read in filter limits, up to 3 shells may be read, and each may
  !	be corrected if required
  !
90 DO NCYC=1,3
120  DO I=1,2
130	CALL TPOS (10,400-I*25)
	WRITE (OUTTERM,450) RMM(I)
	CALL TPOS (400,400-I*25)
	CALL CREAD (K1,K2,K3,INT,IC,VMM(I,NCYC),*130,*410)
        !
        !	Convert to atomic units
        !
        !-st	VMM(I,NCYC)=MIN(SNGL(VMM(I,NCYC)*DC),F1(NPN))
	VMM(I,NCYC)=MIN(real(VMM(I,NCYC)*DC,kind=sp),F1(NPN))
	CALL WHITEPEN
	CALL POSITN (VMM(I,NCYC),RQ(1))
	CALL JOIN (VMM(I,NCYC),RQ(2))
     enddo
     IF (VMM(2,NCYC).LE.VMM(1,NCYC)) CALL ERRMSG ('RMAX < RMIN ',*120)
150  CALL TPOS (10,325)
     CALL WTEXT ('Enter MOVE, NEXT or CONTINUE')
     CALL TPOS (400,325)
     CALL CREAD (K1,K2,K3,INT,IC,V,*150,*410)
     IF (IC.EQ.1) GOTO 150
     !
     !	Here for move
     !
     IF (K1.EQ.'M') GOTO 120
     !
     !	Check for continue ( = not next).
     !
     IF (K1.NE.'N') GOTO 180
     !
     !	If more than one shell is requested, the rspace spectrum must
     !	be saved for future use.
     !
     IF (NCYC.EQ.1) CALL UCOPY (FFA,FFA1,4096)
     IF (NCYC.EQ.2) CALL UCOPY (FFA,FFA2,4096)
  enddo
  !
  !	Back-transform each shell, calc phase and magnitude if required
  !
180 DO ICYC=1,NCYC
     !
     !	Now process transforms in reverse order
     !	The last one is in ffa, previous ones have been stored
     !
     IF (ICYC.EQ.1) GOTO 190
     ICOD=4096
     IF (ICYC.EQ.NCYC) CALL UCOPY (FFA1,FFA,4096)
     IF (ICYC.EQ.NCYC-1) CALL UCOPY (FFA2,FFA,4096)

190  RMIN1=VMM(1,NCYC-ICYC+1)
     RMAX1=VMM(2,NCYC-ICYC+1)
     IMIN=RMIN1/RSTEP+1
     IF (IMIN.LT.1) IMIN=1
     IMAX=RMAX1/RSTEP+2
     IF (IMAX.GT.2049) IMAX=2049
     RMIN1=RMIN1/DC
     RMAX1=RMAX1/DC
     CALL UZERO (FFA,IMIN-1)
     CALL UZERO (FFI,IMIN-1)
     IF (IMAX.LE.2048) THEN
        CALL UZERO (FFA(IMAX),2049-IMAX)
        CALL UZERO (FFI(IMAX),2049-IMAX)
     ENDIF
     CALL FFT (FFA,FFI,TABLE,1)
     !
     !	Calc phase, magnitude and exafs = 2*real
     !
     DO I=1,NPN
	F1(I)=PI/2.
	IF (ABS(FFA(I)).GT.1.E-10) F1(I)=ATAN(FFI(I)/FFA(I))
	F2(I)=SQRT(FFI(I)*FFI(I)+FFA(I)*FFA(I))
  	FFA(I)=2.*FFA(I)
     enddo
     IPHASE=0
     !
     !	Plot backtransformed spectrum.
     !
     RQ(1)=-RRR
     RQ(2)=-RQ(1)
     JJ=2
     IF (INTOPT.LT.0) THEN
        JJ=0
        CALL GRINIT
     ENDIF
210  PLOTTITLE=ITL(1)
     PLOTXSCALE=RKC
     PLOTYSCALE=RKC**IFTW
     CALL PLT (RKI,FFA,1,NPN,1,*220)
     !
     !	If the backscattering phase is required the shell radius and hence 2kr is needed.
     !
220  CALL TPOS (10,300)
     CALL WTEXT ('Enter shell radius or CONTINUE')
     CALL TPOS (400,300)
     CALL CREAD (K1,K2,K3,INT,IC,V,*220,*410)
     IF (V.LT..1) GOTO 320
     IPHASE=1
     V=V*DC
     DO I=1,NPN
  	F1(I)=F1(I)-2.*V*RKI(I)+PI/2.
     enddo
     !
     !	Now fix pi shifts etc.
     !
     DO J=1,3
	QT=F1(1)
	DO I=2,NPN
           TT=QT-F1(I)
           IF (ABS(TT).LT.1.) cycle
           DO K=I,NPN
              !-st  240	F1(K)=F1(K)+DSIGN(PI,DBLE(TT))
              F1(K)=F1(K)+SIGN(PI,real(TT,kind=dp))
           enddo
           QT=F1(I)
        enddo
     enddo
260  M=0
     DO J=1,NPN
 	IF (F1(J).LT.0.) M=1
     enddo
     IF (M.EQ.0) GOTO 290
     DO J=1,NPN
  	F1(J)=F1(J)+PI
     enddo
     GOTO 260
290  WRITE (LOGFILE,440) RN(1),A(1)
     !
     !	Correct magnitude for occupancy and temperature effects
     !	lifetimes too complicated
     !
     DO I=1,NPN
	AMP=RKI(I)*RKI(I)
	AMP=EXP(-A2(1)*AMP)*RN2(1)
	IF (AMP.NE.0.) F2(I)=F2(I)/AMP
     enddo
     !
     !	Plot magnitude and phase
     !
     RQ(1)=0
     RQ(2)=1.666667
     JJ=4
     IF (KEYWORD(1:1).NE.'F') GOTO 310
     JJ=0
     IF (IGF(2).NE.3) CALL GRINIT
310  PLOTTITLE=ITL(9)
     PLOTXSCALE=RKC
     PLOTYSCALE=1
     CALL PLT (RKI,F2,1,NPN,2,*320)
     RQ(2)=16.6666667
     PLOTTITLE=ITL(10)
     CALL PLT (RKI,F1,1,NPN,-1,*320)
     !
     !	Remove k**n weighting for output
     !
320  DO I=1,NPN
	IF (RKI(I).GT.0.) THEN
           DO J=1,IFTW
              FFA(I)=FFA(I)/RKI(I)
           enddo
	ENDIF
     enddo
     !
     !	PRINT    (K1='P') Prints exafs, and phase+mag if relevant
     !	REPLACE  (K1='R') Replaces current spectrum
     !	CONTINUE (K1='N') Processes next shell (if any)
     !	END      (K1='E') Terminates command
     !
     PRINTED=.FALSE.
340  CALL TPOS (10,275)
     CALL WTEXT ('Enter  PRINT, REPLACE, NEXT or END')
     CALL TPOS (450,275)
     CALL CREAD (K1,K2,K3,INT,IC,V,*340,*410)
     IF (K1.EQ.'N') GOTO 400
     IF (K1.EQ.'E') GOTO 410
     IF (K1.NE.'R') GOTO 360
     !
     !	Replace current spectrum
     !
     DO I=1,NPN-ISTART+1
	J=I+ISTART-1
	ENER(I)=EVI(J)
	XABS(I,JEXP)=FFA(J)
     enddo
     NP=NPN-ISTART+1
     DO I=0,JEXP,JEXP
	RMS(inds(INDEMIN0)+I)=ENER(1)/EC-1.E-3
	RMX(inds(INDEMAX0)+I)=ENER(NP)/EC+1.E-3
	EMIN0(I)=RMS(inds(INDEMIN0)+I)
	EMAX0(I)=RMX(inds(INDEMAX0)+I)
     ENDDO
     CALL FLIM (*351)
351  CALL TPOS (10,250)
     CALL WTEXT ('Current spectrum has been replaced')
     IF (PRINTED) LFS(1)=PRDSN
     GOTO 340
     !
     !	Here for print
     !	Print ENER and EXAFS unless phase and magnitude also requested
     !
360  CALL OUTNAM (12,PRDSN,' ',*340)
     CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*340)
     CALL GETDAT (CDATE,CTIME,OUTFILE)
     WRITE (OUTFILE,430) LFS(1),RMIN1,RMAX1
     IF (IPHASE.EQ.0) THEN
        DO I=1,NPN-ISTART+1
           J=I+ISTART-1
           E=EVI(J)
           WRITE (OUTFILE,*) E/EC,FFA(J)
        enddo
        CALL EFILE ('Spectrum')
        PRINTED=.TRUE.
     ELSE
380     DO I=1,NPN-ISTART+1
           J=I+ISTART-1
           E=EVI(J)
           WRITE (OUTFILE,*) E/EC,FFA(J),F1(J),F2(J)
        enddo
        CALL EFILE ('Spectrum')
     ENDIF
     GOTO 340
400  CONTINUE
  enddo
410 CONTINUE
420 RETURN
430 FORMAT (' Fourier filtered spectrum : ',A60/' window limits : RMIN',F10.4,' RMAX',F10.4)
440 FORMAT (2X,' Amplitudes corrected for N',F6.3,' and alpha',f6.3)
450 FORMAT (' Enter ',A4)
END SUBROUTINE FFILTER

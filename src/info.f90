SUBROUTINE INFO
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_COMPAR
  Use Common_GAMMA_EDGES

  !-st	LOGICAL ALL    ! never used

  character*9 :: OPTS(6)
  data OPTS/'K-EDGES','L3-EDGES','M5-EDGES','K-WIDTHS','L1-WIDTHS','L3-WIDTHS'/
  CALL FINDOPT (6,OPTS,IOPT,0,'Keyword dependent',*100)
  IF (INTOPT.LE.0.OR.INTOPT.GT.IPARATOMS) NUMFLAG=0
  IF (IOPT.EQ.1) THEN
     DO I=1,103,6
        IF (I.LT.103) THEN
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),KEDGE(I+J),J=0,5)
        ELSE
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),KEDGE(I+J),J=0,0)
        ENDIF
     ENDDO
  ELSEIF (IOPT.EQ.2) THEN
     DO I=1,103,6
        IF (I.LT.103) THEN
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),LEDGE(I+J),J=0,5)
        ELSE
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),LEDGE(I+J),J=0,0)
        ENDIF
     ENDDO
  ELSEIF (IOPT.EQ.3) THEN
     DO I=1,103,6
        IF (I.LT.103) THEN
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),MEDGE(I+J),J=0,5)
        ELSE
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),MEDGE(I+J),J=0,0)
        ENDIF
     ENDDO
  ELSEIF (IOPT.EQ.4) THEN
     DO I=1,103,6
        IF (I.LT.103) THEN
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),GAMMAK(I+J),J=0,5)
        ELSE
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),GAMMAK(I+J),J=0,0)
        ENDIF
     ENDDO
  ELSEIF (IOPT.EQ.5) THEN
  ELSEIF (IOPT.EQ.4) THEN
     DO I=1,103,6
        IF (I.LT.103) THEN
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),GAMMAL3(I+J),J=0,5)
        ELSE
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),GAMMAL3(I+J),J=0,0)
        ENDIF
     ENDDO
     DO I=1,103,6
        IF (I.LT.103) THEN
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),GAMMAL1(I+J),J=0,5)
        ELSE
           WRITE (6,'(6(A,F10.2,1X))') (ELS(I+J),GAMMAL1(I+J),J=0,0)
        ENDIF
     ENDDO
  ENDIF
100 RETURN
END SUBROUTINE INFO

MODULE Common_FACTORIALS
  !      COMMON /FAC/ FAC(0:69)
  !      REAL*8 FAC
  !
  !    FACTORIALS 0 TO 69.
  !
  !    Modifications may be needed for the definition of an appropriate real type 
  !    for FAC to hold numbers with an exponent of up to 10e73 and with a high precision
  USE Definition
  implicit none
  private
  real(float), dimension(0:69), public :: FAC = (/ &
       1.000000000E00_float,1.000000000E00_float,2.000000000E00_float,6.000000000E00_float,24.00000000E00_float, &
       120.0000000E00_float,720.0000000E00_float,5040.000000E00_float,40320.00000E00_float,362880.0000E00_float, &
       3628800.000E00_float,39916800.00E00_float,479001600.0E00_float,6227020800.E00_float,8.717829120E10_float, &
       1.307674368E12_float,2.092278989E13_float,3.556874281E14_float,6.402373706E15_float,1.216451004E17_float, &
       2.432902008E18_float,5.109094217E19_float,1.124000728E21_float,2.585201674E22_float,6.204484017E23_float, &
       1.551121004E25_float,4.032914611E26_float,1.088886945E28_float,3.048883446E29_float,8.841761994E30_float, &
       2.652528598E32_float,8.222838654E33_float,2.631308369E35_float,8.683317619E36_float,2.952327990E38_float, &
       1.033314797E40_float,3.719933268E41_float,1.376375309E43_float,5.230226174E44_float,2.039788208E46_float, &
       8.159152832E47_float,3.345252661E49_float,1.405006118E51_float,6.041526306E52_float,2.658271575E54_float, &
       1.196222209E56_float,5.502622160E57_float,2.586232415E59_float,1.241391559E61_float,6.082818640E62_float, &
       3.041409320E64_float,1.551118753E66_float,8.065817517E67_float,4.274883284E69_float,2.308436973E71_float, &
       1.269640335E73_float,7.109985878E74_float,4.052691950E76_float,2.350561331E78_float,1.326831185E80_float, &
       8.320987112E81_float,5.075802138E83_float,3.146997326E85_float,1.982608315E87_float,1.268869322E89_float, &
       8.247650591E90_float,5.443449390E92_float,3.647111091E94_float,2.480035542E96_float,1.711224524E98_float/)
end MODULE Common_FACTORIALS


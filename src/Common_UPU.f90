MODULE Common_UPU

  !	PARAMETER (IPAR4=IPARNUNITS*4)
  !      COMMON/UPU/MG,NR(0:IPARNUNITS),NT(0:IPARNUNITS),DR(0:IPARNS),NTORA
  !     1(4,IPARNUNITS),NTORB(4,IPARNUNITS),UNAME(IPARNUNITS),IRESNUM(IPARN
  !     2UNITS),ISEQNUM(0:IPARNS),CENTCORD(3,IPARNUNITS),ATLABEL(0:IPARNS),
  !     TOR0(IPARNUNITS),IRAD(IPARNCLUS),RAD(0:IPARNS),RADTH(0:IPARNS),RADP
  !     4HI(0:IPARNS),RADX(0:IPARNS),RADY(0:IPARNS),RADZ(0:IPARNS),MAXCLUS,
  !     4ASTAR,BSTAR,CSTAR,BACOSC,CACOSB,BCCOSA,KEXP(IPARNCLUS),ITAD(IPARNC
  !     5LUS),COFFSET(3,IPARNCLUS),ICRESNUM(IPARNCLUS),CUNAM
  !     6E(IPARNCLUS)
  !      CHARACTER UNAME*12,CUNAME*12,ATLABEL*4
  !
  !	data NTORA/IPAR4*0/                                   !   in COMMON /UPU 
  !	data NTORB/IPAR4*0/,UNAME/IPARNUNITS*' '/             !   in COMMON /UPU 

  Use Definition
  Use Parameters
  implicit none
  private
  integer(i4b), parameter :: IPAR4=IPARNUNITS*4
  integer(i4b),                 public :: MG
  integer(i4b), dimension(:),   public :: NR(0:IPARNUNITS)
  integer(i4b), dimension(:),   public :: NT(0:IPARNUNITS)
  real(dp),     dimension(:),   public :: DR(0:IPARNS)
  integer(i4b), dimension(:,:), public :: NTORA(4,IPARNUNITS)
  data NTORA/IPAR4*0/
  integer(i4b), dimension(:,:), public :: NTORB(4,IPARNUNITS)
  data NTORB/IPAR4*0/
  character*12, dimension(:),   public :: UNAME(IPARNUNITS)
  data UNAME/IPARNUNITS*' '/
  integer(i4b), dimension(:),   public :: IRESNUM(IPARNUNITS)
  integer(i4b), dimension(:),   public :: ISEQNUM(0:IPARNS)
  real(dp),     dimension(:,:), public :: CENTCORD(3,IPARNUNITS)
  character*4,  dimension(:),   public :: ATLABEL(0:IPARNS)
  real(dp),     dimension(:),   public :: TOR0(IPARNUNITS)       ! never used ?
  integer(i4b), dimension(:),   public :: IRAD(IPARNCLUS)
  real(dp),     dimension(:),   public :: RAD(0:IPARNS)
  real(dp),     dimension(:),   public :: RADTH(0:IPARNS)
  real(dp),     dimension(:),   public :: RADPHI(0:IPARNS)
  real(dp),     dimension(:),   public :: RADX(0:IPARNS)
  real(dp),     dimension(:),   public :: RADY(0:IPARNS)
  real(dp),     dimension(:),   public :: RADZ(0:IPARNS)
  integer(i4b),                 public :: MAXCLUS
  real(dp),                     public :: ASTAR,BSTAR,CSTAR,BACOSC,CACOSB,BCCOSA
  integer(i4b), dimension(:),   public :: KEXP(IPARNCLUS)
  integer(i4b), dimension(:),   public :: ITAD(IPARNCLUS)
  real(dp),     dimension(:,:), public :: COFFSET(3,IPARNCLUS)
  integer(i4b), dimension(:),   public :: ICRESNUM(IPARNCLUS)
  character*12, dimension(:),   public :: CUNAME(IPARNCLUS)
end MODULE Common_UPU


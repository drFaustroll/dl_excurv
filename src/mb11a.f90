SUBROUTINE MB11A (M,N,A,IA,W,*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  !-st	COMMON /DEBUG/DFLAGS(8)   ! never used common block
  !-st	LOGICAL DFLAGS
  LOGICAL DFLAGS(8)
  DIMENSION A(IA,*),W(*)
  !
  !	Partition the working space array W
  !	The first partition holds the first components of the vectors of
  !	the elementary transformations.
  !
  IF (DFLAGS(2)) WRITE (LOGFILE,400) 'MB11A called with N=',N,' and M=',M
  NRW=M
  !
  !	The second partition records row interchanges
  !
  NCW=M+M
  !
  !	The third partition records column interchanges
  !	Set the initial records of row and column interchanges
  !
  DO I=1,M
     N1=NRW+I
     XXX=I
     W(N1)=XXX+.5
  enddo
  DO I=1,N
     N1=NCW+I
     XXX=I
     W(N1)=XXX+.5
  enddo
  !
  !	'KK' counts the separate elementary transformations
  !
  KK=1
  !
  !	Find largest row and make row interchanges
  !
30 RMAX=0.
  DO  I=KK,M
     SUM=0.
     DO  J=KK,N
	SUM=SUM+A(I,J)*A(I,J)
     enddo
     IF (RMAX-SUM.lt.0.)then
        RMAX=SUM
        IR=I
     endif
  enddo
  IF (RMAX.EQ.0.) RETURN 1
  IF (IR.GT.KK) THEN
     N3=NRW+KK
     SUM=W(N3)
     N4=NRW+IR
     W(N3)=W(N4)
     W(N4)=SUM
     DO  J=1,N
        SUM=A(KK,J)
        A(KK,J)=A(IR,J)
        A(IR,J)=SUM
     enddo
  ENDIF
  !
  !	Find largest element of pivotal row, and make column interchanges
  !
  RMAX=0.
  SUM=0.
  DO J=KK,N
     SUM=SUM+A(KK,J)*A(KK,J)
     IF (RMAX-ABS(A(KK,J)).lt.0.)then
        RMAX=ABS(A(KK,J))
        IR=J
     endif
  enddo
  IF (IR-KK) 120,120,100
100 N5=NCW+KK
  RMAX=W(N5)
  N6=NCW+IR
  W(N5)=W(N6)
  W(N6)=RMAX
  DO  I=1,M
     RMAX=A(I,KK)
     A(I,KK)=A(I,IR)
     A(I,IR)=RMAX
  enddo
  !
  !	Replace the pivotal row by the vector of the transformation
  !
120 SIGMA=SQRT(SUM)
  BSQ=SQRT(SUM+SIGMA*ABS(A(KK,KK)))
  W(KK)=SIGN(SIGMA+ABS(A(KK,KK)),A(KK,KK))/BSQ
  A(KK,KK)=-SIGN(SIGMA,A(KK,KK))
  KP=KK+1
  IF (KP-N) 130,130,190
130 DO  J=KP,N
     A(KK,J)=A(KK,J)/BSQ
  enddo
  !
  !	Apply the transformation to the remaining rows of A
  !
  IF (KP-M) 150,150,190
150 DO  I=KP,M
     SUM=W(KK)*A(I,KK)
     DO  J=KP,N
	SUM=SUM+A(KK,J)*A(I,J)
     enddo
     A(I,KK)=A(I,KK)-SUM*W(KK)
     DO  J=KP,N
	A(I,J)=A(I,J)-SUM*A(KK,J)
     enddo
  enddo
  KK=KP
  GOTO 30
  !
  !	At this stage the reduction of a is complete. Now we build up the
  !	generalized inverse. Apply the first elementary transformation.
  !
190 KK=M
  KP=M+1
  SUM=W(M)/A(M,M)
  IF (N-M) 220,220,200
200 DO  J=KP,N
     A(M,J)=-SUM*A(M,J)
  enddo
220 A(M,M)=1./A(M,M)-SUM*W(M)
  !
  !	Now apply the other (m-1) transformations
  !
230 KP=KK
  KK=KP-1
  IF (KK) 310,310,240
  !
  !	First transform the last (M-KK) rows
  !
240 DO  I=KP,M
     SUM=0.
     DO  J=KP,N
	SUM=SUM+A(KK,J)*A(I,J)
     enddo
     DO  J=KP,N
	A(I,J)=A(I,J)-SUM*A(KK,J)
     enddo
     W(I)=-SUM*W(KK)
  enddo
  !
  !	Then calculate the new row in position KK
  !
  DO  J=KP,N
     SUM=-W(KK)*A(KK,J)
     DO I=KP,M
	SUM=SUM-A(I,KK)*A(I,J)
     enddo
     A(KK,J)=SUM/A(KK,KK)
  enddo
  !
  !	AND REVISE THE COLUMN IN POSITION KK
  !
  SUM=1.-W(KK)**2
  DO  I=KP,M
     SUM=SUM-A(I,KK)*W(I)
     A(I,KK)=W(I)
  enddo
  A(KK,KK)=SUM/A(KK,KK)
  GOTO 230
  !
  !	RESTORE THE ROW INTERCHANGES
  !
310 DO  I=1,M
320  N1=NRW+I
     IR=int(W(N1))
     IF (I-IR) 330,350,350
330  SUM=W(N1)
     N2=NRW+IR
     W(N1)=W(N2)
     W(N2)=SUM
     DO  J=1,N
	SUM=A(I,J)
	A(I,J)=A(IR,J)
	A(IR,J)=SUM
     enddo
     GOTO 320
350  CONTINUE
  enddo
  !
  !	RESTORE THE COLUMN INTERCHANGES
  !
  DO J=1,N
360  N1=NCW+J
     IR=int(W(N1))
     IF (J-IR) 370,390,390
370  SUM=W(N1)
     N2=NCW+IR
     W(N1)=W(N2)
     W(N2)=SUM
     DO  I=1,M
	SUM=A(I,J)
	A(I,J)=A(I,IR)
	A(I,IR)=SUM
     enddo
     GOTO 360
390  CONTINUE
  enddo
  RETURN
400 FORMAT (2(A,I4))
END SUBROUTINE MB11A

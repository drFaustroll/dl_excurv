SUBROUTINE CARTCORD (I,FRACX,FRACY,FRACZ)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_ELS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Common_CONVIA

  real(float) :: CA(3),SA(3),TA,TB,TX,TY,TZ
  CA(1)=COS(CELLALPHA*PI/180.)
  CA(2)=COS(CELLBETA *PI/180.)
  CA(3)=COS(CELLGAMMA*PI/180.)
  SA(1)=SIN(CELLALPHA*PI/180.)
  SA(2)=SIN(CELLBETA *PI/180.)
  SA(3)=SIN(CELLGAMMA*PI/180.)
  TA=(CA(1)-CA(2)*CA(3))/SA(3)
  TB=SA(1)*SA(1)+SA(2)*SA(2)+SA(3)*SA(3)+2._float*CA(1)*CA(2)*CA(3)-2._float
  IF (TB.GT.0.) THEN
     TB=SQRT(TB)/SA(3)
  ELSE
     TB=0._float
  ENDIF
  TX=ACELL*FRACX+BCELL*CA(3)*FRACY+CCELL*CA(2)*FRACZ
  TY=BCELL*SA(3)*FRACY+CCELL*TA*FRACZ
  TZ=CCELL*TB*FRACZ
  UX(I)=TX
  UY(I)=TY
  UZ(I)=TZ
  RETURN
END SUBROUTINE CARTCORD

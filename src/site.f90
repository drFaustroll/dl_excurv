SUBROUTINE SITE
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_COMPAR
  Use Include_IPA
  Use Include_PA1
  !
  !	Define or modifiy a mixed site
  !
  DIMENSION VEC(10)
  CHARACTER*4 IP1,IP2,IP3
  CHARACTER OPTS(1)*11
  DATA OPTS/'(site_name)'/
  IF (KEYWORD.EQ.' ') THEN
     IF (NSITES.LE.0) RETURN
     CALL WTEXT ('CHANGE ATOM (symbol) will use these mixtures:')
     DO I=1,NSITES
        IF (ITYPEA(I).GT.0) NAMEA(I)=ELS(ITYPEA(I))
        IF (ITYPEB(I).GT.0) NAMEB(I)=ELS(ITYPEB(I))
        IF (ITYPEC(I).GT.0) NAMEC(I)=ELS(ITYPEC(I))
        WRITE (6,'(I3,2X,A,3(1X,A,1X,''['',I2,'']'',F5.2))')  &
             I,ELS(-I),NAMEA(I),ITYPEA(I),PERCA(I),NAMEB(I),ITYPEB(I), &
             PERCB(I),NAMEC(I), ITYPEC(I),PERCC(I)
     ENDDO
     RETURN
  ENDIF
  CALL FINDOPT (1,OPTS,IOPT,0,'none',*290)
  IF (NSITES.GT.0) THEN
     DO I=1,NSITES
        IF (KEYWORD.EQ.ELS(-I)) GOTO 10
     ENDDO
  ENDIF
  NSITES=NSITES+1
  I=NSITES
10 ELS(-I)=KEYWORD
  IF (LRIS.EQ.0) CALL WTEXT ('Enter definition of site '//ELS(-I))
  ITYPEA(I)=0
  NAMEA(I)=' '
  PERCA(I)=0.
  ITYPEB(I)=0
  NAMEB(I)=' '
  PERCB(I)=0.
  ITYPEC(I)=0
  NAMEC(I)=' '
  PERCC(I)=0.
11 IF (LRIS.EQ.0) CALL WTEXT ('Enter Component A and proportion')
  CALL CREAD (IP1,IP2,IP3,IDUM,IC,PERCA(I),*11,*280)
  IF (IP1.EQ.' '.OR.IC.EQ.0) GOTO 11
  VEC(1)=0.
  ITYPEA(I)=EXPRESS(IP1,IC,VEC)
  IF (IC.EQ.0) THEN
     DO J=-10,103
        IF (IP1.EQ.ELS(J)) THEN
           ITYPEA(I)=J
           GOTO 101
        ENDIF
     ENDDO
     CALL ERRMSG ('Invalid component',*11)
  ENDIF
101 NAMEA(I)=ELS(ITYPEA(I))
12 IF (LRIS.EQ.0) CALL WTEXT ('Enter Component B and proportion')
  CALL CREAD (IP1,IP2,IP3,IDUM,IC,PERCB(I),*12,*280)
  IF (IP1.EQ.' '.OR.IC.EQ.0) GOTO 12
  VEC(1)=0.
  ITYPEB(I)=EXPRESS(IP1,IC,VEC)
  IF (IC.EQ.0) THEN
     DO J=-10,103
        IF (IP1.EQ.ELS(J)) THEN
           ITYPEB(I)=J
           GOTO 102
        ENDIF
     ENDDO
     CALL ERRMSG ('Invalid component',*12)
  ENDIF
102 NAMEB(I)=ELS(ITYPEB(I))
13 IF (LRIS.EQ.0) CALL WTEXT ('Enter Component C and proportion')
  CALL CREAD (IP1,IP2,IP3,IDUM,IC,PERCC(I),*13,*280)
  IF (IP1.EQ.' '.OR.IC.EQ.0) GOTO 13
  VEC(1)=0.
  ITYPEC(I)=EXPRESS(IP1,IC,VEC)
  IF (IC.EQ.0) THEN
     DO J=-10,103
        IF (IP1.EQ.ELS(J)) THEN
           ITYPEC(I)=J
           GOTO 103
        ENDIF
     ENDDO
     CALL ERRMSG ('Invalid component',*13)
  ENDIF
103 NAMEC(I)=ELS(ITYPEC(I))
280 CALL FLIM (*290)
290 RETURN
END SUBROUTINE SITE

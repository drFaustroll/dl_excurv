Module Common_FMAT
!      COMMON /FMAT/ FNEW(9,12,125),FTERM(12,9,125),FMAT(12,12,125),F3(9,
!     112,39),FK(9,9,25)
!      COMPLEX FNEW,FTERM,FMAT,F3,FK
Use definition
implicit none
private
complex(imag), dimension(:,:,:), public ::  FNEW( 9,12,125)
complex(imag), dimension(:,:,:), public :: FTERM(12, 9,125)
complex(imag), dimension(:,:,:), public ::  FMAT(12,12,125)
complex(imag), dimension(:,:,:), public ::    F3( 9,12, 39)
complex(imag), dimension(:,:,:), public ::    FK( 9, 9, 25)
end Module Common_FMAT

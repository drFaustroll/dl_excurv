FUNCTION FPVAL (CHSTR,ERR)
  !======================================================================C
  Use Definition
  !
  !	Convert a character string to a floating point number. J.W.Campbell.
  !
  !	CHSTR (I) CHARACTER STRING CONTAINING THE FLOATING POINT
  !	          NUMBER (NO SPACES ALLOWED IN STRING)
  !	ERR (O)   FLAG =.FALSE. CONVERSION OK, =.FALSE. SYNTAX ERROR
  !
  !	FPVAL returns the converted value (0. if syntax error)
  !
  CHARACTER*(*)CHSTR
  CHARACTER*14 DIG
  LOGICAL ERR,END1
  real(float)  X
  DATA DIG/'E0123456789.+-'/
  FPVAL=0.0
  END1=.FALSE.
  ERR =.FALSE.
  IEXP=0
  M1=2
  M2=14
  I=0
  IMAX=LEN(CHSTR)
  !
  !	Interpret number
  !
10 S=1.0
  IDIG=0
  IFDEC=0
  X=0.0
20 I=I+1
  IF (I.GT.IMAX) THEN
     END1=.TRUE.
     IF (IEXP.EQ.1) GOTO 40
     GOTO 30
  ENDIF
  J=INDEX(DIG(M1:M2),CHSTR(I:I))
  IF (J.EQ.0) GOTO 50
  J=J+M1-1
  M1=1+IEXP
  !
  !	Digit
  !
  IF (J.GE.2.AND.J.LE.11) THEN
     IDIG=IDIG+IFDEC
     X=10.0*X+J-2
     IF (M2.GT.12) M2=12
     GOTO 20
     !
     !	Decimal point
     !
  ELSE IF (J.EQ.12) THEN
     IF (IEXP.EQ.1) GOTO 50
     IFDEC=1
     M2=11
     GOTO 20
     !
     !	Sign
     !
  ELSE IF (J.GE.13) THEN
     IF (J.EQ.14) S=-1.0
     M2=12
     GOTO 20
     !
     !	Exponent
     !
  ELSE
     IEXP=1
     M1=2
     M2=14
  ENDIF
  !
  !	Evaluate number
  !
30 FPVAL=S*X/(10.**IDIG)
  IF (END1) RETURN
  GOTO 10
  !
  !	Evaluate and apply exponent
  !
40 IEX=NINT(S*X)
  IF (ABS(IEX).GT.37) THEN
     IEX=SIGN(37,IEX)
     CALL ERRMSGI ('Exponent error - to ',IEX,*45)
  ENDIF
45 FPVAL=FPVAL*10.**IEX
  RETURN
  !
  !	Error condition
  !
50 ERR=.TRUE.
  FPVAL=0.0
END FUNCTION FPVAL

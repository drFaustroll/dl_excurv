SUBROUTINE PLIST
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_TL
  Use Common_F
  Use Common_VAR
  Use Index_IGF
  Use Common_PMTX
  Use Common_UPU
  Use Index_INDS
  Use Common_DISTANCE
  Use Include_IPA
  Use Include_PA1
  !
  !	Plot parameter table on graphs
  !
  CHARACTER*4 IP1,K2,KP3
  LOGICAL ALL
  !
  !	Prepare and output parameter tables
  !
  !	IGF(9)=4, No parameter table (EXPLOT needs parameters if VFORMAT=2)
  !
  ALL=IGF(2).EQ.2.AND.IGF(11).EQ.2
  IF (IGF(9).EQ.4.AND..NOT.ALL) RETURN
  !
  !	ITB determines table postion, 0 top or 23 bottom
  !
  ITB=1
  IF (USED3.AND.IGF(ITNUM).NE.5) THEN
     IF (IGF(2).EQ.1) THEN
        ITB=21
        IF (USED4) RETURN
     ENDIF
  ENDIF
  IGF9=IGF(9)
  IF (ALL) IGF(9)=1
  CALL PLPAR (ITB)
  IGF(9)=IGF9
  QDQ=.FALSE.
  RETURN
END SUBROUTINE PLIST

SUBROUTINE SORTTAB (AMAG,N,INDEX)
  !======================================================================C
  DIMENSION AMAG(*),INDEX(*)
  IF (N.LE.0) RETURN
  DO  I=1,N
     INDEX(I)=I
  enddo
  L=N/2+1
  IR=N
  DO  M=1,1000000
     IF (L.GT.1) THEN
        L=L-1
        RRA=AMAG(L)
        IRRA=INDEX(L)
     ELSE
        RRA=AMAG(IR)
        IRRA=INDEX(IR)
        AMAG(IR)=AMAG(1)
        INDEX(IR)=INDEX(1)
        IR=IR-1
        IF (IR.EQ.1) THEN
           AMAG(1)=RRA
           INDEX(1)=IRRA
           RETURN
        ENDIF
     ENDIF
     I=L
     J=L*2
     IF (J.LE.IR) THEN
        DO  K=1,1000000
           IF (J.LT.IR) THEN
              IF (AMAG(J).LT.AMAG(J+1)) J=J+1
           ENDIF
           IF (RRA.LT.AMAG(J)) THEN
              AMAG(I)=AMAG(J)
              INDEX(I)=INDEX(J)
              I=J
              J=J+I
           ELSE
              J=IR+1
           ENDIF
           IF (J.GT.IR) GOTO 30
        enddo
     ENDIF
30   AMAG(I)=RRA
     INDEX(I)=IRRA
  enddo
  RETURN
END SUBROUTINE SORTTAB

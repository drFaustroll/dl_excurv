SUBROUTINE WTEXTI (STR,II)
  !======================================================================C
  !
  !	Subroutine to output a text string and integer to the terminal and
  !	if required to the output file using the subroutine 'wtext'
  !
  ! PARAMETERS
  !
  !	   STR (I)    TEXT STRING TO BE OUTPUT (TYPE = CHARACTER)
  !	              MAX OF 65 CHARACTERS IN LENGTH (ANY EXCESS
  !	              CHARACTERS WILL BE LOST)
  !	    II (I)    THE INTEGER VALUE TO BE OUTPUT FOLLOWING THE
  !	              TEXT STRING (NO SPACES INSERTED)
  !
  CHARACTER*(*) STR,STREND
  CHARACTER*15 VALSTR
  CHARACTER*80  TXT
  NSTR=1
  GOTO 10
  ENTRY WTEXTIT (STR,II,STREND)
  NSTR=2
10 CONTINUE
  L=LEN(STR)
  TXT=STR
  VALSTR=' '
  WRITE(VALSTR,40)II
  DO I=1,15
     I1=I
     IF (VALSTR(I:I).NE.' ') GOTO 30
  enddo
30 LL=64+I1
  L=MIN0(L,LL)
  TXT(L+1:)=VALSTR(I1:15)
  IF (NSTR.EQ.2) THEN
     LL=NCSTR(TXT)
     TXT(LL+1:)=STREND
  ENDIF
  CALL WTEXT (TXT)
40 FORMAT(I15)
  RETURN
END SUBROUTINE WTEXTI

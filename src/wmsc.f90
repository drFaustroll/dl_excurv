SUBROUTINE WMSC (KSFCH)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Index_INDS
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  Use Include_XA
  !
  !	Write a multiple scattering output file
  !
  CHARACTER*2 KSFCH
  WRITE (PLOTFILE,30) KSFCH,EF0(0),VPI0(0),AFAC0(0),IDLMAX,ITLMAX
  DO  I=1,NS
     IF (NU(I).LT.1) cycle
     WRITE (PLOTFILE,60) I,NU(I),N(I),IT(I),R(I),A(I),ANG(I)
  enddo
  WRITE (PLOTFILE,40)
  DO JE=1,NP
     ET=ENER(JE)/EC
     IF (JE.LT.JS(IEXP).OR.JE.GT.JF(IEXP)) THEN
        IF (IOMAX.LE.3) THEN
           WRITE (PLOTFILE,50) ET,0.,0.,0.,0.
        ELSE
           WRITE (PLOTFILE,50) ET,0.,0.,0.,0.,0.,0.
        ENDIF
     ELSE
        IE=JE-JS(IEXP)+1
        SUM=XD(IE,IEXP)+XT(IE,IEXP)+X4(IE,IEXP)+X5(IE,IEXP)
        SG=XABT(JE,1)-SUM
        IF (IOMAX.LE.3) THEN
           WRITE (PLOTFILE,50) ET,SUM,SG,XD(IE,IEXP),XT(IE,IEXP),XABT(JE,IEXP)
        ELSE
           WRITE (PLOTFILE,50) ET,SUM,SG,XD(IE,IEXP),XT(IE,IEXP),X4(IE,IEXP),X5(IE,IEXP),XABT(JE,IEXP)
        ENDIF
     ENDIF
  enddo
  RETURN
  !
30 FORMAT (' &PT'/1X,A2,': EF',F6.2,' VPI',F6.2,' AFAC',F5.2,' DLMAX',I2,' TLMAX',I2)
40 FORMAT (' &END'/5X,'ENER',3X,'MS',10X,'SINGLE',6X,'DOUBLE',6X,'TRIPLE',6X,'TOTAL')
50 FORMAT (1X,F8.3,7E12.4)
60 FORMAT (I3,'(',I1,'):N',I3,' T',I2,' R',F6.2,' A',F6.3,' ANG',F5.0)
END SUBROUTINE WMSC

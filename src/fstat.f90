SUBROUTINE FSTAT (IUN,IRET,DSN)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  !
  !	Determines whether a unit has been allocated to a specific
  !	File. If so the file name is listed. a return code is also set
  !
  !	    IUN (I)   FORTRAN UNIT NUMBER
  !	    IRET (O)   FLAG = 0, FILE NOT ALLOCATED
  !	                   = 1, FILE ALLOCATED
  !	    DSN (O)   CHARACTER*60 FILE NAME. RETURN
  !	              AS BLANK IF NOT AVAILABLE.
  !
  CHARACTER*60 DSN,FNAM
  LOGICAL OPN
  !
  !	Examine file status
  !
  IRET=0
  DSN=' '
  !-st	OUTTERM=OUTTERM
  INQUIRE(UNIT=IUN,IOSTAT=IOS,NAME=FNAM,OPENED=OPN,ERR=10)
  IF (IOS.GT.0) GOTO 10
  IF (.NOT.OPN) GOTO 10
  WRITE (OUTTERM,20) FNAM
  DSN=FNAM
  IRET=1
10 RETURN
  !
20 FORMAT(' FILE ALLOCATED: ',A)
END SUBROUTINE FSTAT

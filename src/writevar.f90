SUBROUTINE WRITEVAR
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_ICF
  Use Index_INDS
  Use Include_PA1

  real(float) CFAC
  call eqv_convia
  J=1
10 IF (J.GE.inds(INDN).AND.J.LT.inds(INDXE)) THEN
     LEN=IPARNS+1
  ELSEIF (J.GE.inds(INDXE).AND.J.LT.inds(INDLMAX)) THEN
     LEN=IPARNP
  ELSE
     LEN=1
  ENDIF
  CFAC=CONVIA(ICF(J))
  WRITE (OUTFILE,20) IVARS(J),PA1(J),RMS(J),RMX(J),ICF(J)
  J=J+LEN
  IF (J.LE.IPARNPARAMS) GOTO 10
  RETURN
20 FORMAT (1X,A,3E12.4,I3)
END SUBROUTINE WRITEVAR

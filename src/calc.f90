SUBROUTINE CALC
  !======================================================================C
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_COMPAR
  CHARACTER*11 :: OPTS(2)
  LOGICAL      :: CENTRAL
  DATA OPTS/'POTENTIALS','PHASESHIFTS'/
  !
  !	Command CALC - calculate potentials or phaseshifts.
  !
  IF (INTOPT.EQ.0.OR.INTOPT.GT.IPARNP) THEN
     IS=1
     IF=IPARNP
     IF (iset(IATABS).EQ.1) IS=0
     CENTRAL=.TRUE.
  ELSE
     IS=IABS(INTOPT)
     IF=IABS(INTOPT)
     CENTRAL=INTOPT.LT.0
  ENDIF
  CALL FINDOPT (2,OPTS,IOPT,1,'Atom Number - default All',*30)
  GOTO (10,20) IOPT
10 CALL CALCPOT (.FALSE.,IS,IF,CENTRAL)
  RETURN
20 CALL CALCPHASE (.FALSE.,IS,IF,CENTRAL)
30 RETURN
END SUBROUTINE CALC

MODULE Common_DISTANCE

  !      COMMON /DISTANCE/ DISTANCES(0:IPARNS,0:IPARNS),WEIGHTINGS(0:IPARNS
  !     1,0:IPARNS),BONDANGS(0:IPARNS,0:IPARNS),BAWT(0:IPARNS,0:IPARNS),DIS
  !     2TVARS(0:IPARNS,0:IPARNS),WEIGHTVARS(0:IPARNS,0:IPARNS),BONDANGVARS
  !     3(0:IPARNS,0:IPARNS),BAWTVARS(0:IPARNS,0:IPARNS),BONDS(IPARNS,0:IPA
  !     4RNS),BONDVARS(0:IPARNS,0:IPARNS)
  !      CHARACTER*6 DISTVARS,WEIGHTVARS,BONDANGVARS,BAWTVARS,BONDVARS

  Use Definition
  Use Parameters
  implicit none
  private
  real(dp),    dimension(:,:), public ::  DISTANCES(0:IPARNS,0:IPARNS)
  real(dp),    dimension(:,:), public :: WEIGHTINGS(0:IPARNS,0:IPARNS)
  real(dp),    dimension(:,:), public ::   BONDANGS(0:IPARNS,0:IPARNS)
  real(dp),    dimension(:,:), public ::       BAWT(0:IPARNS,0:IPARNS)
  character*6, dimension(:,:), public ::   DISTVARS(0:IPARNS,0:IPARNS)
  character*6, dimension(:,:), public :: WEIGHTVARS(0:IPARNS,0:IPARNS)
  character*6, dimension(:,:), public ::BONDANGVARS(0:IPARNS,0:IPARNS)
  character*6, dimension(:,:), public ::   BAWTVARS(0:IPARNS,0:IPARNS)
  real(dp),    dimension(:,:), public ::        BONDS(IPARNS,0:IPARNS)
  character*6, dimension(:,:), public ::   BONDVARS(0:IPARNS,0:IPARNS)
end MODULE Common_DISTANCE

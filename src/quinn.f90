SUBROUTINE QUINN (WP,X,RS,EI)
  !======================================================================C
  !
  !	Calculate the QUINN contribution to ineleastic scattering
  !	below the plasmon threshold
  !
  Use Common_convia
  !	PISQRT=DSQRT(PI)
  !	ALPHAQ=(4./(9.*PI))**(1./3.)
  ALPHAQ=.521061761*RS
  !	PFQ=PISQRT/(32.*(ALPHAQ*RS)**1.5)
  PFQ=.055389182/(ALPHAQ**1.5)
  !-st	TEMP1=ATAN(DSQRT(PI/ALPHAQ))
  !-st	TEMP2=DSQRT(ALPHAQ/PI)/(1.+ALPHAQ/PI)

  TEMP1=ATAN(SQRT(PI/ALPHAQ))
  TEMP2=SQRT(ALPHAQ/PI)/(1.+ALPHAQ/PI)

  PFQ=PFQ*(TEMP1+TEMP2)
  !	WKC=(SQRT(1.+WP)-1.)**2.
  WKC=WP+1.2/WP
  EKC=WKC+1.
  ARG=(X*X-EKC)/(.3*EKC)
  IF (ARG.LT.80.) THEN
     GAM=(PFQ/X)*(X*X-1.)**2
     F=1./(1.+EXP(ARG))
     EI=-GAM*F*.5
  ELSE
     EI=0.
  ENDIF
  RETURN
END SUBROUTINE QUINN

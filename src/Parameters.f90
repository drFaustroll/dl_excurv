MODULE Parameters
  USE Definition
  implicit none
  integer(i4b), parameter :: IPARNPOINTS   =   4000    ! 'max points'
  integer(i4b), parameter :: IPARNS        =     64    ! 'max shells'
  integer(i4b), parameter :: IPARNP        =     96    ! 'max phases' (12 before)
  integer(i4b), parameter :: IPARNSV       =     32    !  do not change:  related to EQIVALENCE in Common_IPA.f90
  integer(i4b), parameter :: IPARNPV       =     13    !  do not change:  related to EQIVALENCE in Common_IPA.f90
  integer(i4b), parameter :: IPARNVARS     =     48
  integer(i4b), parameter :: IPARLMAX      =     25    ! 'max angular momentum'
  integer(i4b), parameter :: IPARDLMAX     =     25
  integer(i4b), parameter :: IPARTLMAX     =      9
  integer(i4b), parameter :: IPARATOMS     =    512
  integer(i4b), parameter :: IPARNSPV      =     23    !  do not change:  related to EQIVALENCE in Common_IPA.f90
  integer(i4b), parameter :: IPARNSP       =      3  
  integer(i4b), parameter :: IPARPHPOINTS  =     90
  integer(i4b), parameter :: IPARXLMAX     =      9
  integer(i4b), parameter :: IPARXLOUT     =     28
  integer(i4b), parameter :: IPARXNPOINTS  =    200
  integer(i4b), parameter :: IPARXIND      =    200
  integer(i4b), parameter :: IPARXNR       =      8
  integer(i4b), parameter :: IPARXATOMS    =     48
  integer(i4b), parameter :: IPARXLM       = IPARXLOUT+IPARXLMAX             !  28 + 9  =  37
  integer(i4b), parameter :: IPARXLM1      = IPARXLM+1                       !  37 + 1  =  38
  integer(i4b), parameter :: IPARXLMAX1    = IPARXLMAX+1                     !   9 + 1  =  10
  integer(i4b), parameter :: IPARXLMAX1S   = IPARXLMAX1*IPARXLMAX1           !  10 * 10 = 100
  integer(i4b), parameter :: IPARXLOUT1S   = (IPARXLOUT+1)*(IPARXLOUT+1)     ! (28 + 1) * (28 + 1) = 841
  integer(i4b), parameter :: IPARXNH       = IPARXLMAX1S*IPARXNR             ! 100 * 8  = 800
  integer(i4b), parameter :: IPARNPARAMS   = 10+IPARNSV*(IPARNS+1)+IPARNSPV*(IPARNSP+1)+IPARNPV*IPARNP+115
  !------------------------                  10+     32*(    64+1)+      23*(      3+1)+     13*12    +115 = 2453
  integer(i4b), parameter :: IPARGSET      =     15
  integer(i4b), parameter :: IPARFTOPTS    =      8
  integer(i4b), parameter :: IPARMAP       =     15
  integer(i4b), parameter :: IPARNUNITS    =     25
  integer(I4B), parameter :: IPARWSPACE    = 500000
  integer(i4b), parameter :: IPARGRID      =    999
  integer(i4b), parameter :: IPARSETOPTS   =     21       
  integer(i4b), parameter :: IPARCONVEX    =      1
  integer(i4b), parameter :: IPARAL        =     20
  integer(i4b), parameter :: IPARNCLUS     =      4
  integer(i4b), parameter :: IPAREXNPOINTS =     50
  integer(i4b), parameter :: IPARPATH      =    500    ! 'max paths'
  integer(i4b), parameter :: IPARPATH1     =  10000    ! 'max path-numbers'
  integer(i4b), parameter :: IPARXRD       =     10
  integer(i4b), parameter :: IPARNA        =    400
!  integer(i4b), parameter :: IPART=(IPARTLMAX+1)*(IPARTLMAX+1)
  integer(i4b), parameter :: IPARNP1=IPARNP+1
END MODULE Parameters

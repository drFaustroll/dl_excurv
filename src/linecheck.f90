SUBROUTINE LINECHECK (*)
  !======================================================================C
  Use Parameters
  Use Common_PMTX
  Use Common_GRAPHICS
  IF (QDQ) THEN
     CALL FQQGETGP2 (TX,TY)
  ELSE
     CALL FGETGP2 (TX,TY)
  ENDIF
  IF (TY.GE.GYMIN-1.5) THEN
     CALL TYPECS ('etc.')
     RETURN 1
  ENDIF
  RETURN
END SUBROUTINE LINECHECK

SUBROUTINE SYSHLP (TEXT)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  !
  !	List help details for a requested command for 'excurv94'
  !
  !	   TEXT (I)   CHARACTER STRING CONTAINING THE NAME OF THE COMMAND
  !	              (A BLANK STRING MAY BE GIVEN, IN WHICH CASE THE
  !	               SUBROUTINE SHOULD REQUEST THE INPUT OF THE
  !	               REQUIRED COMMAND NAME)
  !
  PARAMETER(MXLB=20,MXB=25)
  LOGICAL ERR,END1,RD,FIRST
  CHARACTER*80 TXT(MXLB,MXB),SAVTXT
  CHARACTER*12 DSN
  DIMENSION NCH(MXLB,MXB)
  CHARACTER CMD*12,AA*2,HEAD*4,A*1,CM*4
  CHARACTER*(*) TEXT
  !
  !	Set or read help item and open help file
  !
  CALL WTEXT ('The internal help system is unfortunately not operational.')
  CALL WTEXT ('Some information on the project can be found at this URL:')
  CALL WTEXT ('http://ccpforge.cse.rl.ac.uk/gf/project/excurv/')
  RETURN
  !
  IF (TEXT.NE.' ') THEN
     CMD=TEXT
  ELSE
     CALL WTEXT ('ENTER: INTRO,DATA,PARAMS,COMMANDS,SYS,HELP')
     CALL WTEXT ('       OR A COMMAND NAME')
     READ(INTERM,170)CMD
     CALL UPCASE(CMD)
     IF (CMD.EQ.'='.OR.CMD.EQ.' ')RETURN
  ENDIF
  IF (N.LT.50) DSN = 'excurv88.hlp'
  CALL FILEOPEN (DSN,HELPFILE,'FORMATTED',' ',*150)
  IL=0
  CALL NEXFLD(CMD,IL,M1,M2,*10)
  IF (M2.GT.M1+3)M2=M1+3
  NCC=M2-M1+1
  CM=CMD(M1:M2)
  REWIND HELPFILE
  !
  !	Find and store requested help information
  !
10 READ (HELPFILE,180,END=160,ERR=160) AA,HEAD
  IF (AA.NE.'**') GOTO 10
  IF (HEAD(1:NCC).NE.CM(1:NCC)) GOTO 10
  MB=0
  IMIN=1
  FIRST=.TRUE.
20 MB=MB+1
30 IF (MB.GT.MXB) GOTO 70
  RD  =.TRUE.
  END1=.FALSE.
I1=IMIN
IF (IMIN.EQ.2) THEN
   TXT(1,MB)=SAVTXT
   NCH(1,MB)=NSAV
   IMIN=1
ENDIF
DO  I=I1,MXLB
   IL=I
   IF (RD) THEN
      READ (HELPFILE,170,END=60) TXT(I,MB)
      NCH(I,MB)=NCSTR(TXT(I,MB))
      A=TXT(I,MB)(1:1)
      AA=TXT(I,MB)(1:2)
      IF (AA.EQ.'**') THEN
         IF (IL.EQ.1) GOTO 70
      END1= .TRUE.
      RD  = .FALSE.
      GOTO 40
   ENDIF
   IF (A.EQ.'<') THEN
      IF (FIRST) THEN
         FIRST=.FALSE.
      ELSE
         SAVTXT=TXT(I,MB)
         NSAV=NCH(I,MB)
         IMIN=2
         IF (I.EQ.1) GOTO 30
         RD=.FALSE.
         GOTO 40
      ENDIF
   ENDIF
   IF (A.EQ.'>') THEN
      READ (HELPFILE,170,END=60)
      RD=.FALSE.
      GOTO 40
   ENDIF
   IF (A.EQ.'.') THEN
      IF (NCH(I,MB).EQ.1) GOTO 40
   ENDIF
   cycle
ENDIF
40 TXT(I,MB)=' '
NCH(I,MB)=1
enddo
IF (.NOT.END1) GOTO 20
60 IF (IL.GT.1) GOTO 80
70 MB=MB-1
!
!	Display help data in blocks
!
! <CR> LISTS NEXT BLOCK, "-N" GO BACK N BLOCKS, "+N" ADVANCE N BLOCKS
! "Q" OR "=" TO QUIT. "-" HAS THE SAME EFFECT AS "-1" AND "+" HAS THE
! SAME EFFECT AS "+1"
!
80 NB=1
90 DO  I=1,MXLB
WRITE (OUTTERM,190)TXT(I,NB)(1:NCH(I,NB))
enddo
110 CALL WTEXT (' ')
IF (NB.EQ.MB) THEN
CALL WTEXT ('*END*>>>')
ELSE
CALL WTEXT ('>>>')
ENDIF
READ(INTERM,170)CMD
CALL UPCASE(CMD)
IL=0
CALL NEXFLD(CMD,IL,M1,M2,*130)
IF (CMD(M1:M1).EQ.'=') GOTO 140
IF (M1.EQ.M2) THEN
IF (CMD(M1:M1).EQ.'-') THEN
   IB=-1
   GOTO 120
ENDIF
IF (CMD(M1:M1).EQ.'+') THEN
   IB=1
   GOTO 120
ENDIF
ENDIF
XX=FPVAL(CMD(M1:M2),ERR)
IF (ERR) GOTO 110
IB=NINT(XX)
120 NB=NB+IB
IF (NB.LT.1)NB=1
IF (NB.GT.MB)NB=MB
GOTO 90
130 IF (NB.GE.MB) GOTO 140
NB=NB+1
GOTO 90
140 CLOSE (HELPFILE)
RETURN
!
!	Error conditions
!
150 CALL ERRMSG ('Help file could not be opened',*161)
160 CALL ERRMSG ('Help not available for: '//CMD,*161)
161 RETURN
170 FORMAT(A)
180 FORMAT(A2,A4)
190 FORMAT(' ',A)
END SUBROUTINE SYSHLP

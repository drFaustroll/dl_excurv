SUBROUTINE CREAD (IP1,IP2,IP3,INT,IC,V,*,*)
  !======================================================================C
  Use Parameters
  Use Common_risch
  Use Common_B1
  Use Common_B2
  Use Common_PLFLAG
  Use Common_RCBUF
  !
  ! 	Additional entry points: 'AREAD' and 'SREAD'
  !
  !	Read command line
  !
  !	    IP1 (O)   Returns the first parameter as a character string
  !	    IP2 (O)   Returns the 2'nd  parameter as a character string
  !	    IP3 (O)   Returns the 3'rd  parameter as a character string
  !	    INT (O)   Returns the integer equivalent of the last
  !	              parameter found (nb. subroutine only looks for a
  !	              max of 3 parameters)
  !	     IC (O)   Returns 1 if last param was a number otherwise 0
  !	      V (O)   Real value equivalent of last parameter found
  !
  !	ENTRY AREAD (ITYP,DSN,LLD,*,*)
  !
  !	   ITYP (I)   FILE TYPE FLAG =1      INPUT SPECTRUM
  !	                             =2      INPUT PARAMETERS
  !	                             =3      INPUT PHASESHIFTS
  !	                             =4      INPUT COMMANDS
  !	                             =5      HELP FILE
  !	                             =6      INPUT CHARGE DENSITY
  !	                             =7      INPUT MAP
  !	                             =11     OUTPUT SPECTRUM
  !	                             =12     OUTPUT FFT
  !	                             =13     OUTPUT PARAMETERS
  !	                             =14     OUTPUT PLOT
  !	                             =15     OUTPUT FOR BACKGROUND JOB
  !	                             =20-25  OUTPUT PHASESHIFTS
  !	    DSN (O)   FILENAME
  !	    LLD (O)   NUMBER OF CHARACTERS IN FILE NAME (0 IF ABSENT)
  !
  !
  !	ENTRY SREAD (*,*)   Returns command string in BUF in common /RCBUF/
  !	                    without case change.
  !
  !  RETURN 1  No parameters found, special option found and processed
  !	       or command ignored after an error
  !
  !  RETURN 2  '=' Given to skip this input
  !
  !  ';' Acts as a record separator except in comments. the remainder
  !	 of the record is stored for the next call to cread or aread.
  !
  !	Special options (both entries)
  !
  !  !COMMENT                             COMMENT RECORD
  !  HELP [NAME]                          REQUEST HELP
  !  %COMMAND_FILE_NAME                   READ NEXT COMMANDS FROM THIS
  !	                                  COMMAND FILE
  !  *SYSTEM_COMMAND                      REQUEST A SYSTEM COMMAND
  !  ////                                 EMERGENCY STOP
  !  =                                    SKIP COMMAND
  !
  DIMENSION     :: VEC(10)
  LOGICAL       :: MATCH
  CHARACTER*25  :: HLPSTR
  CHARACTER*(*) :: IP1,IP2,IP3,DSN
  DATA ICMD/4/
  !
  !	Initialise parameters for a call to 'cread'
  !
  NPROMPT=NPROMPT+1
  IF (NPROMPT.GT.1000) THEN
     CALL WTEXT ('Too many carriage returns')
     CALL EXITQ
     STOP
  ENDIF
  IP1=' '
  IP2=' '
  IP3=' '
  INT=0
  V=0.0
  IC=1
  IENT=1
  GOTO 10
  !
  ENTRY AREAD (ITYP,DSN,LLD,*,*)
  !
  !	Initialisations
  !
  DSN=' '
  LLD=0
  IENT=2
  GOTO 10
  !
  ENTRY SREAD (*,*)
  IENT=3
  !
  !	Restart from here - get string from terminal, command file or
  !	previous read
  !
10 IF (LRIS.GT.0) GOTO 50
  IF (LMDSN.GT.0) GOTO 30
  IF (BACKGROUND) STOP 'Cant read from terminal'
  !
  !	Read from terminal
  !
20 READ (INTERM,140,END=130,ERR=130)BUF
  NNC=NCSTR(BUF)
  NCM=MIN0(NNC,75)
  WRITE (LOGFILE,150)BUF(1:NCM)
  IF (NNC.GT.75)WRITE (LOGFILE,160)BUF(76:80)
  IMAX=80
  LSTBUF=BUF
  GOTO 60
  !
  !	Read from command file
  !
30 BUF=' '
  READ(ICMD,140,END=40,ERR=40)BUF
  WRITE (LOGFILE,170)BUF(1:NCSTR(BUF))
  IMAX=120
  LSTBUF=' '
  GOTO 60
  !
  !	End of command file reached
  !
40 CLOSE (COMFILE)
  MDSN=' '
  LMDSN=0
  CALL WTEXT ('End of command file, enter command')
  GOTO 20
  !
  !	Read from stored string
  !
50 BUF=RIS
  IMAX=LRIS
  RIS=' '
  LRIS=0
  LSTBUF=' '
  !
  !	Process text buffer
  !
  !	Look for comment line
  !
60 K=INDEX(BUF,'!')
  IF (K.GT.0) THEN
     IF (K.EQ.1)RETURN 1
     IF (BUF(1:K-1).EQ.' ')RETURN 1
  ENDIF
  !
  !	Look for end of record marker and save remainder of record if found
  !
  K=INDEX(BUF,';')
  IF (K.GT.0.AND.K.LT.IMAX) THEN
     LRIS=IMAX-K
     RIS(1:LRIS)=BUF(K+1:IMAX)
     BUF(K:IMAX)=' '
  ENDIF
  !
  !	Get first field of current buffer
  !
  IL=0
  CALL NEXFLD(BUF,IL,M1,M2,*130)
  !
  !	Look for emergency stop
  !
  IF (BUF(M1:M1+3).EQ.'////') STOP
  !
  !	Look for skip command code '='
  !
  IF (BUF(M1:M2).EQ.'=')RETURN 2
  !
  !	Look for command file name
  !
  IF (BUF(M1:M1).EQ.'%') THEN
     IF (M1.EQ.M2) THEN
        CALL ERRMSG ('Invalid syntax: '//BUF(M1:M2),*130)
        RETURN 1
     ENDIF
     CALL FNAME (BUF(M1+1:M2),MDSN,KMDSN,*80)
     CALL FILEOPEN (MDSN,COMFILE,'FORMATTED','dat',*70)
     GOTO 90
70   LMDSN=0
     MDSN=' '
80   RETURN 1
90   LMDSN=KMDSN
     GOTO 10
  ENDIF
  !
  !	Look for system command
  !
  IF (BUF(M1:M1).EQ.'*') THEN
     IF (PLCMD) THEN
        CALL ALPHAM
     ENDIF
     IF (M1.EQ.M2) THEN
        CALL ERRMSG ('Invalid syntax: '//BUF(M1:M2),*130)
        RETURN 1
     ENDIF
     CALL SYSCMD (BUF(M1+1:IMAX))
     RETURN 1
  ENDIF
  !
  !	Look for 'help'
  !
  HLPSTR=BUF(M1:M2)
  CALL UPCASE(HLPSTR)
  IF (MATCH(HLPSTR(1:4),'HELP')) THEN
     IF (PLCMD) THEN
        CALL ALPHAM
     ENDIF
     CALL NEXFLD(BUF,IL,M1,M2,*100)
     CALL UPCASE(BUF(M1:M2))
     CALL SYSHLP(BUF(M1:M2))
     RETURN 1
100  CALL SYSHLP(' ')
     RETURN 1
  ENDIF
  !
  !	If this point reached, process as a normal command
  !
  IF (IENT.EQ.2) GOTO 120
  IF (IENT.EQ.3) RETURN
  !
  !	For entry 'cread'
  !
  CALL UPCASE(BUF)
  NP=1
  IP1=BUF(M1:M2)
  CALL NEXFLD(BUF,IL,M3,M4,*110)
  NP=2
  M1=M3
  M2=M4
  IP2=BUF(M3:M4)
  CALL NEXFLD(BUF,IL,M3,M4,*110)
  NP=3
  M1=M3
  M2=M4
  IP3=BUF(M3:M4)
  !
  !	Try and process last parameter as a number
  !
110 VEC(1)=0.
  XX=EXPRESS(BUF(M1:M2),IC,VEC)
  IF (IC.NE.0) THEN
     V=XX
     IF (V.LE.2.1307E9) INT=NINT(V)
  ENDIF
  RETURN
  !
  !	For entry 'aread'
  !
120 CONTINUE
  CALL FNAME (BUF(M1:M2),DSN,LLD,*130)
  RETURN
  !
  !	End of file on reads
  !
130 RETURN 1
140 FORMAT(A)
150 FORMAT(' REC> ',A)
160 FORMAT('      ',A)
170 FORMAT(' CMD> ',A)
END SUBROUTINE CREAD

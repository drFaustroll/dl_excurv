SUBROUTINE SPACE (NIN)
  !======================================================================C
  !-st	Use Parameters
  Use Definition
  Use Common_PMTX
  IF (QDQ) THEN
     CALL FQQRMOVE2(.5_dp*real(NIN, Kind = dp),0.0_dp)
  ELSE
     CALL FRMOVE2(.5_dp*real(NIN, Kind = dp),0.0_dp)
  ENDIF
  RETURN
END SUBROUTINE SPACE

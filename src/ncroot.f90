FUNCTION NCROOT (STRING)
  !======================================================================C
  CHARACTER*(*) STRING
  DO  NCROOT=LEN(STRING),1,-1
     IF (STRING(NCROOT:NCROOT).EQ.'.'.OR.STRING(NCROOT:NCROOT).EQ.'/') RETURN
  enddo
  NCROOT=NCSTR(STRING)
  RETURN
END FUNCTION NCROOT

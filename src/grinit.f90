SUBROUTINE GRINIT
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PLFLAG
  Use Index_IGF
  Use Common_PMTX

  INTEGER BLACK, GREEN
  PARAMETER (BLACK = 0, GREEN = 2)
  DIMENSION ICOD(6)
  DATA ICOD/27,91,63,51,56,104/
  IF (.NOT.PLFLAG) CALL PAPER
  IF (JJ.EQ.0.OR.JJ.EQ.1.OR.JJ.EQ.5) THEN
     IF (IGF(2).EQ.1) THEN
        IF (IGF(ITNUM).EQ.5) CALL X11_RESIZE
        CALL FCOLOR (BLACK)
        IF (IGF(ITNUM).EQ.4) THEN
           DO  I=1,6
	      WRITE (OUTTERM,20) char(ICOD(I))
           enddo
           CALL XTCLEAR
        ELSE
           CALL FCLEAR
        ENDIF
        CALL FCOLOR (GREEN)
        IF (IGF(ITNUM).EQ.5) THEN
           CALL QQX11_RESIZE
           CALL FQQCOLOR (BLACK)
           CALL FQQCLEAR
           CALL FQQCOLOR (7)
        ENDIF
     ENDIF
  ENDIF
  USED3=.FALSE.
  USED4=.FALSE.
  PLCMD=.TRUE.
20 FORMAT (A,$)
  RETURN
END SUBROUTINE GRINIT

SUBROUTINE AXES1 (IMIN,IMAX,FP,ISTEP,I1,NXY,XDAT,YDAT,LIMAG,NXDAT,NYDAT,IMAG1,ISIZERANGE,ONEZERO,GBOG)
  !===========================================================================================================
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Common_GRAPHICS
  !       
  !	Calculate tick positions and generate axes for graphs
  !       
  DIMENSION :: XDAT(*),YDAT(*)
  CHARACTER :: TSTRING*20,STRING*20,IMAG1*7
  LOGICAL   :: ONEZERO,GBOG
  IF (ISTEP.LT.0) RETURN
  IF (NXY.EQ.1) THEN
     CALL FTEXTJUSTIFY (17)
  ELSE
     CALL FTEXTJUSTIFY (10)
  ENDIF
  J=IMIN
  LASTPLACE=0
  DO LOOP2=1,20
     X=real(J,kind=dp)/FP
     ICX=I1
     IF (J.EQ.0) ICX=0
     WRITE (TSTRING,'(I20)') J
     DO IS=1,20
        IF (TSTRING(IS:IS).NE.' ') GOTO 111
     ENDDO
     IS=20
111  LSTR=21-IS
     STRING=TSTRING(IS:)
     IF (ICX) 7,8,9
9    NDIGITS=LSTR
     IF (TSTRING(IS:IS).EQ.'-') NDIGITS=NDIGITS-1
     NZEROS=MAX(ICX-NDIGITS,0)
     IOLDICX=ICX
     ICX=ICX-NZEROS
     K=20
     L=LSTR+NZEROS+1
     DO LL=ICX,1,-1
        STRING(L:L)=TSTRING(K:K)
        K=K-1
        L=L-1
     ENDDO
     !	string(lstr+nzeros+1-icx:)=tstring(20-icx:)
     !	l=lstr+nzeros+1-(ioldicx-nzeros)
     IF (NZEROS.NE.0) THEN
        DO I=NZEROS,1,-1
           STRING(L:L)='0'
           L=L-1
        ENDDO
	   ENDIF
	   STRING(L:L)='.'
	   GOTO 8
 7	   STRING(LSTR+1:LSTR-ICX)='000000000'
	   GOTO 8
 8	   NDIG1=NCSTR(STRING)
	   IF (NXY.EQ.1) THEN
!       
!	X-axis annotation
!       
	      IF (NXDAT.GT.8) GOTO 888
	      NXDAT=NXDAT+1
	      XDAT(NXDAT)=X
	      IF (X.LT.GXMIN.OR.X.GT.GXMAX) GOTO 888
	      IF (GYMIN.LT.0..AND.GYMAX.GT.0..AND.IGF(2).EQ.1.AND.IGF(6).NE.3)THEN
		 Y=-YDIFF*.03
		 GBOG=.TRUE.
	      ELSE
		 Y=GYMIN-YDIFF*.015
	      ENDIF
	      IF (ONEZERO) THEN
		 X=GXMIN-XDIFF*.075
		 X=GXMIN-XDIFF*.015
	      ENDIF
	   ELSEIF (NXY.EQ.2) THEN
!       
!	Y-axis annotation
!       
	      IF (NYDAT.GT.8) GOTO 888
	      NYDAT=NYDAT+1
	      Y=X
	      YDAT(NYDAT)=Y
	      X=GXMIN-XDIFF*.015
	      IF (ONEZERO) GOTO 333
	   ENDIF
!       
!	Draw for X and Y
!       
	   CALL FMOVE2 (X,Y)
	   CALL FDRAWSTR (STRING(1:NDIG1))
 333	   ONEZERO=.FALSE.
	   J=J+ISTEP
	   IF (J.GT.IMAX) GOTO 888
           enddo
 888	CONTINUE
	CALL FTEXTJUSTIFY (0)
	IF (LIMAG.EQ.0) RETURN
	IF (NXY.EQ.1) THEN
	   X=GXMAX
	   Y=GYMIN-YDIFF*.05
	   CALL PLOTCSX (X,Y,IMAG1(1:LIMAG))
	ELSE
	   AAA=NCSTR(IMAG1(1:LIMAG))
	   AAA=AAA*CSIZE*YDIFF
	   X=.075
!       
!       PC only
!       
	   IF (JJ.GT.4) X=.150
	   CALL PLOTCSY (GXMIN-XDIFF*X,GYMAX-AAA,IMAG1(1:LIMAG))
	ENDIF
	RETURN
      END SUBROUTINE AXES1

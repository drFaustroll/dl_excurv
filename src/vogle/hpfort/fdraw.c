#include <stdio.h>
#include "../vogle.h"

/*
 * draw_
 */
void
fdraw_(x, y, z)
	float	*x, *y, *z;
{
	draw(*x, *y, *z);
}

/*
 * draw2_
 */
void
fdraw2_(x, y)
	float	*x, *y;
{
	draw2(*x, *y);
}

/*
 * rdraw_
 */
void
frdraw_(dx, dy, dz)
	float	*dx, *dy, *dz;
{
	rdraw(*dx, *dy, *dz);
}

/*
 * rdraw2_
 */
void
frdraw2_(dx, dy)
	float	*dx, *dy;
{
	rdraw2(*dx, *dy);
}

/*
 * sdraw2_
 */
void
fsdraw2_(xs, ys)
	float 	*xs, *ys;
{
	sdraw2(*xs, *ys);
}

/*
 * rsdraw2_
 */
void
frsdraw2_(dxs, dys)
	float	*dxs, *dys;
{
	rsdraw2(*dxs, *dys);
}

/*
 * setdash_
 */
void
fsetdash_(dxs)
	float	*dxs;
{
	setdash(*dxs);
}

/*
 * linestyle_
 */
void
flinestyle_(path, len)
	unsigned char	*path;
	int	len;
{
	unsigned char	buf[BUFSIZ];
	register unsigned char	*p;

	strncpy(buf, path, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	linestyle(buf);
}

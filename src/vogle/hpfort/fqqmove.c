#include "../vogle.h"

/*
 * move_
 */
void
fqqmove_(x, y, z)
	float 	*x, *y, *z;
{
	qqmove(*x, *y, *z);
}


/*
 * move2_
 */
void
fqqmove2_(x, y)
	float	*x, *y;
{
	qqmove2(*x, *y);
}

/*
 * rmove_
 */
void
fqqrmove_(dx, dy, dz)
	float	*dx, *dy, *dz;
{
	qqrmove(*dx, *dy, *dz);
}

/*
 * rmove2_
 */
void
fqqrmove2_(dx, dy)
	float	*dx, *dy;
{
	qqrmove2(*dx, *dy);
}

/*
 * smove2_
 */
void
fqqsmove2_(xs, ys)
	float 	*xs, *ys;
{
	qqsmove2(*xs, *ys);
}

/*
 * rsmove2_
 */
void
fqqrsmove2_(dxs, dys)
	float	*dxs, *dys;
{
	qqrsmove2(*dxs, *dys);
}


#include "../vogle.h"

/*
 * pushmatrix_
 */
void
fpushmatrix_()
{
	pushmatrix();
}

/*
 * popmatrix_
 */
void
fpopmatrix_()
{
	popmatrix();
}

/*
 * getmatrix_
 */
void
fgetmatrix_(mat)
	float	*mat;
{
	getmatrix(mat);
}

/*
 * loadmatrix_
 */
void
floadmatrix_(mat)
	float	*mat;
{
	loadmatrix(mat);
}

/*
 * multmatrix_
 */
void
fmultmatrix_(mat)
	float	*mat;
{
	multmatrix(mat);
}

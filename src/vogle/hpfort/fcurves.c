#include "../vogle.h"

/*
 * curvebasis_
 */
void
fcurvebasis_(basis)
	float	basis[4][4];
{
	curvebasis(basis);
}

/*
 * curveprecision_
 */
void
fcurveprecision_(nsegments)
	int	*nsegments;
{
	curveprecision(*nsegments);
}

/*
 * rcurve_
 */
void
frcurve_(geom)
	float	geom[4][4];
{
	rcurve(geom);
}

/*
 * curve_
 */
void
fcurve_(geom)
	float	geom[4][3];
{
	curve(geom);
}

/*
 * curven_
 */
void
fcurven_(n, geom)
	int	*n;
	float	geom[][3];
{
	curven(*n, geom);
}

#include "../vogle.h"

/*
 * prefposition_
 */
void
fprefposition_(x, y)
	int	*x, *y;
{
	prefposition(*x, *y);
}

/*
 * prefsize_
 */
void
fprefsize_(x, y)
	int	*x, *y;
{
	prefsize(*x, *y);
}

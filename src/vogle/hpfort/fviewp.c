#include "../vogle.h"

/*
 * pushviewport_
 */
void
fpushviewport_()
{
	pushviewport();
}

/*
 * popviewport_
 */
void
fpopviewport_()
{
	popviewport();
}

/*
 * viewport_
 */
void
fviewport_(xlow, xhigh, ylow, yhigh)
	float	*xlow, *ylow, *xhigh, *yhigh;
{
	viewport(*xlow, *xhigh, *ylow, *yhigh);
}

/*
 * getviewport_
 */
void
fgetviewport_(left, right, bottom, top)
	float	*left, *right, *bottom, *top;
{
	getviewport(left, right, bottom, top);
}


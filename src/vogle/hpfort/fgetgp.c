#include "../vogle.h"

/*
 * getgp_
 */
void
fgetgp_(x, y, z)
	float	*x, *y, *z;
{
	getgp(x, y, z);
}

/*
 * getgp2_
 */
void
fgetgp2_(x, y)
	float	*x, *y;
{
	getgp2(x, y);
}

/*
 * sgetgp2_
 */
void
fsgetgp2_(x, y)
	float	*x, *y;
{
	sgetgp2(x, y);
}

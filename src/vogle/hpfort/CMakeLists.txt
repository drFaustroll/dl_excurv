set(libsrch
  farcs.c
faspect.c
fattr.c
fbuffer.c
fclip.c
fcurves.c
fdraw.c
fdrivers.c
fgetgp.c
fgetstr.c
fmatrix.c
fmove.c
fobjects.c
fpatches.c
fpoints.c
fpoly.c
fpref.c
fqqdrivers.c
fqqgetgp.c
fqqmove.c
fqqtext.c
fqqviewing.c
fqqviewp.c
frect.c
ftext.c
ftrans.c
fviewing.c
fviewp.c
fyobbarays.c
)
if (${BUILD_SHARED_LIBS})
  add_library(hpfort SHARED ${libsrch} 
    )
  set_target_properties(hpfort PROPERTIES 
    VERSION ${dl_excurv_VERSION}
    SOVERSION ${dl_excurv_VERSION_MAJOR})
else()
  add_library(hpfort STATIC ${libsrch}
    )
endif()

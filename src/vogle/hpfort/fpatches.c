#include "../vogle.h"

/*
 * patchbasis_
 */
void
fpatchbasis_(ubasis, vbasis)
        float	*ubasis, *vbasis;
{
	patchbasis(ubasis, vbasis);
}

/*
 * patchprecision_
 */
void
fpatchprecision_(useg, vseg)
        int     *useg, *vseg;
{
	patchprecision(*useg, *vseg);
}

/* 
 * patchcurves_
 */
void
fpatchcurves_(nu, nv)
	int	*nu, *nv;
{
	patchcurves(*nu, *nv);
}

/*
 * patch_
 */
void
fpatch_(geomx, geomy, geomz)
	float	*geomx, *geomy, *geomz;
{
	patch(geomx, geomy, geomz);
}

/*
 * rpatch_
 */
void
frpatch_(geomx, geomy, geomz, geomw)
	float	*geomx, *geomy, *geomz, *geomw;
{
	rpatch(geomx, geomy, geomz, geomw);
}

#include "../vogle.h"

/*
 * getgp_
 */
void
fqqgetgp_(x, y, z)
	float	*x, *y, *z;
{
	qqgetgp(x, y, z);
}

/*
 * getgp2_
 */
void
fqqgetgp2_(x, y)
	float	*x, *y;
{
	qqgetgp2(x, y);
}

/*
 * sgetgp2_
 */
void
fqqsgetgp2_(x, y)
	float	*x, *y;
{
	qqsgetgp2(x, y);
}

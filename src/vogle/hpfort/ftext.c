#include <stdio.h>
#include "../vogle.h"

/*
 * font_
 */
void
ffont_(fontfile, len)
	char	*fontfile;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, fontfile, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	font(buf);
}

/*
 * textsize_
 */
void
ftextsize_(width, height)
	float	*width, *height;
{
	textsize(*width, *height);
}

/*
 * boxtext_
 */
void
fboxtext_(x, y, l, h, s, len)
	float	*x, *y, *l, *h;
	char	*s;
	int	len;
{
	char		buf[BUFSIZ];
	register char   *p;

	strncpy(buf, s, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	boxtext(*x, *y, *l, *h, buf);
}

/*
 * boxfit_
 */
void
fboxfit_(l, h, nchars)
	float	*l, *h;
	int	*nchars;
{
	boxfit(*l, *h, *nchars);
}

/*
 * textang_
 */
void
ftextang_(ang)
	float	*ang;
{
	textang(*ang);
}

/*
 * drawchar_
 */
void
fdrawchar_(s)
	char	*s;
{
	drawchar(*s);
}

/*
 * drawstr_
 */
void
fdrawstr_(s, len)
	char	*s;
	int	len;
{
        char            buf[BUFSIZ];
	register char   *p;

	strncpy(buf, s, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	drawstr(buf);
}

/*
 * drawstrf_
 */
void
fdrawstrf_(s, len)
	char	*s;
	int	len;
{
        char            buf[BUFSIZ];
	register char   *p;

	strncpy(buf, s, len);
	buf[len] = 0;

/*	for (p = &buf[len - 1]; *p == ' '; p--)
		;
	*++p = 0;
*/

	drawstr(buf);
}

/*
 * getfontsize_
 */
void
fgetfontsize_(cw, ch)
	float 	*cw, *ch;
{
	getfontsize(cw, ch);
}

/*
 * getcharsize_
 */
void
fgetcharsize_(c, cw, ch)
	char	*c;
	float 	*cw, *ch;
{
	getcharsize(*c, cw, ch);
}

/*
 * fixedwidth_
 */
void
ffixedwidth_(i)
	int	*i;
{
	fixedwidth(*i);
}

/*
 * centertext
 */
void
fcentertext_(i)
	int	*i;
{
	centertext(*i);
}

/*
 * textjustify
 */
void
ftextjustify_(i)
	int	*i;
{
	textjustify(*i);
}

/*
 * numchars_
 */
int
fnumchars_()
{
	return(numchars());
}

/*
 * strlength_
 */
float
fstrlength_(str, len)
	char	*str;
	int	len;
{
        char            buf[BUFSIZ];
	register char   *p;

	strncpy(buf, str, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	return(strlength(buf));
}
/*
 * fwidth_
 */
void
ffwidth_(str)
	float	*str;
{
/*
	fprintf (stdout,"%s \n",*vdevice.dev.devname);
	if (strncmp(device, "hpgla4", 6) == 0)
	PEN_WIDTH(*str);
*/
	return;
}

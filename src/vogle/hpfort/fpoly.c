#include "../vogle.h"

/*
 * polyfill_
 */
void
fpolyfill_(onoff)
	int	*onoff;
{
	polyfill(*onoff);
}

/*
 * hatchang_
 */
void
fhatchang_(ang)
	float	*ang;
{
	hatchang(*ang);
}

/*
 * hatchpitch_
 */
void
fhatchpitch_(pitch)
	float	*pitch;
{
	hatchpitch(*pitch);
}

/*
 * polyhatch_
 */
void
fpolyhatch_(onoff)
	int	*onoff;
{
	polyhatch(*onoff);
}

/*
 * poly2_
 */
void
fpoly2_(n, parray)
	int	*n;
	float	parray[][2];
{
	poly2(*n, parray);
}

/*
 * poly_
 */
void
fpoly_(n, parray)
	int	*n;
	float	parray[][3];
{
	poly(*n, parray);
}

/*
 * makepoly_
 */
void
fmakepoly_()
{
	makepoly();
}

/*
 * closepoly_
 */
void
fclosepoly_()
{
	closepoly();
}

/*
 * backface_
 */
void
fbackface_(onoff)
	int	*onoff;
{
	backface(*onoff);
}

/*
 * backfacedir_
 */
void
fbackfacedir_(clockwise)
	int	*clockwise;
{
	backfacedir(*clockwise);
}

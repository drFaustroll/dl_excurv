#include <stdio.h>
#include "../vogle.h"

/*
 * voutput_
 */
void
fvoutput_(path, len)
	char	*path;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, path, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	voutput(buf);
}

/*
 * vinit_
 */
int
fvinit_(dev, len)
	char	*dev;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, dev, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	return(vinit(buf));
}

/*
 * vnewdev_
 */
void
fvnewdev_(dev, len)
	char	*dev;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, dev, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	vnewdev(buf);
}

/*
 * vgetdev_
 */
void
fvgetdev_(buf, len)
	char	*buf;
	int	len;
{
	register char	*p;

	(void)vgetdev(buf);

	for (p = &buf[len - 1]; *p == ' '; p--)
		;
}
	
/*
 * vexit_
 */
void
fvexit_()
{
	vexit();
}

fgflush_()
{
	gflush();
}

/*
 * clear_
 */
void
fclear_()
{
	clear();
}

/*
 * color_
 */
void
fcolor_(col)
	int	*col;
{
	color(*col);
}

/*
 * _mapcolor
 */
void
fmapcolor_(indx, red, green, blue)
	int	*indx, *red, *green, *blue;
{
	mapcolor(*indx, *red, *green, *blue);
}

/*
 * getkey_
 */
int
fgetkey_()
{
	return(getkey());
}

/*
 * checkkey_
 */
int
fcheckkey_()
{
	return(checkkey());
}

/*
 * getdepth_
 */
int
fgetdepth_()
{
	return(getdepth());
}

/*
 * locator_
 */
int
flocator_(xaddr, yaddr)
	float	*xaddr, *yaddr;
{
	return(locator(xaddr, yaddr));
}

/*
 * slocator_
 */
int
fslocator_(xaddr, yaddr)
	float	*xaddr, *yaddr;
{
	return(slocator(xaddr, yaddr));
}

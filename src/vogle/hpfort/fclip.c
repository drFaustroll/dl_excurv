#include "../vogle.h"

/*
 * clipping
 *
 *	turn clipping on/off
 */
void
fclipping_(onoff)
	int	*onoff;
{
	vdevice.clipoff = !*onoff;
}

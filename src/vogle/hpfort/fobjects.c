#include <stdio.h>
#include "../vogle.h"

/*
 * makeobj_
 */
void
fmakeobj_(n)
	int	*n;
{
	makeobj(*n);
}

/*
 * closeobj_
 */
void
fcloseobj_()
{
	closeobj();
}

/*
 * delobj_
 */
void
fdelobj_(n)
	int	*n;
{
	delobj(*n);
}

/*
 * genobj_
 */
int
fgenobj_()
{
	return(genobj());
}

/*
 * getopenobj_
 */
int
fgetopenobj_()
{
	return(getopenobj());
}

/*
 * callobj_
 */
void
fcallobj_(n)
	int	*n;
{
	callobj(*n);
}

/*
 * isobj_
 */
int
fisobj_(n)
	int	*n;
{
	return(isobj(*n));
}

/*
 * saveobj_
 */
void
fsaveobj_(n, file, len)
	int	*n;
	char	*file;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, file, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	saveobj(*n, buf);
}

/*
 * loadobj_
 */
void
floadobj_(n, file, len)
	int	*n;
	char	*file;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, file, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	loadobj(*n, buf);
}


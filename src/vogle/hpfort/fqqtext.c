#include <stdio.h>
#include "../vogle.h"
extern	float	queery();

/*
 * font_
 */
void
fqqfont_(fontfile, len)
	char	*fontfile;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, fontfile, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	qqfont(buf);
}

/*
 * textsize_
 */
void
fqqtextsize_(width, height)
	float	*width, *height;
{
	qqtextsize(*width, *height);
}

/*
 * boxtext_
 */
void
fqqboxtext_(x, y, l, h, s, len)
	float	*x, *y, *l, *h;
	char	*s;
	int	len;
{
	char		buf[BUFSIZ];
	register char   *p;

	strncpy(buf, s, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	qqboxtext(*x, *y, *l, *h, buf);
}

/*
 * boxfit_
 */
void
fqqboxfit_(l, h, nchars)
	float	*l, *h;
	int	*nchars;
{
	qqboxfit(*l, *h, *nchars);
}

/*
 * textang_
 */
void
fqqtextang_(ang)
	float	*ang;
{
	qqtextang(*ang);
}

/*
 * drawchar_
 */
void
fqqdrawchar_(s)
	char	*s;
{
	qqdrawchar(*s);
}

/*
 * drawstr_
 */
void
fqqdrawstr_(s, len)
	char	*s;
	int	len;
{
        char            buf[BUFSIZ];
	register char   *p;

	strncpy(buf, s, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	qqdrawstr(buf);
}

/*
 * drawstrf_
 */
void
fqqdrawstrf_(s, len)
	char	*s;
	int	len;
{
        char            buf[BUFSIZ];
	register char   *p;

	strncpy(buf, s, len);
	buf[len] = 0;

/*	for (p = &buf[len - 1]; *p == ' '; p--)
		;
	*++p = 0;
*/

	qqdrawstr(buf);
}

/*
 * getfontsize_
 */
void
fqqgetfontsize_(cw, ch)
	float 	*cw, *ch;
{
	qqgetfontsize(cw, ch);
}

/*
 * getcharsize_
 */
void
fqqgetcharsize_(c, cw, ch)
	char	*c;
	float 	*cw, *ch;
{
	qqgetcharsize(*c, cw, ch);
}

/*
 * fixedwidth_
 */
void
fqqfixedwidth_(i)
	int	*i;
{
	qqfixedwidth(*i);
}

/*
 * centertext
 */
void
fqqcentertext_(i)
	int	*i;
{
	qqcentertext(*i);
}

/*
 * textjustify
 */
void
fqqtextjustify_(i)
	int	*i;
{
	qqtextjustify(*i);
}

/*
 * numchars_
 */
int
fqqnumchars_()
{
	return(qqnumchars());
}

/*
 * strlength_
 */
float
fqqstrlength_(str, len)
	char	*str;
	int	len;
{
        char            buf[BUFSIZ];
	register char   *p;

	strncpy(buf, str, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	return(qqstrlength(buf));
}
/*
 * fwidth_
 */
void
fqqwidth_(str)
	float	*str;
{
	fprintf (stdout,"%s \n",*qqvdevice.dev.devname);
/*
	if (strncmp(device, "hpgla4", 6) == 0)
	PEN_WIDTH(*str);
*/
	return;
}
float
fqueery_(str, len)
	char	*str;
	int	len;
{
        char            buf[BUFSIZ];
	register char   *p;

	strncpy(buf, str, len);
	buf[len] = 0;

/*
	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;
*/

	return(queery(buf));
}

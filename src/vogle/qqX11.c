/*
 * VOGL/VOGLE driver for X11.
 * 
 * Define VOGLE if this driver is really for the VOGLE Libarary.
 *
 */
#define VOGLE 1

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#ifdef VOGLE

#include "vogle.h"
static	char	*me = "vogle";
/* #define LARGEFONT       "-adobe-courier-medium-r-normal--24-240-75-75-m-150-iso8859-1" */
#define LARGEFONT       "-adobe-times-medium-r-normal--18-180-75-75-P-94-iso8859-1"
#define SMALLFONT       "*times-medium-r-*-140-*"
/*
#define SMALLFONT       "-adobe-times-roman-r-normal--14-140-75-75-p-74-iso8859-1"
#define SMALLFONT       "-adobe-times-medium-r-normal--20-140-100-100-p-96-iso8859-1"
*/

#else

#include "vogl.h"
static	char	*me = "vogl";
#define LARGEFONT	"9x15bold"
#define SMALLFONT	"6x13bold"

#endif

#define MIN(x,y)	((x) < (y) ? (x) : (y))
#define MAX(x,y)	((x) > (y) ? (x) : (y))
#define	CMAPSIZE	256
#define	EV_MASK		KeyPressMask|ButtonReleaseMask|ExposureMask|ButtonPressMask

static	int		qqmaxw = -1, qqmaxh = -1;
static	Window		qqwinder;
static	Display		*qqdisplay;
static	int		qqscreen;
static	unsigned long	qqcarray[CMAPSIZE];
static	Colormap	qqcolormap;

static	Drawable	qqtheDrawable = -1;
static	GC		qqtheGC;
static	XGCValues	qqtheGCvalues;
static	Pixmap		qqbbuff;		/*qq Back buffer pixmap */
static	int		qqback_used = 0;	/*qq Have we backbuffered ? */

static	XFontStruct	*qqfont_id = (XFontStruct *)NULL;
XEvent			qqevent;

static	unsigned long	qqcolour;
static	unsigned int	qqh, qqw;
static	char		*qqsmallf, *qqlargef;
static	char		qquse_toolkit_win = 0;
extern	char	*getenv();

/*
 * qqvo_xt_set_win
 *
 *	Just sets the drawable to the partucular window.
 */
qqvo_xt_set_win(dis, win, xw, xh)
	Display		*dis;
	Drawable	win;
	int		xw, xh;
{
	int	backb;

	backb = (qqtheDrawable == qqbbuff);

	qqwinder = win;

	qqvdevice.sizeX = qqvdevice.sizeY = MIN(xh, xw);
	qqvdevice.sizeSx = xw;
	qqvdevice.sizeSy = xh;

        if (xw > qqmaxw || xh > qqmaxh) {
		if (qqback_used) {
			qqback_used = 0;
			XFreePixmap(qqdisplay, qqbbuff);
			qqX11_backbuf();
		}
        }

	qqdisplay = dis;
	if (backb)
		qqtheDrawable = qqbbuff;
	else
		qqtheDrawable = win;

	return(1);
}

/*
 * qqvo_xt_window
 *
 *	Tells VOGL/VOGLE to use a window from an X11 toolkit (eg xview)
 *	and not to make it's own window.
 */
qqvo_xt_window(dis, win, xw, xh)
	Display	*dis;
	Window	win;
	int	xw, xh;
{
	int	backb, i, depth;

	backb = (qqtheDrawable == qqbbuff);

	qqdisplay = dis;
	qqwinder = win;
	qqscreen = DefaultScreen(qqdisplay);
	qqcolormap = DefaultColormap(qqdisplay, qqscreen);
	depth = qqvdevice.depth = DefaultDepth(qqdisplay, qqscreen);
	qqtheDrawable = qqwinder;

	qquse_toolkit_win = 1;
	qqw = xw;
	qqh = xh;

	/*
	 * Set our standard colors...
	 */
	if (qqvdevice.depth == 1) {
		/*
		 * Black and white - anything that's not black is white.
		 */
		qqcarray[0] = BlackPixel(qqdisplay, qqscreen);
		for (i = 1; i < CMAPSIZE; i++)
			qqcarray[i] = WhitePixel(qqdisplay, qqscreen);
	} else {
		/*
		 * Color, try to get our colors close to what's in the
		 * default qqcolormap.
		 */
		qqX11_mapcolor(0, 0, 0, 0);
		qqX11_mapcolor(1, 255, 0, 0);
		qqX11_mapcolor(2, 0, 255, 0);
		qqX11_mapcolor(3, 255, 255, 0);
		qqX11_mapcolor(4, 0, 0, 255);
		qqX11_mapcolor(5, 255, 0, 255);
		qqX11_mapcolor(6, 0, 255, 255);
		qqX11_mapcolor(7, 255, 255, 255);
	}

	if ((qqsmallf = XGetDefault(qqdisplay, me, "smallfont")) == (char *)NULL)
		qqsmallf = getenv("EXFONT2");
	if (qqsmallf == (char *)NULL)
		qqsmallf =SMALLFONT;

	if ((qqlargef = XGetDefault(qqdisplay, me, "largefont")) == (char *)NULL)
		qqlargef = LARGEFONT;

	/*
	 * Create Graphics Context and Drawable
	 */
	qqtheGC = XDefaultGC(qqdisplay, qqscreen);
	qqtheGCvalues.graphics_exposures = False;
	qqtheGCvalues.cap_style = CapButt;
	XChangeGC(qqdisplay, qqtheGC, GCGraphicsExposures|GCCapStyle, &qqtheGCvalues);
	qqX11_color(0);

	qqvdevice.sizeX = qqvdevice.sizeY = MIN(xh, xw);
	qqvdevice.sizeSx = xw;
	qqvdevice.sizeSy = xh;

        if (qqback_used && (xw > qqmaxw || xh > qqmaxh)) {
                qqback_used = 0;
		XFreePixmap(qqdisplay, qqbbuff);
                qqX11_backbuf();
        }

	if (backb)
		qqtheDrawable = qqbbuff;
	else
		qqtheDrawable = win;


#ifndef VOGLE
	qqvdevice.devname = "X11";
#endif

	return(1);
}

/*
 *	vo_xt_win_size
 *
 * If the X toolkit has changed the window size, then
 * you might wish to call this routine to tell vogl/vogle about it.
 */
qqvo_xt_win_size(xw, xh)
	int	xw, xh;
{
	char	backb;

	qqw = xw;
	qqh = xh;

	qqvdevice.sizeX = qqvdevice.sizeY = MIN(qqh, qqw);
	qqvdevice.sizeSx = qqw;
	qqvdevice.sizeSy = qqh;

	backb = (qqtheDrawable == qqbbuff);

	if (qqback_used) {

		/* Have to re allocate the back buffer */

		XFreePixmap(qqdisplay, qqbbuff);

		qqbbuff = XCreatePixmap(qqdisplay,
			(Drawable)qqwinder,
			(unsigned int)qqvdevice.sizeSx + 1,
			(unsigned int)qqvdevice.sizeSy + 1,
			(unsigned int)qqvdevice.depth
		);
	}
	if (backb)
		qqtheDrawable = (Drawable)qqbbuff;
}

/*
 * X11_init
 *
 *	initialises X11 display.
 */
qqX11_init()
{
	int		i;
	int		x, y, prefx, prefy, prefxs, prefys;
	unsigned int	bw, depth, mask;
	Window		rootw, childw;
	char		*av[2], name[128], *geom;
static	char	*path = "windoe";
	char	buf[128];
	FILE *fp;

	XSetWindowAttributes    theWindowAttributes;
	XWindowAttributes	retWindowAttributes;
        XSizeHints              theSizeHints;
        unsigned long           theWindowMask;
	XWMHints                theWMHints;


	if (qquse_toolkit_win)
		return(1);

	av[0] = me;
	av[1] = (char *)NULL;

	if ((qqdisplay = XOpenDisplay((char *)NULL)) == (Display *)NULL) {
		fprintf(stderr,"%s: X11_init: can't connect to X server\n", me);
		exit(1);
	}

	qqscreen = DefaultScreen(qqdisplay);
	qqwinder = RootWindow(qqdisplay, qqscreen);
	qqcolormap = DefaultColormap(qqdisplay, qqscreen);
	depth = qqvdevice.depth = DefaultDepth(qqdisplay, qqscreen);

	/*
	 * Set our standard colors...
	 */
	if (qqvdevice.depth == 1) {
		/*
		 * Black and white - anything that's not black is white.
		 */
		qqcarray[0] = BlackPixel(qqdisplay, qqscreen);
		for (i = 1; i < CMAPSIZE; i++)
			qqcarray[i] = WhitePixel(qqdisplay, qqscreen);
	} else {
		/*
		 * Color, try to get our colors close to what's in the
		 * default qqcolormap.
		 */
		qqX11_mapcolor(0, 0, 0, 0);
		qqX11_mapcolor(1, 255, 0, 0);
		qqX11_mapcolor(2, 0, 255, 0);
		qqX11_mapcolor(3, 255, 255, 0);
		qqX11_mapcolor(4, 0, 0, 255);
		qqX11_mapcolor(5, 255, 0, 255);
		qqX11_mapcolor(6, 0, 255, 255);
		qqX11_mapcolor(7, 255, 255, 255);
	}

	getprefposandsize(&prefx, &prefy, &prefxs, &prefys);

	/*
	 * NEED TO USE XGRABPOINTER here???
	 */
	XQueryPointer(qqdisplay, qqwinder, &rootw, &childw, &x, &y, &x, &y, &mask);

	if (childw == None)
		childw = rootw;

/*
	if (!XGetWindowAttributes(qqdisplay, childw, &retWindowAttributes)) {
		fprintf(stderr,"Can't get window attributes.");
		exit(1);
	}

	x = retWindowAttributes.x;
	y = retWindowAttributes.y;
	w = retWindowAttributes.width;
	h = retWindowAttributes.height;
	bw = retWindowAttributes.border_width;
	depth = qqvdevice.depth = retWindowAttributes.depth;

	XTranslateCoordinates(qqdisplay,
			childw, retWindowAttributes.root,
			0, 0,
			&x, &y,
			&rootw
	);
*/

	XGetGeometry(qqdisplay, childw, &rootw, &x, &y, &qqw, &qqh, &bw, &depth);

        theWindowAttributes.backing_store = WhenMapped;
        theWindowAttributes.save_under = True;
        theWindowAttributes.border_pixel = qqcarray[1];


	/*
	 * See if there is something in the .Xdefaults file regarding
	 * VOGL/VOGLE.
	 */

	if ((qqsmallf = XGetDefault(qqdisplay, me, "smallfont")) == (char *)NULL)
		qqsmallf = getenv("EXFONT2");
	if (qqsmallf == (char *)NULL)
		qqsmallf =SMALLFONT;

	if ((qqlargef = XGetDefault(qqdisplay, me, "qqlargefont")) == (char *)NULL)
		qqlargef =LARGEFONT;

	geom = XGetDefault(qqdisplay, me, "Geometry");

	if (geom != (char *)NULL) {
		mask = XParseGeometry(geom, &x, &y, &qqw, &qqh);

		if (mask & XValue)
			theSizeHints.flags |= USPosition;

		if (mask & YValue)
			theSizeHints.flags |= USPosition;

		if (mask & WidthValue)
			theSizeHints.flags |= USSize;

		if (mask & HeightValue)
			theSizeHints.flags |= USSize;

		if (mask & XNegative)
			 x = DisplayWidth(qqdisplay, qqscreen) - 2*bw - qqw + x;

		if (mask & YNegative)
			y = DisplayHeight(qqdisplay, qqscreen) - 2*bw - qqh + y;

	} else
		theSizeHints.flags = PPosition | PSize;

	if (prefx > -1) {
	        x = prefx;
	        y = prefy;
	}

	if (prefxs > -1) {
	        qqw = prefxs;
	        qqh = prefys;
	}

	if (bw == 0)
		bw = 4;

	x -= bw;
	y -= bw;

	if (x <= 0)
		x = 0;

	if (y <= 0)
		y = 0;

	qqw -= 4 * bw;
	qqh -= 4 * bw;
	qqw = 400;
	qqh = 600;
	if ((fp = fopen(path, "r")) == (FILE *)NULL) {
		sprintf(buf, "vinput: couldn't open %s \n", path);
/*		verror(buf);*/
  }
	else
	{
	fscanf (fp,"%d",&x);
	fscanf (fp,"%d",&y);
	fscanf (fp,"%d",&qqh);
	fscanf (fp,"%d",&qqw);
	fclose(fp);
	x -= bw;
	y -= bw;
  }

        theWindowMask = CWBorderPixel|CWBackingStore;

        qqwinder = XCreateWindow(qqdisplay,
                                qqwinder,
                                x, y,
                                qqw, qqh,
                                bw,
                                (int)qqvdevice.depth,
                                InputOutput,
                                CopyFromParent,
                                theWindowMask,
                                &theWindowAttributes
                        );
 
        theSizeHints.x = x;
        theSizeHints.y = y;
        theSizeHints.width = qqw;
        theSizeHints.height = qqh;

#ifndef VOGLE
	if (qqvdevice.wintitle)
		strcpy(name, qqvdevice.wintitle);
	else
		sprintf(name, "%s %d (win id 0x%x)", me, getpid(), qqwinder);
#else
	sprintf(name, "%s %d (win id 0x%x)", me, getpid(), qqwinder);
#endif

	XSetStandardProperties(qqdisplay,
		qqwinder,
		name,
		name,
		None,
		av,
		1,
		&theSizeHints
	);

        theWMHints.initial_state = NormalState;
        theWMHints.input = True;
        theWMHints.flags = StateHint | InputHint;
        XSetWMHints(qqdisplay, qqwinder, &theWMHints);

	XSelectInput(qqdisplay, qqwinder, EV_MASK);

	qqtheDrawable = (Drawable)qqwinder;

	/*
	 * Create Graphics Context and Drawable
	 */
	qqtheGC = XDefaultGC(qqdisplay, qqscreen);
	qqtheGCvalues.graphics_exposures = False;
	qqtheGCvalues.cap_style = CapButt;
	XChangeGC(qqdisplay, qqtheGC, GCGraphicsExposures|GCCapStyle, &qqtheGCvalues);
	qqtheDrawable = (Drawable)qqwinder;
	qqX11_color(0);

	XMapRaised(qqdisplay, qqwinder);
	XFlush(qqdisplay);

	/*
	 * Wait for Exposure event.
	 */
	do {
		XNextEvent(qqdisplay, &qqevent);
	} while (qqevent.type != Expose && qqevent.type != MapNotify);

	/*
	 * Set the input Focus to us.

        if (prefx == -1 && prefxs == -1)
                XSetInputFocus(qqdisplay, qqwinder, RevertToParent, CurrentTime);
	 */

	/*
	 *  Let VOGL/VOGLE know about the window size.
	 *  (We may have been resized..... )
	 */
	if (!XGetWindowAttributes(qqdisplay, qqwinder, &retWindowAttributes)) {
		fprintf(stderr,"Can't get window attributes.");
		exit(1);
	}

	x = retWindowAttributes.x;
	y = retWindowAttributes.y;
	qqw = retWindowAttributes.width;
	qqh = retWindowAttributes.height;

	XTranslateCoordinates(qqdisplay,
			qqwinder, retWindowAttributes.root,
			0, 0,
			&x, &y,
			&rootw
	);

	qqvdevice.sizeX = qqvdevice.sizeY = MIN(qqh, qqw);
	qqvdevice.sizeX = qqw;
	qqvdevice.sizeY = qqh;
	qqvdevice.sizeSx = qqw;
	qqvdevice.sizeSy = qqh;

	if (qqback_used && (qqmaxw < qqw || qqmaxh < qqh)) {
		qqback_used = 0;
		qqX11_backbuf();
	}

	return(1);
}

/*
 * qqX11_exit
 *
 *	cleans up before returning the window to normal.
 */
qqX11_exit()
{
	if (qqback_used) 
		XFreePixmap(qqdisplay, qqbbuff);

	if (qqfont_id != (XFontStruct *)NULL)
		XFreeFont(qqdisplay, qqfont_id);

	qqfont_id = (XFontStruct *)NULL;

	if (qquse_toolkit_win)
		return(1);

	XDestroyWindow(qqdisplay, qqwinder);

	XSync(qqdisplay, 0);

	XCloseDisplay(qqdisplay);

	return(1);
}

/*
 * qqX11_draw
 *
 *	draws a line from the current graphics position to (x, y).
 *
 * Note: (0, 0) is defined as the top left of the window in X (easy
 * to forget).
 */
qqX11_draw(x, y)
	int	x, y;
{
	if (x == qqvdevice.cpVx && y == qqvdevice.cpVy)
		/*
		 * Hack for some X servers... my MIT X11 R5 manual states:
		 * CapButt	the results are device-dependent, but the
		 *		desired effect is that a single pixel is
		 *		drawn.
		 *
		 * This does work on a real MIT R5 server... but on some
		 * machines, we have to do this XDrawPoint thing.
		 * (It's probably faster this way anyway).
		 */
		XDrawPoint(qqdisplay, qqtheDrawable, qqtheGC, x, qqvdevice.sizeSy - y);
	else
		XDrawLine(qqdisplay,
			qqtheDrawable,
			qqtheGC,
			qqvdevice.cpVx, qqvdevice.sizeSy - qqvdevice.cpVy,
			x, qqvdevice.sizeSy - y
		);

	if (qqvdevice.sync)
		XSync(qqdisplay, 0);
}

qqX11_pnt(x, y)
	int	x, y;
{
	XDrawPoint(qqdisplay,
		qqtheDrawable,
		qqtheGC,
		x, qqvdevice.sizeSy - y
	);

	if (qqvdevice.sync)
		XSync(qqdisplay, 0);
}

/*
 * qqX11_getkey
 *
 *	grab a character from the keyboard - blocks until one is there.
 */
int
qqX11_getkey()
{
	char	c;

	do {
		XNextEvent(qqdisplay, &qqevent);
		if (qqevent.type == KeyPress) {
			if (XLookupString((XKeyEvent *)&qqevent, &c, 1, NULL, NULL) > 0)
				return((int)c);
			else
				return(0);
		}
	} while (qqevent.type != KeyPress);
}

/*
 * qqX11_checkkey
 *
 *	Check if there has been a keyboard key pressed.
 *	and return it if there is.
 */
int
qqX11_checkkey()
{
	char	c;

	if (!XCheckWindowEvent(qqdisplay, qqwinder, KeyPressMask, &qqevent))
		return(0);

	if (qqevent.type == KeyPress)
		if (XLookupString((XKeyEvent *)&qqevent, &c, 1, NULL, NULL) > 0)
			return((int)c);

	return(0);
}

/*
 * qqX11_locator
 *
 *	return the window location of the cursor, plus which mouse button,
 * if any, is been pressed.
 */
int
qqX11_locator(wx, wy)
	int	*wx, *wy;
{
	Window		rootw, childw;
	int		x, y;
	unsigned int	mask;

	XQueryPointer(qqdisplay, qqwinder, &rootw, &childw, &x, &y, wx, wy, &mask);

	*wy = (int)qqvdevice.sizeSy - *wy;

	return(mask >> 8);
}

#ifdef VOGLE
/*
 * qqX11_clear
 *
 * Clear the qqscreen (or current buffer )to current qqcolour
 */
qqX11_clear()
{
	XSetBackground(qqdisplay, qqtheGC, qqcolour);
	XFillRectangle(qqdisplay,
		qqtheDrawable,
		qqtheGC,
		0,
		0,
		(unsigned int)qqvdevice.sizeSx + 1,
		(unsigned int)qqvdevice.sizeSy + 1
	);

	if (qqvdevice.sync)
		XFlush(qqdisplay);
}

#else 

/*
 * qqX11_clear
 *
 * Clear the qqscreen (or current buffer )to current qqcolour
 */
qqX11_clear()
{
	unsigned int	qqw = qqvdevice.maxVx - qqvdevice.minVx;
	unsigned int	qqh = qqvdevice.maxVy - qqvdevice.minVy;

	XSetBackground(qqdisplay, qqtheGC, qqcolour);

	XFillRectangle(qqdisplay,
		qqtheDrawable,
		qqtheGC,
		qqvdevice.minVx,
		qqvdevice.sizeSy - qqvdevice.maxVy, 
		w, 
		h
	);

	if (qqvdevice.sync)
		XFlush(qqdisplay);
}
#endif

/*
 * qqX11_color
 *
 *	set the current drawing color index.
 */
qqX11_color(ind)
        int	ind;
{
	qqcolour = qqcarray[ind];
	XSetForeground(qqdisplay, qqtheGC, qqcolour);
}

/*
 * qqX11_mapcolor
 *
 *	change index i in the color map to the appropriate r, g, b, value.
 */
qqX11_mapcolor(i, r, g, b)
	int	i;
	int	r, g, b;
{
	int	stat;
	XColor	tmp;

	if (i >= CMAPSIZE)
		return(-1);


	/*
	 * For Black and White.
	 * If the index is 0 and r,g,b != 0 then we are remapping black.
	 * If the index != 0 and r,g,b == 0 then we make it black.
	 */
	if (qqvdevice.depth == 1) {
		if (i == 0 && (r != 0 || g != 0 || b != 0)) 
			qqcarray[i] = WhitePixel(qqdisplay, qqscreen);
		else if (i != 0 && r == 0 && g == 0 && b == 0)
			qqcarray[i] = BlackPixel(qqdisplay, qqscreen);
	} else {
		tmp.red = (unsigned short)(r / 255.0 * 65535);
		tmp.green = (unsigned short)(g / 255.0 * 65535);
		tmp.blue = (unsigned short)(b / 255.0 * 65535);
		tmp.flags = 0;
		tmp.pixel = (unsigned long)i;

		if ((stat = XAllocColor(qqdisplay, qqcolormap, &tmp)) == 0) {
			fprintf(stderr, "XAllocColor failed (status = %d)\n", stat);
			exit(1);
		}
		qqcarray[i] = tmp.pixel;
	}

	XFlush(qqdisplay);
	return(0);
}
	
/*
 * qqX11_font
 *
 *   Set up a hardware font. Return 1 on success 0 otherwise.
 *
 */
qqX11_font(fontfile)
        char	*fontfile;
{
	XGCValues	xgcvals;
	char	*name = fontfile;
extern	float	queery();

	if (qqfont_id != (XFontStruct *)NULL)
		XFreeFont(qqdisplay, qqfont_id);

	if (strcmp(fontfile, "small") == 0) {
		if ((qqfont_id = XLoadQueryFont(qqdisplay, qqsmallf)) == (XFontStruct *)NULL) {
			fprintf(stderr, "%s qqX11.c couldn't open small font '%s'\n", me, qqsmallf);
			fprintf(stderr, "You'll have to redefine it....\n");
			return(0);
		} else
			name = qqsmallf;
		
	} else if (strcmp(fontfile, "large") == 0) {
		if ((qqfont_id = XLoadQueryFont(qqdisplay, qqlargef)) == (XFontStruct *)NULL) {
			fprintf(stderr, "%s qqX11.c couldn't open large font '%s'\n", me, qqlargef);
			fprintf(stderr, "You'll have to redefine it....\n");
			return(0);
		}
			name = qqlargef;
	fprintf(stderr," name: %s \n",name);
	} else {
		if ((qqfont_id = XLoadQueryFont(qqdisplay, fontfile)) == (XFontStruct *)NULL) {
			fprintf(stderr, "%s qqX11.c couldn't open fontfile '%s'\n", me, fontfile);
			return(0);
		}
	}

	/*
	qqvdevice.hheight = qqfont_id->max_bounds.ascent + qqfont_id->max_bounds.descent;
	*/
	qqvdevice.hheight = qqfont_id->ascent + qqfont_id->descent;
	qqvdevice.hwidth = qqfont_id->max_bounds.width;


	xgcvals.font = XLoadFont(qqdisplay, name);
	XChangeGC(qqdisplay, qqtheGC, GCFont, &xgcvals);

	return(1);
}

/* 
 * qqX11_char
 *
 *	 outputs one char - is more complicated for other devices
 */
qqX11_char(c)
	char	c;
{
	XDrawString(qqdisplay, qqtheDrawable, qqtheGC, qqvdevice.cpVx, (int)(qqvdevice.sizeSy - qqvdevice.cpVy), &c, 1);

	if (qqvdevice.sync)
		XFlush(qqdisplay);
}

/*
 * qqX11_string
 *
 *	Display a string at the current drawing position.
 */
qqX11_string(s)
        char	s[];
{
	XDrawString(qqdisplay, qqtheDrawable, qqtheGC, qqvdevice.cpVx, (int)(qqvdevice.sizeSy - qqvdevice.cpVy), s, strlen(s));
	if (qqvdevice.sync)
		XFlush(qqdisplay);
}

/*
 * qqX11_fill
 *
 *	fill a polygon
 */
qqX11_fill(n, x, y)
	int	n, x[], y[];
{
	char	buf[BUFSIZ];
	XPoint	plist[128];
	int	i;

	if (n > 128) {
		sprintf(buf, "%s: more than 128 points in a polygon", me);
/*		verror(buf);*/
	}

	for (i = 0; i < n; i++) {
		plist[i].x = x[i];
		plist[i].y = qqvdevice.sizeSy - y[i];
	}

	XFillPolygon(qqdisplay, qqtheDrawable, qqtheGC, plist, n, Nonconvex, CoordModeOrigin);

	qqvdevice.cpVx = x[n-1];
	qqvdevice.cpVy = y[n-1];

	if (qqvdevice.sync)
		XFlush(qqdisplay);
	return(0);
}

/*
 * qqX11_backbuf
 *
 *	Set up double buffering by allocating the back buffer and
 *	setting drawing into it.
 */
int
qqX11_backbuf()
{
	if (!qqback_used) {
		qqbbuff = XCreatePixmap(qqdisplay,
			(Drawable)qqwinder,
			(unsigned int)qqvdevice.sizeSx + 1,
			(unsigned int)qqvdevice.sizeSy + 1,
			(unsigned int)qqvdevice.depth
		);

		qqmaxw = MAX(qqvdevice.sizeSx + 1, qqmaxw);
		qqmaxh = MAX(qqvdevice.sizeSy + 1, qqmaxh);
	}

	qqtheDrawable = (Drawable)qqbbuff;

	qqback_used = 1;

	return(1);
}

/*
 * qqX11_swapbuf
 *
 *	Swap the back and from buffers. (Really, just copy the
 *	back buffer to the qqscreen).
 */
qqX11_swapbuf()
{
	XCopyArea(qqdisplay,
		qqtheDrawable,
		qqwinder,
		qqtheGC,
		0, 0,
		(unsigned int)qqvdevice.sizeSx + 1,
		(unsigned int)qqvdevice.sizeSy + 1,
		0, 0
	);
	XSync(qqdisplay, 0);
	return(0);
}

/*
 * qqX11_frontbuf
 *
 *	Make sure we draw to the qqscreen.
 */
qqX11_frontbuf()
{
	qqtheDrawable = (Drawable)qqwinder;
      return(0);
}

/*
 * Syncronise the display with what we think has been sent to it...
 */
qqX11_sync()
{
	XSync(qqdisplay, 0);
      return(0);
}

#undef VORTDUMP
#ifdef VORTDUMP
/*
 * HACK
 * Dump the contents of the current buffer to a VORT file....
 * ONLY WORKS WITH 8Bit Drawables!
 */
#include "vort.h"

qqX11_dump_pixmap(filename, dx, dy, dw, dh)
	char	*filename;
	int	dx, dy, dw, dh;
{
	XImage	*ximage;
	image	*im;
	unsigned char	*line, *rm, *gm, *bm;
	XColor	*cols;
	int	i;

	if (dw > qqvdevice.sizeSx || dw < 0)
		dw = qqvdevice.sizeSx;
	if (dh > qqvdevice.sizeSy || dh < 0)
		dh = qqvdevice.sizeSy;

	if (dx > qqvdevice.sizeSx || dx < 0)
		dx = 0;
	if (dy > qqvdevice.sizeSy || dy < 0)
		dy = 0;

	ximage = XGetImage(qqdisplay, 
			qqtheDrawable, 
			dx, dy,
			(unsigned int)dw,
			(unsigned int)dh,
			AllPlanes,
			ZPixmap
		);

	if (!ximage) {
		fprintf(stderr, "X11_dump_pixmap: can't do XGetImage\n");
		exit(1);
	}

	if ((im = openimage(filename, "w")) == (image *)NULL) {
		fprintf(stderr, "X11_dump_pixmap: can't open %s\n", filename);
		exit(1);
	}

	if (!(rm = (unsigned char *)malloc(256))) {
		fprintf(stderr, "X11_dump_pixmap: can't alloc rm\n");
		exit(1);
	}
	if (!(gm = (unsigned char *)malloc(256))) {
		fprintf(stderr, "X11_dump_pixmap: can't alloc gm\n");
		exit(1);
	}
	if (!(bm = (unsigned char *)malloc(256))) {
		fprintf(stderr, "X11_dump_pixmap: can't alloc bm\n");
		exit(1);
	}
	if (!(cols = (XColor *)malloc(256 * sizeof(XColor)))) {
		fprintf(stderr, "X11_dump_pixmap: can't alloc cols\n");
		exit(1);
	}

	/*
	 * Get our qqcolormap...
	 */
	for (i = 0; i < 256; i++) {
		cols[i].pixel = (unsigned long)i;
		cols[i].red = cols[i].green = cols[i].blue = 0;
		cols[i].flags = DoRed | DoGreen | DoBlue;
	}

	XQueryColors(qqdisplay, qqcolormap, cols, 256);

	for (i = 0; i < 256; i++) {
		rm[i] = (unsigned char)(cols[i].red >> 8);
		gm[i] = (unsigned char)(cols[i].green >> 8);
		bm[i] = (unsigned char)(cols[i].blue >> 8);
	}

	imagetype(im) = PIX_RLECMAP;
	imageheight(im) = dh;
	imagewidth(im) = dw;
	imagedate(im) = time(0);
	titlelength(im) = 0;
	setcmap(im, 256, rm, gm, bm);

	writeheader(im);

	line = (unsigned char *)ximage->data;
	for (i = 0; i < dh; i++) {
		writemappedline(im, line);
		line += ximage->bytes_per_line;
	}
	
	closeimage(im); 

	free(rm);
	free(gm);
	free(bm);
	free(cols);
}

#endif

#ifndef VOGLE
/*
 * qqX11_setlw
 *
 *	Set the line width....
 */
void
qqX11_setlw(w)
	int	w;
{
	XGCValues vals;

	vals.line_width = qqw;
	XChangeGC(qqdisplay, qqtheGC, GCLineWidth, &vals);
      return(0);
}

/*
 * qqX11_setls
 *
 *	Set the line style....
 */

qqX11_setls(lss)
	int	lss;
{
	unsigned ls = lss;
	char	dashes[16];
	int	i, n, a, b, offset;

	if (ls == 0xffff) {
		XSetLineAttributes(qqdisplay, qqtheGC, qqvdevice.attr->a.lw, LineSolid, CapButt, JoinMiter);
		return;
	}

	for (i = 0; i < 16; i++)
		dashes[i] = 0;

	for (i = 0; i < 16; i++)	/* Over 16 bits */
		if ((ls & (1 << i)))
			break;

	offset = i;

#define	ON	1
#define	OFF	0
		
	a = b = OFF;
	if (ls & (1 << 0))
		a = b = ON;

	n = 0;
	for (i = 0; i < 16; i++) {	/* Over 16 bits */
		if (ls & (1 << i))
			a = ON;
		else
			a = OFF;

		if (a != b) {
			b = a;
			n++;
		}
		dashes[n]++;
	}
	n++;

	XSetLineAttributes(qqdisplay, qqtheGC, qqvdevice.attr->a.lw, LineOnOffDash, CapButt, JoinMiter);
	XSetDashes(qqdisplay, qqtheGC, offset, dashes, n);
}

#else
/*
 * qqX11_setlw (this one for VOGLE only)
 *
 *	Set the line width....THICK or THIN
 */
qqX11_setlw(w)
	int	w;
{
	XGCValues vals;

	if (w == 0)
		w = 1;
	else if (w == 1)
		w = 2;

	vals.line_width = qqw;
	XChangeGC(qqdisplay, qqtheGC, GCLineWidth, &vals);
      return(0);
}

#endif 

/*
 * the device entry
 */
static DevEntry qqX11dev = {
	"qqX11",
	"large",
	"small",
	qqX11_backbuf,
	qqX11_char,
	qqX11_checkkey,
	qqX11_clear,
	qqX11_color,
	qqX11_draw,
	qqX11_exit,
	qqX11_fill,
	qqX11_font,
	qqX11_frontbuf,
	qqX11_getkey,
	qqX11_init,
	qqX11_locator,
	qqX11_mapcolor,
#ifndef VOGLE
	qqX11_setls,
#endif
	qqX11_setlw,
	qqX11_string,
	qqX11_swapbuf,
	qqX11_sync
};

/*
 * _qqX11_devcpy
 *
 *	copy the X11 device into qqvdevice.dev.
 */
void
_qqX11_devcpy()
{
	qqvdevice.dev = qqX11dev;
}
/*
 * X11_resize
 *
 *	initialises X11 display.
 */
void
qqx11_resize_()
{
	int		i;
	int		x, y, prefx, prefy, prefxs, prefys;
	unsigned int	bw, depth, mask;
	Window		rootw, childw;
	char		*av[2], name[128], *geom;
static	char	*path = "windoe";
	char	buf[128];
	FILE *fp;

	XSetWindowAttributes    theWindowAttributes;
	XWindowAttributes	retWindowAttributes;
        XSizeHints              theSizeHints;
        unsigned long           theWindowMask;
	XWMHints                theWMHints;


	/*
	 *  Let VOGL/VOGLE know about the window size.
	 *  (We may have been resized..... )
	 */
	if (!XGetWindowAttributes(qqdisplay, qqwinder, &retWindowAttributes)) {
		fprintf(stderr,"Can't get window attributes.");
		exit(1);
	}

	x = retWindowAttributes.x;
	y = retWindowAttributes.y;
	qqw = retWindowAttributes.width;
	qqh = retWindowAttributes.height;

	XTranslateCoordinates(qqdisplay,
			qqwinder, retWindowAttributes.root,
			0, 0,
			&x, &y,
			&rootw
	);
	if ((fp = fopen(path, "w")) == (FILE *)NULL) {
		sprintf(buf, "voutput: couldn't open %s \n", path);
/*		verror(buf);*/
  }
	else
	{
/*
	fprintf (stderr," coords: %d %d %d %d \n",x,y,h,w);
*/
	fprintf (fp,"%d \n",x);
	fprintf (fp,"%d \n",y);
	fprintf (fp,"%d \n",qqh);
	fprintf (fp,"%d \n",qqw);
	fclose(fp);
/*
	fprintf (stderr," written it again \n");
*/
  }

	qqvdevice.sizeX = qqvdevice.sizeY = MIN(qqh, qqw);
	qqvdevice.sizeX = qqw;
	qqvdevice.sizeY = qqh;
	qqvdevice.sizeSx = qqw;
	qqvdevice.sizeSy = qqh;

	if (qqback_used && (qqmaxw < qqw || qqmaxh < qqh)) {
		qqback_used = 0;
		qqX11_backbuf();
	}

	return;
}
/*
typedef struct {
    XExtData	*ext_data;	/ hook for extension to hang data /
    Font        fid;            / Font id for this font /
    unsigned	direction;	/ hint about direction the font is painted /
    unsigned	min_char_or_byte2;/ first character /
    unsigned	max_char_or_byte2;/ last character /
    unsigned	min_byte1;	/ first row that exists /
    unsigned	max_byte1;	/ last row that exists /
    Bool	all_chars_exist;/ flag if all characters have non-zero size/
    unsigned	default_char;	/ char to print for undefined character /
    int         n_properties;   / how many properties there are /
    XFontProp	*properties;	/ pointer to array of additional properties/
    XCharStruct	min_bounds;	/ minimum bounds over all existing char/
    XCharStruct	max_bounds;	/ maximum bounds over all existing char/
    XCharStruct	*per_char;	/ first_char to last_char information /
    int		ascent;		/ log. extent above baseline for spacing /
    int		descent;	/ log. descent below baseline for spacing /
} XFontStruct;
*/
float
queery(string)
	char	*string;
{
	XID	trifid;
	int	nchars;
	float	xw, xs, qqVsx;
	float	d, a1, a2, b1, b2, c1, c2, A, B;
/*
	int	*direction = (int *)NULL;
	int	*ascent = (int *)NULL, *descent = (int *)NULL;
*/
	int	direction;
	int	ascent ,descent;
	XCharStruct	overall;
/*	string="What a load of bollocks"; */
	nchars=strlen(string);
/*	overall = (XCharStruct *)malloc(256 * sizeof(XCharStruct)); */
	trifid = qqfont_id->fid;
	XQueryTextExtents(qqdisplay,trifid,string,nchars,&direction,&ascent,
	&descent,&overall);
	qqVsx = (float)(qqvdevice.maxVx - qqvdevice.minVx);
	A = (float)overall.width / qqVsx;
	return(A);
}

#include "vogle.h"

static int qqplanes[] = {'\01', '\02', '\04', '\010', '\020', '\040'};

float qqwc[2][6];

/*
 * clip
 *
 * Clips a 3D line using Homogeneous clipping.
 * Reference: Newman and Sproull
 *
 */
void
qqclip(p0, p1)
	register Vector	p0, p1;
{
	float	t, t1, t2, dx, dy, dz, dw;
	int	vx, vy, c1, c2, i;

	c1 = qqMakeEdgeCoords(0, p0);
	c2 = qqMakeEdgeCoords(1, p1);

	if (!(c1 & c2)) {

		t1 = 0.0;
		t2 = 1.0;
		for (i = 0; i < 6; i++)
			if (qqwc[0][i] < 0.0 || qqwc[1][i] < 0.0) {
				t = qqwc[0][i] / (qqwc[0][i] - qqwc[1][i]);
				if (qqwc[0][i] < 0.0) {
					if (t > t1) 
					    t1 = t;
				} else if (t < t2) 
					    t2 = t;
			}
		 
		 if (t2 >= t1) {
			qqvdevice.cpVvalid = 1;
			dx = p1[V_X] - p0[V_X];
			dy = p1[V_Y] - p0[V_Y];
			dz = p1[V_Z] - p0[V_Z];
			dw = p1[V_W] - p0[V_W];
			if (t2 != 1.0) {
				p1[V_X] = p0[V_X] + t2 * dx;
				p1[V_Y] = p0[V_Y] + t2 * dy;
				p1[V_Z] = p0[V_Z] + t2 * dz;
				p1[V_W] = p0[V_W] + t2 * dw;
				qqvdevice.cpVvalid = 0;
			}
			if (t1 != 0.0) {
				p0[V_X] = p0[V_X] + t1 * dx;
				p0[V_Y] = p0[V_Y] + t1 * dy;
				p0[V_Z] = p0[V_Z] + t1 * dz;
				p0[V_W] = p0[V_W] + t1 * dw;
			}
			qqvdevice.cpVx = WtoVx(p0);
			qqvdevice.cpVy = WtoVy(p0);

			if (qqvdevice.attr->a.style) {
				qqdashline(p0, p1);
				return;
			}

			vx = qqWtoVx(p1);
			vy = qqWtoVy(p1);
			(*qqvdevice.dev.Vdraw)(vx, vy);
			qqvdevice.cpVx = vx;
			qqvdevice.cpVy = vy;
		}
	}
}

/*
 * MakeEdgeCoords
 *
 * calculates distance from point to each clipping plane in Homogeneous
 * clipping coordinates. Return code if on outside of any clipping plane
 * is non-zero.
 *
 */
int
qqMakeEdgeCoords(i, p)
	int	i;
	Vector	p;
{
	int	j, k = 0;

	qqwc[i][0] = p[V_W] + p[V_X];
	qqwc[i][1] = p[V_W] - p[V_X];
	qqwc[i][2] = p[V_W] + p[V_Y];
	qqwc[i][3] = p[V_W] - p[V_Y];
	qqwc[i][4] = p[V_W] + p[V_Z];
	qqwc[i][5] = p[V_W] - p[V_Z];
	
	for (j = 0; j < 6; j++)
		if (qqwc[i][j] < 0.0)
			k |= qqplanes[j];

	return(k);
}

/*
 * quickclip
 *
 * A variation on the above that assumes p0 is a valid position in device coords
 *
 */
void
qqquickclip(p0, p1)
	register Vector	p0, p1;
{
	register float	t, t1;
	register int	vx, vy, i;

	t1 = 1.0;

	qqwc[0][0] = p0[V_W] + p0[V_X];
	qqwc[0][1] = p0[V_W] - p0[V_X];
	qqwc[0][2] = p0[V_W] + p0[V_Y];
	qqwc[0][3] = p0[V_W] - p0[V_Y];
	qqwc[0][4] = p0[V_W] + p0[V_Z];
	qqwc[0][5] = p0[V_W] - p0[V_Z];
	qqwc[1][0] = p1[V_W] + p1[V_X];
	qqwc[1][1] = p1[V_W] - p1[V_X];
	qqwc[1][2] = p1[V_W] + p1[V_Y];
	qqwc[1][3] = p1[V_W] - p1[V_Y];
	qqwc[1][4] = p1[V_W] + p1[V_Z];
	qqwc[1][5] = p1[V_W] - p1[V_Z];

	for (i = 0; i < 6; i++)
		if (qqwc[0][i] >= 0.0 && qqwc[1][i] < 0.0) {
			t = qqwc[0][i] / (qqwc[0][i] - qqwc[1][i]);
			if (t < t1)
				    t1 = t;
		}
	 
	qqvdevice.cpVvalid = 1;
	if (t1 != 1.0) {
		p1[V_X] = p0[V_X] + t1 * (p1[V_X] - p0[V_X]);
		p1[V_Y] = p0[V_Y] + t1 * (p1[V_Y] - p0[V_Y]);
		p1[V_Z] = p0[V_Z] + t1 * (p1[V_Z] - p0[V_Z]);
		p1[V_W] = p0[V_W] + t1 * (p1[V_W] - p0[V_W]);
		qqvdevice.cpVvalid = 0;
	}

	if (qqvdevice.attr->a.style) {
		qqdashline(p0, p1);
		return;
	}

	vx = qqWtoVx(p1);
	vy = qqWtoVy(p1);

	(*qqvdevice.dev.Vdraw)(vx, vy);
	qqvdevice.cpVx = vx;
	qqvdevice.cpVy = vy;
}

/*
 * clipping
 *
 * 	Turns clipping on or off.
 */
void
qqclipping(onoff)
	int	onoff;
{
	qqvdevice.clipoff = !onoff;
}

#include <stdio.h>
#include "vogle.h"

static TokList		*qqcurrent;

/*
 * newtokens
 *
 *	returns the space for num tokens
 */
Token *
qqnewtokens(num)
	int	num;
{
	TokList	*tl;
	Token	*addr;
	int	size;

	if (qqvdevice.tokens == (TokList *)NULL || num >= MAXTOKS - qqcurrent->count) {
		if ((tl = (TokList *)malloc(sizeof(TokList))) == (TokList *)NULL)
			verror("newtokens: malloc returns NULL");

		if (qqvdevice.tokens != (TokList *)NULL)
			qqcurrent->next = tl;
		else 
			qqvdevice.tokens = tl;

		tl->count = 0;
		tl->next = (TokList *)NULL;
		if (num > MAXTOKS)
			size = num;
		else
			size = MAXTOKS;
		if ((tl->toks = (Token *)malloc(size * sizeof(Token))) == (Token *)NULL)
			qqverror("newtokens: malloc returns NULL");

		qqcurrent = tl;
	}

	addr = &qqcurrent->toks[qqcurrent->count];
	qqcurrent->count += num;

	return(addr);
}

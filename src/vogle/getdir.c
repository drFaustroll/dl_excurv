/* Last Update: 29/06/88                                                 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*       subroutine getdir (irc,dirnam)                                  */
/*                                                                       */
/* Purpose: Return (in dirnam) the default directory of the user name    */
/*          supplied in dirnam.                                          */
/*                                                                       */
/*       integer       irc                                               */
/*       character*(*) dirnam                                            */
/*                                                                       */
/* irc   : Return code 0 for success                                     */
/*                     1 for invalid user name                           */
/* dirnam: On calling getdir, contains the user's login name             */
/*         On return, contains the name of the user's default directory  */
/*                                                                       */
/*-----------------------------------------------------------------------*/

#include <pwd.h>
#include <stdio.h>

#ifdef IBM
void
getdir(irc,dirname)
#else
void
getdir_(irc,dirname)
#endif

int *irc ;
char *dirname ;

{	char *p = dirname;
	struct passwd *info;
	info = getpwnam (dirname);
	if (info == NULL ) *irc = 1;
	else {
		*irc = 0;
		while ((*p++ = *(info->pw_dir)++) != '\0')
		;
	}
}

#include <stdio.h>

extern double	sqrt();

#include "vogle.h"

static	int	qqdo_dash = 1;

/* a == b or b > a */
#define NEAR(a, b)	(((a) - (b)) / (a) < 0.0001)
#define checkdash(l)	((*(++l->dashp) == '\0' ? *(l->dashp = l->style) : *l->dashp) != '0')

/*
 * Set the current dash length
 */
void
qqsetdash(d)
	float	d;
{	
	if (d < 0.0)
		d = -d;

	if (d == 0.0)
		d = 0.1;

	qqvdevice.attr->a.dash = d;
}
	
/*
 * Set the current linestyle as a character string of 1's and 0's
 */
void
qqlinestyle(l)
	unsigned char	*l;
{
	Attribute *line;

	line = &qqvdevice.attr->a;

	if (!l || !*l) {
		line->style = NULL;
	} else {
		if (line->style)
			free(line->style);	/* probably could realloc */
		line->style = (unsigned char *)vallocate(strlen(l) + 1);
		strcpy(line->style, l);
		line->dashp = line->style;
		line->adist = 0.0;
		qqdo_dash = *line->dashp != '0';
	}

	return;
}

/*
 * Draw dashed lines (Duh)
 * Assumes p0 - p1 are valid (possibly clipped endpoints)
 */
qqdashline(p0, p1)
	register Vector p0, p1;
{
	int	vx, vy, sync;
	float	dx, dy, dz, dw, dist, ldist, tdist;
	Vector	pd;
	Attribute *line;


	line = &qqvdevice.attr->a;
	if (line->dash == 0.0) {
		vx = WtoVx(p1);
		vy = WtoVy(p1);

		(*qqvdevice.dev.Vdraw)(vx, vy);
		return;
	}

	/*
	 * The distance for this line segment
	 */
	dx = p1[V_X] - p0[V_X];
	dy = p1[V_Y] - p0[V_Y];
	dz = p1[V_Z] - p0[V_Z];
	dw = p1[V_W] - p0[V_W];
	ldist = sqrt(dx*dx + dy*dy + dz*dz + dw*dw);

	/* 
	 * If this distance is less than it takes to
	 * complete the current dash then just do it.
	 */
	if (ldist <= (line->dash - line->adist)) {

		if (NEAR(line->dash, line->adist)) {
			line->adist = 0.0;
			qqdo_dash = checkdash(line);
		}

		line->adist += ldist;

		vx = WtoVx(p1);
		vy = WtoVy(p1);

		if (qqdo_dash)
			(*qqvdevice.dev.Vdraw)(vx, vy);

		qqvdevice.cpVx = vx;
		qqvdevice.cpVy = vy;

		return;

	} else {
		if (sync = qqvdevice.sync)                /* We'll sync at the end */
			qqvdevice.sync = 0;

		/* 
		 * If this distance will take us over the end of a
		 * dash then break it up.
		 */

		/*
		 * Handle the initial case where we start in the middle
		 * of a dash.
		 */

		tdist = 0.0;
		copyvector(pd, p0);

		if (line->adist > 0.0) {

			tdist = (line->dash - line->adist);

			if (NEAR(line->dash, line->adist)) {
				line->adist = 0.0;
				qqdo_dash = checkdash(line);
			}

			line->adist += tdist;

			dist = tdist / ldist;
			pd[V_X] += dx * dist;
			pd[V_Y] += dy * dist;
			pd[V_Z] += dz * dist;
			pd[V_W] += dw * dist;
			vx = WtoVx(pd);
			vy = WtoVy(pd);


			if (qqdo_dash)
				(*qqvdevice.dev.Vdraw)(vx, vy);

			qqvdevice.cpVx = vx;
			qqvdevice.cpVy = vy;
		}

		dx *= line->dash / ldist;
		dy *= line->dash / ldist;
		dz *= line->dash / ldist;
		dw *= line->dash / ldist;
		dist = line->dash;
		
		while (tdist <= ldist - dist) {

			if (NEAR(line->dash, line->adist)) {
				line->adist = 0.0;
				qqdo_dash = checkdash(line);
			}

			pd[V_X] += dx;
			pd[V_Y] += dy;
			pd[V_Z] += dz;
			pd[V_W] += dw;

			vx = WtoVx(pd);
			vy = WtoVy(pd);

			line->adist += dist;
			tdist += dist;

			if (qqdo_dash)
				(*qqvdevice.dev.Vdraw)(vx, vy);

			qqvdevice.cpVx = vx;
			qqvdevice.cpVy = vy;
		}


		/*
		 * Check the last little bit....
		 */
		if (NEAR(line->dash, line->adist)) {
			line->adist = 0.0;
			qqdo_dash = checkdash(line);
		}

		dx = p1[V_X] - pd[V_X];
		dy = p1[V_Y] - pd[V_Y];
		dz = p1[V_Z] - pd[V_Z];
		dw = p1[V_W] - pd[V_W];
		dist = sqrt(dx*dx + dy*dy + dz*dz + dw*dw);

		line->adist += dist;

		vx = WtoVx(p1);
		vy = WtoVy(p1);

		if (qqdo_dash)
			(*qqvdevice.dev.Vdraw)(vx, vy);

		qqvdevice.cpVx = vx;
		qqvdevice.cpVy = vy;
	}

	if (sync) {
		qqvdevice.sync = 1;
		(*qqvdevice.dev.Vsync)();
	}

}

/*
 * draw
 *
 * draw a line form the logical graphics position to the
 * the world coordinates x, y, z.
 *
 */
void
qqdraw(x, y, z)
	float		x, y, z;
{
	Token	*tok;
	int	vx, vy;
	Vector	res;


	if (!qqvdevice.initialised)
		verror("draw: vogle not initialised");

	if (qqvdevice.inpolygon) {
		(*qqvdevice.pdraw)(x, y, z);

		qqvdevice.cpW[V_X] = x;
		qqvdevice.cpW[V_Y] = y;
		qqvdevice.cpW[V_Z] = z;

		qqvdevice.cpVvalid = 0;

		return;
	}

	if (qqvdevice.inobject) {
		tok = qqnewtokens(4);

		tok[0].i = DRAW;
		tok[1].f = x;
		tok[2].f = y;
		tok[3].f = z;

		qqvdevice.cpW[V_X] = x;
		qqvdevice.cpW[V_Y] = y;
		qqvdevice.cpW[V_Z] = z;

		qqvdevice.cpVvalid = 0;

		return;
	}

	if (!qqvdevice.cpVvalid)
		qqmultvector(qqvdevice.cpWtrans, qqvdevice.cpW, qqvdevice.transmat->m);

	qqvdevice.cpW[V_X] = x;
	qqvdevice.cpW[V_Y] = y;
	qqvdevice.cpW[V_Z] = z;
	qqmultvector(res, qqvdevice.cpW, qqvdevice.transmat->m);

	if (qqvdevice.clipoff) {	
		vx = qqWtoVx(res);		/* just draw it */
		vy = qqWtoVy(res);
	 
		if (qqvdevice.attr->a.style) {
			qqdashline(qqvdevice.cpWtrans, res);
			qqvdevice.cpVvalid = 0;
			return;
		}

		(*qqvdevice.dev.Vdraw)(vx, vy);

		qqvdevice.cpVx = vx;
		qqvdevice.cpVy = vy;

		qqvdevice.cpVvalid = 0;
	} else {
		if (qqvdevice.cpVvalid)
			qqquickclip(qqvdevice.cpWtrans, res);
		else
			qqclip(qqvdevice.cpWtrans, res);
	}

	qqvdevice.cpWtrans[V_X] = res[V_X];
	qqvdevice.cpWtrans[V_Y] = res[V_Y];
	qqvdevice.cpWtrans[V_Z] = res[V_Z];
	qqvdevice.cpWtrans[V_W] = res[V_W];
}


/*
 * draw2
 *
 * draw a line from the logical graphics position  to the
 * the world coordinates x, y.
 *
 */
void
qqdraw2(x, y)
	float		x, y;
{
	if (!qqvdevice.initialised)
		verror("draw2: vogle not initialised");

	qqdraw(x, y, 0.0);
}

/*
 * rdraw
 *
 * 3D relative draw from the logical graphics position by dx, dy, dz.
 *
 */
void
qqrdraw(dx, dy, dz)
	float		dx, dy, dz;
{
	if (!qqvdevice.initialised) 
		verror("rdraw: vogle not initialised");

	qqdraw((qqvdevice.cpW[V_X] + dx), (qqvdevice.cpW[V_Y] + dy), (qqvdevice.cpW[V_Z] + dz));
}

/*
 * rdraw2
 *
 * 2D relative draw from the logical graphics position by dx, dy.
 *
 */
void
qqrdraw2(dx, dy)
	float		dx, dy;
{
	if (!qqvdevice.initialised) 
		verror("rdraw2: vogle not initialised");

	qqdraw((qqvdevice.cpW[V_X] + dx), (qqvdevice.cpW[V_Y] + dy), 0.0);
}

/*
 * sdraw2
 *
 * Draw directly in proportion to screen coordinates.
 */
void
qqsdraw2(xs, ys)
	float 	xs, ys;
{
	int	nx, ny;

	if (!qqvdevice.initialised) 
		verror("sdraw2: vogle not initialised");

	nx = (xs / 2 + 0.5) * (qqvdevice.maxVx - qqvdevice.minVx);
	ny = (0.5 + ys / 2) * (qqvdevice.maxVy - qqvdevice.minVy);

	(*qqvdevice.dev.Vdraw)(nx, ny);

	qqvdevice.cpVx = nx;
	qqvdevice.cpVy = ny;
	qqvdevice.cpVvalid = 0;
}

/*
 * rsdraw2
 *
 * Relative draw as a fraction of screen size.
 */
void
qqrsdraw2(dxs, dys)
	float	dxs, dys;
{
	int	ndx, ndy;

	if (!qqvdevice.initialised) 
		verror("rsdraw2: vogle not initialised");

	ndx = dxs * qqvdevice.sizeSx / 2;
	ndy = dys * qqvdevice.sizeSy / 2;

	(*qqvdevice.dev.Vdraw)(qqvdevice.cpVx + ndx, qqvdevice.cpVy + ndy);

	qqvdevice.cpVx += ndx;
	qqvdevice.cpVy += ndy;
}


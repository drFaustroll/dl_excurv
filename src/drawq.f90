SUBROUTINE DRAWQ (*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_PLFLAG
  Use Common_PGROUP
  Use Index_IGF
  Use Common_ATOMCORD
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Common_DISTANCE
  Use Common_VIEW
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  LOGICAL :: CLEAR,POSITIVE,IUCLUS
  SAVE UXM
  CHARACTER*8 :: OPTS(4)
  CHARACTER*4 :: TEMP,KP1,KP2,KP3
  DATA OPTS/'CLUSTER','NOCLEAR','POSITIVE','UNIT'/
  DATA TEMP /'bc2d'/,IUCLUS/.TRUE./
  MAXK=1
  SROT=ROT(1)
  KLM=1
  !	DO KLM=1,MAXK
  TEMP(1:1)=CHAR(27)
  TEMP(2:2)=CHAR(92)
  IF (MOD(KLM,2).EQ.0.AND.IGF(ITNUM).NE.5) THEN
     WRITE (OUTTERM,10) TEMP
  ELSE
     WRITE (OUTTERM,*)
  ENDIF
10 FORMAT (A,$)
  CALL FINDOPT (4,OPTS,IOPT,1,'None',*70)
  CLEAR=IOPT.NE.2
  IF (IOPT.EQ.1) THEN
     IUCLUS=.TRUE.
  ELSEIF (IOPT.EQ.4) THEN
     IUCLUS=.FALSE.
  ENDIF
  POSITIVE=IOPT.EQ.3
  !	IF (NAT(1).LT.1) THEN
  !	  KEYWORD=' '
  !	  CALL SYMMETRY (.TRUE.,.TRUE.,*70)
  !	ENDIF
  MAXCLUS=0
  DO I=0,NS
     MAXCLUS=MAX(MAXCLUS,IABS(ICLUS(I)))
  enddo
  JJ=0
  IDIVIS=2
  IF (IUCLUS) THEN
     MMAX=MAXCLUS
  ELSE
     MMAX=MG
     IF (INTOPT.LE.0.OR.INTOPT.GT.IPARNUNITS.OR.MG.LE.0) RETURN 1
     IF (NR(INTOPT).LE.0.OR.NR(INTOPT).GT.NS) RETURN 1
  ENDIF
  DO JCLUS=1,MMAX
     IF (INTOPT.GT.0.AND.JCLUS.NE.INTOPT) cycle
     IF (NAT(JCLUS).LT.1) THEN
        KEYWORD=' '
        CALL SYMMETRY (.TRUE.,.TRUE.,*70)
     ENDIF
     DO KCLUS=1,NS
	IF (IUCLUS) THEN
           JJCLUS=JCLUS
           IF (ICLUS(KCLUS).EQ.JCLUS) GOTO 30
	ELSE
           JJCLUS=IABS(ICLUS(NR(INTOPT)))
           IF (NU(KCLUS).EQ.JCLUS) GOTO 30
	ENDIF
     ENDDO
     cycle
30   IF (MMAX.NE.1.AND.INTOPT.EQ.0) JJ=JJ+1
     IF (CLEAR) THEN
        UXM=0.
        DO I=1,NS
           IF (IUCLUS) THEN
              IF (IABS(ICLUS(I)).NE.JCLUS) cycle
           ELSE
              IF (NU(I).NE.JCLUS) cycle
           ENDIF
           IF (N(I).GT.0.AND.RAD(I)/DC.GT.UXM) UXM=RAD(I)/DC
        enddo
     ENDIF
     IF (UXM.EQ.0.) UXM=1.
     IF (VX.NE.0..OR.VY.NE.0..OR.VZ.NE.0.) THEN
        VVX=VX*UXM
        VVY=VY*UXM
        IF (VVX.EQ.0..AND.VVY.EQ.0.) THEN
           VVX=1.E-10
           VVY=1.E-10
        ENDIF
        VVZ=VZ*UXM
     ELSE
        IF (CLEAR) THEN
           CALL MINOVER (UXM,POSITIVE,JJCLUS)
        ELSE
           VVX=VVX*UXM
           VVY=VVY*UXM
           VVZ=VVZ*UXM
        ENDIF
     ENDIF
     IF (JJ.LE.1.AND.CLEAR) CALL GRINIT
     IF (IGF(1).EQ.2) THEN
50      IF (LMDSN.EQ.0.AND.LRIS.EQ.0) THEN
           IF (IGF(2).NE.3) CALL TPOS (0,740)
           CALL WTEXT ('Select Frame (0, 1-9)')
        ENDIF
        IDIVIS=3
        CALL CREAD (KP1,KP2,KP3,JJ,IC,V,*50,*70)
        IF (JJ.LT.0.OR.JJ.GT.9) CALL ERRS (*50)
     ENDIF
     IF (IGF(2).NE.3) THEN
        PLCMD=.TRUE.
        IF (IGF(2).EQ.2.OR.IGF(ITNUM).EQ.5) THEN
           CALL PSPACE (0.,1.,0.,1.)
        ELSE
           CALL PSPACE (0.,.763,0.,1.)
           CALL PSPACE (0.,1.,0.,1.)
        ENDIF
        IF (JJ.NE.0) THEN
           F2=MOD(JJ-1,IDIVIS)
           F1=(JJ-1)/IDIVIS
           F2=IDIVIS-1-F2
           IF (IGF(2).EQ.2.OR.IGF(ITNUM).EQ.5) THEN
	      DIVIS=IDIVIS
	      XDIVIS=IDIVIS
           ELSE
	      DIVIS=IDIVIS
	      XDIVIS=DIVIS/.763
           ENDIF
           DIVIS=IDIVIS
           CALL PSPACE (F1/XDIVIS,(F1+1.)/XDIVIS,F2/DIVIS,(F2+1.)/DIVIS)
           IF (JJ.EQ.3) THEN
	      USED3=.TRUE.
           ELSEIF (JJ.EQ.4) THEN
	      USED4=.TRUE.
           ENDIF
        ENDIF
        CALL DRAWQQ (UXM,JCLUS,CLEAR,IUCLUS)
     ENDIF
     VVX=VVX/UXM
     VVY=VVY/UXM
     VVZ=VVZ/UXM
     NUMFLAG=2
     IF (MAXK.EQ.10) CALL PCHANGE ('ROT1',ROT(1)+36.)
     KEYWORD='N'
     NUMFLAG=2
     IF (MAXK.EQ.10) CALL PCHANGE ('ROT1',SROT)
  enddo
  RETURN
70 RETURN 1
END SUBROUTINE DRAWQ

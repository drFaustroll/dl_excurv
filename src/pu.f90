SUBROUTINE PU (K,*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_PLFLAG
  Use Index_IGF
  Use Common_PMTX
  Use Common_UPU
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  !
  !	Process the PLOT UNIT command
  !
  !      *** INCLUDES CALLS TO GHOST LIBRABRY ROUTINES ***
  !
  CHARACTER*4 LABEL,IP1,IP2,IP3,YES
  LOGICAL CLEAR
  DATA YES/'YES'/
  CLEAR=.TRUE.
  IF (K.LT.0) THEN
     K=-K
     CLEAR=.FALSE.
  ELSEIF (K.EQ.0) THEN
     K=1
  ENDIF
  IF (K.GT.MG) THEN
     CALL ERRMSGI ('Unit Not Defined : ',K,*130)
  ENDIF
  NBOND=0
  JJ=0
  IF (CLEAR) CALL GRINIT
  IF (IGF(2).NE.3) THEN
     PLCMD=.TRUE.
     !	  IF (CLEAR) CALL ERASE
     !	  CALL PSPACE (.04,.9,.04,.86)
     IF (IGF(2).EQ.2.OR.IGF(ITNUM).EQ.5) THEN
        CALL PSPACE (0.,1.,0.,1.)
     ELSE
        CALL PSPACE (0.,.6,.2136,1.)
     ENDIF
     CALL PUQ (K,CLEAR,UXM)
  ENDIF
  !	IF (IGF(2).EQ.1) GOTO 10
  !	WRITE (PLOTFILE) 'UNIT'
  !	WRITE (PLOTFILE) K,IPA,PA1,IUP,UXM,IGF,CLEAR  !-st this is the only place were IUP used, 
  !-st                                                   and it was commented already so I have decided 
  !-st                                                   to purge all EQUIVALENCE(IUP(1),MG) blocks
  !  10
  CALL TPOS (0,720)
  CALL WTEXT ('$Do you wish to change unit parameters ? [n] ')
  CALL CREAD (IP1,IP2,IP3,INT,IC,V,*125,*125)
  NY=NCSTR(IP1)
  IF (IP1(1:NY).NE.YES(1:NY)) GOTO 120
  CALL WTEXT ('$Name ? ['//UNAME(K)//'] ')
  CALL CREAD (IP1,IP2,IP3,INT,IC,V,*20,*120)
  UNAME(K)=IP1
20 CALL WTEXTIT ('$Residue number ? [',IRESNUM(K),'] ')
  CALL CREAD (IP1,IP2,IP3,INT,IC,V,*30,*120)
  GOTO 40
30 INT=IRESNUM(K)
40 IRESNUM(K)=INT
  CALL WTEXTIT ('$Pivotal atom ? [',NR(K),'] ')
  CALL CREAD (IP1,IP2,IP3,INT,IC,V,*50,*120)
  GOTO 60
50 INT=NR(K)
60 PIV(K)=INT
  IPIV(K)=INT
  PIV2(K)=INT
  CALL WTEXTIT ('$Planar atom A ? [',IPLA(K),'] ')
  CALL CREAD (IP1,IP2,IP3,INT,IC,V,*70,*120)
  PLA(K)=INT
  IPLA(K)=INT
  PLA2(K)=INT
70 CALL WTEXTIT ('$Planar Atom B ? [',IPLB(K),'] ')
  CALL CREAD (IP1,IP2,IP3,INT,IC,V,*80,*120)
  PLB(K)=INT
  IPLB(K)=INT
  PLB2(K)=INT
80 DO  J=1,4
     CALL WTEXTIT ('$Torsion angle A - atom '//CHAR(48+J)//' ? [',NTORA(J,K),'] ')
     CALL CREAD (IP1,IP2,IP3,INT,IC,V,*90,*120)
     NTORA(J,K)=INT
90   CONTINUE
  enddo
  DO  J=1,4
     CALL WTEXTIT ('$Torsion angle B - atom '//CHAR(48+J)//' ? [',NTORB(J,K),'] ')
     CALL CREAD (IP1,IP2,IP3,INT,IC,V,*100,*120)
     NTORB(J,K)=INT
100  CONTINUE
  enddo
  CALL WTEXT (' ')
  CALL WTEXT ('Atom labels:')
  DO  I=1,NS
     IF (NU(I).EQ.K) THEN
        CALL WTEXTIT ('$ ',I,' ['//ATLABEL(I)//']')
        CALL CREAD (IP1,IP2,IP3,INT,IC,V,*110,*120)
        ATLABEL(I)=IP1
     ENDIF
110  CONTINUE
  enddo
120 CALL UTAB (0)
125 RETURN
130 RETURN 1
END SUBROUTINE PU

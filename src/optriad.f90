SUBROUTINE OPTRIAD (AR,ATH,APHI,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - rotation about triad //(111)
  !
  CALL POLTOCAR (AR,ATH,APHI,TX,TY,TZ)
  TU=TZ
  TZ=TY
  TY=TX
  TX=TU
  CALL CARTOPOL (TX,TY,TZ,AR,ATH,APHI)
  IS=IS+1
  RETURN 1
END SUBROUTINE OPTRIAD

SUBROUTINE POLANG (ANGFAC,POLLEG,JSHELL,JSYMS,NPG,AMPFAC,TTH1,TPHI1,TTH2,TPHI2)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_SOUPER
  Use Common_POL
  Use Common_YLM
  Use Include_IPA
  Use Include_PA2
  Use Include_XY
  !
  !	Calculate  angular part of dipole operator for exact polarisation dependent case.
  !
  !	COMPLEX CI,A,B,ABA(-1:1),SUM1(-3:3),SUM2(-3:3),SUM(IPARNSP)
  COMPLEX SUM1(-3:3),SUM2(-3:3),SUM(IPARNSP)
  DIMENSION JSYMS(0:IPARNS),ANGFAC(IPARNSP)
  DO I=1,IPARNSP
     ANGFAC(I)=AFAC02(I)*AFAC02(0)*AMPFAC
  ENDDO
  IF (iset(ISING).EQ.2.AND.iset(IEXTYP).EQ.2) THEN
     CALL WTEXT ('Exact polarisation dependence only available with curved wave theory - THEORY set to CW')
     iset(IEXTYP)=1
  ENDIF
  IF (iset(ISING).NE.1.OR.ABS(POLLEG).LT.1.E-1.OR.IPOL(IEXP).EQ.0) RETURN
  !	CI=(0.,1.)
  NUM=N(JSHELL)
  IF (NUM.LE.0.OR.ICLUS(JSHELL).LE.0) RETURN
  THETAI=TTH1
  PHII=TPHI1
  THETAF=TTH2
  PHIF=TPHI2
  DO JEXP=IEXP,IEXP+JSPEC-1
     SUM(JEXP)=0.
  ENDDO
  DO KA2=1,NUM
     !	IF (DEBUG) WRITE (6,'(A,4F8.3)') 'POLANG:',THETAI/AC,PHII/AC,THETAF/AC,PHIF/AC
     CALL SHM4 (THETAI,PHII,1,IEDGE)
     CALL SHM4 (THETAF,PHIF,2,IEDGE)
     !
     !	Calculate angle factors
     !
     DO JEXP=IEXP,IEXP+JSPEC-1
	DO M0=-LINITIAL(JEXP),LINITIAL(JEXP)
           SUM1(M0)=0
           SUM2(M0)=0
           !	WRITE (6,'(a,3(6f8.3/))') 'ylm',(conjg(ylm(2,iedge,j)),j=-1,1), &
           !                              (ylm(1,iedge,j),j=-1,1),ccang1(0,0,1)*ccang2(0,0,1)*12.*pi
           DO MF=-IEDGE,IEDGE
              SUM1(M0)=SUM1(M0)+CCANG1(M0,MF,JEXP)*CONJG(YLM(2,IEDGE,MF))
              SUM2(M0)=SUM2(M0)+YLM(1,IEDGE,MF)*CCANG2(MF,M0,JEXP)
           ENDDO
	ENDDO
	DO M0=-LINITIAL(JEXP),LINITIAL(JEXP)
           SUM(JEXP)=SUM(JEXP)+SUM1(M0)*SUM2(M0)
	ENDDO
        !
        !	End of JEXP loop
        !
     ENDDO
     RR=1.
     CALL PERFOP (RR,THETAI,PHII,OPS(KA2,JSYMS(JSHELL),NPG))
     CALL PERFOP (RR,THETAF,PHIF,OPS(KA2,JSYMS(JSHELL),NPG))
     !
     !	END OF NUM LOOP
     !
  ENDDO
  DO JEXP=IEXP,IEXP+JSPEC-1
     IF (IEDGE.GT.LINITIAL(JEXP)) THEN
        WT=(LINITIAL(JEXP)+1)*NUM
     ELSE
        WT=LINITIAL(JEXP)*NUM
     ENDIF
     ANGFAC(JEXP)=ANGFAC(JEXP)*12.*PI*SUM(JEXP)/(POLLEG*WT)
     !	ANGFAC(JEXP)=-ANGFAC(JEXP)*12.*PI*SUM(JEXP)/(WT)
     IF (DEBUG) WRITE (7,'(A,I2,4F8.3)') 'ANGFAC',JEXP,ANGFAC(JEXP),12.*PI*SUM(JEXP)/real(NUM,kind=dp),POLLEG
  ENDDO
  RETURN
END SUBROUTINE POLANG

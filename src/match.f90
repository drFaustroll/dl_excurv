LOGICAL FUNCTION MATCH (STR1,STR2)
  !======================================================================C
  !
  !	See if two character strings match as far as the number of
  !	characters in the first string
  !
  !	STR1 (I)   FIRST CHARACTER STRING OR SUBSTRING
  !	STR2 (I)   SECOND CHARACTER STRING OR SUBSTRING
  !
  CHARACTER*(*) STR1,STR2
  MATCH=.FALSE.
  L1=LEN(STR1)
  L2=LEN(STR2)
  IF (L2.LT.L1) RETURN
  IF (STR1.EQ.STR2(1:L1)) MATCH=.TRUE.
  RETURN
END FUNCTION MATCH

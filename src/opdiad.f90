SUBROUTINE OPDIAD (ATH,APHI,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - 2-fold rotation about x
  !
  Use Common_convia
  ATH=PI-ATH
  APHI=2.*PI-APHI
  IS=IS+1
  RETURN 1
END SUBROUTINE OPDIAD

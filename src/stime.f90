INTEGER FUNCTION STIME()
  !
  ! calls the ibm function time which returns the current time in seconds
  !
  integer  CURRTIME, time(8)

  CALL date_and_time(values=time)
  CURRTIME = ((time(5)*24)+time(6))*60+time(7)
  STIME = CURRTIME
  RETURN
END FUNCTION STIME

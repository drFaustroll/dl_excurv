SUBROUTINE COSQT (TABLE)
  !======================================================================C
  Use Definition
  Use Common_CONVIA
  !       
  !	COSQT generates quarter-length cosine table
  !       
  real(dp) :: TABLE(*)
  N=2048
  ND4P1=N/4+1
  SCL=2.*PI/real(N,kind=dp)
  DO I=1,ND4P1
     ARG=real(I-1,kind=dp)*SCL
     TABLE(I)=COS(ARG)
  ENDDO
  RETURN
END SUBROUTINE COSQT

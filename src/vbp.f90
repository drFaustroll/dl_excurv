SUBROUTINE VBP (IZIN,IQIN,XIN,RHOIN,NR,VHOUT)
  !======================================================================C
  Use Definition
  IMPLICIT real(float) (A-H,O-Z)
  PARAMETER (NRM=999)
  real(sp)    XIN(NRM),RHOIN(NRM),VHOUT(NRM)           ! must be single prec.
  real(float) X(NRM),RHO(NRM),VH(NRM),RH(NRM),F(NRM)   ! this work
  DATA FI4/0.25/, F2 / 2.0/, F12/12.0/, F30/30.0/
  !
  !	****************************************************************
  !	*                                                              *
  !	*  VBP SOLVES THE POISSON EQUATION FOR THE COULOMB POTENTIAL   *
  !	*  VH DUE TO AN ELECTRON DENSITY RHO, WHICH IS ASSUMED TO BE   *
  !	*  TABULATED ON AN EQUISPACED LOGARITHMIC GRID X. AFTER        *
  !	*  MULTIPLYING BY THE INTEGRATING FACTOR R**1/2 THE BAYLISS-   *
  !	*  PEEL NUMEROV PREDICTOR-CORRECTOR METHOD - CPC VOL.25 7      *
  !	*  (1982) - IS USED.                                           *
  !	*                                                              *
  !	*          ***** NB. 64-BIT PRECISION NECESSARY *****          *
  !	*                                                              *
  !	****************************************************************
  !	*                                                              *
  !	*  INPUT :                                                     *
  !	*                                                              *
  !	*  Z             : ATOMIC NUMBER.                              *
  !	*  Q             : TOTAL CHARGE IN RHO.                        *
  !	*  X(I),I=1,NR   : LOG(R) GRID.                                *
  !	*  RHO(I),I=1,NR : 4*PI*(R**2)*ELECTRON DENSITY.               *
  !	*                                                              *
  !	****************************************************************
  !	*                                                              *
  !	*  OUTPUT :                                                    *
  !	*                                                              *
  !	*  VH(I),I=1,NR  : COULOMB POTENTIAL (ENERGY) IN HARTREES.     *
  !	*                                                              *
  !	****************************************************************
  !
  !	LOAD R**1/2 INTO RH AND SET UP INHOMOGENEOUS TERM F(I)
  !
  Z=IZIN
  Q=IQIN
  DO  I=1,NR
     X(I)=XIN(I)
     RHO(I)=RHOIN(I)
     RH(I)=EXP(X(I)/F2)
     F(I)=-RH(I)*RHO(I)
     !	write (7,*) 'vbp',i,xin(i),rhoin(i)
  enddo
  !
  !	SET UP CONSTANTS FOR INTEGRATION ALGORITHM
  !
  H=X(2)-X(1)
  H2=H*H
  FC=H2/F12
  FM=H2*FI4/F30
  !
  !	BACKWARD INTEGRATION FROM CORRECT BOUNDARY CONDITION AT R(NR)
  !	USING BAYLISS-PEEL ALGORITHM
  !
  VH(NR)=Q/RH(NR)
  VH(NR-1)=Q/RH(NR-1)
  DO I=NR-2,1,-1
     VHP=F2*VH(I+1)-VH(I+2)+H2*(FI4*VH(I+1)+F(I+1))
     VHC=VHP+FC*(FI4*(VHP+VH(I+2)-F2*VH(I+1))+F(I)+F(I+2)-F2*F(I+1))
     VH(I)=VHC+FM*(VHC-VHP)
  enddo
  !
  !	DIVIDE BY R**1/2 TO GET COULOMB POTENTIAL AND ADD IN NUCLEAR
  !	CONTRIBUTION
  !
  DO I=1,NR
     !	   VH(I)=VH(I)/RH(I) - Z/(RH(I)*RH(I))
     VH(I)=VH(I) - Z/RH(I)
     VHOUT(I)=VH(I)
     !	write (*,*) 'vbp2:',i,rh(i),vh(i)
     !	pause
  enddo
  RETURN
END SUBROUTINE VBP

SUBROUTINE GETQUAD (TTX,TTY,I,J,REVLABEL,QUAD)
  !======================================================================C
  Use Parameters
  LOGICAL REVLABEL(0:IPARATOMS),QUAD(0:IPARATOMS,8)
  IF (TTX.GT.0.) THEN
     IF (SIGN(1.,TTX).EQ.SIGN(1.,TTY)) REVLABEL(I)=.TRUE.
     IF (TTY.GT.0.) THEN
        QUAD(I,3)=.TRUE.
        QUAD(J,1)=.TRUE.
     ELSEIF (TTY.LT.0.) THEN
        QUAD(I,4)=.TRUE.
        QUAD(J,2)=.TRUE.
     ENDIF
  ELSEIF (TTX.LT.0.) THEN
     IF (SIGN(1.,TTX).EQ.SIGN(1.,TTY)) REVLABEL(J)=.TRUE.
     IF (TTY.GT.0.) THEN
        QUAD(I,2)=.TRUE.
        QUAD(J,4)=.TRUE.
     ELSEIF (TTY.LT.0.) THEN
        QUAD(I,1)=.TRUE.
        QUAD(J,3)=.TRUE.
     ENDIF
  ENDIF
  IF (ABS(TTY).GT.ABS(TTX)) THEN
     IF (TTY.GT.0.) THEN
        QUAD(I,7)=.TRUE.
        QUAD(J,5)=.TRUE.
     ELSEIF (TTY.LT.0.) THEN
        QUAD(I,5)=.TRUE.
        QUAD(J,7)=.TRUE.
     ENDIF
  ELSEIF (ABS(TTY).LT.ABS(TTX)) THEN
     IF (TTX.GT.0.) THEN
        QUAD(I,8)=.TRUE.
        QUAD(J,6)=.TRUE.
     ELSEIF (TTX.LT.0.) THEN
        QUAD(I,6)=.TRUE.
        QUAD(J,8)=.TRUE.
     ENDIF
  ENDIF
  RETURN
END SUBROUTINE GETQUAD

SUBROUTINE SETTAB (IOPT)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Index_IGF
  Use Common_PMTX
  Use Include_PA1

  IF (IGF(2).EQ.2) THEN
     IF (IGF(IPLOTTR).EQ.2.OR.IGF(IPLOTTR).EQ.4) THEN
        !
        !	Postscript
        !
        CALL PSPACE (1.05,1.45,0.,1.)
        AAA=39.
     ELSE
        !
        !	HP
        !
        CALL PSPACE (1.,SQRT(2.),0.,1.)
        AAA=39.
     ENDIF
     CALL MAPP (0.,AAA,AAA,0.)
     CALL FFONT ('small')
  ELSE
     IF (IGF(ITNUM).EQ.5) THEN
        QDQ=.TRUE.
        CALL QQX11_RESIZE
        CALL ERASE
        CALL FQQFONT ('small')
        !	    AAA=FQUEERY('G')
        !	    IF (AAA.NE.0.) THEN
        !	       AAA=1./AAA
        !	    ELSE
        !	       AAA=35.
        !	    ENDIF
        AAA=41.
        CALL MAPP (0.,AAA,AAA*.65,0.)
     ELSE
        !
        !      UNIX
        !
        CALL PSPACE (.6,1.,0.,1.)
        !
        !      PC
        !
        !          CALL PSPACE (.55,1.,0.,1.)
        AAA=40.
        CALL MAPP (0.,AAA,AAA,0.)
        CALL FTEXTSIZE (1.6,-.8)
        CALL FTEXTSIZE (1.55,-.775)
     ENDIF
  ENDIF
  CALL FTEXTJUSTIFY (0)
  CALL FULL
  RETURN
END SUBROUTINE SETTAB

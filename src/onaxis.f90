SUBROUTINE ONAXIS (I,TPHI,TTHETA,*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_SYMOPS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2

  JAXIS=1
  IF (JROT.LT.0) JAXIS=2
  IF (JTRIAD.EQ.3) THEN
     JAXIS=2
     JROT=-2
     JCX=1
     JMIR=1
  ENDIF
  JSX=1
  IF (JMIR.EQ.2.AND.JCX.EQ.2) JCX=1
  IF (JAXIS.EQ.2.AND.JCX.EQ.2) JCX=1
  TTHETA=0.
  TPHI=0.
  IF (N(I).EQ.1.AND.RADTH(I).GT.PID2) TTHETA=PI
  IF (RADTH(I).NE.0.AND.ABS(RADTH(I)-PI).GT..00001) THEN
     DO  IW=OUTTERM,LOGFILE,OUTDIFF
        WRITE (IW,20) 'Onaxis: Shell',I,': THETA Invalid for Special Position.', 'THETA=',TTHETA/AC,' assumed.'
     enddo
  ENDIF
  RETURN
20 FORMAT (A,I3,A,A,F5.0,A)
END SUBROUTINE ONAXIS

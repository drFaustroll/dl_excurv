SUBROUTINE README (*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_MATEL
  !       
  !	Read files containing squares of dipole transition rates (matrix elements)
  !       
  CHARACTER*60 FILENAME
  GOTO 30
  !       
  !	Error in reading file
  !       
10 CLOSE (INFILE)
20 CALL ERRS(*30)
  !       
  !	Read from file
  !       
30 CALL WTEXT ('Filename for matrix elements ?')
  CALL AREAD (1,FILENAME,LD,*30,*80)
  CALL FILEOPEN (FILENAME,INFILE,'FORMATTED','dat',*20)
  ICODE=1
  !	IF (LHEAD.NE.0) ICODE=-1
  NME=IPARPHPOINTS
  CALL RDSPEC (INFILE,NME,FILENAME,XME,1,RME,2,ICODE,*10)
  IF (LINITIAL(1).GT.1) THEN
     REWIND INFILE
     CALL RDSPEC (INFILE,NME,FILENAME,XME,1,RME(1,2),4,ICODE,*10)
  ENDIF
  CALL EFILE ('Matrix elements')
  IF (NME.LT.10) CALL ERRMSG ('Less than 10 points in spectrum',*80)
80 RETURN
END SUBROUTINE README

MODULE Common_PGROUP

  !      COMMON /PGROUP/PGROUP(IPARNCLUS),NPGR(IPARNCLUS),NPGCLASS(IPARNCLU
  !     1S),  SPGSYM
  !      CHARACTER PGROUP*6,SPGSYM*20
  !        data PGROUP/IPARNCLUS*' '/NPGR/IPARNCLUS*0/

  Use Definition
  Use Parameters
  implicit none
  private

  character*6,  dimension(IPARNCLUS), public :: PGROUP
  data PGROUP/IPARNCLUS*' '/
  integer(i4b), dimension(IPARNCLUS), public :: NPGR
  data  NPGR /IPARNCLUS*0/
  integer(i4b), dimension(IPARNCLUS), public :: NPGCLASS
  character*20, public :: SPGSYM
end MODULE Common_PGROUP


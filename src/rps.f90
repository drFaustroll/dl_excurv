SUBROUTINE RPS (K)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_F
  Use Common_POT
  Use Common_P
  Use Common_RCBUF
  Use Include_IPA
  Use Include_PA1
  !
  !	Read phase shift file
  !
  !	      K (I)   The number of the phaseshifts file (1-IPARNP)
  !	   IDSN (O)   Returns the file name string for printing
  !
  CHARACTER*60 IDS2*60,IST(12)*8
  LOGICAL ERR,NUMS,PISHIFT,FIRSTTIME
  DIMENSION X(60)
  DATA IST/'Central','Second','Third','Fourth','Fifth ','Sixth','Seventh','Eighth','Ninth','Tenth','Eleventh','Twelth'/
  CALL UZERO (X,60)
  !
  !	Input file name if required
  !
10 IF (LRIS+LMDSN.EQ.0) WRITE (OUTTERM,150) IST(K)
  CALL AREAD (3,IDS2,LD,*10,*80)
  CALL FILEOPEN (IDS2,INFILE,'FORMATTED',' ',*140)
  KK=K+IPARNSP+1
  LFS(KK)=IDS2
  FIRSTTIME=.TRUE.
  IPTYPE=2
  PISHIFT=.FALSE.
  NHEAD=0
  DO  L=1,IPARLMAX+1
     DO  I=1,IPARPHPOINTS
	PHS(I,K,L)=0.
     enddo
  enddo
  DO  I=1,IPARPHPOINTS
     EREF(I,K)=0.
     EN(I,K)=0.
  enddo
  NUMS=.FALSE.
  IE=0
  NPH(K)=0
  !
  !	Read next record, check for header records before start
  !
30 READ (INFILE,160,END=70,ERR=90) BUF
  ICOLL=0
  IL=0
40 CALL NEXFLD (BUF,IL,M1,M2,*50)
  ICOLL=ICOLL+1
  X(ICOLL)=FPVAL(BUF(M1:M2),ERR)
  IF (.NOT.NUMS.AND.(ERR.OR.INDEX(BUF,'.').EQ.0)) THEN
     !
     !	Header record
     !
     IF (NHEAD.LT.10) CALL WTEXT (BUF)
     GOTO 30
  ENDIF
  IF (ERR) GOTO 100
  GOTO 40
  !
  !	Numbers only record
  !
50 NUMS=.TRUE.
  IF (FIRSTTIME) THEN
     ZED=X(1)
     HOLE=X(2)
     WRITE (OUTTERM,'(''Z='',F4.0,''Hole code'',F3.0)') ZED,HOLE
     FIRSTTIME=.FALSE.
     IF (ZED.LT.1..OR.ZED.GT.103.) CALL ERRMSG ('Invalid atomic number',*140)
     IF (HOLE.EQ.0.) THEN
        LASTHOLE(K)=-99
     ELSE
        LASTHOLE(K)=HOLE
     ENDIF
     GOTO 30
  ENDIF
  IF (ICOLL.EQ.IPTYPE*2-1) THEN
     !
     !	New energy point
     !
     IF (IE.GE.IPARPHPOINTS) GOTO 70
     IE=IE+1
     EN(IE,K)=X(1)
     IF (ICOLL.EQ.3) THEN
        EREF(IE,K)=CMPLX(X(2),X(3))
     ELSE
        EREF(IE,K)=0.
     ENDIF
     NCOLL=0
     !
     !	Check for monotonically increasing array
     !
     IF (IE.GT.1) THEN
        !
        !	Next line essential to avoid PC bug
        !
        TEST=EN(IE,K)-EN(IE-1,K)
        !	WRITE (6,*) IE,EN(IE,K),EN(IE-1,K),TEST
        IF (TEST.LE.0.) GOTO 110
     ENDIF
  ELSE
     DO  I=1,ICOLL
        NCOLL=NCOLL+1
        IF (NCOLL.GE.(IPARLMAX+IPTYPE)*IPTYPE) cycle
        !
        !	Real part
        !
        IF (IPTYPE.EQ.1) THEN
           !
           !	Only the real part to be read
           !
           PHS(IE,K,NCOLL)=X(I)
        ELSEIF (MOD(NCOLL,2).EQ.0) THEN
           J=(NCOLL+1)/2
           !
           !	X(I) is the imaginary part
           !
           PHS(IE,K,J)=CMPLX(XM1,X(I))
           IF (IE.GT.1) THEN
	      IF (ABS(XM1-REAL(PHS(IE-1,K,J))).GT.3.) PISHIFT=.TRUE.
           ENDIF
        ELSE
           XM1=X(I)
        ENDIF
     enddo
  ENDIF
  GOTO 30
  !
  !	End of file
  !
70 NPH(K)=IE
  IHCODE(K)=0
  V0EV(K)=0
  !
  !	Fix PI shifts
  !
  CALL FIXPI (K,PISHIFT)
  CALL WTEXTI ('Number Of Energy Points: ',NPH(K))
  IF (NPH(K).LT.10) GOTO 120
  WRITE (OUTTERM,170) IST(K),LFS(KK)(1:NCSTR(LFS(KK)))
  CLOSE (INFILE)
80 RETURN
  !
  !	Error conditions
  !
90 CALL ERRMSG ('Phaseshifts file read error',*130)
100 CALL ERRMSG ('Phaseshifts syntax error: '//BUF(M1:M2),*130)
110 CALL ERRMSGI ('Energy step negative or zero at point',IE,*130)
120 CALL ERRMSG ('Less than 10 points in phaseshift file',*130)
130 CLOSE (INFILE)
  NPH(K)=0
140 CALL ERRS (*10)
150 FORMAT (' Enter filename for ',A,' atom')
160 FORMAT(A)
170 FORMAT (A,' Atom phaseshifts in ',A)
END SUBROUTINE RPS

SUBROUTINE ZPOL (ZZ,CEXAFS,JEXP,QUIET)
  !======================================================================
  Use Definition
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_POL
  Use Include_IPA
  !	Use Common_IPA
  COMPLEX ZZ(-3:3,-3:3),CEXAFS,AARR(-3:3,-2:2),BARR(-2:2,-2:2)
  LOGICAL QUIET
  CEXAFS=0.
  IF (iset(ISING).EQ.2) THEN
     DO M1=-IEDGE,IEDGE
        DO M2=-LINITIAL(JEXP),LINITIAL(JEXP)
           AARR(M1,M2)=0.
           DO M3=-IEDGE,IEDGE
              AARR(M1,M2)=AARR(M1,M2)+ZZ(M1,M3)*CCANG2(M3,M2,JEXP)
           ENDDO
        ENDDO
     ENDDO
     DO M1=-LINITIAL(JEXP),LINITIAL(JEXP)
        DO M2=-LINITIAL(JEXP),LINITIAL(JEXP)
           BARR(M1,M2)=0.
           DO M3=-IEDGE,IEDGE
              BARR(M1,M2)=BARR(M1,M2)+CCANG1(M1,M3,JEXP)*AARR(M3,M2)
           ENDDO
        ENDDO
     ENDDO
     DO M1=-LINITIAL(JEXP),LINITIAL(JEXP)
        CEXAFS=CEXAFS+BARR(M1,M1)
     ENDDO
     CEXAFS=CEXAFS*6.
     IF (DEBUG.AND..NOT.QUIET) WRITE (7,*) 'CEXAFS',CEXAFS
  ELSE
     DO M1=-IEDGE,IEDGE
        CEXAFS=CEXAFS+ZZ(M1,M1)
     ENDDO
     CEXAFS=CEXAFS*2./real(2*IEDGE+1,kind=dp)
  ENDIF
  RETURN
END SUBROUTINE ZPOL

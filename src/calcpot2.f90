SUBROUTINE CALCPOT2 (INT,INT1,IZT,IZD,IOND,RMT,RMT2,IWM,IHOLE,MATRIX,V0,RHO0,SAME,INTERVAL,NPTS,RHOAV,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_b2
  Use Common_TL
  Use Common_F
  Use Common_ELS
  Use Common_PLFLAG
  Use Index_IGF
  Use Common_POT
  Use Common_PMTX
  Use Common_ICF
  Use Common_MATEL
  Use Common_P
  Use Common_PRDSN
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  !
  !	GENERATE MUFFIN-TIN POTENTIALS
  !
  !	RWS     = WIGNER SEITZ RADIUS. LATTICE PARAMETER AND ATOMIC VOLUME
  !	          ARE CALCULATED ASSUMING ATOMIC SPHERES ARE IN CONTACT.
  !	JWS     = LOG. GRID POINT NEAREST TO RWS.
  !	DXL     = LOG. GRID INTERVAL.
  !	EDXL    = EXP (DXL)
  !	NEND(I) = MAXIMUM EFFECTIVE RADIUS FOR ATOM TYPE I.
  !	NPTS    = NUMBER OF POINTS IN RADIAL GRID (SHOULD BE ODD).
  !	ALF(INT)  = EXCHANGE PARAMETER ( 1 FOR SLATER ETC.).
  !	RAT     = ATOMIC RADIUS. OVERRIDES DEFAULT VALUES IF NON-ZERO.
  !	ION(1)  = IONICITY. AT PRESENT ONLY -1,0,1 ACCEPTABLE.
  !	CMAG    = THE MADELUNG POTENTIAL FOR AN IONIC SOLID
  !	RMT     = MUFFIN TIN RADIUS
  !	THE IDENTITY OF EACH SHELL IS STORED IN ARRAY IDD(ISHL), I.E.
  !	THE ATOMIC CHARGE DENSITY ASSOCIATED WITH SHELL ISHL IS ROAT(J,IX)
  !	WHERE IX=IDD(ISHL). THE CORRESPONDING COULOMB POTENTIAL IS
  !	CLAT(J,IX). THE TABULATED CHARGE DENSITIES IN EXF1.RHO.DATA
  !	FROM THE DESCLAUX PROGRAM ARE 4PI*RHO*R**2.
  !	INPUT UNITS ARE ANGSTROMS AND EV. OUTPUT UNITS ARE BOHR RADII
  !	AND RYDBERGS, AS ARE WORKING UNITS.
  !	MUFFIN-TIN POTENTIAL *R (COL) , SUPERPOSED POTENTIAL*R (CLAT)
  !	AND ATOMIC POTENEIAL*R (CLAT2) ARE OUTPUT.
  !	UP TO 26 PHASESHIFTS FOR EACH ENERGY POINT ARE OUTPUT.
  !	THE ENERGY GRIDS FOR RHO, MTP ADN PHASE SHIFTS ARE NOT OUTPUT.
  !	FOR RHO THE LOG GRID HAS LOG(ESTART) =-8.8 , LOG(INT) =.05
  !
  !	LMAX is the maximum angular momentum for phaseshifts (3 to 26)
  !
  !	INCLUDES CODE PROVIDED BY P.DURHAM
  !
  !      *** INCLUDES CALLS TO GHOST LIBRABRY ROUTINES ***


  real(dp)  AD(6),ADD(6),AN(6),BDD(6)
  dimension KD(6),IDD(7),JDD(7),IN(6)
  real(dp)  ANN(6),BNN(6)
  dimension IZD(2),IOND(2)

  real(dp)  DORE1(251),DORE2(251)
  real(dp)  ROAT2(IPARGRID,2)
  real(dp)  CLAT(IPARGRID),CLAT2(IPARGRID,2)
  real(dp)  COL(IPARGRID)

  real(dp)  AV(IPARGRID),RHOT(IPARGRID)

  LOGICAL SAME
  CHARACTER IP1*12,IP2*12,IP3*12
  DATA ADD/0.,1.,1.414213562,1.732050808,2.,2.236067977/
  DATA IDD/1,2,1,2,1,2,1/,ANN/1.,12.,6.,24.,12.,24./
  DATA BDD/0.,1.,1.154700538,1.632993161,1.914854215,2./
  DATA JDD/1,2,1,1,2,1,2/,BNN/1.,8.,6.,12.,24.,8./
  !
  !	If rmt = 0, that ion is not available
  !-st  print *,rmt 
  IF (RMT.LT..1) THEN
     CALL ERRMSG ('Muffin tin radius invalid',*170)
  ENDIF
  XMT=ALOG(RMT)
  ALAT=AMAX1(RMT+RMT2,RMT*2./SQRT(2.),RMT2*2./SQRT(2.))
  IZTT=IZT
  !
  !	DXL is the log-grid increment
  !
  DXLS(INT)=(XMT-XSTARTS(INT))/real(NPTS-1,dp)
  EDXL=EXP(DXLS(INT))
  !
  !	OMA=5.656854249*RMT**3
  !
  !	RWS is the Wigner-Seitz radius, JWS the corresponding grid point
  !
  RWS=ALAT**3/(2.96192*(1.+(RMT2/RMT)**3))
  OMA=RWS*4./3.*PI
  RWS=RWS**(1./3.)
  IF (RWS.LT.RMT) RWS=RMT
  XWS=ALOG(RWS)
  RJWS=(XWS-XSTARTS(INT)+DXLS(INT)+.00001)/DXLS(INT)+1.00001
  JWS=RJWS
  IWS=JWS-1
  !
  !	Calculate neighboring shells positions and number of atoms/shell
  !
  DO  LOGPOG=0,0,-1
     XMADCOR=0.
     XSUM=0
     KD(1)=0
     DO I=1,6
	IF (IOND(1).EQ.0) THEN
           AD(I)=ALAT*ADD(I)
           AN(I)=ANN(I)
           IN(I)=IDD(I+LOGPOG)
	ELSE
           AD(I)=ALAT*BDD(I)
           AN(I)=BNN(I)
           IN(I)=JDD(I+LOGPOG)
	ENDIF
	IF (I.EQ.1) cycle
	XSUM=XSUM+AN(I)
	KD(I)=(ALOG(AD(I)*(EDXL-1.)/(EDXL+1.))-XSTARTS(INT))/DXLS(INT)+1
	XSIGN=IOND(IN(I))
	XMADCOR=XMADCOR+2.*XSIGN/AD(I)*AN(I)
     enddo
     XMAD=IOND(1)*2.*1.763/ALAT
     WRITE (7,'(A,2F10.3)') 'XMAD',XMAD*27.21165/2.,XMADCOR*27.21165/2.
     XMAD=XMAD+XMADCOR
     !
     !	Read in charge density and convert to log grid based on mtr
     !	XGD is the log grid
     !	GRID contains the radial coordinate of the grid points
     !	G2 is GRID**2
     !
     IHC=IHOLE
     CALL CALCGRID (INT)
     FRACJWS=(RWS-GRID(IWS))/(GRID(JWS)-GRID(IWS))
     XFRACJWS=(XWS-XGD(IWS))/DXLS(INT)
     IF (FRACJWS.LT.0.) THEN
        FRACJWS=0.
        XFRACJWS=0.
     ENDIF
     IF (DEBUG) THEN
        WRITE (7,'(A,2I4,2F7.2)') 'iws,jws         ',IWS,JWS,GRID(IWS)/DC,GRID(JWS)/DC
        WRITE (7,'(A,2F12.4,I4)')  'rws,rjws,npts   ',RWS/DC,RJWS,NPTS
        WRITE (7,'(A,2E12.4)')     'fracjws,xfracjws',FRACJWS,XFRACJWS
     ENDIF
     NENDS(1,INT)=IPARGRID
     NENDS(2,INT)=IPARGRID
     !
     !	H-L code needs ROAT and VMT(INT), Xalpha code needs VMT for
     !	calculations. Both need COL (R*V(R)) for plots.
     !
     !	Loop over both atom types
     !
     DO INCD=1,2
	CALL CHDEN (ROAT2(1,INCD),IZTT,IOND(INCD),INCD,JWS,IHC,IWM,GRID,DORE1,DORE2,NENDS(1,INT),XSTARTS(INT),*170)
        !
        !	Just output rho if MATRIX=1
        !
	IF (MATRIX.EQ.1) THEN
           CALL OUTNAM (16,PRDSN,' ',*170)
           CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*170)
           WRITE (OUTFILE,250) IZD(INCD),ELS(IZD(INCD)),IOND(INCD)
           WRITE (OUTFILE,260) real(IZD(INCD),kind=dp)
           WRITE (OUTFILE,270) 251
           !
           !	core is being used as workspace for storing charge density here
           !
           WRITE (OUTFILE,280) (DORE1(I),DORE2(I),I=1,251)
           CALL EFILE ('Atomic Charge Density')
           RETURN 1
	ENDIF
        !
        !	For a -ve core-hole code, IZT is not IZD(1), so
        !	make sure IZTT is no changed for SAME option.
        !
	IF (.NOT.SAME) THEN
           IHC=0
           IZTT=IZD(2)
	ENDIF
        !
        !	Solve Poissons equation return potential in array COL(J)
        !
	JMAX=NENDS(INCD,INT)
        !	IF (DEBUG) CALL WTEXT ('Calling VBP:')
	CALL VBP (IZD(INCD),IZD(INCD)-IOND(INCD),XGD,ROAT2(1,INCD),JMAX,COL)
        !
        !	Calculate atomic coulomb potential CLAT(J) and charge den ROAT(J)
        !
	ZZ=-2*IZD(INCD)
	DO J=1,JMAX
           !
           !	Calculate electrostatic terms
           !
           CE=GRID(J)* SQRT(GRID(J))
           CLAT2(J,INCD)=2.*CE*COL(J)
           !
           !	Contribution of central atom to superposed V and rho.
           !
           IF (INCD.EQ.1) THEN
              CLAT(J)=CLAT2(J,INCD)/G2(J)
              ROAT(J)=ROAT2(J,INCD)/G2(J)
              IF (DEBUG.AND.MOD(J,50).EQ.1) WRITE (LOGFILE,'(A,I4,2F15.6,E15.6)') 'CALCPOT2:',J,GRID(J),CLAT(J)
           ENDIF
        enddo
     enddo

     IF (IGRAPH.EQ.0) GOTO 60
     RQ(1)=0.
     RQ(2)=IZD(1)
     DO I=20,300
	IF (ROAT2(I,1).LE.RQ(2)) cycle
	RQ(2)=ROAT2(I,1)
     enddo
     JJ=1
     PLOTTITLE=ITL(18)
     PLOTXSCALE=DC
     PLOTYSCALE=1.
     PLOTXLAB='Angstroms'
     PLOTYLAB='4 pi rho(r)'
     QRAT=1.

     CALL PLT (GRID,ROAT2,1,JWS,1,*60)
     CALL POSITN (RMT,RQ(1))
     CALL JOIN (RMT,RQ(2))
     !
     !	Sum coulomb potential over shells using lowdin alpha expansion
     !	Sum charge density    over shells using lowdin alpha expansion
     !	Input (CLAT2,ROAT2) is R**2 (V,4piRHO)
     !	Output (CLAT,ROAT ) is just (V,4piRHO)
     !	Calculate muffin tin potential
     !
60   JMAX=MIN0(NENDS(1,INT),NENDS(2,INT))
     CALL SUMAX (XGD,CLAT,ROAT,CLAT2,ROAT2,JMAX,DXLS(INT),GRID,AD,AN,IN,KD,XSTARTS(INT))
     !
     !	INITIALISE SUPERPOSED CHARGE DENSITY RHOT(I) AND COULOMB
     !	POTENTIAL VHT(J) WITH THOSE OF CENTRAL ATOM
     !
62   DO  J=1,IPARGRID
	DO K=1,2
           !	ROAT2(J,K)=ROAT2(J,K)/GRID(J)**2
           ROAT2(J,K)=ROAT2(J,K)/G2(J)
	ENDDO
	RHOT(J)=ROAT2(J,IN(1))
	AV(J)=0.
        !	VHT(J)=VH(J,ID(1))
     enddo
     !
     !	SUPERPOSE AND SPHERICALLY AVERAGE ATOMIC CHARGE DENSITIES AND
     !	COULOMB POTENTIALS IN LOOP OVER SHELLS
     !
     DO  K=2,6
	IAT=IN(K)
	POPS=AN(K)
        !	CALL SPHRAV (GRID,ROAT2(1,IAT),AD(K),AV,WS,IPARGRID)
	DO J=1,IPARGRID
           RHOT(J)=RHOT(J)+POPS*AV(J)
        enddo
        !	   CALL SPHRAV (GRID,VH(1,IAT),AD(K),AV,WS,IPARGRID)
        !	DO 81 J=1,NR
        !	VHT(J)=VHT(J)+POPS*AV(J)
        !  81	CONTINUE
     enddo
     !	WRITE (30,*) '&PAR'
     !	WRITE (30,*) 'M1',RWS
     !	WRITE (30,*) '&END'
     !	DO I=1,IPARGRID
     !	GG=GRID(I)**2
     !	WRITE (30,*) GRID(I),ROAT(I)*GG,ROAT2(I,1)*GG,RHOT(I)*GG,AV(I)*GG
     !	IF (ROAT(I).EQ.0..AND.AV(I).EQ.0.) GOTO 35
     !	ENDDO
     !  35	CLOSE (30)
     IF (DEBUG) THEN
        WRITE (7,'(A,6F8.3)') 'AD',AD
        WRITE (7,'(A,6F8.3)') 'AN',AN
        WRITE (7,'(A,6I8)') 'IN',IN
        WRITE (7,'(A,6I8)') 'KD',KD
        WRITE (7,'(A,2I5,4G9.3)') 'JWS,JMAX,C/R,C/R ',JWS,JMAX,CLAT(1),ROAT(1),CLAT2(1,1),ROAT2(1,1)
     ENDIF

     EX=1.2706963*ALF(INT1)
     !
     !	 Madelung correction :
     !	 In order to treat ionic lattices correctly the ionic coulomb
     !	 potential for the cluster is first subtracted, and the correct
     !	 madelung potential for the structure is then added. rmad is
     !	 the ionic coulomb potential for the cluster.
     !
     DO J=1,JMAX
	IF (iset(IGROUND).EQ.0) THEN
           IF (ROAT(J).GT.0.) THEN
              RRS=(ROAT(J))**(1./3.)
           ELSE
              RRS=0.
           ENDIF
           VMT(J)=CLAT(J)-EX*RRS+XMAD+CMAG2(INT1)
	ELSE
           IF (ROAT(J).GT.0.) THEN
              RRS=(ROAT(J)/3.)**(1./3.)
           ELSE
              RRS=0.
           ENDIF
           VMT(J)=CLAT(J)-(1.22177412*RRS+.0504*LOG(30.*RRS+1.))+XMAD+CMAG2(INT1)
	ENDIF
     enddo
     !
     !	Calculate total charge in sphere RWS
     !
     G3I=GRID(IWS)*G2(IWS)
     G3P=RWS*RWS*RWS
     RHOAV=(ROAT(IWS)*G3I+ROAT(JWS)*G3P)*XFRACJWS
     G3I=GRID(1)*G2(1)
     DO  II=1,IWS-1
	G3P=GRID(II+1)*G2(II+1)
	RHOAV=RHOAV+(ROAT(II)*G3I+ROAT(II+1)*G3P)
        !	IF (II.GT.IWS-10) WRITE (6,*) II+1,GRID(II+1)/DC,RHOAV*DXLS(INT)*.5
  	G3I=G3P
     enddo
     RHOAV=RHOAV*DXLS(INT)/2.
     !
     !	Calculate intersphere constant v0 by averaging the muffin tin
     !	sphere and wigner seitz sphere
     !	integrate using INT(f(r)dr) = INT(rf(r)dx)
     !
     G3P=RWS*RWS*RWS
     G3I=GRID(NPTS)*G2(NPTS)
     IF (DEBUG) WRITE (7,*) 'JWS,NPTS,ABS(G3P-G3I)',JWS,NPTS,ABS(G3P-G3I),ROAT(350),VMT(349),VMT(350),IWS,FRACJWS,XFRACJWS
     IF (IWS.LE.NPTS.AND.FRACJWS.LT.1.E-3) THEN
        V0=VMT(NPTS)
        RHO0=ROAT(NPTS)
        !	ELSEIF (ABS(G3P-G3I).LT.1.E-6) THEN
        !	  V0=VMT(JWS)
        !	  RHO0=ROAT(NPTS)
     ELSE
        G3I=GRID(IWS)*G2(IWS)
        G3P=RWS*RWS*RWS
        RHO0=(ROAT(IWS)*G3I+ROAT(JWS)*G3P)*XFRACJWS
        V0=(VMT(IWS)*G3I+VMT(JWS)*G3P)*XFRACJWS
        G3I=GRID(NPTS)*G2(NPTS)
        IF (DEBUG) WRITE (7,'(8G9.3)') (VMT(I),I=NPTS,JWS+1)
        IF (IWS.GT.NPTS) THEN
           DO  II=NPTS,IWS-1
              G3P=GRID(II+1)*G2(II+1)
              RHO0=RHO0+(ROAT(II)*G3I+ROAT(II+1)*G3P)
              V0=V0+(VMT(II)*G3I+VMT(II+1)*G3P)
              G3I=G3P
           enddo
        ENDIF
        G3P=RWS*RWS*RWS
        G3I=GRID(NPTS)*G2(NPTS)
        IF (DEBUG) WRITE (7,*) 'G3P,G3I',G3P,G3I,XFRACJWS
        RHO0=RHO0*3.*DXLS(INT)/(2.*(G3P-G3I))
        IF (DEBUG) WRITE (7,*) 'RHO0,RHO(JWS)',RHO0,ROAT(JWS)
        V0=V0*3.*DXLS(INT)/(2.*(G3P-G3I))
     ENDIF
     !
     !	Calculate intersphere constant v0 by averaging the muffin tin
     !	sphere and wigner seitz sphere
     !
     !	RHO0=0
     !	V0=0
     !	DO  II=NPTS,JWS
     !	RHO0=RHO0+ROAT(II)
     ! 	V0=V0+VMT(II)
     !       enddo
     !	V0=V0+VMT(JWS+1)*FRACJWS
     !	RHO0=RHO0/real(JWS-NPTS+1,kind=dp)
     !	V0=V0/(real(JWS-NPTS+1,kind=dp)+FRACJWS)
     IF (IOUTPUT.NE.0) THEN
        WRITE (LOGFILE,240) NPTS,JWS,NENDS(1,INT),NENDS(2,INT)
        DO  IW=IWM,LOGFILE,OUTDIFF
           IF (IHCODE(INT).GT.0) THEN
              WRITE (IW,180) IHCODE(INT),DXLS(INT)
           ELSE
              WRITE (IW,200) DXLS(INT)
           ENDIF
           !	  WRITE (IW,190) 'Interstitial potential:',V0*.5/EC,
           !     1  ' Interstitial charge:',RHO0
           WRITE (IW,190) 'Lattice constant      :',ALAT/DC,  ' Muffin-tin radius  :',RMT/DC,NPTS
           WRITE (IW,190) 'Atomic volume         :',OMA/DC**3,' Wigner-seitz radius:',RWS/DC,IWS
           WRITE (IW,190) 'Exchange constant     :',ALF(INT1),' Madelung constant  :',CMAG(INT1)+XMAD/EC*.5
           WRITE (IW,220)
        enddo
     ENDIF
  enddo
  DO  I=1,JWS
     RAD1=GRID(I)
     RAD2=G2(I)
     CLAT2(I,1)=CLAT2(I,1)/RAD1
     CLAT(I)=CLAT(I)*RAD1
     COL(I)=VMT(I)*RAD1
     IF (IGRAPH.GT.0.AND.IGF(ITNUM).NE.5) cycle
     IF (MOD(I,INTERVAL).NE.0.AND.IABS(I-NPTS).GT.1) cycle
     DO  IW=IWM,LOGFILE,OUTDIFF
  	WRITE (IW,230) I,RAD1,ROAT2(I,1)*RAD2,ROAT(I)*RAD2,CLAT2(I,1),CLAT(I),COL(I),VMT(I)
     enddo
  enddo
  DO  I=NPTS,IPARGRID
     ROAT(I)=RHO0
     COL(I)=V0*GRID(I)
  enddo

  IF (IGRAPH.EQ.0) GOTO 161
  RQ(1)=-2*IZD(1)
  RQ(2)=-RQ(1)*.2
  JJ=4
  PLOTTITLE=ITL(17)
  CALL PLT (GRID,COL,1,JWS,1,*161)
  PLOTTITLE=ITL(16)
  CALL PLT (GRID,CLAT,1,JWS,0,*161)
  PLOTTITLE=ITL(15)
  CALL PLT (GRID,CLAT2,1,JWS,0,*161)
  CALL POSITN (RMT,RQ(1))
  CALL JOIN (RMT,RQ(2))
  !
  !	E is the starting point and increment for phaseshifts (ryd).
  !
  JJ=2
  PLOTTITLE=ITL(17)
  RQ(1)=VMT(NPTS-10)
  RQ(2)=VMT(JWS+1)+.1
  CALL PLT (GRID,VMT,NPTS-10,JWS+10,2,*161)
  CALL POSITN (GRID(NPTS),V0)
  CALL JOIN (GRID(JWS+10),V0)
  CALL POSITN (GRID(NPTS),V0)
  CALL JOIN (GRID(NPTS),VMT(NPTS))
  IMAX=JWS+10
  VMAX=VMT(IMAX)
  DO I=NPTS-10,JWS+10
     IF (VMT(I).GT.VMAX) THEN
        VMAX=VMT(i)
        IMAX=I
     ENDIF
  ENDDO
  CALL POSITN(GRID(IMAX),VMAX)
  CALL JOIN (GRID(IMAX),RQ(1))
  CALL TPOS (200,740)
  IF (IGF(ITNUM).NE.5) THEN
     CALL CREAD (IP1,IP2,IP3,IP4,IC,VAL,*160,*160)
160  CALL ALPHAM
  ENDIF
161 V0EV(INT)=V0*.5/EC
  DO  I=NPTS,IPARGRID
     VMT(I)=V0
  enddo
  RETURN
170 RETURN 1
180 FORMAT ('Excited atom code     : ',I3,' Log grid increment       :',F10.5)
190 FORMAT (2(A,F10.5),I5)
200 FORMAT ('Log grid increment    :',F10.5)
210 FORMAT (' Cluster ionic coulomb potential =',F10.5)
220 FORMAT ('   I   R       RHO(AT)  RHO(SUP) RV(AT)   RV(SUP)   RV(MT)  V(MT))')
230 FORMAT (I5,F7.4,5F9.4,E12.3)
240 FORMAT ('NPTS =',I5,'  JWS =',I5,' NEND =',I5,' NEND(2) =',I5/)
250 FORMAT ('  Atomic Number ',I4,4X,A2,' Ionic Charge ',I4)
260 FORMAT (F5.0)
270 FORMAT (I5)
280 FORMAT (4E15.8)
END SUBROUTINE CALCPOT2

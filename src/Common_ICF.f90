MODULE Common_ICF

  !      COMMON /ICF/ICF(IPARNPARAMS),RMS(IPARNPARAMS),RMX(IPARNPARAMS),
  !     1LCS(IPARNPARAMS)

  Use Definition
  Use Parameters
  implicit none
  private

  integer(i4b), dimension(:), public :: ICF(IPARNPARAMS)
  integer(i4b), dimension(:), public :: LCS(IPARNPARAMS)
  real(dp),     dimension(:), public :: RMS(IPARNPARAMS)
  real(dp),     dimension(:), public :: RMX(IPARNPARAMS)
end MODULE Common_ICF

SUBROUTINE RDDEN (IZ,ION,IHC,IWM,DEN_DP,*)
  !======================================================================
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_DLV
  !
  !	Read a charge density function for a given atomic number and
  !	ionic charge and for a given core hole option.
  !
  !	  RECORD 1  IZ, ION, HOLCOD, ISTATE
  !	  RECORD 2  DEN(251)
  !
  !	  WHERE  IZ =     ATOMIC NUMBER  (INTEGER)
  !	         ION    = IONIC CHARGE   (INTEGER)
  !	         HOLCOD = HOLE CODE      (CHARACTER*2)
  !	         DEN    = DENSITY POINTS (251 REAL NUMBERS)
  !
  !	     IZ (I)   ATOMIC NUMBER
  !	    ION (I)   IONIC CHARGE
  !	    IHC (I)   HOLE CODE (0 TO 4)
  !	    DEN (O)   DENSITY POINTS (251 REAL VALUES)
  !
  !	RETURN 1: error in opening or reading file, or entry not found
  !

  real(sp)  :: DEN(251), TCOR(251,4)  ! must to be (sp) due to read of charge density file
  real(dp)  :: DEN_DP(251)

  DIMENSION IELS(30)
  CHARACTER :: DSN*13,LABELS(30)*5,LAB*4,TLAB*4,ISYM(-2:2)*2
  DATA ISYM/'--','- ','  ','+ ','++'/
  DATA LABELS/ &
       '1s1/2','2s1/2','2p1/2','2p3/2','3s1/2','3p1/2',                 &
       '3p3/2','3d3/2','3d5/2','4s1/2','4p1/2','4p3/2','4d3/2','4d5/2', &
       '5s1/2','5p1/2','5p3/2','4f5/2','4f7/2','5d3/2','5d5/2','6s1/2', &
       '6p1/2','6p3/2','5f5/2','5f7/2','6d3/2','6d5/2','7s1/2','7p1/2'/
  !
  !	Get name of file and open file
  !
  CALL DENNAM(MAX0(IHC,0),DSN)
  !  print *,' DLV_flag = ', DLV_flag
  !  print *,' DSN_path = ', DSN_path
  !  pause
  DSN_file = trim(DSN_path)//DSN
  !  CALL FILEOPEN(DSN_file,INFILE,'UNFORMATTED',' ',*31)
  CALL FILEOPEN(DSN_file,INFILE,'FORMATTED',' ',*31)
  !
  !	Search file for required function
  !
  !  IF (DEBUG) 
  !  WRITE (*,*) 'Searching for: Z=',IZ,' ion=',ION,' hole=',IHC
10 READ (INFILE,111,END=40,ERR=30) KZ,KION,JHOLE,JSTATE,IELS
  !  IF (DEBUG) WRITE (7,'(4I4,30I2)') KZ,KION,JHOLE,JSTATE,IELS
  IF (IZ.NE.KZ.OR.ION.NE.KION) THEN
     do i=1,251
        READ (INFILE,113,END=40,ERR=30)
     enddo
     GOTO 10
  ENDIF
  DO KKMAX=30,1,-1
     IF (IELS(KKMAX).NE.0) GOTO 20
  ENDDO
20 IF(IHC.GE.0) THEN
     do j=1,251
        READ(INFILE,113,END=40,ERR=30) DEN(j)
     enddo
     !	  WRITE (*,*) DEN,TCOR
  ELSE
     do j=1,251
        READ (INFILE,112,END=40,ERR=30) DEN(j),(TCOR(j,k), k=1,4)
     enddo
     !	  WRITE (*,*)DEN,TCOR
     DO I=1,251
        DEN(I)=DEN(I)-TCOR(I,-IHC)
     ENDDO
     IELS(-IHC)=IELS(-IHC)-1
     KZ=KZ-1
  ENDIF
  !	goto 10
  CLOSE (INFILE)
  !-st******************* ST *********************
  DO I=1,251
     DEN_DP(I)=DEN(I)*1._dp
  ENDDO
  !-st******************* ST *********************
  TLAB=ELS(IZ)
  NTLAB=NCSTR(TLAB)
  TLAB(NTLAB+1:)=ISYM(ION)
  NTLAB=NCSTR(TLAB)
  INUMY=0
  DO KK=1,KKMAX
     INUMY=INUMY+IELS(KK)
  ENDDO
  DO IW=IWM,7
     WRITE (IW,60) IZ,TLAB(1:NTLAB),(LABELS(KK),IELS(KK),KK=1,KKMAX),' - total ',INUMY
  ENDDO
  IF (KZ-ION.NE.INUMY) CALL ERRMSGI ('Error in total number of electrons for Z=',IZ,*50)
  RETURN
  !
  !	Error conditions
  !
30 CALL ERRMSGI ('Unable to read charge density tables for core-hole code ',IHC,*50)
31 CALL ERRMSGI ('Unable to open charge density tables for core-hole code ',IHC,*50)
40 CALL ERRMSGI ('Charge density not available for '//ELS(IZ)//Isym(ION)//' with core-hole code ',IHC,*50)
50 CLOSE (INFILE)
  RETURN 1
60 FORMAT (/' Z=',I4,' (',A,') ',8(A,I2,1X),3(/13X,8(A,I2,1X)))
111 FORMAT(4I12,6(/,5I12))
112 FORMAT(5F15.7)
113 FORMAT(1F15.7)
END SUBROUTINE RDDEN

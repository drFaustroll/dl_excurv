SUBROUTINE GENERATE
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_COMPAR
  Use Index_INDS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  CHARACTER KP1*1,KP2*1,KP3*1,OPTS(2)*8
  DATA OPTS/'LINKED','UNLINKED'/
  CALL FINDOPT (2,OPTS,IOPT,1,'Cluster number',*70)
  COFFSET(1,1)=0
  COFFSET(2,1)=0
  COFFSET(3,1)=0
  IGEN=MAX(2,INTOPT)
50 CALL WTEXTI ('Enter atom number for cluster ',IGEN)
  CALL CREAD (KP1,KP2,KP3,JJ,IC,V,*50,*70)
  IF (JJ.EQ.IRAD(1).OR.JJ.GT.NS.OR.JJ.LT.0) GOTO 50
  JGEN=ICLUS(JJ)
  COFFSET(1,IGEN)=UX(JJ)
  COFFSET(2,IGEN)=UY(JJ)
  COFFSET(3,IGEN)=UZ(JJ)
  CENTCORD(1,IGEN)=CENTCORD(1,1)+UX(JJ)
  CENTCORD(2,IGEN)=CENTCORD(2,1)+UY(JJ)
  CENTCORD(3,IGEN)=CENTCORD(3,1)+UZ(JJ)
  CUNAME(IGEN)=CUNAME(JGEN)
  ICRESNUM(IGEN)=ICRESNUM(JGEN)
  KK=NS
  DO I=0,NS
     IF (IABS(ICLUS(I)).EQ.JGEN) THEN
        KK=KK+1
        DO J=0,IPARNSV-1
           K=J*(IPARNS+1)+inds(INDN)
           IF (K.EQ.inds(INDX)) THEN
              OFFSET=COFFSET(1,IGEN)
           ELSEIF (K.EQ.inds(INDY)) THEN
              OFFSET=COFFSET(2,IGEN)
           ELSEIF (K.EQ.inds(INDZ)) THEN
              OFFSET=COFFSET(3,IGEN)
           ELSE
              OFFSET=0.
           ENDIF
           PA1(K+KK)=PA1(K+I)-OFFSET
        ENDDO
        IF (I.EQ.JJ) THEN
           CLUS(KK)=-IGEN
           ICLUS(KK)=-IGEN
        ELSE
           CLUS(KK)=IGEN
           ICLUS(KK)=IGEN
        ENDIF
        IF (I.EQ.0) RN(KK)=RN(JJ)
        IF (UN(I).GT.0.) UN(KK)=UN(I)+MG
        IF (IOPT.EQ.1) THEN
           LINK(KK)=I
           RLINK(KK)=I
           IF (IABS(ICLUS(I)).EQ.1) THEN
	      LINK(I)=KK
	      RLINK(I)=KK
           ELSEIF (IABS(ICLUS(LINK(I))).EQ.1) THEN
	      RLINK(LINK(I))=KK
	      LINK(LINK(I))=KK
           ENDIF
        ELSEIF (IOPT.EQ.2) THEN
           LINK(KK)=0
           RLINK(KK)=0
           LINK(I)=0
           RLINK(I)=0
        ENDIF
        ISEQNUM((KK))=ISEQNUM(I)
        ATLABEL(KK)=ATLABEL(I)
	IUN=UN(I)
	IF (IUN.GT.0.AND.IUN+MG.LE.IPARNUNITS) THEN
           UNAME(IUN+MG)=UNAME(IUN)
           IRESNUM(IUN+MG)=IRESNUM(IUN)
           IF (I.EQ.IPIV(IUN)) PIV(IUN+MG)=KK
           IF (I.EQ.IPLA(IUN)) PLA(IUN+MG)=KK
           IF (I.EQ.IPLB(IUN)) PLB(IUN+MG)=KK
           DO J=1,4
              IF (I.EQ.NTORA(J,IUN)) NTORA(J,IUN+MG)=KK
              IF (I.EQ.NTORB(J,IUN)) NTORB(J,IUN+MG)=KK
           ENDDO
	ENDIF
     ENDIF
  ENDDO
  RNS=KK
  NS=KK
  CALL FLIM (*60)
60 CALL UTAB (1)
  ICOMMON=.TRUE.
  CALL WTEXTI ('Remember to change the atom-type for the excited atoms in cluster ',IGEN)
  CALL IUZERO (NAT,IPARNCLUS)
  NPGR(IGEN)=1
70 RETURN
END SUBROUTINE GENERATE

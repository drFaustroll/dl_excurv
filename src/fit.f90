SUBROUTINE FIT (IST,INT,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Index_INDS
  Use Common_DISTANCE
  Use Common_WFX
  Use Include_IPA
  Use Include_PA1
  Use Include_XY
  Use Include_XA
  !	include 'Common_IPA.inc'
  !!	Use Common_IPA
  !	include 'Common_PA1.inc'
  !	include 'Common_XY.inc'
  !	include 'Common_XA.inc'
  !======================================================================C
  !
  !	Updates theory and returns fit index etc.
  !
  !	Options are : EXAFS - Fit exafs and display fit index and
  !	                      discrepency index ( default ).
  !	FT                  - As above, but update fourier transform as
  !	                      well ( eg, for use with CP 1 or CP 2 ).
  !	ST                  - Produce additional statistics including
  !	                      the contribution of each shell.
  !
  !	Internal calls ( IST=N ) will write fit index to logfile only,
  !	otherwise output to terminal ( interactive only ) and logfile.
  !
  DIMENSION :: RSIGMA(IPARNPOINTS*(IPARNSP+1))
  dimension :: FSIGMA(IPARNPOINTS*(IPARNSP+1))
  CHARACTER :: IST*1,LABEL*5,OPTS(4)*7,RKEYWORD*64
  LOGICAL   :: LIST
  DATA OPTS/'EXAFS','FT','STATS','QUIET'/
  IF (IXA.EQ.3) CALL CKXA (*70)
  RKEYWORD  = KEYWORD
  LRKEYWORD = LKEYWORD
  KEYWORD   = IST
  LKEYWORD=NCSTR(KEYWORD)
  CALL FINDOPT (4,OPTS,IOPT,1,' ',*70)
  !	LIST=IST.NE.'N'.AND.IST.NE.' '
  LIST=IOPT.NE.4
  IF (IOPT.EQ.3) THEN
     LC=IOR(LC,3_i4b)
     CALL UZERO (SSQ,NS)
  ENDIF
  CALL SETUP (*70)
  FITINDEX =0
  REXAFS   =0.
  RDISTANCE=0.
  RANG     =0.
  SSC      =0.
  SSX      =0.
  SSD      =0.
  SSTT     =0.
  CALL UZERO (WFX,IPARNPOINTS*IPARNSP)
  DO IEXP=1,NSPEC
     DO I=JS(IEXP),JF(IEXP)
	RKN=RK(I,IEXP)**INT
	AA=ABS(XABS(I,IEXP))
	SSTT=SSTT+AA*RKN
	IF (IOPT.EQ.3) THEN
           IF (IXA.EQ.3) SSX=SSX+ABS(XA(I)*RKN)
           SSC=SSC+ABS(XABT(I,IEXP)*RKN)
           SSD=SSD+ABS((XABS(I,IEXP)-XABT(I,IEXP))*RKN)
	ENDIF
     enddo
  enddo
  IF (SSTT.EQ.0.) THEN
     SSTX=1.
  ELSE
     SSTX=SSTT
  ENDIF
  J=0
  DO IEXP=1,NSPEC
     DO I=JS(IEXP),JF(IEXP)
	J=J+1
	RSIGMA(J)=RK(I,IEXP)**INT/SSTX
	IF (USESIGMA) THEN
           IF (ABS(SIGMA(I)).LT.1.E-6) THEN
              FSIGMA(J)=1.E6
           ELSE
              FSIGMA(J)=1./SIGMA(I)
           ENDIF
	ELSE
           FSIGMA(J)=RSIGMA(J)
	ENDIF
	IF (ABS(XABT(I,IEXP)).GT.1.E6) GOTO 20
	WFX(J)=XABS(I,IEXP)-XABT(I,IEXP)
20	CONTINUE
     enddo
  enddo
  NOBS=J
  CALL DISTRFAC (LIST,RSIGMA,NDIST)
  NDOBS=NOBS-J
  J=0
  DO IEXP=1,NSPEC
     DO I=1,NPT(IEXP)
	J=J+1
	REXAFS=REXAFS+ABS(WFX(J)*RSIGMA(J))
	WFX(J)=WFX(J)*FSIGMA(J)
	WFX(J)=WFX(J)*WEX0(0)*WEX0(IEXP)
	FITINDEX=FITINDEX+WFX(J)**2
     ENDDO
  ENDDO
  IF (NOBS.GT.J) THEN
     DO K=J+1,NOBS
        WFX(K)=WFX(K)*RSIGMA(K)
        IF (K.LE.J+NDIST) THEN
           RDISTANCE=RDISTANCE+ABS(WFX(K))
           WFX(K)=WFX(K)*WD
        ELSE
           RANG=RANG+ABS(WFX(K))
           WFX(K)=WFX(K)*WA
        ENDIF
        FITINDEX=FITINDEX+WFX(K)**2
     ENDDO
  ENDIF
  REXAFS=REXAFS*100.
  RDISTANCE=RDISTANCE*100.
  RANG=RANG*100.

  RFAC=REXAFS+RDISTANCE+RANG
  IF (WD+WA.EQ.0.) THEN
     RFAC=REXAFS
     NOBS=J
  ENDIF
  IF (DWMIN.GT.-1..OR.DWMAX.LT.1.) THEN
     NOBS=NOBS+1
     WFX(NOBS)=0.
     DO I=1,NS
        IF (A(I).LT.DWMIN) THEN
           WFX(NOBS)=WFX(NOBS)+(DWMIN-A(I))*100.
        ELSEIF (A(I).GT.DWMAX) THEN
           WFX(NOBS)=WFX(NOBS)+(A(I)-DWMAX)*100.
        ENDIF
     ENDDO
     FITINDEX=FITINDEX+WFX(NOBS)**2
  ENDIF
  RLOW = 999.
  RHIGH=-999.
  DO I=1,NS
     IF (RN(I).GT.0.) THEN
        IF (RAD(I).GT.RHIGH) RHIGH=RAD(I)
        IF (RAD(I).LT.RLOW)  RLOW =RAD(I)
     ENDIF
  ENDDO
  RDIFF=(RHIGH-RLOW)/DC
  IF (RDIFF.LT.0.) RDIFF=1.
  IF (RNIND.EQ.0) THEN
     RINDPOINTS=2.*(RKMAX-RKMIN)*RDIFF/PI+real(NDOBS,kind=dp)*.5
  ELSE
     RINDPOINTS=RNIND
  ENDIF
  IF (DEBUG) WRITE (LOGFILE,'(A,5F8.3,2I4)') 'RINDPOINTS,RNIND,RKMAX,RKMIN,RDIFF,NOBS,NPT', &
       RINDPOINTS,RNIND,RKMAX,RKMIN,RDIFF,NOBS,NPT(IEXP)
  RNU=RINDPOINTS-RNPARS
  IF (RNU.NE.0.) THEN
     CHISQU=RINDPOINTS/RNU/real(NOBS,kind=dp)*FITINDEX
  ELSE
     CHISQU=-1.
  ENDIF
  IF (LIST) THEN
     IF (USESIGMA)  THEN
        LABEL='Sigma'
     ELSE
        LABEL='k^'//CHAR(48+INT)
     ENDIF
     DO IW=OUTTERM,LOGFILE,OUTDIFF
        WRITE (IW,120) LABEL,FITINDEX*1.E3,INT,RFAC,SSTT,REXAFS,WEX0(0),RDISTANCE,WD,RANG,WA,RINDPOINTS,RNPARS,CHISQU*1.E6
        IF (IOPT.EQ.3) THEN
           LC=0
           WRITE (IW,80)
           DO I=1,NS
              WRITE (IW,90) I,SSQ(I),SSQ(I)/SSTX*100.
           ENDDO
           IF (IXA.EQ.3) WRITE (IW,100) SSX,SSX/SSTX*100.
           WRITE (IW,110) SSC,SSD
        ENDIF
     ENDDO
  ENDIF
  !
  !	Update FT if requested (IST='F',IOPT=2)
  !
  IF (IOPT.EQ.2.AND.LF.NE.0) CALL FOURS (1)
  KEYWORD=RKEYWORD
  LKEYWORD=LRKEYWORD
  RETURN
70 KEYWORD=RKEYWORD
  LKEYWORD=LRKEYWORD
  RETURN 1
  !
80 FORMAT (' Amplitude of Shell ( +% of amplitude of experiment ) :')
90 FORMAT (28X,I3,F11.4,' (',F8.3,' % )')
100 FORMAT (' Amplitude of XADD Spectrum :   ',F11.4,' (',F8.3,' % )')
110 FORMAT (' Amplitude of Theory :',10X,F11.4/' Amplitude of Difference :',6X,F11.4)
120 FORMAT (/ &
       ' Fit Index: with '   ,A ,' Weighting', F11.4/ &
       ' R-Factor : with k**',I1,'  Weighting',F11.4/ &
       ' Amplitude of Experiment',7X,F12.4/           &
       ' R(exafs)    =',F13.4,' Weight: ',F5.2/       &
       ' R(distances)=',F13.4,' Weight:',F6.2/        &
       ' R(angles)   =',F13.4,' Weight:',F6.2/        &
       ' N(ind) =',F6.1,' Np =',F3.0,' Chi^2 = ',F11.4,'E-6')!/3F6.1)
END SUBROUTINE FIT

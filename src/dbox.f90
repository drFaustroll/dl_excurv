SUBROUTINE DBOX (UXT)
  !======================================================================C
  DIMENSION BX(16),BY(16),BZ(16)
  COMPLEX TXYZ,P3D
  DATA BX/-1.,-1.,-1.,-1.,-1., 1., 1., 1., 1., 1., 1.,-1.,-1.,1., 1.,-1./
  DATA BY/-1., 1., 1.,-1.,-1.,-1., 1., 1.,-1.,-1., 1., 1., 1.,1.,-1.,-1./
  DATA BZ/-1.,-1., 1., 1.,-1.,-1.,-1., 1., 1.,-1.,-1.,-1., 1.,1., 1., 1./
  DO I=1,16
     TXYZ=P3D(BX(I)*UXT,BY(I)*UXT,BZ(I)*UXT)
     IF (I.EQ.1) THEN
        CALL POSITN(REAL(TXYZ),AIMAG(TXYZ))
     ELSE
        CALL JOIN (REAL(TXYZ),AIMAG(TXYZ))
     ENDIF
  enddo
  RETURN
END SUBROUTINE DBOX

SUBROUTINE GETLIST (KEY,LKEY,NLIST,LIST)
  !======================================================================C
  use definition
  use parameters
  DIMENSION LIST(*)
  CHARACTER*(*) KEY
  CHARACTER*8 CPAR
  LOGICAL PAR
  NLIST=0
  IF (LKEY.LE.3.OR.KEY(LKEY:LKEY).NE.']') RETURN
  DO I=2,LKEY-1
     IF (KEY(I:I).EQ.'[') GOTO 10
  ENDDO
  RETURN
10 IF (I.EQ.LKEY-1) RETURN
  ISUM=0
  JSUM=-1
  PAR=.FALSE.
  IPAR=0
  CPAR=' '
  DO J=I+1,LKEY
     IF (KEY(J:J).EQ.','.OR.J.EQ.LKEY) THEN
        IF (PAR) THEN
           ISUM=GETPAR(CPAR)
           PAR=.FALSE.
           IPAR=0
           CPAR=' '
        ENDIF
        IF (JSUM.EQ.-1) JSUM=ISUM
        DO K=JSUM,ISUM
           IF (NLIST.GE.IPARNS+1) RETURN
           NLIST=NLIST+1
           LIST(NLIST)=K
        ENDDO
        ISUM=0
        JSUM=-1
     ELSEIF (KEY(J:J).EQ.'-') THEN
        IF (PAR) THEN
           JSUM=GETPAR(CPAR)
           PAR=.FALSE.
           CPAR=' '
           IPAR=0
        ELSE
           JSUM=ISUM
           ISUM=0
        ENDIF
     ELSEIF (.NOT.PAR.AND.KEY(J:J).LT.'A') THEN
        ISUM=10*ISUM+ICHAR(KEY(J:J))-48
     ELSE
        IPAR=IPAR+1
        CPAR(IPAR:IPAR)=KEY(J:J)
        PAR=.TRUE.
     ENDIF
  ENDDO
  IF (NLIST.GT.0) THEN
     IF (LIST(1).GT.99) THEN
        KEY(I:I)=CHAR(LIST(1)/100+48)
        NL=MOD(LIST(I),100)
        KEY(I+1:I+1)=CHAR(NL/10+48)
        KEY(I+2:I+2)=CHAR(MOD(LIST(1),10)+48)
        LKEY=I+2
     ELSEIF (LIST(1).GT.9) THEN
        KEY(I:I)=CHAR(LIST(1)/10+48)
        KEY(I+1:I+1)=CHAR(MOD(LIST(1),10)+48)
        LKEY=I+1
     ELSE
        KEY(I:I)=CHAR(MOD(LIST(1),10)+48)
        LKEY=I
     ENDIF
     KEY(LKEY+1:)=' '
     IF (KEY(2:5).EQ.'CELL') THEN
        KEY(LKEY:LKEY)=' '
        LKEY=LKEY-1
     ENDIF
  ENDIF
  RETURN
END SUBROUTINE GETLIST

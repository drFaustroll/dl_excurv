SUBROUTINE FLIM (*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_CONVIA
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_PMTX
  Use Common_ICF
  Use Index_INDS
  !	include 'Common_IPA.inc'
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  !!	Use Common_IPA
  !	include 'Common_PA1.inc'
  !	include 'Common_PA2.inc'
  !	include 'Common_XY.inc'
  !
  !	Finds energy and phaseshift limits, checks parameters and performs
  !	unit conversion. Calculates the wave vector RK.
  !
  !	RETURN 1 on error
  !
  !-st      COMMON /XRD/NXRD,TWOTHETA(IPARXRD),RINTENSE(IPARXRD),TINTENSE(IPARXRD)   !!-st never used
  !
  call eqv_convia
  !
  !	Check parameters against limits
  !	Atomic units block /PA2/ from input units /PA1/
  !
  DO I=1,IPARNPARAMS
     !	WRITE (LOGFILE,*) IVARS(I),PA1(I),RMS(I),RMX(I),ICF(I),LCS(I)
     PA1(I)=AMAX1(AMIN1(PA1(I),RMX(I)),RMS(I))
  enddo
  DO I=1,IPARNPARAMS
     PA2(I)=PA1(I)*CONVIA(ICF(I))
     !	call all_IPA
     IPA(I)=PA2(I)
     !	call IPA_all
  enddo
  !	IF (NP.LT.100) THEN
  !	WRITE (7,*) 'NSPEC,NP',NSPEC,NP,EMIN02(0),EMAX02(0)
  !	DO J=1,NP
  !	WRITE (7,'(7F8.3)') ENER(J),(XABS(J,I),I=1,NSPEC),(XABT(J,I),I=1,NSPEC)
  !	ENDDO
  !	ENDIF
  DO JEXP=1,NSPEC
     IERR=0
45   JS(JEXP)=1
     DO I=1,NP
	IF (ENER(I).LT.EMIN02(0).OR.ENER(I).LT.EMIN02(JEXP)) THEN
           JS(JEXP)=I
	ELSEIF (ENER(I).GT.EMAX02(0).OR.ENER(I).GT.EMAX02(JEXP)) THEN
           GOTO 70
	ENDIF
     ENDDO
     I=NP+1
70   JF(JEXP)=I-1
     LC=IOR(LC,71_i4b)
     LF=2
     NPT(JEXP)=JF(JEXP)-JS(JEXP)+1
     IF (NPT(JEXP).LT.3) THEN
        IERR=IERR+1
        !
        !	The lower bit of ID is 1 when a refinement is occuring
        !
        IF (ID.EQ.0.AND.IERR.EQ.1) THEN
           DO I=0,IPARNSP
	      EMIN0(I)=0.
	      EMAX0(I)=9999.
	      EMIN02(I)=0.
	      EMAX02(I)=9999.
           ENDDO
           CALL ERRMSG ('EMIN, EMAX Reset ( < 3 Points )',*45)
        ENDIF
        CALL ERRMSG ('Experiment and phaseshifts incompatible',*80)
80      CALL WTEXT ('Check column combination and file names')
        RETURN 1
     ENDIF
  ENDDO
  LC=IAND(LC,-1_i4b-8_i4b)
  RETURN
END SUBROUTINE FLIM

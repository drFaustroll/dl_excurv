FUNCTION FLAGQ (XI,X,Y,NX,NORD,NEXT)
  !======================================================================C
  DIMENSION X(NX),Y(NX)
  !	****************************************************************
  !	*                                                              *
  !	*  LAGRANGE INTERPOLATOR FOR REAL FUNCTION Y OF REAL           *
  !	*  VARIABLE X.                                                 *
  !	*                                                              *
  !	****************************************************************
  !	*                                                              *
  !	*  INPUT:                                                      *
  !	*  =====                                                       *
  !	*                                                              *
  !	*  XI    : VALUE OF X TO WHICH Y(X) IS TO BE INTERPOLATED      *
  !	*  X(I)  : TABLE OF X VALUES                                   *
  !	*  Y(I)  : TABLE OF Y VALUES                                   *
  !	*  NX    : NUMBER OF VALUES OF X,Y                             *
  !	*  NORD  : ORDER OF INTERPOLATION                              *
  !	*  NEXT  : THIS FUNCTION SETS NEXT TO:                         *
  !	*          -1 IF EXTRAPOLATION BEYOND START OF X/Y GRID OCCURS *
  !	*           0 IF NORMAL INTERPOLATION OCCURS                   *
  !	*          +1 IF EXTRAPOLATION BEYOND END OF X/Y GRID OCCURS   *
  !	*                                                              *
  !	****************************************************************
  !
  !	SET EXTRAPOLATION INDEX
  !
  NEXT=0
  IF (XI.LT.X(1)) NEXT=-1
  IF (XI.GT.X(NX)) NEXT=1
  !
  !	Reset order of interpolation to be <= number of points if
  !	necessary, and calculate range of points around IX, which
  !	is the grid point before XI
  !
  IF (NORD.GT.NX) NORD=NX
  NM=(NORD-1)/2
  NP=NORD/2
  !
  !	Determine IX by searching X(J). If XI falls on a grid point,
  !	accept the corresponding y-value. correct IX if it falls too
  !	close to the ends of the grid.
  !
  DO J=1,NX
     IF (X(J).EQ.XI) THEN
        FLAGQ=Y(J)
        RETURN
     ENDIF
     IF (X(J).GT.XI) GOTO 20
  enddo
20 IND=MAX(J-1,NM+1)
  IX=MIN(IND,NX-NP)
  !
  !	Perform interpolation
  !
  A=0
  B=1.
  DO J=IX-NM,IX+NP
     B=B*(XI-X(J))
     C=1.0E0
     DO I=IX-NM,IX+NP
	IF (I.EQ.J) THEN
           XD=XI
	ELSE
           XD=X(J)
	ENDIF
	C=C*(XD-X(I))
     enddo
     A=A+Y(J)/C
  enddo
  FLAGQ=A*B
  RETURN
END FUNCTION FLAGQ

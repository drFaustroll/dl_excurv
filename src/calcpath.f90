SUBROUTINE CALCPATH (*,ICAP)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_VTIME
  Use Common_VAR
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_SOUPER
  Use Common_PATHS
  Use Index_INDS
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY

  DIMENSION JSYMS(0:IPARNS),IXFAC(48),IUSEIT(7),SYMPATHS(7),RMSANG(25)
  dimension AN(0:5,0:5,0:5),DN(0:5,0:5,7),KP(0:6,25),IORD(25),III(0:5,3)
  dimension IBASE(0:IPARNS+1),ANGLIST(7),IANGDONE(7),SAR3(49),SATH3(49)
  dimension SAPHI3(49),SPL3(49),SMINANG3(49),TORY(49),IFACS(25)
  COMPLEX ECM(IPARNPOINTS,IPARNP)
  LOGICAL COND(25),SING,DOUB,TRIP,QUAD,QUIN,SAMESHELL12
  logical LWRITE,CONDY,PTEST,MSSYM1,L3,L4,L5,XTRIP,NEXTSHELL(48)
  !  logical LORD(5)
  !  EQUIVALENCE (LORD(1),SING),(LORD(2),DOUB),(LORD(3),TRIP),(LORD(4),QUAD),(LORD(5),QUIN)
  !-st   above EQUIVALENCE is in the scope of only this subroutine
  !
  !	 1   0120
  !	 2   01210
  !	 3   02120
  !	 4   012120
  !	 5   0121210
  !	 6   0212120
  !	 7   01230
  !	 8   012130
  !	 9   012310
  !	10   013230
  !	11   0121230
  !	12   0121310
  !	13   0123130
  !	14   0123210
  !	15   0123230
  !	16   0131230
  !	17   0132130
  !	18   012340
  !     19   0132340
  !	20   0142340
  !	21   0121340
  !	22   0124340
  !	23   0123140
  !	24   0123410
  !     25   0123450
  !
  DATA KP / &
       0,1,2,0,0,0,0, 0,1,2,1,0,0,0, 0,2,1,2,0,0,0, 0,1,2,1,2,0,0, &
       0,1,2,1,2,1,0, 0,2,1,2,1,2,0, 0,1,2,3,0,0,0, 0,1,2,1,3,0,0, &
       0,1,2,3,1,0,0, 0,1,3,2,3,0,0, 0,1,2,1,2,3,0, 0,1,2,1,3,1,0, &
       0,1,2,3,1,3,0, 0,1,2,3,2,1,0, 0,1,2,3,2,3,0, 0,1,3,1,2,3,0, &
       0,1,3,2,1,3,0, 0,1,2,3,4,0,0, 0,1,3,2,3,4,0, 0,1,4,2,3,4,0, &
       0,1,2,1,3,4,0, 0,1,2,4,3,4,0, 0,1,2,3,1,4,0, 0,1,2,3,4,1,0, &
       0,1,2,3,4,5,0/
  data IORD/2,3,3,4,5,5,3,3*4,7*5,4,7*5/
  data ANGLIST /30.,45.,60.,90.,120.,135.,150./
  IF (DEBUG) WRITE (LOGFILE,*) 'calcpath: mssym=',mssym,' filter=',FILTER
  LWRITE=(IOUTPUT.GT.0.OR.DEBUG.OR.iset(IMSC).EQ.3).AND.(.NOT.BACKGROUND)
  CALL GETTIM (1,CPU1,ELAPS)
  IF (IOUTPUT.GT.1) THEN
     IWMIN=OUTTERM
  ELSE
     IWMIN=LOGFILE
  ENDIF
  MSSYM1=MSSYM.AND.iset(ISING).EQ.0
  IF (PATHOVER.OR.PATH1OVER) THEN
     IF (PATHOVER.AND.IAND(LC,1_i4b).EQ.0) THEN
        LC=IOR(LC,1_i4b)
        CALL WTEXT ('Full calculation needed - too many paths to use stored data')
     ENDIF
     IF (RMINMAG.GT.0.) THEN
        CALL WTEXT ('Too many path numbers, MINMAG set to zero')
        RMINMAG=0.
     ENDIF
     PATHOVER=.FALSE.
     PATH1OVER=.FALSE.
  ENDIF
  IF (IEXP.EQ.1) THEN
     NUMPATHS=0
     NPATHS=0
  ENDIF
  PM=PLMAX2*1.1
  IF (iset(IEXTYP).EQ.1) THEN
     IDLMAX=MIN(IDLMAX,12)
     DLMAX=IDLMAX
     DLMAX2=IDLMAX
     ITLMAX=MIN(ITLMAX,9)
     TLMAX=ITLMAX
     TLMAX2=ITLMAX
     !	write (6,*) 'linitial(iexp)',linitial(iexp),iexp
     IF (NOTREAD.OR.LINITIAL(IEXP).NE.0) CALL RDMS (*230)
     IF(iset(ISING).EQ.2.OR.IATMAX.GT.2.OR.IOMAX.GT.3) CALL RSM(0,ITLMAX,ITLMAX)
  ENDIF
  !
  !	RSM calcs R+S coeffs - independent of all parameters except ITLMAX
  !
  JSYMS(0)=1
  IBASE(0)=-1
  IBASE(1)=0
  MAXSHELL=0
  !
  !	Symmetry check - also defines MAXSHELL and IBASE, used for indexing paths for filters
  !
  DO I=1,NS
     NN=N(I)
     IF (ICLUS(I).LT.0) NN=1
     !
     !	Symmetry check only for iset(IMSC)=2 (ALL) - exclude central atoms/empty shells
     !
     IF (iset(IMSC).EQ.2.AND.NN.GT.0.AND.ICLUS(I).GT.0) THEN
        JCLUS=IABS(ICLUS(I))
        IF (NAT(JCLUS).EQ.0) CALL ERRMSGI ('Space group undefined for cluster ',JCLUS,*230)
        NPG=NPGR(JCLUS)
        IF (iset(ISING).EQ.2.AND.NPG.NE.1) THEN
           CALL WTEXT ('Exact polarisation dependence only available forspace group C1')
           CALL WTEXT ('- use EXPAND to generate coordinates')
           RETURN
        ENDIF
        DO JSYM1=1,6
           JSYM=JSYM1
           IF (NVALS(JSYM,NPG).NE.0) THEN
              IF (NN.EQ.NVALS(JSYM,NPG)) THEN
                 IF (JSYM.EQ.3.AND.NVALS(3,NPG).EQ.NVALS(6,NPG)) CALL DK56 (I,JSYM,*141)
                 IF (JSYM.EQ.4.AND.NVALS(4,NPG).EQ.NVALS(5,NPG)) CALL DK45 (I,JSYM,JCLUS,*141)
                 IF (JSYM.EQ.5.AND.NVALS(5,NPG).EQ.NVALS(6,NPG)) CALL DK56 (I,JSYM,*141)
                 GOTO 141
              ENDIF
           ENDIF
        ENDDO
        CALL ERRMSGI ('Invalid Occupation Number:',NN,*230)
141     JSYMS(I)=JSYM
        IF (DEBUG) WRITE (LOGFILE,'(I2,A,48A)') I,' ',(OPS(K,JSYMS(I),NPG),K=1,NVALS(JSYMS(I),NPG))
     ELSE
        NPG=1
        JSYMS(I)=1
     ENDIF
     IF (NN.GT.0.AND.R2(I).LE.PLMAX2*.55) MAXSHELL=I
     IBASE(I+1)=IBASE(I)+NN
  ENDDO
  IF (MAXSHELL.EQ.0) RETURN
  !
  !	Loop over all clusters
  !
  DO JCLUS=1,MAXCLUS
     IF (KEXP(JCLUS).NE.IEXP) GOTO 999
     CALL GETECM (ECM,JCLUS)
     III(0,1)=IRAD(JCLUS)
     III(0,2)=0
     MAXGROUPS=1
     IF (iset(IMSC).EQ.1) MAXGROUPS=MG
     !
     !	Loop over all groups - all atoms comprise a single group for iset(IMSC)= 2
     !
     DO IGROUP=1,MAXGROUPS
        !-st	DO I=1,5
        !-st           LORD(I)=NAT(JCLUS)+1.GE.I.AND.IATMAX.GE.I.AND.IOMAX.GE.I
        !-st	ENDDO
        sing = NAT(JCLUS)+1.GE.1.AND.IATMAX.GE.1.AND.IOMAX.GE.1
        doub = NAT(JCLUS)+1.GE.2.AND.IATMAX.GE.2.AND.IOMAX.GE.2
        trip = NAT(JCLUS)+1.GE.3.AND.IATMAX.GE.3.AND.IOMAX.GE.3
        quad = NAT(JCLUS)+1.GE.4.AND.IATMAX.GE.4.AND.IOMAX.GE.4
        quin = NAT(JCLUS)+1.GE.5.AND.IATMAX.GE.5.AND.IOMAX.GE.5
	NPG=NPGR(JCLUS)
	CALL GETTIM (1,CPU4,ELAPS)
        !
        !	Loop IA1 over all shells
        !
	DO  IA1=1,MAXSHELL
           !	CALL GETCOORD (1,IA1,AR1,ATH1,APHI1,IAT2)
           CALL GETCOORD1 (1,1,IA1,DN)
           IF (DEBUG.AND.IA1.EQ.2) THEN
              WRITE (7,'(A,2I3)') 'JSYMS,NPG',JSYMS(IA1),NPG
              NIA1=N(IA1)
              DO I=0,NIA1
                 WRITE (7,'(I3,2F8.3)') I,DN(0,1,2)/AC,DN(0,1,3)/AC
                 !	  CALL PERFOP (AR1,ATH1,APHI1,OPS(I,JSYMS(IA1),NPG))
                 CALL PERFOP1 (DN(0,1,1),OPS(I,JSYMS(IA1),NPG))
              ENDDO
           ENDIF
           CALL UZERO (AN,36*6)
           CALL UZERO (DN,36*3)
           DO I=0,5
              DO J=0,5
                 AN(I,J,I)=PI
              ENDDO
           ENDDO
           IF (N(IA1).EQ.0.OR.ICLUS(IA1).NE.JCLUS) cycle
           IF (iset(IMSC).EQ.1.AND.NU(IA1).NE.IGROUP) cycle
           III(1,1)=IA1
           III(1,2)=0
           IF (LWRITE) WRITE (6,*) 'IA1:',IA1
           CALL GETCOORD1 (1,1,IA1,DN)
           !
           !	For low order scattering, inner loops are executed only once
           !
           IA5MAX=IRAD(JCLUS)
           IF (QUIN) IA5MAX=MAXSHELL
           IA4MAX=IRAD(JCLUS)
           IF (QUAD) IA4MAX=MAXSHELL
           IA3MAX=IRAD(JCLUS)
           IF (TRIP) IA3MAX=MAXSHELL
           !
           !	LA1 etc run 1 to n over all atoms in cluster(s)
           !
           LA1=IBASE(IA1)+1
           III(1,3)=LA1
           !
           !	Loop IA2 over all shells
           !
           DO  IA2=0,MAXSHELL
              IF (RN(IA2).LT..001.OR.IABS(ICLUS(IA2)).NE.JCLUS) cycle
              IF (iset(IMSC).EQ.1.AND.NU(IA2).NE.IGROUP) cycle
              III(2,1)=IA2
              SAMESHELL12=IA1.EQ.IA2
              CALL IUZERO (IANGDONE,7)
              NIA2=N(IA2)
              IF (IA2.EQ.IRAD(JCLUS).OR.iset(IMSC).EQ.1) NIA2=1
              MAXNE=1
              !
              !	Symmetry code
              !
              DO I=1,NIA2
                 IXFAC(I)=1
              ENDDO
              DO I=1,7
                 IUSEIT(I)=-1
              ENDDO
              IF (NIA2.GT.1.AND.MSSYM1) THEN
                 CALL GETCOORD1 (1,2,IA2,DN)
                 DO K=1,NIA2
                    LA2=IBASE(IA2)+K
                    CALL RESANG3 (0,1,2,DN,AN)
                    AA=MIN(AN(0,1,2),AN(1,2,0),AN(2,0,1))/AC
                    BB=DN(0,1,1)+DN(1,2,1)+DN(2,0,1)
                    !
                    !	Test for seven special angles
                    !
                    DO I=1,7
                       IF (ABS(AA-ANGLIST(I)).LT.1.E-3) THEN
                          IF (IANGDONE(I).NE.0.OR.IUSEIT(I).EQ.-1) THEN
                             IANGDONE(I)=IANGDONE(I)+1
                             IF (IANGDONE(I).EQ.1) THEN
                                IUSEIT(I)=K
                                SYMPATHS(I)=BB
                             ELSE
                                IF (ABS(SYMPATHS(I)-BB).LT..001) THEN
                                   IXFAC(IUSEIT(I))=IXFAC(IUSEIT(I))+1
                                   IXFAC(K)=0
                                ENDIF
                             ENDIF
                          ENDIF
                       ENDIF
                    ENDDO
                    CALL PERFOP1 (DN(0,2,1),OPS(K,JSYMS(IA2),NPG))
                 ENDDO
              ENDIF
              !
              !	Loop IA3 over all shells
              !
              DO  IA3=0,IA3MAX
                 IF (RN(IA3).LT..001.OR.IABS(ICLUS(IA3)).NE.JCLUS) cycle
                 III(3,1)=IA3
                 L3=ICLUS(IA3).LT.0
                 !
                 !	Loop IA4 over all shells
                 !
                 DO  IA4=0,IA4MAX
                    IF (RN(IA4).LT..001.OR.IABS(ICLUS(IA4)).NE.JCLUS) cycle
                    III(4,1)=IA4
                    !	L4=IA4.EQ.0
                    L4=ICLUS(IA4).LT.0
                    !
                    !	Loop IA5 over all shells
                    !
                    DO  IA5=0,IA5MAX
                       IF (RN(IA5).LT..001.OR.IABS(ICLUS(IA5)).NE.JCLUS) cycle
                       IF (IOUTPUT.GE.2) WRITE (7,*) 'IA1-5',IA1,IA2,IA3,IA4,IA5
                       III(5,1)=IA5
                       !	L5=IA5.EQ.0
                       L5=ICLUS(IA5).LT.0
                       !
                       !	Now the inner set of loops over atoms with shells IA1-IA5
                       !
                       CALL GETCOORD1 (1,2,IA2,DN)
                       !
                       !	Loop over all atoms in shell IA2
                       !
                       DO KA2=1,NIA2
                          IF (IXFAC(KA2).EQ.0) GOTO 700
                          III(2,2)=KA2-1
                          LA2=IBASE(IA2)+KA2
                          III(2,3)=LA2
                          IF (LA2.EQ.LA1) GOTO 700
                          IF (IA1.EQ.1.AND.IOUTPUT.GE.2) WRITE (7,'(A,I3,3F10.2)') 'KA2',KA2,DN(0,2,1)/DC,DN(0,2,2)/AC,DN(0,2,3)/AC
                          IFAC=N(IA1)*IXFAC(KA2)
                          !
                          !	DN,AN calculated outside 3-scatterer block - needed for other blocks
                          !
                          CALL RESANG3 (0,1,2,DN,AN)
                          !
                          !	Condition for calculation of scattering within 2-atom paths
                          !
                          IF (L5.AND.L4.AND.L3.AND.(LA2.GT.LA1.OR.LA2.EQ.IBASE(IRAD(JCLUS))+ 1)) THEN
                             IF (NPATHS.LT.IPARPATH1-27) THEN
                                NBASE=NPATHS+1
                                NPATHS=NPATHS+6
                             ELSE
                                PATH1OVER=.TRUE.
                             ENDIF
                             CONDY=.FALSE.
                             DO IJ=1,6
                                PL(IJ)=0.
                                RMSANG(IJ)=999.
                                K1=KP(IORD(IJ),IJ)
                                DO I=0,IORD(IJ)
                                   K2=KP(I,IJ)
                                   PL(IJ)=PL(IJ)+DN(K1,K2,1)
                                   IF (DN(K1,K2,1).LT.RMINDIST2) PL(IJ)=999.
                                   K3=KP(I+1,IJ)
                                   RMSANG(IJ)=MIN(RMSANG(IJ),AN(K1,K2,K3))
                                   K1=K2
                                ENDDO
                                IFACS(IJ)=IFAC
                                !
                                !	Set up conditions for paths
                                !
                                COND(IJ)=PTEST(PL(IJ),RMSANG(IJ),IORD(IJ),NBASE+IJ,1)
                                IF (IA2.EQ.IRAD(JCLUS).AND.K2.EQ.2) COND(IJ)=.FALSE.
                                !
                                !	Palindromic paths duplicated if IA1 and IA2 in same shell - ignore one set
                                !
                                IF (SAMESHELL12.AND.(IJ.EQ.3.OR.IJ.EQ.6)) COND(IJ)=.FALSE.
                                IF (III(K2,1).NE.III(KP(1,IJ),1)) THEN
                                   IFACS(IJ)=IFACS(IJ)*2
                                ENDIF
                                CONDY=CONDY.OR.COND(IJ)
                             ENDDO
                             !
                             !	Use fast algorithm for double and two-atom triple terms
                             !
                             IF (CONDY) THEN
                                IF (LWRITE) THEN
                                   DO IW=IWMIN,LOGFILE,OUTDIFF
                                      WRITE (IW,250) &
                                           IFAC,'P:',NBASE,IVARS(inds(INDR)+IA1),RAD(IA1)/DC,AN(0,1,2)/AC,'X ', &
                                           DN(1,2,1)/DC,AN(1,2,0)/AC,IVARS(inds(INDR)+IA2),RAD(IA2)/DC,PL(1)/DC,&
                                           PL(2)/DC,PL(3)/DC
                                   ENDDO
                                ENDIF
                                CALL MSCAL (COND,JCLUS,IFACS,III,DN,NBASE,RMSANG,ECM,AN,KP,IORD,6,0,2,ICAP,JSYMS,NPG)
                             ENDIF
                          ENDIF
                          IF (RN(IA3).LT..001.OR.IABS(ICLUS(IA3)).NE.JCLUS) GOTO 700
                          IF (iset(IMSC).EQ.1.AND.NU(IA3).NE.IGROUP) GOTO 700
                          !
                          !	Test that 3-atom clusters are required and at least 1 path is
                          !	within pathlength limits - the test depends only on distances
                          !	and occupation numbers so can be performed outside the KA3 loop
                          !
                          XTRIP=.FALSE.
                          IF (TRIP) THEN
                             !	  CALL GETCOORD (1,IA3,DN(0,3,1),DN(0,3,2),DN(0,3,3),IAT3)
                             CALL GETCOORD1 (1,3,IA3,DN)
                             NIA3=N(IA3)
                             IF (IA3.EQ.IRAD(JCLUS).OR.iset(IMSC).EQ.1) NIA3=1
                             DO MA3=1,NIA3
                                CALL RESANG3 (0,1,3,DN,AN)
                                CALL RESANG3 (0,2,3,DN,AN)
                                CALL RESANG3 (1,2,3,DN,AN)
                                SAR3(MA3)=DN(0,3,1)
                                SATH3(MA3)=DN(0,3,2)
                                SAPHI3(MA3)=DN(0,3,3)
                                SPL3(MA3)=DN(0,1,1)+DN(1,2,1)+DN(2,3,1)+DN(0,3,1)
                                SMINANG3(MA3)=999.
                                K1=3
                                K2=0
                                K3=1
                                DO I=1,4
                                   SMINANG3(MA3)=MIN(SMINANG3(MA3),AN(K1,K2,K3))
                                   K1=MOD(K1+1,4)
                                   K2=MOD(K2+1,4)
                                   K3=MOD(K3+1,4)
                                ENDDO
                                CALL CTOR2 (DN,0,1,2,3,TORY(MA3),TOR2)
                                IF (DN(2,3,1).GE.RMINDIST2.AND.SPL3(MA3).LE.PM) XTRIP=.TRUE.
                                CALL PERFOP1 (DN(0,3,1),OPS(MA3,JSYMS(IA3),NPG))
                                NEXTSHELL(MA3)=.TRUE.
                             ENDDO
                             SPL3(NIA3+1)=-1.
                          ENDIF
                          IF (.NOT.XTRIP) GOTO 700
                          !
                          !	Loop over all atoms in shell IA3 (symmetry incremented at 800)
                          !
                          !	CALL GETTIM (1,CPU6,ELAPS)
                          CALL GETCOORD1 (1,3,IA3,DN)
                          DO KA3=1,NIA3
                             T3=DN(0,1,1)+DN(1,2,1)+DN(2,3,1)
                             III(3,2)=KA3-1
                             LA3=IBASE(IA3)+KA3
                             III(3,3)=LA3
                             IF (LA3.EQ.LA1.OR.LA3.EQ.LA2) GOTO 800
                             CALL RESANG3 (0,1,3,DN,AN)
                             CALL RESANG3 (0,2,3,DN,AN)
                             CALL RESANG3 (1,2,3,DN,AN)
                             IF (L5.AND.L4) THEN
                                IF (NPATHS.LT.IPARPATH1-27) THEN
                                   NBASE=NPATHS+1
                                   NPATHS=NPATHS+11
                                ELSE
                                   PATH1OVER=.TRUE.
                                ENDIF
                                IF (IA1.EQ.1.AND.IOUTPUT.GE.2) &
                                     WRITE (7,'(A,I3,3F10.2)') 'KA3',KA3,DN(0,3,1)/DC,DN(0,3,2)/AC,DN(0,3,3)/AC
                                CONDY=.FALSE.
                                DO IJ=7,17
                                   PL(IJ)=0.
                                   RMSANG(IJ)=999.
                                   K1=KP(IORD(IJ),IJ)
                                   DO I=0,IORD(IJ)
                                      K2=KP(I,IJ)
                                      PL(IJ)=PL(IJ)+DN(K1,K2,1)
                                      IF (DN(K1,K2,1).LT.RMINDIST2) PL(IJ)=999.
                                      K3=KP(I+1,IJ)
                                      RMSANG(IJ)=MIN(RMSANG(IJ),AN(K1,K2,K3))
                                      K1=K2
                                   ENDDO
                                   !
                                   !	Set up conditions for paths
                                   !
                                   COND(IJ)=PTEST(PL(IJ),RMSANG(IJ),IORD(IJ),NBASE+IJ,1)
                                   IFACS(IJ)=IFAC
                                   IF (COND(IJ)) THEN
                                      IF (.NOT.NEXTSHELL(KA3)) THEN
                                         COND(7)=.FALSE.
                                         COND(14)=.FALSE.
                                      ENDIF
                                      !	    IF (IA2.EQ.IRAD(JCLUS).AND.K2.EQ.2) COND(IJ)=.FALSE.
                                      IF (IA3.EQ.IRAD(JCLUS).AND.K2.EQ.3) COND(IJ)=.FALSE.
                                      IF (III(K2,1).NE.III(1,1)) THEN
                                         IF (LA3.GT.LA1) THEN
                                            IFACS(IJ)=IFACS(IJ)*2
                                         ELSE
                                            COND(IJ)=.FALSE.
                                         ENDIF
                                      ENDIF
                                      !	    COND(18)=.FALSE.
                                      CONDY=CONDY.OR.COND(IJ)
                                   ENDIF
                                ENDDO
                                IF (LWRITE) THEN
                                   DO IW=IWMIN,LOGFILE,OUTDIFF
                                      WRITE (IW,250) &
                                           IFAC,'Q:',NBASE,IVARS(inds(INDR)+IA2),RAD(IA2)/DC,AN(0,2,3)/AC,'X ', &
                                           DN(2,3,1)/DC,AN(2,3,0)/AC,IVARS(inds(INDR)+IA3),RAD(IA3)/DC,PL(7)/DC,&
                                           PL(8)/DC,PL(9)/DC
                                   ENDDO
                                ENDIF
                                IF (MSSYM1.AND.KA3.LT.NIA3.AND.COND(7).OR.COND(14)) THEN
                                   CALL CTOR2 (DN,0,1,2,3,TOR,TOR2)
                                   DO MA3=KA3+1,NIA3
                                      IF (NEXTSHELL(MA3)) THEN
                                         IF (ABS(RMSANG(7)-SMINANG3(MA3)).LT.1.E-3.AND. &
                                              ABS(PL(7)-SPL3(MA3)).LT.1.E-3.AND.ABS(TOR+TORY(MA3)).LT.1.E-3) THEN
                                            IFACS(7)=IFACS(7)*2
                                            IFACS(14)=IFACS(14)*2
                                            NEXTSHELL(MA3)=.FALSE.
                                            GOTO 67
                                         ENDIF
                                      ENDIF
                                   ENDDO
                                ENDIF
67                              IF (CONDY) &
                                     CALL MSCAL(COND(7),JCLUS,IFACS(7),III,DN,NBASE,RMSANG(7), &
                                     ECM,AN,KP(0,7),IORD(7),11,6,3,ICAP,JSYMS,NPG)
                             ENDIF
                             IF (RN(IA4).LT..001.OR.IABS(ICLUS(IA4)).NE.JCLUS) GOTO 800
                             IF (iset(IMSC).EQ.1.AND.NU(IA4).NE.IGROUP) GOTO 800
                             !
                             !	Test that 4-atom clusters are required and at least 1 path is
                             !	within pathlength limits - the test depends only on distances
                             !	and occupation numbers so can be performed outside the KA3 loop
                             !
                             IF (QUAD) THEN
                                !	  CALL GETCOORD (1,IA4,AR4,ATH4,APHI4,IAT4)
                                CALL GETCOORD1 (1,4,IA4,DN)
                                T3A=T3+DN(0,4,1)
                                NIA4=N(IA4)
                                IF (IA4.EQ.IRAD(JCLUS).OR.iset(IMSC).EQ.1) NIA4=1
                                DO MA4=1,NIA4
                                   !	  CALL RESANG2 (DN(0,3,1),DN(0,3,2),DN(0,3,3),AR4,ATH4,APHI4,XR34)
                                   CALL RESANG3A (0,3,4,DN,*55)
                                   IF (T3A+DN(3,4,1).LE.PM) GOTO 66
                                   !	  CALL PERFOP (AR4,ATH4,APHI4,OPS(MA4,JSYMS(IA4),NPG))
55                                 CALL PERFOP1 (DN(0,4,1),OPS(MA4,JSYMS(IA4),NPG))
                                ENDDO
                             ENDIF
                             GOTO 800
66                           CALL GETCOORD1 (1,4,IA4,DN)
                             !
                             !	Loop over all atoms in shell IA4
                             !
                             DO KA4=1,NIA4
                                T4=DN(0,1,1)+DN(1,2,1)+DN(2,3,1)+DN(3,4,1)
                                III(4,2)=KA4-1
                                LA4=IBASE(IA4)+KA4
                                III(4,3)=LA4
                                IF (LA4.EQ.LA1.OR.LA4.EQ.LA2.OR.LA4.EQ.LA3) GOTO 900
                                CALL RESANG3 (0,1,4,DN,AN)
                                CALL RESANG3 (0,2,4,DN,AN)
                                CALL RESANG3 (0,3,4,DN,AN)
                                CALL RESANG3 (1,2,4,DN,AN)
                                CALL RESANG3 (1,3,4,DN,AN)
                                CALL RESANG3 (2,3,4,DN,AN)
                                IF (L5) THEN
                                   IF (NPATHS.LT.IPARPATH1-27) THEN
                                      NBASE=NPATHS+1
                                      NPATHS=NPATHS+7
                                   ELSE
                                      PATH1OVER=.TRUE.
                                   ENDIF
                                   CONDY=.FALSE.
                                   DO IJ=18,24
                                      PL(IJ)=0.
                                      RMSANG(IJ)=999.
                                      K1=KP(IORD(IJ),IJ)
                                      DO I=0,IORD(IJ)
                                         K2=KP(I,IJ)
                                         PL(IJ)=PL(IJ)+DN(K1,K2,1)
                                         IF (DN(K1,K2,1).LT.RMINDIST2) PL(IJ)=999.
                                         K3=KP(I+1,IJ)
                                         RMSANG(IJ)=MIN(RMSANG(IJ),AN(K1,K2,K3))
                                         K1=K2
                                      ENDDO
                                      IFACS(IJ)=IFAC
                                      COND(IJ)=PTEST(PL(IJ),RMSANG(IJ),IORD(IJ),NBASE+IJ,1)
                                      IF (IA4.EQ.IRAD(JCLUS).AND.K2.EQ.4) COND(IJ)=.FALSE.
                                      IF (III(K2,1).NE.III(1,1)) THEN
                                         IF (LA4.GT.LA1) THEN
                                            IFACS(IJ)=IFACS(IJ)*2
                                         ELSE
                                            COND(IJ)=.FALSE.
                                         ENDIF
                                      ENDIF
                                      CONDY=CONDY.OR.COND(IJ)
                                   ENDDO
                                   !
                                   !	Code for 4 scatterers
                                   !
                                   IF (CONDY) &
                                        CALL MSCAL(COND(18),JCLUS,IFACS(18),III,DN,NBASE,RMSANG(18), &
                                        ECM,AN,KP(0,18),IORD(18),7,17,4,ICAP,JSYMS,NPG)
                                ENDIF
                                !
                                !	Test that 5-atom clusters are required and at least 1 path is
                                !	within pathlength limits - the test depends only on distances
                                !	and occupation numbers so can be performed outside the KA3 loop
                                !
                                IF (QUIN) THEN
                                   IF (IA5.EQ.IRAD(JCLUS)) GOTO 900
                                   IF (RN(IA5).LT..001.OR.IABS(ICLUS(IA5)).NE.JCLUS) GOTO 900
                                   IF (iset(IMSC).EQ.1.AND.NU(IA5).NE.IGROUP) GOTO 900
                                   !	  CALL GETCOORD (1,IA5,AR5,ATH5,APHI5,IAT5)
                                   CALL GETCOORD1 (1,5,IA5,DN)
                                   T4A=T4+DN(0,5,1)
                                   NIA5=N(IA5)
                                   IF (iset(IMSC).EQ.1) NIA5=1
                                   DO MA5=1,NIA5
                                      !	  CALL RESANG2 (DN(0,4,1),DN(0,4,2),DN(0,4,3),AR5,ATH5,APHI5,XR45)
                                      CALL RESANG3A (0,4,5,DN,*56)
                                      IF (T4A+DN(4,5,1).LE.PM) GOTO 65
                                      !	  CALL PERFOP (AR5,ATH5,APHI5,OPS(MA5,JSYMS(IA5),NPG))
56                                    CALL PERFOP1 (DN(0,5,1),OPS(MA5,JSYMS(IA5),NPG))
                                   ENDDO
                                ENDIF
                                GOTO 900
65                              CALL GETCOORD1 (1,5,IA5,DN)
                                !
                                !	Loop over all atoms in shell IA5
                                !
                                DO KA5=1,NIA5
                                   III(5,2)=KA5-1
                                   LA5=IBASE(IA5)+KA5
                                   III(5,3)=LA5
                                   IF (LA5.EQ.LA1.OR.LA5.EQ.LA2.OR.LA5.EQ.LA3.OR.LA5.EQ.LA4) GOTO 1000
                                   IF (NPATHS.LT.IPARPATH1-27) THEN
                                      NBASE=NPATHS+1
                                      NPATHS=NPATHS+1
                                   ELSE
                                      PATH1OVER=.TRUE.
                                   ENDIF
                                   CALL RESANG3 (0,1,5,DN,AN)
                                   CALL RESANG3 (0,2,5,DN,AN)
                                   CALL RESANG3 (0,3,5,DN,AN)
                                   CALL RESANG3 (0,4,5,DN,AN)
                                   CALL RESANG3 (1,2,5,DN,AN)
                                   CALL RESANG3 (1,3,5,DN,AN)
                                   CALL RESANG3 (1,4,5,DN,AN)
                                   CALL RESANG3 (2,3,5,DN,AN)
                                   CALL RESANG3 (2,4,5,DN,AN)
                                   CALL RESANG3 (3,4,5,DN,AN)
                                   !	CONDY=.FALSE.
                                   IJ=25
                                   PL(IJ)=0.
                                   RMSANG(IJ)=999.
                                   K1=KP(IORD(IJ),IJ)
                                   DO I=0,IORD(IJ)
                                      K2=KP(I,IJ)
                                      PL(IJ)=PL(IJ)+DN(K1,K2,1)
                                      IF (DN(K1,K2,1).LT.RMINDIST2) PL(IJ)=999.
                                      K3=KP(I+1,IJ)
                                      RMSANG(IJ)=MIN(RMSANG(IJ),AN(K1,K2,K3))
                                      K1=K2
                                   ENDDO
                                   COND(IJ)=PTEST(PL(IJ),RMSANG(IJ),IORD(IJ),NBASE+IJ,1)
                                   IF (COND(IJ)) THEN
                                      IFACS(IJ)=IFAC
                                      IF (III(K2,1).NE.III(1,1)) THEN
                                         IF (LA5.GT.LA1) THEN
                                            IFACS(IJ)=IFACS(IJ)*2
                                         ELSE
                                            COND(IJ)=.FALSE.
                                         ENDIF
                                      ENDIF
                                      !	CONDY=CONDY.OR.COND(IJ)
                                      !	ENDDO
                                      !
                                      !	Code for 5 scatterers
                                      !
                                      IF (COND(IJ)) &
                                           CALL MSCAL (COND(25),JCLUS,IFACS(25),III,DN,NBASE,RMSANG(25), &
                                           ECM,AN,KP(0,25),IORD(25),1,24,5,ICAP,JSYMS,NPG)
                                   ENDIF
1000                               CALL PERFOP1 (DN(0,5,1),OPS(KA5,JSYMS(IA5),NPG))
                                   DN(5,0,2)=PI-DN(0,5,2)
                                   DN(5,0,3)=DN(0,5,3)+PI
                                   DN(5,0,4)=-DN(0,5,4)
                                   DN(5,0,5)=-DN(0,5,5)
                                   DN(5,0,6)=-DN(0,5,6)
                                ENDDO
                                !	WRITE (6,*) '1000 FINISHED'
900                             CALL PERFOP1 (DN(0,4,1),OPS(KA4,JSYMS(IA4),NPG))
                                DN(4,0,2)=PI-DN(0,4,2)
                                DN(4,0,3)=DN(0,4,3)+PI
                                DN(4,0,4)=-DN(0,4,4)
                                DN(4,0,5)=-DN(0,4,5)
                                DN(4,0,6)=-DN(0,4,6)
                             ENDDO
                             !	WRITE (6,*) '900 FINISHED'
800                          CALL PERFOP1 (DN(0,3,1),OPS(KA3,JSYMS(IA3),NPG))
                             DN(3,0,2)=PI-DN(0,3,2)
                             DN(3,0,3)=DN(0,3,3)+PI
                             DN(3,0,4)=-DN(0,3,4)
                             DN(3,0,5)=-DN(0,3,5)
                             DN(3,0,6)=-DN(0,3,6)
                          ENDDO
                          !	CALL GETTIM (1,CPU7,ELAPS)
                          !	VTIME(10)=VTIME(10)+CPU7-CPU6
                          !	WRITE (6,*) '800 FINISHED'
!700                          print *,ka2,jsyms(ia2),npg
700                       CALL PERFOP1 (DN(0,2,1),OPS(KA2,JSYMS(IA2),NPG))
                          DN(2,0,2)=PI-DN(0,2,2)
                          DN(2,0,3)=DN(0,2,3)+PI
                          DN(2,0,4)=-DN(0,2,4)
                          DN(2,0,5)=-DN(0,2,5)
                          DN(2,0,6)=-DN(0,2,6)
                       ENDDO
                       !	WRITE (6,*) '700 FINISHED'
                    enddo
                    !	WRITE (6,*) '150 
                 enddo
                 !	WRITE (6,*) '160 FINISHED'
              enddo
              !	WRITE (6,*) '170 FINISHED'
           enddo
           !	WRITE (6,*) '180 FINISHED'
        enddo
        !	WRITE (6,*) '190 FINISHED'
	CALL GETTIM (1,CPU5,ELAPS)
	VTIME(6)=VTIME(6)+CPU5-CPU4
     ENDDO
999  continue
  ENDDO
  CALL GETTIM (1,CPU2,ELAPS)
  VTIME(5)=VTIME(5)+CPU2-CPU1
  RETURN
230 RETURN 1
250 FORMAT (I3,1X,A,I5,1X,A3,1X,F6.3,'(',F5.1,')',1X,A3,F6.3,'(',F5.1,')',1X,A3,1X,F6.3,' L',6F6.2)
END SUBROUTINE CALCPATH

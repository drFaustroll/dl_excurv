SUBROUTINE EXPAND1
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_LETTERS
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_COMPAR
  Use Common_SOUPER
  Use Common_SPAREPA1
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  DIMENSION  :: TEMPA(0:IPARNS),TEMPT(0:IPARNS),DN(0:5,0:5,7)
  JCLUS=1
  IF (NAT(JCLUS).LT.1) THEN
     KEYWORD=' '
     CALL SYMMETRY (.TRUE.,.TRUE.,*20)
  ENDIF
  CALL UCOPY (A,TEMPA,IPARNS)
  CALL UCOPY (T,TEMPT,IPARNS)
  DO JCLUS=1,MAXCLUS
     NPG=NPGR(JCLUS)
     DO IA1=0,NS
	CALL GETCOORD1 (1,1,IA1,DN)
        !	CALL GETCOORD (1,IA1,AR1,ATH1,APHI1,IAT2)
	WRITE (6,30) IA1,LETTERS(1),DN(0,1,1)/DC,DN(0,1,2)/AC,DN(0,1,3)/AC,DN(0,1,4)/DC,DN(0,1,5)/DC,DN(0,1,6)/DC
	NN=MAX(1,N(IA1))
	IF (NN.GT.1) THEN
           DO JSYM1=1,6
              JSYM=JSYM1
              IF (NVALS(JSYM,NPG).NE.0) THEN
                 IF (NN.EQ.NVALS(JSYM,NPG)) THEN
                    IF (JSYM.EQ.3.AND.NVALS(3,NPG).EQ.NVALS(6,NPG)) CALL DK56 (IA1,JSYM,*141)
                    IF (JSYM.EQ.4.AND.NVALS(4,NPG).EQ.NVALS(5,NPG)) CALL DK45 (IA1,JSYM,JCLUS,*141)
                    IF (JSYM.EQ.5.AND.NVALS(5,NPG).EQ.NVALS(6,NPG)) CALL DK56 (IA1,JSYM,*141)
                    GOTO 141
                 ENDIF
              ENDIF
           ENDDO
           CALL ERRMSGI ('Invalid Occupation Number:',NN,*20)
141        DO KA2=1,N(IA1)-1
              CALL PERFOP1 (DN(0,1,1),OPS(KA2,JSYM,NPG))
              !	  CALL PERFOP (AR1,ATH1,APHI1,OPS(KA2,JSYM,NPG))
              WRITE (6,30) IA1,LETTERS(KA2+1),DN(0,1,1)/DC,DN(0,1,2)/AC,DN(0,1,3)/AC,DN(0,1,4)/DC,DN(0,1,5)/DC,DN(0,1,6)/DC
              !	  WRITE (6,30) IA1,LETTERS(KA2),AR1/DC,ATH1/AC,APHI1/AC
           ENDDO
	ENDIF
     enddo
  ENDDO
20 RETURN
30 FORMAT (I3,A,F8.3,2F10.2,3X,3F8.3)
END SUBROUTINE EXPAND1

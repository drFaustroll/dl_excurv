SUBROUTINE RDMAP (NSKIP,*)
  !======================================================================C
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_COMPAR
  Use Common_MAPS
  !
  !	Read a map from an iteration map file
  !
  !	NSKIP (I)   THE NUMBER OF MAPS TO BE SKIPPED
  !	RETURN 1  ERROR IN READING MAP
  !
  !	The map file may start with alphanumeric records. The first
  !	numeric only record is taken as the descriptor record of the
  !	first map. there are assumed to be 65 numeric records per
  !	map including the descriptor record. further alphanumeric
  !	records may only occur after the end of all the map records.
  !
  LOGICAL ERR,NUMS
  CHARACTER*80 BUF
  JSKIP=NSKIP*65
  NUMS=.FALSE.
  NHEAD=0
  !
  !	Read header records (until a numeric record has been found) or
  !	read the next descriptor record
  !
10 READ(INFILE,200,END=160,ERR=170)BUF
  IF (BUF.EQ.' ') GOTO 10
  IL=0
  DO N=1,7
     CALL NEXFLD(BUF,IL,M1,M2,*90)
     X=FPVAL(BUF(M1:M2),ERR)
     IF (ERR) THEN
        IF (NUMS) GOTO 180
        GOTO 100
     ENDIF
     GOTO (20,30,40,50,60,70,80) N
20   MAPPOINTS=X
     cycle
30   IMAP=X
     cycle
40   JMAP=X
     cycle
50   X1=X
     cycle
60   Y1=X
     cycle
70   X2=X
     cycle
80   Y2=X
90   CONTINUE
  enddo
  CALL NEXFLD(BUF,IL,M1,M2,*160)
  MAPVARS(1)=BUF(M1:M2)
  CALL NEXFLD(BUF,IL,M1,M2,*160)
  MAPVARS(2)=BUF(M1:M2)
  GOTO 110
  !
  !	Header record
  !
100 NHEAD=NHEAD+1
  IF (NHEAD.LT.10) CALL WTEXT (BUF)
  GOTO 10
  !
  !	Skip maps if required
  !
110 NUMS=.TRUE.
  IF (JSKIP.GT.0) THEN
     JJS=JSKIP-1
     IF (JJS.LE.0) GOTO 130
     DO  JJ=1,JJS
        READ (INFILE,200,END=160,ERR=170)
     enddo
130  JSKIP=0
     GOTO 10
  ENDIF
  !
  !	Read map points
  !
  INC=16/MAPPOINTS
  DO  N=1,IPARMAP,INC
     DO  NN=1,IPARMAP,INC
	READ (INFILE,200,END=160,ERR=170) BUF
	IL=0
	CALL NEXFLD (BUF,IL,M1,M2,*180)
        !	IF (iset(MAPSIZE).NE.99) THEN
	IF (CHAROPT(1:1).NE.'R') THEN
           INEXMAX=3
           IF (CHAROPT(1:1).EQ.'D') INEXMAX=4
           DO  INEX=1,INEXMAX
              CALL NEXFLD (BUF,IL,M1,M2,*180)
           enddo
	ENDIF
	X=FPVAL(BUF(M1:M2),ERR)
	IF (ERR) THEN
           CALL WTEXT ('Comment: '//BUF)
	ELSE
           AP(NN,N)=X
	ENDIF
     enddo
  enddo
  RETURN
  !
  !	Error conditions
  !
160 CALL ERRMSG ('Premature end of map file',*190)
170 CALL ERRMSG ('Error in reading map file',*190)
180 CALL ERRMSG ('Syntax error in map file: ',*185)
185 CALL WTEXT (BUF)
190 RETURN 1
200 FORMAT(A)
END SUBROUTINE RDMAP

MODULE Common_RULES

  !      COMMON /RULES/ NRULES,IRULE(10),RULEX(10),RULEY(10)
  !      CHARACTER RULEX*8,RULEY*64
  !      data NRULES/0/

  Use Parameters
  Use Definition
  implicit none
  private

  integer(i4b), public :: NRULES = 0
  integer(i4b), dimension(10), public :: IRULE
  character*8,  dimension(10), public :: RULEX
  character*64, dimension(10), public :: RULEY
end MODULE Common_RULES

SUBROUTINE SPIN
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_UPU
  Use Common_COMPAR
  Use Index_INDS
  Use Include_IPA
  Use Include_PA1

  LOGICAL LONG
  CHARACTER IP1*4,IP2*4,IP3*4,OPTS(6)*6
  DATA OPTS/'ALONG','BLONG','RLONG','ASHORT','BSHORT','RSHORT'/
  CALL FINDOPT (6,OPTS,IOPT,0,'Unit Number - no default',*10)
  JPIV=IPIV(INTOPT)
  K1=IPLA(INTOPT)
  K2=IPLB(INTOPT)
  IF (INTOPT.LE.0.OR.JPIV.EQ.0.OR.K1.EQ.0.OR.K2.EQ.0) THEN
     CALL WTEXT ('Numeric option must be a unit number for which PIV, PLA and PLB are defined.')
     RETURN
  ENDIF
  IF (KEYWORD(1:1).EQ.'R') THEN
     J=JPIV+inds(INDROT)
     JVECA=IVECA
     JVECB=IVECB
  ELSEIF (KEYWORD(1:1).EQ.'A') THEN
     J=INTOPT+inds(INDTORA)
     JVECA=NTORA(2,INTOPT)
     JVECB=NTORA(3,INTOPT)
  ELSEIF (KEYWORD(1:1).EQ.'B') THEN
     J=INTOPT+inds(INDTORB)
     JVECA=NTORB(2,INTOPT)
     JVECB=NTORB(3,INTOPT)
  ELSE
     GOTO 10
  ENDIF
  SROT=PA1(J)
  WRITE (OUTTERM,'(3(A,I3))') 'Rotating atom',JPIV,' about',JVECA,'-',JVECB
  BESTTOR=180.
  BESTANG=125.
  CALL WTEXT ('Enter optimum torsion angle [180]')
  CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*20,*70)
  BESTTOR=ABS(V)
20 CALL WTEXT ('Enter optimum angle [125]')
  CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*30,*70)
  BESTANG=ABS(V)
30 ISTART=0
  CALL WTEXT ('Enter start [0]')
  CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*40,*70)
  ISTART=V*10.
40 IEND=3600
  CALL WTEXT ('Enter end [360]')
  CALL CREAD(IP1,IP2,IP3,IDUM,IC,V,*50,*70)
  IEND=V*10.
50 INCREMENT=10
  CALL WTEXT ('Enter increment [1]')
  CALL CREAD(IP1,IP2,IP3,IDUM,IC,V,*60,*70)
  INCREMENT=V*10.
60 IF (KEYWORD(2:2).EQ.'L') THEN
     LONG=.TRUE.
     WRITE (OUTTERM,*) 'Rotation TORC Angle   Distance'
     WRITE (77,*) 'Rotation TORC Angle   Distance'
  ELSE
     LONG=.FALSE.
  ENDIF
  U1=999.
  U2=999.
  U3=999.
  VAL1=TORC(INTOPT)
  VAL2=TANG(K1)
  IRCOR=iset(ICOR)
  iset(ICOR)=1
  IF (INCREMENT.EQ.0) RETURN
  DO II=ISTART,IEND,INCREMENT
     FI=real(II,kind=dp)*.1
     NUMFLAG=2
     CALL PCHANGE (IVARS(J),FI)
     IF (LONG) THEN
        WRITE (6,'(F6.1,2F8.1,F7.3)') PA1(J),TORC(INTOPT),TANG(K1),RAD(JPIV)/DC
        WRITE (77,'(F6.1,2F8.1,F7.3)') PA1(J),TORC(INTOPT),TANG(K1),RAD(JPIV)/DC
     ENDIF
     UU=ABS(ABS(TORC(INTOPT))-BESTTOR)
     IF (UU.LT.U1) THEN
        U1=UU
        VAL1=TORC(INTOPT)
        RU1=PA1(J)
     ENDIF
     AA=ABS(ABS(TANG(K1))-BESTANG)
     IF (AA.LT.U2) THEN
        U2=AA
        VAL2=TANG(K1)
        RU2=PA1(J)
     ENDIF
     AA=RAD(JPIV)/DC
     IF (AA.LT.U3) THEN
        U3=AA
        RU3=PA1(J)
     ENDIF
  ENDDO
  WRITE (6,*) 'Best value for Torsion ang =',RU1,'(',VAL1,')'
  WRITE (6,*) 'Best value for Bond angle  =',RU2,'(',VAL2,')'
  WRITE (6,*) 'Best value for Distance    =',RU3,'(',U3,')'
  CALL WTEXT ('Do you want to align it ? [n]')
  CALL CREAD(IP1,IP2,IP3,IDUM,IC,V,*70,*70)
  IF (IP1(1:1).EQ.'Y'.OR.IP1(1:1).EQ.'D') SROT=RU3
  IF (IP1(1:1).EQ.'T') SROT=RU1
  IF (IP1(1:1).EQ.'B') SROT=RU2
70 NUMFLAG=1
  CALL PCHANGE (IVARS(J),SROT)
  iset(ICOR)=IRCOR
  RETURN
10 CALL WTEXT ('Keyword is R,A or B followed by SHORT or LONG')
  RETURN
END SUBROUTINE SPIN

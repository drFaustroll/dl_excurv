FUNCTION EXPRESS (STRING,IC,VEC)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Include_PA1

  DIMENSION ::IFIELD(20),IOP(20),OPER(4),NSIGN(20),VEC(*)
  !	CHARACTER*(*) :: STRING
  CHARACTER :: OPER*1,TBUFF*6,STRING*(*)
  LOGICAL   :: ERR,EXPO,DIGIT,LASTONEOP,IVEC
  DATA OPER/'+','-','*','/'/
  NFIELD=1
  IOP(NFIELD)=1
  IFIELD(NFIELD)=1
  IC=1
  IF (VEC(1).EQ.-1.) THEN
     VEC(1)=0.
     VEC(2)=0.
     VEC(3)=0.
     IVEC=.TRUE.
  ELSE
     IVEC=.FALSE.
  ENDIF
  EXPO=.FALSE.
  DIGIT=.FALSE.
  LASTONEOP=.FALSE.
  ITLEN=NCSTR(STRING)-1
  IF (ITLEN.LT.0) THEN
     GOTO 90
  ELSE
     IF (ITLEN.EQ.0) GOTO 20
  ENDIF
  DO  I=1,ITLEN
     DO K=1,4
	IF (STRING(I:I).EQ.OPER(K).AND..NOT.EXPO) THEN
           IF (LASTONEOP) THEN
              NSIGN(NFIELD)=K
           ELSE
              IF (I.NE.1.AND.NFIELD.LT.19) NFIELD=NFIELD+1
              NSIGN(NFIELD)=1
              IOP(NFIELD)=K
              IFIELD(NFIELD)=I+1
              EXPO=.FALSE.
              DIGIT=.FALSE.
           ENDIF
           LASTONEOP=.TRUE.
           cycle
	ENDIF
     ENDDO
     LASTONEOP=.FALSE.
     IF (STRING(I:I).EQ.'E'.AND.DIGIT) THEN
        EXPO=.TRUE.
        DIGIT=.FALSE.
     ELSE
        DIGIT=.FALSE.
        IF (STRING(I:I).GE.'0'.AND.STRING(I:I).LE.'9') DIGIT=.TRUE.
     ENDIF
  enddo
20 IFIELD(NFIELD+1)=ITLEN+3
  EXPRESS=0.
  TERM=0.
  DO I=1,NFIELD
     SS=FPVAL (STRING(IFIELD(I):IFIELD(I+1)-2),ERR)
     IF (ERR) THEN
        IF (IVEC) THEN
           II=IFIELD(I)
           IF (STRING(II:II).EQ.'X') THEN
	      J=1
           ELSEIF (STRING(II:II).EQ.'Y') THEN
	      J=2
           ELSEIF (STRING(II:II).EQ.'Z') THEN
	      J=3
           ELSE
	      GOTO 90
           ENDIF
           GOTO (41,51,90,90),IOP(I)
41         VEC(J)=VEC(J)+1.
           cycle
51         VEC(J)=VEC(J)-1.
           cycle
        ELSE
           TBUFF=STRING(IFIELD(I):IFIELD(I+1)-2)
           DO J=1,IPARNPARAMS
              IF (TBUFF.EQ.IVARS(J)) GOTO 30
           ENDDO
           GOTO 90
30         SS=PA1(J)
           IC=-1
        ENDIF
     ENDIF
     GOTO (40,50,60,70),IOP(I)
40   EXPRESS=EXPRESS+TERM
     TERM=SS
     cycle
50   EXPRESS=EXPRESS+TERM
     TERM=-SS
     cycle
60   TERM=TERM*SS
     cycle
70   IF (SS.EQ.0.) GOTO 90
     TERM=TERM/SS
  enddo
  EXPRESS=EXPRESS+TERM
  RETURN
90 EXPRESS=0.
  IC=0
  RETURN
END FUNCTION EXPRESS

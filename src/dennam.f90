SUBROUTINE DENNAM (IHC,DSN)
  !======================================================================
  Use Definition
  Use Index_ISET
  Use Common_B1
  !       
  !	Returns the name of a charge density file for use by 'FILOPN'.
  !       
  !	IHC (I)   Hole code (0 to 4)
  !	DSN (O)   Character*60 variable returning the filename.
  !       
  !  CHARACTER*11 :: DSN
  CHARACTER*13 :: DSN
  CHARACTER*2  :: MEM(0:9)
  DATA MEM/'sa','rk','r1','r2','rl','m1','m2','m3','m4','m5'/
  IF (iset(IGROUND).EQ.0) THEN
     !     DSN='chargexa.'//MEM(IHC)
     DSN='f_chargexa.'//MEM(IHC)
  ELSE 
     !     DSN='chargevb.'//MEM(IHC)
     DSN='f_chargevb.'//MEM(IHC)
  ENDIF
  RETURN
END SUBROUTINE DENNAM

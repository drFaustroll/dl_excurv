SUBROUTINE OCDIADXY (DN,IS,*)
  !======================================================================C
  Use Common_convia
  DIMENSION DN(36,*)
  DN(1,2)=PI-DN(1,2)
  DN(1,3)=PID2-DN(1,3)
  TU=DN(1,4)
  DN(1,4)=DN(1,5)
  DN(1,5)=TU
  DN(1,6)=-DN(1,6)
  IS=IS+1
  RETURN 1
END SUBROUTINE OCDIADXY

INTEGER FUNCTION LNBLNK(STRING)
  !======================================================================C
  !
  !	This function supplied by pcs
  !	There may be a system function to do this on some systems
  !	which will cause linking problems perhaps
  !
  !	LNBLNK - returns the index of the last non blank character in a	string
  !
  CHARACTER*(*) STRING
  INTEGER LENGTH,I
  LENGTH=LEN(STRING)
  DO  I=LENGTH,1,-1
     IF( STRING(I:I).NE.' '.AND.STRING(I:I).NE.CHAR(0)) THEN
        LNBLNK=I
        RETURN
     ENDIF
  enddo
  LNBLNK=0
  RETURN
END FUNCTION LNBLNK

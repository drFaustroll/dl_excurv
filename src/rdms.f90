SUBROUTINE RDMS (*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_COMPAR
  Use Common_CDT
  Use Include_IPA
  Use Common_DLV

  INTEGER NCO1(0:3),NCO2(0:3),NCO3(0:3)
  integer NCO4(0:3),NCO5(0:3),NCO6(0:3)
  LOGICAL ITEST
  DATA NCO1/1,1827,3614,1/
  data NCO2/1,22806,47272,1/
  data NCO3/5551,20488,143083,71726/
  data NCO4/118250,451506,984173,1692555/
  data NCO5/12376,46392,98876,166768/
  data NCO6/440986,1701700,3742063,6499211/
  IF (IPARTLMAX.LE.6) THEN
     IF (IEDGE.EQ.0) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscj1.dat',5,2,*150)
     IF (IEDGE.EQ.1) CALL FFOPENQ(INFILE,trim(DSN_path)//'msck1.dat',5,2,*150)
     IF (IEDGE.EQ.2) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscl1.dat',5,2,*150)
     IF (IEDGE.EQ.3) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscm1.dat',5,2,*150)
     READ (INFILE) (CFA(I),I=1,NCO1(IEDGE))
     READ (INFILE) (CFC(I),I=1,NCO2(IEDGE))
     LC1=6
     LC2=LC1
  ELSEIF (IPARTLMAX.EQ.9) THEN
     IF (IEDGE.EQ.0) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscj2.dat',5,2,*150)
     IF (IEDGE.EQ.1) CALL FFOPENQ(INFILE,trim(DSN_path)//'msck2.dat',5,2,*150)
     IF (IEDGE.EQ.2) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscl2.dat',5,2,*150)
     IF (IEDGE.EQ.3) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscm2.dat',5,2,*150)
     READ (INFILE) (CFA(I),I=1,NCO3(IEDGE))
     READ (INFILE) (CFC(I),I=1,NCO4(IEDGE))
     LC1=12
     LC2=9
  ELSEIF (IPARTLMAX.EQ.12) THEN
     IF (IEDGE.EQ.0) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscj3.dat',5,2,*150)
     IF (IEDGE.EQ.1) CALL FFOPENQ(INFILE,trim(DSN_path)//'msck3.dat',5,2,*150)
     IF (IEDGE.EQ.3) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscl3.dat',5,2,*150)
     IF (IEDGE.EQ.3) CALL FFOPENQ(INFILE,trim(DSN_path)//'mscm3.dat',5,2,*150)
     READ (INFILE) (CFA(I),I=1,NCO5(IEDGE))
     READ (INFILE) (CFC(I),I=1,NCO6(IEDGE))
     LC1=15
     LC2=12
  ENDIF
  CLOSE (INFILE)
  IT1=1
  IT2=1
  LMMAX=0
  DO  L1=0,LC1
     DO  L2=0,LC1
	VAFAC1=2.
	IF (L2.NE.L1) VAFAC1=4.
	DSI(L2,L1)=IT1
	LD1=IABS(L1-L2)
	LD2=L1+L2
	ITEST= (L2.GE.0.AND.L1.EQ.L2)
	DO  L3=0,LC2
           IF (L2.GT.L1) GOTO 10
           TSI(L3,L2,L1)=IT2
           LML1=IABS(L2-L3)
           LML2=IABS(L1-L3)
           LMA1=L2+L3
           LMA2=L1+L3
10         LS4=L1-IEDGE
           IF (LS4.LT.0) LS4=LS4+2
           DO  L4=LS4,L1+IEDGE,2
              IF (L1+L4.LT.IEDGE) cycle
              LS5=L2-IEDGE
              IF (LS5.LT.0) LS5=LS5+2
              DO  L5=LS5,L2+IEDGE,2
                 IF (L2+L5.LT.IEDGE) cycle
                 LM=MIN0(L4,L5)
                 LMMAX=MAX0(LM,LMMAX)
                 IF (L3.NE.0) GOTO 40
                 LD3=MAX0(LD1,IABS(L4-L5))
                 LD4=MIN0(LD2,L4+L5)
                 DO  I6=LD3,LD4,2
                    DO  I7=0,LM
                       CFA(IT1)=CFA(IT1)*2.
                       IT1=IT1+1
                    enddo
                 enddo
40               VAFAC2=VAFAC1
                 IF (L2-L1) 60,50,100
50               IF (L2.LT.0) GOTO 60
                 IF (L5.GT.L4) cycle
                 IF (L4.NE.L5) VAFAC2=VAFAC2*2.
60               DO  I6=LML1,LMA1,2
                    DO  I7=LML2,LMA2,2
                       IF (ITEST.AND.I6.GT.I7) cycle
                       VAFAC3=VAFAC2
                       IF (I6.NE.I7.AND.ITEST) VAFAC3=VAFAC3*2.
                       DO  I8=0,LM
                          CFC(IT2)=CFC(IT2)*VAFAC3
                          IT2=IT2+1
                       enddo
                    enddo
                 enddo
100              CONTINUE
              enddo
           enddo
        enddo
     enddo
  enddo
  WRITE (LOGFILE,*) 'lmmax=',LMMAX,IT1-1,IT2-1
  NOTREAD=.FALSE.
  WRITE (OUTTERM,*) 'Clebsch-Gordon coefficients read for l=',IEDGE
  RETURN
150 RETURN 1
END SUBROUTINE RDMS

SUBROUTINE GEOMCAL (IUN,QUIET,JCLUS,*)
  !======================================================================C
  use definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_GEOMETRY
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2

  LOGICAL QUIET
  IF (NAT(JCLUS).LT.1) RETURN 1
  IF (.NOT.QUIET) THEN
     IDIFF=LOGFILE-IUN
     IF (IDIFF.EQ.0) IDIFF=1
     DO IW=IUN,LOGFILE,IDIFF
        WRITE (IW,70) PGROUP(JCLUS)
        WRITE (IW,80) NAT(JCLUS)
        WRITE (IW,60)
     enddo
  ENDIF
  IMAX=MIN(NAT(JCLUS),IPARATOMS)
  DO  I=1,NAT(JCLUS)
     DO  N1=1,I
	TD=(AX(I,JCLUS)-AX(N1,JCLUS))**2+(AY(I,JCLUS)-AY(N1,JCLUS))**2+(AZ(I,JCLUS)-AZ(N1,JCLUS))**2
	ISI=ISHELL( I,JCLUS)
	ISJ=ISHELL(N1,JCLUS)
	IF (TD.LT..75*.75.AND.N1.NE.I) WRITE (OUTTERM,50) I,ISI,N1,ISJ,SQRT(TD)
	IF (I.LE.IMAX) THEN
           IF (TD.LE.0.) THEN
              DIST(N1,I,JCLUS)=0.
           ELSE
              DIST(N1,I,JCLUS)=SQRT(TD)
           ENDIF
           DIST(I,N1,JCLUS)=DIST(N1,I,JCLUS)
           IF (AR(I,JCLUS).EQ.0..OR.AR(N1,JCLUS).EQ.0.) THEN
              ANGLE(N1,I,JCLUS)=0.
           ELSE
              TA=(AR(I,JCLUS)*AR(I,JCLUS)+AR(N1,JCLUS)*AR(N1,JCLUS)-TD)/(2.*AR(I,JCLUS)*AR(N1,JCLUS))
              IF (TA.LT.-.99999) THEN
                 ANGLE(N1,I,JCLUS)=PI
              ELSEIF (TA.GT..99999) THEN
                 ANGLE(N1,I,JCLUS)=0.
              ELSE
                 ANGLE(N1,I,JCLUS)=ACOS(TA)
              ENDIF
              ANGLE(I,N1,JCLUS)=ANGLE(N1,I,JCLUS)
           ENDIF
	ENDIF
     enddo
     ISI=ISHELL(I,JCLUS)
     IF (.NOT.QUIET) THEN
        DO IW=IUN,LOGFILE,IDIFF
           WRITE (IW,90) &
                I,ISI,IT(ISI),NU(ISI),AX(I,JCLUS),AY(I,JCLUS),AZ(I,JCLUS),  &
                AR(I,JCLUS),ATH(I,JCLUS)/AC,APHI(I,JCLUS)/AC,ISYM(I,JCLUS), &
                IDEG(I,JCLUS),ANORM(I,JCLUS),ICENTI(I,JCLUS)
        enddo
     ENDIF
  enddo
  RETURN
50 FORMAT ('Invalid Interatomic Distance for Atom Pair',I3,'(',I3,')',',',I3,'(',I3,')','(',F8.3,' A)')
60 FORMAT (/' ID SHELL TYPE UN X       Y       Z       R       THETA','  PHI ROT DEG ANORM ICENT')
70 FORMAT (/' Point Group ',A6)
80 FORMAT (' Total Number of Atoms: ',I4)
90 FORMAT (3I4,I3,4F8.4,2F7.2,I4,I3,F7.4,I3)
END SUBROUTINE GEOMCAL

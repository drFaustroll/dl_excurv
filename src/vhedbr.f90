DOUBLE PRECISION FUNCTION VHEDBR (D,PI)
  Use Definition
  IMPLICIT real(float) (A-H,O-Z)
  SAVE
  !       
  !	d=density, rrs=1/rs
  !	vhedbar as in in Los Alamos package
  !	from Von Barth Hedin paper 1971.
  !       
  !	vhedbr is returned in atomic units, multiply by 2 for rydbergs
  !       
  RRS=(4.0*PI*D/3.0)**(1./3.)
  VHEDBR=-0.5*(1.22177412*RRS+.0504*LOG(30.0*RRS+1.0))
  RETURN
END FUNCTION VHEDBR

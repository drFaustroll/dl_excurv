SUBROUTINE SET (IP1,K4,INT,*)
  !======================================================================C
  Use Definition
  Use Parameters
  USe Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_SETKW
  Use Common_PMTCH
  Use Common_PMTX
  Use Common_RCBUF
  !-st	Use Common_COMPAR
  Use Common_ICF
  Use Common_VIEW
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XA
  !
  !	Set program options
  !
  CHARACTER*(*) K4
  CHARACTER*12 IP1,K3,KP3,MSG
  CHARACTER CTITLE*5,CRESET*5,CQUIT*4,CLIST*4
  INTEGER(i4b)  :: JSET(IPARSETOPTS),LCSTAT(IPARSETOPTS)
  logical LJSET(2)
  LOGICAL IEXIT
  DATA CTITLE/'TITLE'/,CRESET/'RESET'/,CQUIT/'QUIT'/,CLIST/'LIST'/
  data LCSTAT/67,16,2,0,71,67,0,2,67,16,67,0,67,0,16,0,16,16,67,0,0/
  !
  !	Set program options
  !
  IF (IP1.NE.' '.AND.K4.NE.' ') THEN
     IEXIT=.TRUE.
  ELSE
     IEXIT=.FALSE.
  ENDIF
  DO  I=1,IPARSETOPTS-2
     JSET(I)=ISET(I)
  enddo
  LJSET(1)=LSET(1)
  LJSET(2)=LSET(2)
  IF (IP1.NE.' ') GOTO 90
  GOTO 70
20 DO  I=1,IPARSETOPTS
     ISET(I)=1-MINUS(I)
  enddo
  RETURN 1
40 CALL WTEXT ('SET OPTIONS-------- ')
  CALL WTEXT (' ')
  CALL WTEXT  ('KEYWORD                          OPTIONS')
  CALL WTEXT  ('                  1               2           3    '//'       4      NOW')
  CALL WTEXTI ('FINAL_STATE       ALL             L-1         L+1  '//'              ',ISET(1)+MINUS(1))
  CALL WTEXTI ('QUINN             Off             On               '//'              ',ISET(2)+MINUS(2))
  CALL WTEXTI ('DISORDER          Approx          Exact_ss         '//'              ',ISET(3)+MINUS(3))
  CALL WTEXTI ('WEIGHTING         None            k    k^2    k^3  '//'        k^4   ',ISET(4)+MINUS(4))
  CALL WTEXTI ('POLARISATION      Off             On          Exact'//'              ',ISET(5)+MINUS(5))
  CALL WTEXTI ('THEORY            Curved_wave     Small_atom  Test '//'              ',ISET(6)+MINUS(6))
  CALL WTEXTI ('CORRELATION       Off             On          All  '//'              ',ISET(7)+MINUS(7))
  CALL WTEXTI ('ATOMIC_ABS        Off             On               '//'              ',ISET(8)+MINUS(8))
  CALL WTEXTI ('AVERAGE           Off             On               '//'              ',ISET(9)+MINUS(9))
  CALL WTEXTI ('RELCOR            Off             On               '//'              ',ISET(10)+MINUS(10))
  CALL WTEXTI ('WAVEVEC           Average         Central     Scatt'//'              ',ISET(11)+MINUS(11))
  CALL WTEXTI ('RCOR              Off             On               '//'              ',ISET(12)+MINUS(12))
  CALL WTEXTI ('MS                Off             On          All  '//'              ',ISET(13)+MINUS(13))
  CALL WTEXTI ('MAPSIZE           Small           Large            '//'              ',ISET(14)+MINUS(14))
  CALL WTEXTI ('EXCHANGE          Hedin-Lundqvist Xalpha           '//'              ',ISET(15)+MINUS(15))
  CALL WTEXTI ('DEBUG             Off             On               '//'              ',ISET(16)+MINUS(16))
  CALL WTEXTI ('GROUND_STATE      Xalpha          von_Barth        '//'              ',ISET(17)+MINUS(17))
  CALL WTEXTI ('CONSTANT          None V          Rho         Fermi'//'_energy Charge',ISET(18)+MINUS(18))
  CALL WTEXTI ('SYM               Off             On               '//'              ',ISET(19)+MINUS(19))
  CALL WTEXTI ('SIGMA             Off             On               '//'              ',ISET(20)+MINUS(20))
  CALL WTEXTI ('VIEW              Auto            Current          '//'              ',ISET(21)+MINUS(21))
  CALL WTEXT  ('TITLE             '//GTITLE)
  GOTO 70
50 CALL ERRMSG ('Invalid keyword '//IP1,*70)
60 MSG=K4
  CALL ERRMSG ('Invalid option '//MSG,*70)
70 IF (IEXIT) GOTO 160
  IF (LRIS.GT.0) GOTO 80
  CALL WTEXT (' ')
  CALL WTEXT ('Enter keyword + option , LIST, RESET or QUIT ')
80 CALL CREAD (IP1,K4,KP3,INT,IC,V,*70,*70)
90 NIP1=NCSTR(IP1)
  NIP4=MIN(NIP1,4)
  NK4=NCSTR(K4)
  IF (IP1(1:NIP4).EQ.CRESET(1:NIP4)) GOTO 20
  IF (IP1(1:NIP4).EQ.CLIST(1:NIP4).OR.IP1(1:1).EQ.'?') GOTO 40
  IF (IP1(1:NIP4).EQ.CQUIT(1:NIP4)) GOTO 160
  DO J=1,IPARSETOPTS
     IF (IP1(1:NIP1).EQ.SETKW(J)(1:NIP1)) GOTO 120
  enddo
  IF (IP1(1:NIP1).EQ.CTITLE(1:NIP1)) THEN
     CALL WTEXT ('Enter Title : ')
     CALL SREAD (*110,*50)
110  GTITLE=BUF
     GOTO 70
  ENDIF
  CALL ERRS (*50)
120 IF (K4.EQ.' '.AND.INT.EQ.0) THEN
130  CALL WTEXT ('Option ?')
     CALL CREAD (K4,K3,KP3,INT,IC,V,*130,*70)
     NK4=NCSTR(K4)
  ENDIF
  IF (INT.NE.0) THEN
     IF (INT.LT.0.OR.INT.GT.ISETMAX(J)) CALL ERRS (*60)
  ELSE
     DO  INT=1,ISETMAX(J)
        IF (K4(1:NK4).EQ.SETKWO(INT,J)(1:NK4)) GOTO 150
     enddo
     INT=0
     GOTO 60
150  CONTINUE
  ENDIF
  IF (LMDSN.EQ.0) WRITE (OUTTERM,170) SETKW(J),SETKWO(INT,J),SETKWO(JSET(J)+MINUS(J),J)
  ISET(J)=INT-MINUS(J)
  !
  !	SET WEIGHTING also set PWEIGHT and FTWEIGHT
  !
  IF (J.EQ.4) THEN
     IGF(5)=ISET(J)+1
     IFTSET(2)=ISET(J)
  ENDIF
  INT=0
  GOTO 70
160 IF (ISET(13).NE.JSET(13)) CALL UZERO (XDT,IPARNPOINTS*IPARNSP*4)
  DEBUG=ISET(16).NE.0
  DO I=1,IPARSETOPTS
     IF (ISET(I).NE.JSET(I)) LC=IOR(LCSTAT(I),LC)
  ENDDO
  LF=2
  !	IF (ISET(9).NE.JSET(9)) NOTREAD=.TRUE.
  IF (ISET(15).NE.JSET(15).OR.ISET(17).NE.JSET(17)) THEN
     V0=0
     V02=0
  ENDIF
  IF (J.EQ.21) THEN
     IF (ISET(21).EQ.0) THEN
        VX=0.
        VY=0.
        VZ=0.
     ELSE
        VX=VVX
        VY=VVY
        VZ=VVZ
     ENDIF
  ENDIF
  RETURN
170 FORMAT(' Keyword: ',A14,' Option: ',A14,' ( previously: ',A14,')')
END SUBROUTINE SET

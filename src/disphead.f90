SUBROUTINE DISPHEAD
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  !
  !	Display headers and initialise parameters on entry to program
  !
  CHARACTER ::  CDATE*11,CTIME*10
  !
  !	Output initial banner
  !
  CALL WTEXT (' ')
  CALL WTEXT ('************************************************')
  CALL WTEXT ('***                                          ***')
  CALL WTEXT ('***        STFC DARESBURY LABORATORY         ***')
  CALL WTEXT ('***        ==========================        ***')
  CALL WTEXT ('***                                          ***')
  CALL WTEXT ('***                DL_EXCURV                 ***')
  CALL WTEXT ('***                =========                 ***')
  CALL WTEXT ('***                                          ***')
  CALL WTEXT ('*** VERSION 1-r6         2 October 2014      ***')
  CALL WTEXT ('***                                          ***')
  CALL WTEXT ('*** If you have any problems with this       ***')
  CALL WTEXT ('*** version or any suggestions please        ***')
  CALL WTEXT ('*** contact ccp3@stfc.ac.uk                  ***')
  CALL WTEXT ('***                                          ***')
  CALL WTEXT ('************************************************')
  CALL WTEXT (' ')
  !
  !	Print header in output file and initialise timers
  !
  CALL GETTIM (0,CPU,ELAPS)
  CALL GETDAT (CDATE,CTIME,LOGFILE)
  RETURN
END SUBROUTINE DISPHEAD

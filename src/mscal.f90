SUBROUTINE MSCAL (COND,JCLUS,IFACS,III,DN,NBASE,RMSANG,ECM,ANGMAT,KP,IORD,NTERMS,NOFF,KATOM,ICAP,JSYMS,NPG)
  !======================================================================C
  ! SUBROUTINE MSCAL (COND,JCLUS,IFACS,III,DN,NBASE,RMSANG,ECM,ANGMAT,KP,IORD,NTERMS,NOFF,KATOM,ICAP,JSYMS,NPG)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_VTIME
  Use Common_ELS
  Use Common_CDT
  Use Common_MATEL
  Use Common_PATHS
  Use Common_ATMAT
  Use Common_YLM
  Use Common_RSC
  Use Common_FMAT
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2
  Use Include_XY
  Use Include_XA
  !
  !	Calculate EXAFS Multiple Scattering for all paths with 3 unique
  !	scattering atoms.
  !
  !	   IA1    Shell number of first atom     = III(1,1)
  !	   IA2    Shell number of second atom    = III(2,1)
  !	  XDIS    Interatomic vector R1-R2       = DN(1,2,1)
  !	SALPHA    Scattering angle alpha
  !	 SBETA    Scattering angle beta
  !
  !	Hankel function/gamma array dimensions KATOM*(KATOM+1)/2 (15 for KATOM=5)
  !
  parameter(IPART=(IPARTLMAX+1)*(IPARTLMAX+1))

  COMPLEX  &
       TK(IPART,7,22),QK(7,IPART,5),TKK(IPART,IPART,5,5),ZS(-3:3,-3:3), &
       XZ(IPARNSP,11),CE,ECM(IPARNPOINTS,IPARNP),CKC,HH(0:IPARDLMAX*2,15), &
       VIJ(0:IPARDLMAX*2,-IPARDLMAX*2:IPARDLMAX*2),WIJ(0:IPARDLMAX+3,-IPARDLMAX-3:IPARDLMAX+3), &
       WJI(0:IPARDLMAX+3,-IPARDLMAX-3:IPARDLMAX+3),TZ,TZA,FIC,GAMNU(-3:3,0:2,0:IPARLMAX,25), &
       SW(0:IPARLMAX,0:IPARLMAX,25),GAMSQUIG(-3:3,0:2,0:IPARLMAX,25), &
       CI,DZ,H0,TEF,HHU(0:IPARDLMAX*2,0:IPARDLMAX*2),H5(0:IPARDLMAX*2,0:IPARDLMAX*2), &
       H6(0:IPARDLMAX*2,0:IPARDLMAX*2),D2C,C3,C3A,D3,E3,E3A,TEMPC,TEMPCA, &
       TYP(IPARNPOINTS,IPARNSP,11)
  !	complex*16 GAMNU(-3:3,0:2,0:IPARLMAX,25),GAMSQUIG(-3:3,0:2,0:IPARLMAX,25),SW(0:IPARLMAX,0:IPARLMAX,25),CKC
  !-st  LOGICAL COND(*)
  logical COND(NTERMS)
  logical ME0(0:5,0:5),MEVEN(0:5,0:5),LODD,NEED(0:5,0:5)
  LOGICAL SP2,LP,SCOND(11),MEE(150),MEG(150),ALL,DONT
  COMPLEX*16 RL(-2:2,-2:2,0:IPARLMAX,150),EA(-2:2,-2:2,150)
  DIMENSION  &
       PL1(0:3),II(5,3),TYQ(IPARNPOINTS,IPARNSP,11), &
       ANGF(IPARNSP,5,5),INDY(0:5,0:5),ITYP(0:5),UPATH(11),AN(0:5,0:5,0:5), &
       III(0:5,2),ANGMAT(0:5,0:5,0:5),INDANG(0:5,0:5,0:5), &
       INDRK(0:5,0:5),INDSK(0:5,0:5,0:5),ANGANG(150),POLLE(5,5),DN(0:5,0:5,3), &
       LEGLIST(39),JSYMS(0:IPARNS),F1(0:15,0:15),F2(0:15,0:15), &
       CFD1(5995),CFT1(150858),CFT1A(150858),ICFD(0:IPARDLMAX,0:IPARDLMAX), &
       ICFT(0:IPARDLMAX,0:IPARDLMAX,0:IPARDLMAX)
  !-st  dimension RMSANG(*),IORD(*),KP(0:6,*),IFACS(*)
  dimension RMSANG(NTERMS),IORD(NTERMS),KP(0:6,NTERMS),IFACS(NTERMS)
  DATA CI/(0.,1.)/
  DONT=iset(IEXTYP).EQ.3
  IF (DN(1,2,1).LT.RMINDIST2) THEN
     WRITE (LOGFILE,*) 'xdis=',DN(1,2,1)
     WRITE (OUTTERM,*) 'xdis=',DN(1,2,1)
     DONT=.TRUE.
     !	  RETURN
  ENDIF
  CALL GETTIM (1,CPU1,ELAPS)
  ALL=IAND(LC,1_i4b).EQ.1
  CALL LUZERO (NEED,36)
  IF (.NOT.DONT) THEN
     IF (iset(IEXTYP).EQ.1.AND.KATOM.EQ.2.AND.iset(ISING).NE.2) THEN
        SP2=COND(1).OR.COND(2).OR.COND(3)
        LP=COND(4).OR.COND(5).OR.COND(6)
     ELSE
        SP2=.FALSE.
        LP=.TRUE.
     ENDIF
     CALL LUZERO (MEE,150)
     CALL LUZERO (MEG,150)
     DO IJ=1,NTERMS
        SCOND(IJ)=COND(IJ)
        IF (COND(IJ).AND.ALL) THEN
           DO I=0,IORD(IJ)
              NEED(KP(I,IJ),KP(I+1,IJ))=.TRUE.
           ENDDO
        ENDIF
     ENDDO
     IF (SP2) THEN
        DO IJ=1,3
           SCOND(IJ)=.FALSE.
        ENDDO
     ENDIF
     REMA0=A2(0)
     DO I=2,KATOM
        IF (III(I,1).EQ.III(0,1)) A2(III(0,1))=A2(III(1,1))
     ENDDO
     CALL UZERO (AN,6**3)
     JB=0
     !	  CALL IUZERO (INDANG,6**3)
     DO I=0,KATOM
        ITYP(I)=IT(III(I,1))
        DO J=0,KATOM
           IF (I.NE.J) THEN
              IF (NEED(I,J)) THEN
                 !-st	      ME0(I,J)=ABS(DN(I,J,2)).LT.1.E-3.OR.ABS(DN(I,J,2)-SNGL(PI)).
                 ME0(I,J)=ABS(DN(I,J,2)).LT.1.E-3.OR.ABS(DN(I,J,2)-real(PI,kind=sp)).LT.1.E-3
                 !-st	      MEVEN(I,J)=ABS(DN(I,J,2)-SNGL(PID2)).LT.1.E-3
                 MEVEN(I,J)=ABS(DN(I,J,2)-real(PID2,kind=sp)).LT.1.E-3
              ENDIF
              DO K=0,KATOM
                 IF (J.NE.K) THEN
                    IF (I.EQ.K) THEN
                       AN(I,J,K)=1.
                    ELSE
                       !	        IF (ABS(ANGMAT(I,J,K)-PI).LT..001) ANGMAT(I,J,K)=ANGMAT(I,J,K)-RMEM2(1)
                       AN(I,J,K)=SIN(ANGMAT(I,J,K)*.5)**2
                    ENDIF
                    IF (NEED(I,J).AND.NEED(J,K)) THEN
                       JB=JB+1
                       INDANG(I,J,K)=JB
                       ANGANG(JB)=ANGMAT(I,J,K)
                       !	        TT2=DN(I,J,2)
                       !	        TT1=DN(J,K,2)
                       P2=DN(I,J,3)
                       P1=DN(J,K,3)
                       CALL CALCEA (DN(J,K,2),DN(I,J,2),P2-P1,EA(-2,-2,JB))
                    ENDIF
                 ENDIF
              ENDDO
           ENDIF
        ENDDO
     ENDDO
     !
     !	Calculate the mean square variations in path length UPATH
     !
     CALL DWALI (5,KP,IORD,AN,UPATH,NTERMS,III)
     IF (DEBUG) WRITE (7,*) 'mscal:',(COND(I),I=1,NTERMS),IFACS(I)
     !
     !	need to fix this for mixed sites
     !
     IF (IATOM(ITYP(0)).LT.0) ITYP(0)=JTYPE(-IATOM(ITYP(0)),1)
     MAXL=MAX(IDLMAX,IEDGE)
     MAXNU=MAX(NUMAX,IEDGE)
     !
     !	Calculate Euler angles and the index arrays used for both them and
     !	the rotation matrices. Terms are calculated expicitly for both
     !	directions at each vertex.
     !
     !	  CALL IUZERO (INDSK,6**3)
     JC=0
     DO I=0,KATOM
        DO J=1,KATOM
           IF (J.NE.I) THEN
              DO K=0,KATOM
                 IF (K.NE.J) THEN
                    JC=JC+1
                    INDSK(I,J,K)=JC
                 ENDIF
              ENDDO
           ENDIF
        ENDDO
     ENDDO
     !
     !	Set up index arrays for Hankel functions, gammas, and YLMs
     !	Only one YLM for each leg (path indices always increase as in:
     !	01, 12, 02 etc. Use YLM(-O)=(-1)**L.YL-M(O) for the others.
     !
     JA=0
     DO I=0,KATOM-1
        DO J=I+1,KATOM
           JA=JA+1
           INDY(I,J)=JA
           INDY(J,I)=JA
        ENDDO
     ENDDO
     DIV=1./(2*IEDGE+1)
  ENDIF
  !
  !	Calculate angle factors if in single crystal mode
  !	The POLLEs relate to angle at central atom - gamma for (2) and (4)
  !	PI for (3) and (5)
  !	Indices as for COND 1=(2),2=(3a),3=(3b),4=(4),5=(5a),6=(5b)
  !
  DO I=1,KATOM-1
     DO J=I+1,KATOM
        !	IF (NEED(I,J).OR.NEED(J,I)) THEN
        ! 	T=anglex(dn(j,0,2),dn(j,0,3),dn(0,i,2),dn(0,i,3))
        CALL LPC (ANGMAT(I,0,J),IEDGE,PL1)
        !	  CALL LPC (T,IEDGE,PL1)
        POLLE(I,J)=PL1(IEDGE)
        !         T=anglex(dn(i,0,2),dn(i,0,3),dn(0,j,2),dn(0,j,3))
        !	  CALL LPC (T,IEDGE,PL1)
        POLLE(J,I)=PL1(IEDGE)
        !	ENDIF
     ENDDO
  ENDDO
  JA=0
  CALL IUZERO (INDRK,6**2)
  DO I=1,KATOM
     POLLE(I,I)=(-1)**IEDGE
  ENDDO
  DO I=1,KATOM
     DO J=1,KATOM
	CALL POLANG (ANGF(1,I,J),POLLE(I,J),III(I,1),JSYMS,NPG,RN2(III(0,1)),DN(0,I,2),DN(0,I,3),DN(J,0,2),DN(J,0,3))
        !	write (6,'(a,2i2,2F7.3,7F8.2)')  &
        !      'ANGF',i,j,angf(1,i,j),polle(i,j),dn(0,i,2)/ac,dn(j,0,2)/ac,dn(0,i,3)/ac,dn(j,0,3)/ac, &
        !       angmat(i,0,2j)/ac,angmat(j,0,i)/ac,anglex(dn(j,0,2),dn(j,0,3),dn(0,i,2),dn(0,i,3))/ac
	DO JEXP=IEXP,IEXP+JSPEC-1
           ANGF(JEXP,I,J)=ABS(ANGF(JEXP,I,J))
	ENDDO
	JA=JA+1
	INDRK(I,J)=JA
     ENDDO
  ENDDO
  !
  !	iset(IEXTYP)=1 for Curved Wave, 2 for Small Atom/R+A
  !
  IF (iset(IEXTYP).EQ.2.AND.ALL) THEN
     DO I=1,JB
        CALL CALCRL (ANGANG(I),MAXL,MAXNU,RL(-2,-2,0,I),EA(-2,-2,I),MEE(I),MEG(I),npt(1))
     ENDDO
  ELSEIF (iset(IEXTYP).EQ.1.AND.ALL) THEN
     IF (LP) THEN
        DO I=0,KATOM-1
           DO J=I+1,KATOM
              IF (NEED(I,J).OR.NEED(J,I)) THEN
                 CALL SHM4 (DN(I,J,2),DN(I,J,3),INDY(I,J),MAX(MAXL,IDLMAX*2))
              ENDIF
           ENDDO
        ENDDO
     ENDIF
     IF (SP2) THEN
        CALL MLP (ANGMAT(0,1,2),IDLMAX+IEDGE+1,F1)
        !-st	    CALL MLP (SNGL(PI-ANGMAT(1,2,0)),IDLMAX+IEDGE+1,F2)
        CALL MLP (real(PI-ANGMAT(1,2,0),kind=sp),IDLMAX+IEDGE+1,F2)
     ENDIF
  ENDIF
  !
  !	Calculate Hankel Functions for vectors AT1, AT2 and AT2-AT1
  !
  !	Loop through energy points
  !
  IF (.NOT.DONT) THEN
     DO  IEP=1,JPT
	IF (.NOT.ALL) GOTO 77
        !	DO IEP=1,1
	ILMAX=IDLMAX
	JLMAX=ITLMAX
	KLMAX=IEDGE
	DO I=1,KATOM
           IF (ITYP(I).GT.0) THEN
              KLMAX=MAX(KLMAX,LMAXVALS(IEP,ITYP(I)))
              !	klmax=idlmax
           ENDIF
	ENDDO
        !	WRITE (6,*) 'KLMAX',KLMAX,(LMAXVALS(IEP,ITYP(I)),I=0,5)
	IF (IEP.NE.1.AND.iset(ISING).NE.2) THEN
           ILMAX=MIN(ILMAX,KLMAX)
           JLMAX=MIN(JLMAX,KLMAX)
	ENDIF
	DO  I=0,KATOM-1
           DO  J=I+1,KATOM
              IF (.NOT.(NEED(I,J).OR.NEED(J,I))) cycle
              IF (I.EQ.0) THEN
                 !	  LMH=IDLMAX+IEDGE
                 LMH=ILMAX+IEDGE
              ELSE
                 !	  LMH=IDLMAX*2
                 LMH=ILMAX*2
              ENDIF
              JA=INDY(I,J)
              !
              !	Distance may be zero for paths with central atom as scatterer
              !
              IF (DN(I,J,1).LT.RMINDIST2) THEN
                 CALL CZERO (HH(0,JA),(LMH+1)*2)
                 cycle
              ENDIF
              !
              !	Calculate wave vector if first time or if XE in use
              !
              CKC=SQRT(ECM(IEP,ITYP(I))+ECM(IEP,ITYP(J)))*DN(I,J,1)
              !	IF (IEP.EQ.NPT(1)) WRITE (6,*) 'CKC',CKC,I,J,DN(I,J,1)
              !
              !	Calculate first two Hankel functions explicitly HH(0,JA), HH(1,JA)
              !
              TEF=CI*CKC
              H0=EXP(TEF)
              DZ=-1./CKC
              HH(0,JA)=CI*H0*DZ
              IF (iset(IEXTYP).EQ.1) THEN
                 HH(1,JA)=DZ*(H0-HH(0,JA))
                 !
                 !	Calculate hankel functions for L = 2 to LMH
                 !
                 CL=1.
                 DO  L=2,LMH
                    CL=CL+2.
                    HH(L,JA)=-HH(L-1,JA)*DZ*CL-HH(L-2,JA)
                    IF (ABS(REAL(HH(L,JA))).GT.1.E18) THEN
                       CALL CZERO (HH(L,JA),(LMH-L+1)*2)
                       GOTO 21
                    ENDIF
                 enddo
21               IF (LP) THEN
                    SL=1
                    DO  L3=0,JLMAX+JLMAX
                       DO  M3=-L3,L3
                          IF (I.NE.0) THEN
                             VIJ(L3,M3)=HH(L3,JA)*YLM(JA,L3,-M3)
                          ELSEIF (L3.LE.JLMAX+IEDGE) THEN
                             WIJ(L3,M3)=HH(L3,JA)*YLM(JA,L3,-M3)
                             WJI(L3,-M3)=WIJ(L3,M3)*SL
                          ENDIF
                       enddo
                       SL=-SL
                    enddo
                    LLMIN=0
                    LLMAX=JLMAX
                    IF (I.EQ.0) THEN
                       LLMIN=IEDGE
                       LLMAX=IEDGE
                    ENDIF
                    IC1=0
                    DO  L1=LLMIN,LLMAX
                       IC=IRSC(L1)
                       DO  M1=-L1,L1
                          IC1=IC1+1
                          IC3=0
                          DO  L2=0,JLMAX
                             LSTART=IABS(L2-L1)
                             LODD=MOD(LSTART,2).EQ.1
                             L2P1=L2+L1
                             DO  M3=-L2-M1,L2-M1
                                IC3=IC3+1
                                IF (I.NE.0) THEN
                                   TKK(IC3,IC1,I,J)=0.
                                   IF (MEVEN(I,J)) THEN
                                      IF (MOD(L1+L2+M3,2).NE.0) cycle
                                      IC=JRSC(IC1,IC3)
                                   ENDIF
                                ELSE
                                   TK(IC3,IC1,J)=0.
                                   QK(IC1,IC3,J)=0.
                                ENDIF
                                !	  M3=M2-M1
                                IF (IABS(M3).LE.L2P1) THEN
                                   DO  L3=LSTART,L2P1,2
                                      IF (IABS(M3).GT.L3) cycle
                                      IC=IC+1
                                      IF (I.NE.0) THEN
                                         TKK(IC3,IC1,I,J)=TKK(IC3,IC1,I,J)+VIJ(L3,M3)*RSC(1,IC)
                                      ELSE
                                         IF (NEED(I,J)) THEN
                                            TK(IC3,IC1,J)=TK(IC3,IC1,J)+WIJ(L3,M3)*RSC(1,IC)
                                         ENDIF
                                         IF (NEED(J,I)) THEN
                                            !	        WRITE (7,'(A,3I2,5F9.5)') 'TK',M1,L2,M2,TK(IC3,IC1,1),WIJ(L3,M3,0,1),DN(0,1,2)
                                            QK(IC1,IC3,J)=QK(IC1,IC3,J)+WJI(L3,M3)*RSC(2,IC)
                                         ENDIF
                                      ENDIF
                                   enddo
                                ENDIF
                                IF (I.NE.0) THEN
                                   IF (LODD) THEN
                                      TKK(IC3,IC1,J,I)=-TKK(IC3,IC1,I,J)
                                   ELSE
                                      TKK(IC3,IC1,J,I)=TKK(IC3,IC1,I,J)
                                   ENDIF
                                ELSEIF (J.LE.2.AND.NEED(I,J)) THEN
                                   TK(IC3,IC1,J)=TMAT(IEP,L2,ITYP(J))*TK(IC3,IC1,J)
                                ENDIF
                             enddo
                          enddo
                       enddo
                    enddo
                 ENDIF
              ELSEIF (iset(IEXTYP).EQ.2) THEN
                 CALL SWC (CKC,SW(0,0,JA),MAXNU*2,ILMAX)
                 CALL CALCGAM (SW(0,0,JA),GAMNU(-3,0,0,JA),GAMSQUIG(-3,0,0,JA),MAXNU,ILMAX)
              ENDIF
              !
              !	End of atoms loop
              !
           enddo
        enddo
        !
        !	Calculate multiple scattering Z-matrix
        !
	CALL GETTIM (1,CPU2,ELAPS)
	IF (iset(IEXTYP).EQ.1) THEN
           NLEGS=1
           IF (KATOM.EQ.2) NLEGS=2
           LEGLIST(1)=1
           LEGLIST(2)=2
           DO IJ=1,NTERMS
              IF (COND(IJ).AND.SCOND(IJ)) THEN
                 ILEG=0
                 IOLEG=0
                 K1=0
                 DO I=1,IORD(IJ)
                    K2=KP(I,IJ)
                    ILEG=ILEG*10+K2
                    DO J=1,NLEGS
                       IF (LEGLIST(J).EQ.ILEG) GOTO 94
                    ENDDO
                    NLEGS=NLEGS+1
                    LEGLIST(NLEGS)=ILEG
                    J=NLEGS
                    CALL ADDALEG (TK(1,1,IOLEG),TKK(1,1,K1,K2),TK(1,1,NLEGS),ITYP(K2),IEP,MEVEN(K1,K2),JLMAX)
94                  IOLEG=J
                    K1=K2
                 ENDDO
                 CALL ZSUM (ZS,QK(1,1,KP(IORD(IJ),IJ)),TK(1,1,J),XZ(1,IJ),IPART,JLMAX,IEP.NE.1)
              ENDIF
           ENDDO
           IF (SP2) THEN
              DZ=0
              TZ=0
              TZA=0
              !
              !	Calculate product matrix of hankel functions
              !
              !	    LMXT=MIN0(IDLMAX,ITLMAX)
              LMXT=MIN0(ILMAX,JLMAX)
              !	    LMXT3=LMXT+IDLMAX
              LMXT3=LMXT+ILMAX
              !	    KMAX=IDLMAX+IEDGE
              KMAX=ILMAX+IEDGE
              IF (COND(2).OR.COND(3)) THEN
                 DO L=0,LMXT3
                    DO M=L,LMXT3
                       HHU(L,M)=HH(M,3)*HH(L,3)
                       HHU(M,L)=HHU(L,M)
                       IF (M.LE.KMAX) THEN
                          IF (COND(2)) THEN
                             H5(L,M)=HH(M,1)*HH(L,1)
                             H5(M,L)=H5(L,M)
                          ENDIF
                          IF (COND(3)) THEN
                             H6(L,M)=HH(M,2)*HH(L,2)
                             H6(M,L)=H6(L,M)
                          ENDIF
                       ENDIF
                    ENDDO
                 ENDDO
              ENDIF
              !
              !	--- L1 LOOP ---
              !
              KU=0
              KW=0
              !	    DO  L1=0,IDLMAX
              DO  L1=0,ILMAX
                 !	    IF (IEP.NE.1.AND.L1.GT.LMAXVALS(IEP,ITYP(1)).AND.L1.GT.LMAXVALS(IEP,ITYP(2))) cycle
                 L1P=L1+IEDGE
                 L1M=L1-IEDGE
60               IF (L1+L1M.GE.IEDGE) GOTO 70
                 L1M=L1M+2
                 GOTO 60
                 !
                 !	--- L2 LOOP ---
                 !
                 !   70	    DO  L2=0,IDLMAX
70               DO  L2=0,ILMAX
                    IF (L2.GT.L1.OR.(.NOT.COND(2).AND..NOT.COND(3))) THEN
                       ITEST1=1
                       L3MAX=0
                    ELSE
                       ITEST1=0
                       L3MAX=LMXT
                       IF (MOD(L2,2).EQ.MOD(L1,2)) THEN
                          IPARL=0
                       ELSE
                          IPARL=1
                       ENDIF
                       IF (L2.NE.L1) ITEST1=-1
                       !
                       !	TRIPLE SCATTERING
                       !
                       C3=TMAT(IEP,L1,ITYP(1))*TMAT(IEP,L2,ITYP(1))
                       E3=TMAT(IEP,L1,ITYP(2))*TMAT(IEP,L2,ITYP(2))
                    ENDIF
                    !
                    !	Double scattering
                    !
                    D2C=TMAT(IEP,L1,ITYP(1))*TMAT(IEP,L2,ITYP(2))
                    L2P=L2+IEDGE
                    L2M=L2-IEDGE
80                  IF (L2+L2M.GE.IEDGE) GOTO 90
                    L2M=L2M+2
                    GOTO 80
                    !
                    !	--- L3 LOOP ---
                    !
90                  KCD=DSI(L2,L1)
                    IF (IEP.EQ.1) THEN
                       ICFD(L2,L1)=KU
                    ELSE
                       KU=ICFD(L2,L1)
                    ENDIF
                    LMT1=IABS(L1-L2)
                    LTA1=L1+L2
                    SLN=(-1.)**LTA1
                    !
                    !	Double scattering
                    !
                    IF (COND(1)) THEN
                       DO  L4=L1M,L1P,2
                          D3=D2C*HH(L4,1)
                          DO  L5=L2M,L2P,2
                             TEMPC=D3*HH(L5,2)
                             LZMAX=MIN(L4+L5,LTA1)
                             LZMIN=MAX(IABS(L5-L4),LMT1)
                             LM=MIN0(L4,L5)
                             !
                             !	--- Double scattering L6 loop ---
                             !
                             DO  LZ=LZMIN,LZMAX,2
                                KU=KU+1
                                IF (IEP.EQ.1) THEN
                                   CFD1(KU)=0.
                                   DO I=0,LM
                                      !	        CFD1(KU)=CFD1(KU)+FFF(I,L4,L5)*CFA(KCD)
                                      CFD1(KU)=CFD1(KU)+F1(I,L4)*F2(I,L5)*CFA(KCD)
                                      KCD=KCD+1
                                   ENDDO
                                ENDIF
                                DZ=DZ+TEMPC*HH(LZ,3)*CFD1(KU)
                             enddo
                          enddo
                       enddo
                    ENDIF
                    IF (ITEST1.NE.1) THEN
                       DO  L3=0,L3MAX
                          !
                          !	Triple scattering
                          !
                          C3A=C3*TMAT(IEP,L3,ITYP(2))
                          E3A=E3*TMAT(IEP,L3,ITYP(1))
                          KCT=TSI(L3,L2,L1)
                          IF (IEP.EQ.1) THEN
                             ICFT(L3,L2,L1)=KW
                          ELSE
                             KW=ICFT(L3,L2,L1)
                          ENDIF
                          LMI1=IABS(L2-L3)
                          LMI2=IABS(L1-L3)
                          LMA1=L2+L3
                          LMA2=L1+L3
                          IF (L1.NE.L2) THEN
                             DO  I4=L1M,L1P,2
                                DO  I5=L2M,L2P,2
                                   LM=MIN0(I4,I5)
                                   IF (COND(2)) TEMPC=H5(I5,I4)*C3A
                                   IF (COND(3)) TEMPCA=H6(I5,I4)*E3A
                                   DO  I6=LMI1,LMA1,2
                                      DO  I7=LMI2,LMA2,2
                                         KW=KW+1
                                         IF (IEP.EQ.1) THEN
                                            CFT1(KW)=0.
                                            CFT1A(KW)=0.
                                            DO I=0,LM
                                               IF (COND(2)) THEN
                                                  CFT1(KW)=CFT1(KW)+F1(I,I4)*F1(I,I5)*CFC(KCT)
                                               ENDIF
                                               IF (COND(3)) THEN
                                                  CFT1A(KW)=CFT1A(KW)+F2(I,I4)*F2(I,I5)*CFC(KCT)*SLN
                                               ENDIF
                                               KCT=KCT+1
                                            ENDDO
                                         ENDIF
                                         IF (COND(2)) THEN
                                            TZ=TZ+HHU(I7,I6)*TEMPC*CFT1(KW)
                                         ENDIF
                                         IF (COND(3)) THEN
                                            TZA=TZA+HHU(I7,I6)*TEMPCA*CFT1A(KW)
                                         ENDIF
                                      enddo
                                   enddo
                                enddo
                             enddo
                          ELSE
                             !
                             !	--- L4 LOOP ---
                             !
                             DO  I4=L1M,L1P,2
                                !
                                !	--- L5 LOOP ---
                                !
                                DO  I5=L2M,MIN0(I4,L2P),2
                                   LM=MIN0(I4,I5)
                                   IF (COND(2)) TEMPC =H5(I5,I4)*C3A
                                   IF (COND(3)) TEMPCA=H6(I5,I4)*E3A
                                   DO  I6=LMI1,LMA1,2
                                      LMM=I6+IPARL
                                      DO  I7=LMM,LMA2,2
                                         KW=KW+1
                                         IF (IEP.EQ.1) THEN
                                            CFT1(KW)=0.
                                            CFT1A(KW)=0.
                                            DO I=0,LM
                                               IF (COND(2)) THEN
                                                  CFT1(KW)=CFT1(KW)+F1(I,I4)*F1(I,I5)*CFC(KCT)
                                               ENDIF
                                               IF (COND(3)) THEN
                                                  CFT1A(KW)=CFT1A(KW)+F2(I,I4)*F2(I,I5)*CFC(KCT)*SLN
                                               ENDIF
                                               KCT=KCT+1
                                            ENDDO
                                         ENDIF
                                         IF (COND(2)) TZ=TZ+HHU(I7,I6)*TEMPC*CFT1(KW)
                                         IF (COND(3)) TZA=TZA+HHU(I7,I6)*TEMPCA*CFT1A(KW)
                                      enddo
                                   enddo
                                enddo
                             enddo
                          ENDIF
                          !
                          !	END OF L6,L5,L4,L3 LOOPS ---
                          !
                       enddo
                    ENDIF
                    !
                    !	--- END OF L2 LOOP ---
                    !
                 enddo
                 !
                 !	--- END OF L1 LOOP ---
                 !
              enddo
              DO JEXP=IEXP,IEXP+JSPEC-1
                 XZ(JEXP,1)=DZ *2.*DIV
                 XZ(JEXP,2)=TZ *2.*DIV
                 XZ(JEXP,3)=TZA*2.*DIV
              ENDDO
              !	    IF (IEP.EQ.1) WRITE (6,*) 'KW,KU',KW,KU
           ENDIF
	ELSEIF (iset(IEXTYP).EQ.2) THEN
           !
           !	Rehr and Albers code
           !
           MUM=MAX(0,NUMAX)
           MUJ=MIN(IEDGE,MUM)
           !	  CALL MAKELEG1 (MUJ,MUM,GAMNU,GAMSQUIG,RL,FK,FNEW,FTERM,FMAT,KATOM,5,ITYP,INDY,IEP,INDANG,INDRK,INDSK,NEED,MEE,MEG,MJ,MU)
           CALL MAKELEG1 (MUJ,MUM,GAMNU,GAMSQUIG,RL,KATOM,5,ITYP,INDY,IEP,INDANG,INDRK,INDSK,NEED,MEE,MEG,MJ,MU,iep.eq.npt(1))
           !	if (iep.eq.jpt) write (7,*) 'idlmax,mj,mu',idlmax,mj,mu
           NLEGMAX=0
           NLEGS=0
           DO IJ=1,NTERMS
              IF (COND(IJ)) THEN
                 K1=KP(0,IJ)
                 K2=KP(1,IJ)
                 K3=KP(2,IJ)
                 K5=KP(IORD(IJ),IJ)
                 ILEG=K5*1000+K1*100+K2*10+K3
                 NLEGS=NLEGS+1
                 LEGLIST(NLEGS)=ILEG
                 IF (NLEGS.GT.1) THEN
                    DO K=1,NLEGS-1
                       IF (LEGLIST(K).EQ.ILEG) THEN
                          DO I=1,MJ
                             DO J=1,MU
                                !	        F3(I,J,NLEGS)=F3(I,J,K)
                             ENDDO
                          ENDDO
                          !	        GOTO 115
                       ENDIF
                    ENDDO
                 ENDIF
                 !	    CALL MULTLEG1 (MJ,MU,F3(1,1,NLEGS),FK(1,1,INDRK(K5,K2)),FNEW(1,1,INDSK(K1,K2,K3)))
                 CALL MULTLEG1 (MJ,MU,F3(1,1,NLEGS),FK(1,1,INDRK(K5,K2)),FNEW(1,1,INDSK(K1,K2,K3)))
115              DO JJ=2,IORD(IJ)-1
                    K1=K2
                    K2=K3
                    K3=KP(JJ+1,IJ)
                    ILEG=ILEG*10+K3
                    NLEGS=NLEGS+1
                    LEGLIST(NLEGS)=ILEG
                    DO K=1,NLEGS-1
                       IF (LEGLIST(K).EQ.ILEG) THEN
                          DO I=1,MJ
                             DO J=1,MU
                                !	      F3(I,J,NLEGS)=F3(I,J,K)
                             ENDDO
                          ENDDO
                          !	      GOTO 116
                       ENDIF
                    ENDDO
                    CALL MULTLEG2 (MJ,MU,F3(1,1,NLEGS),F3(1,1,NLEGS-1),FMAT(1,1,INDSK(K1,K2,K3)))
                    !	    CALL MULTLEG2 (MJ,MU,NLEGS,NLEGS-1,INDSK(K1,K2,K3))
116                 CONTINUE
                 ENDDO
                 JJ=IORD(IJ)-1
                 K4=KP(JJ,IJ)
                 CALL MULTLEG3 (MJ,MU,F3(1,1,NLEGS),FTERM(1,1,INDSK(K4,K5,0)),TZ)
                 FIC=TZ*DIV
                 DO I=0,IORD(IJ)
                    J=INDY(KP(I,IJ),KP(I+1,IJ))
                    FIC=FIC*HH(0,J)
                 ENDDO
                 DO JEXP=IEXP,IEXP+JSPEC-1
                    XZ(JEXP,IJ)=FIC
                 ENDDO
              ENDIF
           ENDDO
	ENDIF
	CALL GETTIM (1,CPU3,ELAP)
	VTIME(9)=VTIME(9)+CPU3-CPU2
        !
        !	Calculate Debye-Waller factors and chi(e)
        !
77	KUMPATHS=NUMPATHS
	DO IJ=1,NTERMS
           IF (COND(IJ)) THEN
              P=1./real(IORD(IJ),kind=dp)
              CE=0
              DO I=1,IORD(IJ)
                 CE=CE+ECM(IEP,ITYP(KP(I,IJ)))
              ENDDO
              CE=CEXP(-CE*P*UPATH(IJ))
              !	  if (iep.eq.1) write (7,*) 'upath',ij,ce,upath(ij)
              IF (KUMPATHS.LT.IPARPATH) KUMPATHS=KUMPATHS+1
              KSPEC=1
              DO JEXP=IEXP,IEXP+JSPEC-1
                 IF (ALL) THEN
                    TYP(IEP,KSPEC,IJ)=XZ(JEXP,IJ)*CAP(IEP,JCLUS,ICAP)*IFACS(IJ)*RATIO(IEP,ICAP)
                 ELSE
                    TYP(IEP,KSPEC,IJ)=SPECTRE(IEP,KUMPATHS,KSPEC)
                 ENDIF
                 TYQ(IEP,KSPEC,IJ)=TYP(IEP,KSPEC,IJ)*CE*ANGF(JEXP,KP(1,IJ),KP(IORD(IJ),IJ))
                 XDT(IEP,JEXP,IORD(IJ)-1)=XDT(IEP,JEXP,IORD(IJ)-1)+TYQ(IEP,KSPEC,IJ)
                 IF (DEBUG.AND.IEP.LE.3.AND.IJ.EQ.1) WRITE (LOGFILE,*) 'XD in MSCAL',XD(IEP,JEXP)
                 KSPEC=KSPEC+1
              ENDDO
           ENDIF
	ENDDO
        !
        !	End energy loop
        !
     enddo
  ELSE
     !-st	  CALL CZERO (TYQ,IPARNPOINTS*IPARNSP*12*2)
     TYQ(:,:,:) = 0.0_dp
  ENDIF
  DO IJ=1,NTERMS
     IF (COND(IJ)) THEN
        DO I=1,2
           DO J=1,5
              IF (KP(J,IJ).EQ.0) THEN
                 II(J,1)=-1
                 II(J,2)=0
              ELSE
                 II(J,1)=III(KP(J,IJ),1)
                 II(J,2)=III(KP(J,IJ),2)
              ENDIF
           ENDDO
        ENDDO
        CALL SAVEPATH (NBASE+IJ-1,TYP(1,1,IJ),TYQ(1,1,IJ),IFACS(IJ),IJ+NOFF,II,RMSANG(IJ))
        COND(IJ)=.FALSE.
     ENDIF
  ENDDO
  CALL GETTIM (1,CPU4,ELAPS)
  VTIME(4)=VTIME(4)+CPU4-CPU1
  RETURN
END SUBROUTINE MSCAL

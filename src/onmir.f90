SUBROUTINE ONMIR (I,TPHI,TTHETA,JCLUS,*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_SYMOPS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2

  TTHETA=RADTH(I)
  IF (NPGCLASS(JCLUS).EQ.7) THEN
     TP=PID2
     TF=AMOD(RADPHI(I),TP)
     IF (ABS(TF-TP).LT..001.OR.ABS(TF).LT..001) GOTO 10
  ENDIF
  IF (ABS(RADTH(I)-PID2).GT..001) THEN
     CALL WTEXTI ('Invalid theta value for shell ',I)
     RETURN 1
  ENDIF
  TTHETA=PID2
10 JMIR=1
  TPHI=RADPHI(I)
  RETURN
END SUBROUTINE ONMIR

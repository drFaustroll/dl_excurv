SUBROUTINE INQU
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_COMPAR
  Use Common_P
  Use Common_VIEW
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_XY
  Use Include_FT

  CHARACTER FTOPTS(0:2)*6,IP1*4,IP2*4,IP3*4
  IF (INTOPT.EQ.0) THEN
     KW=OUTTERM
  ELSE
     KW=LOGFILE
  ENDIF
  IF (KEYWORD(1:1).EQ.'N') THEN
     DO IEXP=1,NSPEC
        CALL WTEXTI (CHAR(48+IEXP)//': NPT = ',NPT(IEXP))
     ENDDO
     RETURN
  ENDIF
  CALL FIT ('Q',iset(IWEIGHT),*2)
2 WRITE (KW,10) NSPEC,NP,LMAX,IDLMAX,ITLMAX
  DO IEXP=1,NSPEC
     WRITE (KW,30) IEXP,JS(IEXP),JF(IEXP),NPT(IEXP)
     WRITE (KW,40) ENER(1)/EC,ENER(NP)/EC
     WRITE (KW,50) ENER(JS(IEXP))/EC,ENER(JF(IEXP))/EC
     WRITE (KW,45) RK(1,IEXP)/RKC,RK(NP,IEXP)/RKC
     WRITE (KW,55) RK(JS(IEXP),IEXP)/RKC,RK(JF(IEXP),IEXP)/RKC
     WRITE (KW,60) XABS(1,IEXP),XABS(NP,IEXP)
  ENDDO
  IF (NSPEC.GT.2) THEN
     CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*5,*7)
  ENDIF
5 WRITE (KW,70) LC,LF,ID,NPS,IXA
  WRITE (KW,90) (IFTSET(JQ),JQ=1,7),CENT,(NPH(KQ),KQ=1,NPS)
  WRITE (KW,100) VVX,VVY,VVZ
7 RETURN
10 FORMAT (I2,' Spectra.',' Number of points read ',I5,', angular momentum ',3I3)
30 FORMAT (' Spectrum:',I4/' Theory calculated from point ',I4,' (JS) to point',I4,' (JF) (',I4,' points)')
40 FORMAT (' Energy at point  1',F12.5,' energy at point NP ',F11.5)
50 FORMAT (' Energy at point  JS',F11.5,' energy at point JF ',F11.5)
45 FORMAT (' Wave-vector at point  1',F12.5,' wave-vector at point NP ',F11.5)
55 FORMAT (' Wave-vector at point  JS',F11.5,' wave-vector at point JF ',F11.5)
60 FORMAT (' Absorbance at point   1',F12.5,'  absorbance at point NP ',F11.5)
70 FORMAT (' Theory level',I6,5X,'FT level',I8/' Mode',I14,5X,'NPS (atoms)',I5,' XADD flag',I7)
90 FORMAT (' FTRANS flags',18X,7I1,' FT window centre',F8.3/' NPH (1,NPS)',5X,6I3)
100 FORMAT (' Viewing direction:',8X,3F8.3)
END SUBROUTINE INQU

SUBROUTINE CALCPOT (AUTO,IS,IF,CENTRAL)
  !======================================================================C
  Use Definition
  Use Common_DLV
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_POT
  Use Common_PMTX
  Use Common_MATEL
  Use Common_P
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_PA2

  !
  !	Generate muffin-tin potentials
  !
  !	RWS     = Wigner-Seitz radius. lattice parameter and atomic volume
  !	          are calculated assuming atomic spheres are in contact.
  !	JWS     = Log. grid point nearest to RWS.
  !	DXL     = Log. grid interval.
  !	EDXL    = EXP (DXL)
  !	NEND(I) = Maximum effective radius for atom type I.
  !	NPTS    = Number of points in radial grid (should be odd).
  !	ALF(I)  = X-alpha exchange parameter ( 1 for Slater etc.).
  !	RAT     = Atomic radius. overrides default values if non-zero.
  !	ION(1)  = Ionicity. at present only -1,0,1 acceptable.
  !	CP      = Constant potential correction, eg the madelung potential
  !	          for an ionic solid. ( input in ev).
  !	RMT     = Atomic ( 12 fold) or ionic (6 fold) radius, table
  !	          of defaults (defmtrm1,defmtr,defmtrp1) are in angstroms
  !	          - converted to bohr radii before use.
  !	RHOINT  = Interstitial charge density * 4 pi.
  !	RHOAT   = Charge density * 4 pi.
  !	RHOAT2  = Charge density * 4 pi. * R**2.
  !
  !	The identity of each shell is stored in array IDD(ISHL), i.e. the
  !	atomic charge density associated with shell ISHL is roat2(j,ix)
  !	where IX=IDD(ISHL). The corresponding coulomb potential (*r**2) is
  !	CLAT2(J,IX). The tabulated charge densities in exf1.rho.data
  !	from the Desclaux program are 4pi*rho*r**2.
  !	Input units are angstroms and ev. output units are bohr radii
  !	and rydbergs, as are working units.
  !	Muffin-tin potential *R (in COL), Superposed potential*R (in CLAT)
  !	and atomic poteneial*R (in CLAT2) are output. Note use of these
  !	array as workspace following calculations.
  !	Up to 26 phaseshifts for each energy point are calculated.
  !	The energy grids for rho, mtp adn phase shifts are not output.
  !	For rho the log grid has log(estart) =-8.8 , log(int) =.05
  !

  !      *** INCLUDES CALLS TO GHOST LIBRABRY ROUTINES ***

  LOGICAL   :: AUTO,SAME,CENTRAL,CENTRAL1
  CHARACTER :: IP1*12,IP2*12,IP3*12,HOLELABS(9)*25
  DIMENSION :: IZD(2),IOND(2),MINI(IPARNSP),MAXI(IPARNSP)
  dimension :: MINAT(IPARNSP),MAXAT(IPARNSP)
  DATA HOLELABS / &
       '1S Core Hole (K-edge)',    '2S Core Hole (L1-edge)',   '2P1/2 Core Hole (L2-edge)', &
       '2P3/2 Core Hole (L3-edge)','3S Core Hole (M1-edge)',   '3P1/2 Core Hole (M2-edge)', &
       '3P3/2 Core Hole (M3-edge)','3D3/2 Core Hole (M4-edge)','3D5/2 Core Hole (M5-edge)'/
  !
  !	Calculate log. grid for charge densities.
  !
  IGRAPH=0
  IWM=7
  INTERVAL=50
  ICHARGEOUT=0
  IF (AUTO) GOTO 10
  if(.not.DLV_flag)then
     IF (IS.NE.IF) CALL WTEXT ('Calculating potentials for all atoms:')
     IF (LRIS+LMDSN.EQ.0) then
        CALL WTEXT('Enter : G (Graphics), T (Terminal Output), M (Charge densities) or C (Continue) [C]')
     endif
  endif
  CALL CREAD (IP1,IP2,IP3,INTER,IC,V,*10,*100)
  IF (INTER.NE.0) INTERVAL=INTER
  IF (IP1(1:1).EQ.'G') IGRAPH=1
  IF (IP1(1:1).EQ.'T') IWM=6
  IF (IP1(1:1).EQ.'M') ICHARGEOUT=1
10 V0AV=0
  RHO0AV=0
  NV0=0
  IF (AUTO.AND.iset(ITORTOISE).EQ.4) CALL SETMTR
  DO IG=1,IPARNSP
     MINAT(IG)=99
     MAXAT(IG)=1
     MINI(IG)=1
     MAXI(IG)=1
  ENDDO
  IG=1
  DO I=2,IPARNP
     IF (LASTHOLE(I).NE.-99) IG=IG+1
     IF (IATOM(I).GT.1.AND.IATOM(I).LE.MINAT(IG)) THEN
        MINAT(IG)=IATOM(I)
        MINI(IG)=I
     ENDIF
     IF (IATOM(I).GT.1.AND.IATOM(I).GE.MAXAT(IG)) THEN
        MAXAT(IG)=IATOM(I)
        MAXI(IG)=I
     ENDIF
     !	IF (IATOM(1).GT.0.AND.IATOM(I).EQ.IATOM(1)) MAXI(IG)=I
  ENDDO
  IG=1
  IF (IS.GT.2) THEN
     DO I=2,IS-1
        IF (LASTHOLE(I).NE.-99) IG=IG+1
     ENDDO
  ENDIF
  DO INT=IS,IF
     INT1=MAX(INT,1)
     IF (INT1.NE.1.AND.LASTHOLE(INT1).NE.-99) IG=IG+1
     IF (IATOM(INT1).LE.0) GOTO 90
     NEIGH=-1
     NEIGHDEF=MINI(IG)
     IF (IATOM(INT1).EQ.MINAT(IG)) NEIGHDEF=MAXI(IG)
     !	write (6,*) int1,iatom(int1),MINI,MAXI,NEIGHDEF
     IF (IATOM(INT1).LE.6) NEIGHDEF=INT1
     IF (LASTNEIGH(INT).GT.0) THEN
        IF (IATOM(LASTNEIGH(INT)).GT.0) NEIGHDEF=LASTNEIGH(INT)
     ENDIF
     !  20	IF (NEIGH.GE.0.AND.NEIGH.LE.IPARNP) GOTO 40
     IF (AUTO) GOTO 30
20   IF (LRIS+LMDSN.EQ.0) then
        if(.not.DLV_flag)then
           CALL WTEXT ('Atom: '//CHAR(48+INT)//' ('//ELS(IATOM(INT1))//'). Enter neighbouring atom [' &
                //CHAR(48+NEIGHDEF)//' ('//ELS(IATOM(NEIGHDEF))//')]')
        endif
     endif
     CALL CREAD (IP1,IP2,IP3,NEIGH,IC,V,*30,*100)
     IF (IC.EQ.0) THEN
        DO NEIGH=MAX(NPS,IF),1,-1
           IF (IP1.EQ.ELS(IATOM(NEIGH))) GOTO 40
        ENDDO
        NEIGH=-1
        GOTO 20
     ELSE
        GOTO 40
     ENDIF
30   NEIGH=NEIGHDEF
40   IF (NEIGH.GT.0) THEN
        IF (IATOM(NEIGH).GT.0) LASTNEIGH(INT)=NEIGH
     ENDIF
     IHOLE=0
     CENTRAL1=CENTRAL.OR.LASTHOLE(INT).NE.-99
     IF (.NOT.CENTRAL1) GOTO 70
     JEXP=1
     IF (INT.GT.1) THEN
        DO II=2,INT
           IF (LASTHOLE(II).NE.-99) JEXP=JEXP+1
        ENDDO
     ENDIF
     IF (LASTHOLE(INT).NE.-99.AND.LASTHOLE(INT).NE.-98) THEN
        IHOLE=LASTHOLE(INT)
     ELSE
        IHOLE=IEDGECODE(JEXP)
     ENDIF
     ITHOLE=IHOLE/IEDGECODE(JEXP)

     IF (AUTO) GOTO 70
     IF (LRIS+LMDSN.NE.0) GOTO 60
50   if(.not.DLV_flag) CALL WTEXTIT ('Select Code For Exited Atom [',ITHOLE,'] :')
     if(.not.DLV_flag) CALL WTEXT ('No Correction                        (0)')
     if(.not.DLV_flag) CALL WTEXT (HOLELABS(IEDGECODE(JEXP))//' (relaxed approximation) (1)')
     if(.not.DLV_flag) CALL WTEXT (HOLELABS(IEDGECODE(JEXP))//' (Z+1 approximation)    (-1)')
60   CALL CREAD (IP1,IP2,IP3,ITHOLE,IC,V,*65,*100)
     IF (IABS(ITHOLE).GT.1.OR.IC.EQ.0) CALL ERRS (*50)
     IHOLE=ITHOLE*IEDGECODE(JEXP)
     !
     !     Calculate atomic volume, Wigner-Seitz radius etc.
     !
65   LASTHOLE(INT)=IHOLE
70   IHCODE(INT)=IHOLE
     IZP=0
     IF (DEBUG) CALL WTEXTI ('IHOLE ',IHOLE)
     IF (IHOLE.LT.0) IZP=1
     IZT=IZP+IATOM(INT1)
     IF (IZT.LT.1.OR.IZT.GT.103) THEN
        IF (IS.EQ.IF) THEN
           CALL ERRMSGI ('Invalid Atomic Number : ',IZT,*100)
        ELSE
           GOTO 90
        ENDIF
     ENDIF
     !
     !	NEIGH=0 will ensure only one charge atom type is used
     !	even if it is a central atom - excurv92 compatibility.
     !
     IF (NEIGH.EQ.0) THEN
        NEIGH=NEIGHDEF
        SAME=.TRUE.
     ELSE
        SAME=.FALSE.
     ENDIF
     IF (IATOM(NEIGH).LT.1.OR.IATOM(NEIGH).GT.103) THEN
        CALL ERRMSGI ('Invalid Atomic Number for neighbour : ',IATOM(NEIGH),*100)
     ENDIF
     !
     !	NPOTS(INC) is the number of potential points read in. It is either
     !	0 or the number of the first point in the interstitial region
     !	NPTS is the number of the first point in the interstitial region
     !	NEND(INC) is IPARGRID or the effective limit of the wavefunction
     !
     IOND(1) = ION(INT1)
     IOND(2) =-IOND(1)
     IZD(1)  = IATOM(INT1)
     RMT     = RMTR2(INT1)
     !	if (iond(2).eq.-1) then
     !	  izd(2)=11
     !	  rmt2=2.570944
     !	elseif (iond(2).eq.1) then
     !	  izd(2)=9
     !	  rmt2=1.79588
     !	else
     IZD(2)=IATOM(NEIGH)
     RMT2=RMTR2(NEIGH)
     !	endif
     IF (NGRID.GT.10) THEN
        NPTS=NGRID
     ELSE
        NPTS=349
     ENDIF
     XSTARTS(INT)=-8.80487526
     !
     !	RMT can be optimised by using the following code in a least sq. routine
     !
     IF (iset(ITORTOISE).EQ.2.AND.RHO0G.GT.0.) THEN
        TARGET=RHO0G
     ELSEIF (iset(ITORTOISE).EQ.3.AND.FE0G.NE.0.) THEN
        TARGET=FE0G*EC*2.
     ELSEIF (iset(ITORTOISE).EQ.1.AND.V0G.LT.0.) THEN
        TARGET=V0G*EC*2.
     ELSEIF (iset(ITORTOISE).EQ.4.AND.NEIGH.NE.INT) THEN
        TARGET=IZD(1)
     ELSE
        TARGET=0.
     ENDIF
     IF (TARGET.NE.0.) THEN
        TARGET2=TARGET
        IF(iset(ITORTOISE).EQ.1.OR.iset(ITORTOISE).EQ.3)TARGET2=TARGET/EC*.5
        IF (.NOT.AUTO) WRITE (OUTTERM,*) 'Target potential is:',target2
        WRITE (LOGFILE,*) 'Target potential is:',target2
        STEP=0.d0
        OLDDIFF=0.d0
        RMT0=RMT
        DIFFMIN=99.d0
        RMTMIN=RMT
        RMT2MIN=RMT2
        RATYOH=RMT2/RMT
        DO I=1,50
           CALL CALCPOT2 (INT,INT1,IZT,IZD,IOND,RMT,RMT2,IWM,IHOLE,ICHARGEOUT,V0,RHO0,SAME,INTERVAL,NPTS,RHOAV,*100)
           EFERMI=V0+(.75d0*PI*RHO0)**(2.d0/3.d0)
           IF (iset(ITORTOISE).EQ.2) THEN
              DIFF=RHO0-TARGET
           ELSEIF (iset(ITORTOISE).EQ.3) THEN
              DIFF=EFERMI-TARGET
           ELSEIF (iset(ITORTOISE).EQ.1) THEN
              DIFF=V0-TARGET
           ELSEIF (iset(ITORTOISE).EQ.4) THEN
              DIFF=RHOAV-TARGET
           ENDIF
           DDIFF=OLDDIFF-DIFF
           IF (ABS(DIFF).LT.ABS(DIFFMIN)) THEN
              DIFFMIN=DIFF
              RMTMIN=RMT
              RMT2MIN=RMT2
           ENDIF
           IF (ABS(DIFF).LT..001.OR.ABS(DDIFF).LT..0001) GOTO 80
           IF (I.EQ.1) THEN
              TSTEP=.01
           ELSE
              IF (ABS(STEP-OLDSTEP).LT.1.E-6) THEN
                 TSTEP=1.
              ELSE
                 TSTEP=OLDDIFF/DDIFF*(STEP-OLDSTEP)+OLDSTEP
              ENDIF
              IF (ABS(TSTEP-STEP).GT..5) TSTEP=SIGN(ABS(STEP)+.1,TSTEP)
              IF (TSTEP.LT.-RMT0*.5) GOTO 80
           ENDIF
           OLDDIFF=DIFF
           OLDSTEP=STEP
           STEP=TSTEP
           RMT=RMT0+STEP
           RMT2=RMT*RATYOH
           IF (RMT.LT.RMT0*.5.OR.RMT.GT.RMT0*2.2) GOTO 80
        ENDDO
80      RMT=RMTMIN
        RMT2=RMT2MIN
        DIFF=TARGET+DIFFMIN
        IF(iset(ITORTOISE).EQ.1.OR.iset(ITORTOISE).EQ.3) DIFF=DIFF/EC*.5
        IF (.NOT.AUTO) WRITE (OUTTERM,110) 'Best value of RMT:',RMT/DC, RMT2/DC,' giving V0 of',DIFF
        WRITE (LOGFILE,110) 'Best value of RMT :',RMT/DC,RMT2/DC,' giving V0 of',DIFF
     ENDIF
     IF (DEBUG) WRITE (7,'(A,2F9.4)') 'RMT',RMT,RMT2
     IF (RMT.LT.0) RETURN
     CALL CALCPOT2 (INT,INT1,IZT,IZD,IOND,RMT,RMT2,IWM,IHOLE,ICHARGEOUT,V0,RHO0,SAME,INTERVAL,NPTS,RHOAV,*100)
     V0S(INT)=V0
     RHO0S(INT)=RHO0
     REMRMTR2(INT)=RMT
     DO I=1,IPARGRID
	POT(I,INT)=VMT(I)*GRID(I)
	RHO(I,INT)=ROAT(I)*G2(I)
     ENDDO
     NPOTS(INT)=NPTS
     EFERMI=V0+(.75d0*PI*RHO0)**(2.d0/3.d0)
     WRITE (OUTTERM,120) ELS(IZD(1)),ELS(IZD(2)),RHO0,EFERMI/EC*.5,V0/EC*.5,RHOAV,IZD(1)
     WRITE (LOGFILE,120) ELS(IZD(1)),ELS(IZD(2)),RHO0,EFERMI/EC*.5,V0/EC*.5,RHOAV,IZD(1)
     NV0=NV0+1
     V0AV=V0AV+V0
     RHO0AV=RHO0AV+RHO0
90   IF (INT.NE.0) CENTRAL=.FALSE.
  enddo
  IF (NV0.EQ.0) THEN
     CALL WTEXT ('No atoms defined')
     RETURN
  ENDIF
  IF (IABS(IS).NE.1.OR.AUTO) GOTO 100
  V0AV=V0AV/(EC*real(NV0,kind=dp))*.5d0
  RHO0AV=RHO0AV/real(NV0,kind=dp)
  EFERMI=V0AV*EC*2.d0+(.75d0*PI*RHO0AV)**(2.d0/3.d0)
  !	IF (IZZ.EQ.1) CALL TPOS (200,720)
  WRITE (OUTTERM,'(A,F8.3,F7.4,F8.3)') 'Average values of V0,RHO0,FE0 :',V0AV,RHO0AV,EFERMI/EC*.5
  IF (iset(ITORTOISE).NE.1) V0G=V0AV
  IF (iset(ITORTOISE).NE.3) RHO0G=RHO0AV
  IF (iset(ITORTOISE).NE.2) FE0G=EFERMI/EC*.5d0
100 RETURN
110 FORMAT (A,F8.3,'(',F8.3,')',A,F8.3)
120 FORMAT (A,' (',A,') Rho0: ',F7.4,' Efermi:',F7.3,' V0:',F8.3,' Electrons:',F7.3,' (Z=',I3,')')
END SUBROUTINE CALCPOT

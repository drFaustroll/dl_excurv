SUBROUTINE BJNSER (X,L,JL,NL,IFL,LU,*)
  !======================================================================C
  Use Definition
  Use Parameters
  !
  !     Calculates spherical bessel functions JL and NL
  !
  !	X   = argument of jl and nl
  !	L   = l value calculated (no offset)
  !	JL  = jl bessel function (abramowitz conventions)
  !	NL  = nl bessel function (abramowitz yl conventions)
  !	IFL = 0 return both jl and nl
  !	      1 return jl only
  !	      2 return nl only
  !	LU  = logical unit number for error messages
  !
  !	JL and NL are calculated by a series
  !	expansion according to 10.1.2 and 10.1.3
  !	in Abramowitz and Stegun (ninth printing),page 437
  !
  IMPLICIT real(float) (A-H,O-Z)
  SAVE
  COMPLEX(imag2) :: X,U,UX,DEL,PJ,PN,JL,NL
  DATA NITER /20/, TOL /1.E-15/
  IF (L.GT.0) CALL ERRMSG ('L>0 in BJNSER',*130)
  IF (REAL(X,kind=float).LE.0.) CALL ERRMSG ('X.LE.0 in BJNSER',*130)
  LP1=L+1
  U=.5*X*X
  DJL=1.
  FAC=-1.
  DO IL=1,LP1
     FAC=FAC+2.
     DJL=FAC*DJL
  enddo
  DNL=DJL/real(2*L+1,kind=float)
  IF (IFL.EQ.2) GOTO 90
  PJ=1.
  NF=1
  NFAC=2*L+3
  DEN=real(NFAC,kind=float)
  SGN=-1.
  UX=U
  DO IL=1,NITER
     DEL=SGN*UX/DEN
     PJ=PJ+DEL
     TREL=ABS(DEL/PJ)
     IF (TREL.LE.TOL) GOTO 80
     SGN=-SGN
     UX=U*UX
     NF=NF+1
     NFAC=NFAC+2
     DEN=real(NF*NFAC,kind=dp)*DEN
  enddo
  CALL ERRMSG (' JL does not converge in BJNSER',*130)
80 JL=PJ*(X**L)/DJL
90 IF (IFL.EQ.1) RETURN
  PN=1.
  NF=1
  NFAC=1-2*L
  DEN=real(NFAC,kind=float)
  SGN=-1.
  UX=U
  DO IL=1,NITER
     DEL=SGN*UX/DEN
     PN=PN+DEL
     TREL=ABS(DEL/PN)
     IF (TREL.LE.TOL) GOTO 120
     SGN=-SGN
     UX=U*UX
     NF=NF+1
     NFAC=NFAC+2
     DEN=real(NF*NFAC,kind=float)*DEN
  enddo
  CALL ERRMSG (' JL does not converge in BJNSER',*130)
120 NL=-PN*DNL/(X**LP1)
  RETURN
130 RETURN 1
END SUBROUTINE BJNSER

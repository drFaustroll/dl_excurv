MODULE Common_ATOMCORD

  !      COMMON /ATOMCORD/NAT(IPARNCLUS),IMIR(IPARNCLUS),KROT(IPARNCLUS),IA
  !     1XIS(IPARNCLUS),KCENT(IPARNCLUS),AR(0:IPARATOMS,IPARNCLUS),ATH(0:IP
  !     2ARATOMS,IPARNCLUS),APHI(0:IPARATOMS,IPARNCLUS),AX(0:IPARATOMS,IPAR
  !     3NCLUS),AY(0:IPARATOMS,IPARNCLUS),AZ(0:IPARATOMS,IPARNCLUS),ISHELL(
  !     40:IPARATOMS,IPARNCLUS),ISYM(0:IPARATOMS,IPARNCLUS),IDEG(0:IPARATOM
  !     5S,IPARNCLUS),ANORM(0:IPARATOMS,IPARNCLUS),ICENTI(0:IPARATOMS,IPARN
  !     6CLUS),NSYM(0:IPARATOMS,IPARNCLUS),SINGLES(0:IPARATOMS,IPARNCLUS),I
  !     7DIADALT
  !      LOGICAL SINGLES,IDIADALT

  Use Definition
  Use Parameters
  implicit none
  private

  integer(i4b), dimension(:),   public ::  NAT(IPARNCLUS)
  integer(i4b), dimension(:),   public ::  IMIR(IPARNCLUS)
  integer(i4b), dimension(:),   public ::  KROT(IPARNCLUS)
  integer(i4b), dimension(:),   public :: IAXIS(IPARNCLUS)
  integer(i4b), dimension(:),   public :: KCENT(IPARNCLUS)
  real(dp),     dimension(:,:), public ::     AR(0:IPARATOMS,IPARNCLUS)
  real(dp),     dimension(:,:), public ::    ATH(0:IPARATOMS,IPARNCLUS)
  real(dp),     dimension(:,:), public ::   APHI(0:IPARATOMS,IPARNCLUS)
  real(dp),     dimension(:,:), public ::     AX(0:IPARATOMS,IPARNCLUS)
  real(dp),     dimension(:,:), public ::     AY(0:IPARATOMS,IPARNCLUS)
  real(dp),     dimension(:,:), public ::     AZ(0:IPARATOMS,IPARNCLUS)
  integer(i4b), dimension(:,:), public :: ISHELL(0:IPARATOMS,IPARNCLUS)
  integer(i4b), dimension(:),   public ::   ISYM(0:IPARATOMS,IPARNCLUS)
  integer(i4b), dimension(:),   public ::   IDEG(0:IPARATOMS,IPARNCLUS)
  real(dp),     dimension(:,:), public ::  ANORM(0:IPARATOMS,IPARNCLUS)
  integer(i4b), dimension(:),   public :: ICENTI(0:IPARATOMS,IPARNCLUS)
  integer(i4b), dimension(:),   public ::   NSYM(0:IPARATOMS,IPARNCLUS)
  logical,      dimension(:,:), public ::SINGLES(0:IPARATOMS,IPARNCLUS)
  logical,                      public :: IDIADALT
end MODULE Common_ATOMCORD


FUNCTION CALCACOS (ARG,MSG,LERR)
  !======================================================================
  CHARACTER :: MSG*(*),MSG1*20
  LOGICAL   :: LERR
  IF (ABS(ARG).GT.1.) THEN
     CALCACOS=ACOS(SIGN(1.,ARG))
     IF (ABS(ARG).GT.1.001) THEN
        MSG1=MSG
        CALL WTEXT ('ACOS -invalid argument for '//MSG1(1:NCSTR(MSG1)))
        LERR=.TRUE.
        RETURN
     ENDIF
  ELSE
     CALCACOS=ACOS(ARG)
  ENDIF
  LERR=.FALSE.
  RETURN
END FUNCTION CALCACOS

SUBROUTINE OCVERTX (DN,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - reflection about plane //z at 0 to x
  !
  Use Common_convia
  DIMENSION DN(36,*)
  DN(1,3)=PI2-DN(1,3)
  DN(1,5)=-DN(1,5)
  IS=IS+1
  RETURN 1
END SUBROUTINE OCVERTX

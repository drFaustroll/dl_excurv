SUBROUTINE FINDOPT (NOPT,OPT,IOPT,IDEFOPT,CHARSTR,*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_COMPAR
  CHARACTER*(*) :: OPT(*),CHARSTR
  CHARACTER*80  :: MSG1,MSG2
  CHARACTER*30  :: SORTED(40),T
  CHARACTER*10  :: BLANKS
  DATA BLANKS/' '/
  DO I=1,NOPT
     SORTED(I)=OPT(I)
  enddo
  DO I=1,NOPT-1
     MIN=I
     DO J=I+1,NOPT
	IF (LLT(SORTED(J),SORTED(MIN))) MIN=J
     enddo
     T=SORTED(MIN)
     SORTED(MIN)=SORTED(I)
     SORTED(I)=T
  enddo
  LO=LEN(OPT(1))+1
  IF (KEYWORD(1:1).EQ.'?') THEN
     CALL WTEXT ('Options available are:')
     DO I=1,NOPT,2
        MSG1=' '//SORTED(I)
        IF (I.EQ.NOPT) THEN
           CALL WTEXT (MSG1(1:LO))
        ELSE
           MSG2=' '//SORTED(I+1)
           CALL WTEXT (MSG1(1:lo)//BLANKS//MSG2(1:LO))
        ENDIF
     ENDDO
     IF (IDEFOPT.GT.0) THEN
        MSG1=OPT(IDEFOPT)
     ELSE
        MSG1='none'
     ENDIF
     CALL WTEXT ('Default Option: '//MSG1(1:LO))
     MSG1=CHARSTR
     CALL WTEXT ('Numeric Option: '//MSG1(1:LEN(CHARSTR)))
     RETURN 1
  ENDIF
  IF (IDEFOPT.GT.0.AND.(KEYWORD.EQ.' '.OR.KEYWORD(1:1).LT.'A')) THEN
     KEYWORD=OPT(IDEFOPT)
     LKEYWORD=NCSTR(KEYWORD)
     IOPT=IDEFOPT
     RETURN
  ELSE
     DO IOPT=1,NOPT
        IF (KEYWORD(1:LKEYWORD).EQ.OPT(IOPT)(1:LKEYWORD)) RETURN
     ENDDO
     DO IOPT=1,NOPT
        IF (OPT(IOPT)(1:1).EQ.'(') RETURN
     ENDDO
     CALL WTEXT ('Keyword '//KEYWORD(1:LKEYWORD)//' not found.')
     RETURN 1
  ENDIF
END SUBROUTINE FINDOPT

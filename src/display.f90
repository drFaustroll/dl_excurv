SUBROUTINE DISPLAY (IWW)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_COMPAR
  Use Common_GEOMETRY

  DIMENSION   :: DISTLIST(IPARATOMS),NUMS(IPARATOMS)
  CHARACTER*6 :: OPTS(6),IP1,IP2,IP3
  LOGICAL     :: ALL
  DATA OPTS/'ALL','NOSYM','CL1','CL2','CL3','CL4'/
  CALL FINDOPT (6,OPTS,IOPT,2,'Shell Number For Restricted Display',*100)
  IF (INTOPT.LE.0.OR.INTOPT.GT.IPARATOMS) NUMFLAG=0
  !	IF (NAT(1).LT.1) THEN
  !	  KEYWORD=' '
  !	  CALL SYMMETRY (.TRUE.,.TRUE.,*100)
  !	ENDIF
  !	IF (NAT(1).LT.2) RETURN
  IF (IOPT.GT.2) THEN
     JCLUS1=IOPT-2
     JCLUS2=JCLUS1
  ELSE
     JCLUS1=1
     JCLUS2=IPARNCLUS
  ENDIF
  DO  JCLUS=JCLUS1,JCLUS2
     IF (NAT(JCLUS).LT.1) THEN
        KEYWORD=' '
        CALL SYMMETRY (.TRUE.,.TRUE.,*100)
     ENDIF
     IF (NAT(JCLUS).LE.0) GOTO 90
     IF (IOPT.EQ.1.OR.NUMFLAG.GT.0) THEN
        ALL=.TRUE.
        DO  I=1,IPARATOMS
           NUMS(I)=I
        enddo
        K=NAT(JCLUS)
     ELSE
        ALL=.FALSE.
        K=0
        DO I=1,NAT(JCLUS)
           IF (NSYM(I,JCLUS).NE.1) GOTO 20
           K=K+1
           NUMS(K)=I
20         CONTINUE
        enddo
     ENDIF
     CALL COL132
     IF (NUMFLAG.NE.0) THEN
     ENDIF
     WRITE (IWW,110) PGROUP(JCLUS)
     WRITE (IWW,120) (NUMS(J),J=1,K)
     IF (NUMFLAG.NE.0) THEN
        WRITE (IWW,130) INTOPT,(DIST(J,INTOPT,JCLUS),J=1,NAT(JCLUS))
     ELSEIF (ALL) THEN
        DO  I=2,NAT(JCLUS)
           WRITE (IWW,130) I,(DIST(J,I,JCLUS),J=1,I-1)
        enddo
     ELSE
        DO  I=2,NAT(JCLUS)
           K=0
           DO J=1,NAT(JCLUS)
              IF (NSYM(J,JCLUS).NE.1) GOTO 40
              K=K+1
              DISTLIST(K)=DIST(J,I,JCLUS)
40            CONTINUE
           enddo
           WRITE (IWW,130) I,(DISTLIST(J),J=1,K)
        enddo
     ENDIF
     IF (LMDSN+LRIS.EQ.0.AND.IWW.EQ.OUTTERM) CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*55,*100)
55   WRITE (IWW,140) (NUMS(J),J=1,K)
     IF (NUMFLAG.NE.0) THEN
        WRITE (IWW,150) INTOPT,(ANGLE(J,INTOPT,JCLUS)/AC,J=1,NAT(JCLUS))
     ELSEIF (ALL) THEN
        DO I=2,NAT(JCLUS)
           WRITE (IWW,150) I,(ANGLE(J,I,JCLUS)/AC,J=1,I-1)
        enddo
     ELSE
        DO  I=2,NAT(JCLUS)
           K=0
           DO  J=1,NAT(JCLUS)
              IF (NSYM(J,JCLUS).NE.1) GOTO 70
              K=K+1
              DISTLIST(K)=ANGLE(J,I,JCLUS)/AC
70            CONTINUE
           enddo
           WRITE (IWW,150) I,(DISTLIST(J),J=1,K)
        enddo
     ENDIF
     IF (LMDSN+LRIS.EQ.0.AND.IWW.EQ.OUTTERM) CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*85,*100)
85   CALL COL80
90   CONTINUE
  enddo
100 RETURN
110 FORMAT (/' Point Group ',A6)
120 FORMAT (/4X,'Interatomic Distances'//18(I6,'/')/I3,'/',17(I6,'/')/I3,'/',17(I6,'/')/I3,'/',17(I6,'/'))
130 FORMAT (I3,'/',18F7.4/18F7.4/18F7.4/18F7.4/8F7.4)
140 FORMAT (/4X,'Bond Angles with Vertex 0'//18(I6,'/')/I3,'/',17(I6,'/')/I3,'/',17(I6,'/')/I3,'/',17(I6,'/'))
150 FORMAT (I3,'/',18F7.2/18F7.2/18F7.2/18F7.2/8F7.2)
END SUBROUTINE DISPLAY

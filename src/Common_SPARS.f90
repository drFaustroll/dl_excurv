MODULE Common_SPARS

  !      COMMON /SPARS/ SPARS(IPARNPARAMS+6,20),SEVEC(IPARNVARS,20),SIVCH(I
  !     1PARNVARS,20)
  !      CHARACTER*24 SIVCH
  Use Definition
  Use Parameters
  implicit none
  private
  real(dp),     dimension(:,:), public :: SPARS(IPARNPARAMS+6,20)
  real(dp),     dimension(:,:), public :: SEVEC(IPARNVARS,20)
  character*24, dimension(:,:), public :: SIVCH(IPARNVARS,20)
end MODULE Common_SPARS


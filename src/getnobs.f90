SUBROUTINE GETNOBS
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_DISTANCE
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_XY

  NOBS=0
  DO  IEXP=1,NSPEC
     DO  I=JS(IEXP),JF(IEXP)
	NOBS=NOBS+1
     enddo
  enddo
  IF (DWMIN.GT.-1..OR.DWMAX.LT.1.) THEN
     NOBS=NOBS+1
  ENDIF
  WEIGHTSUM=0.
  BAWTSUM=0.
  DO  I=1,NS
     DO  J=0,I-1
	IF (WEIGHTINGS(I,J).NE.0..AND.DISTANCES(I,J).NE.0.) THEN
           WEIGHTSUM=WEIGHTSUM+WEIGHTINGS(I,J)
           NOBS=NOBS+1
	ENDIF
	IF (BAWT(I,J).NE.0..AND.BONDANGS(I,J).NE.0.) THEN
           BAWTSUM=BAWTSUM+BAWT(I,J)
           NOBS=NOBS+1
	ENDIF
     enddo
  enddo
  RETURN
END SUBROUTINE GETNOBS

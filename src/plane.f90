SUBROUTINE PLANE
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Include_IPA
  Use Include_PA1

  CHARACTER*1 K1,K2,K3
  CALL WTEXT ('Definition of three-atom plane:')
10 CALL WTEXT ('Enter Atom 1')
  CALL CREAD (K1,K2,K3,IA1,IC,V,*10,*70)
  WRITE (6,*) V
20 CALL WTEXT ('Enter Atom 2')
  CALL CREAD (K1,K2,K3,IA2,IC,V,*20,*70)
  WRITE (6,*) V
30 CALL WTEXT ('Enter Atom 3')
  CALL CREAD (K1,K2,K3,IA3,IC,V,*30,*70)
  WRITE (6,*) V
  CALL WTEXT ('Definition of reference axis:')
  BX=0
  CALL WTEXT ('Enter BX[0]')
  CALL CREAD (K1,K2,K3,IBX,IC,V,*40,*70)
  BX=V
40 BY=0
  CALL WTEXT ('Enter BY[0]')
  CALL CREAD (K1,K2,K3,IBY,IC,V,*50,*70)
  BY=V
50 BZ=1
  CALL WTEXT ('Enter BZ[1]')
  CALL CREAD (K1,K2,K3,IBZ,IC,V,*60,*70)
  BZ=V
60 IF (IA1.LT.0) THEN
     A1X=0.
     A1Y=0.
     A1Z=0.
  ELSE
     A1X=UX(IA1)
     A1Y=UY(IA1)
     A1Z=UZ(IA1)
  ENDIF
  IF (IA2.LT.0) THEN
     A2X=0.
     A2Y=0.
     A2Z=0.
  ELSE
     A2X=UX(IA2)
     A2Y=UY(IA2)
     A2Z=UZ(IA2)
  ENDIF
  IF (IA3.LT.0) THEN
     A3X=0.
     A3Y=0.
     A3Z=0.
  ELSE
     A3X=UX(IA3)
     A3Y=UY(IA3)
     A3Z=UZ(IA3)
  ENDIF
  A2X=A2X-A1X
  A2Y=A2Y-A1Y
  A2Z=A2Z-A1Z
  A3X=A3X-A1X
  A3Y=A3Y-A1Y
  A3Z=A3Z-A1Z
  BX=BX
  BY=BY
  BZ=BZ
  XX1=A2Y*A3Z-A2Z*A3Y
  YY1=A2Z*A3X-A2X*A3Z
  ZZ1=A2X*A3Y-A2Y*A3X
  RRA=SQRT(XX1*XX1+YY1*YY1+ZZ1*ZZ1)
  RRB=SQRT(BX*BX+BY*BY+BZ*BZ)
  IF (RRA.EQ.0..OR.RRB.EQ.0.) THEN
     ANGL=0.
  ELSE
     XX1=XX1/RRA
     YY1=YY1/RRA
     ZZ1=ZZ1/RRA
     BX=BX/RRB
     BY=BY/RRB
     BZ=BZ/RRB
     ANGL=XX1*BX+YY1*BY+ZZ1*BZ
     ANGL=ACOS(ANGL)/AC
  ENDIF
  WRITE (OUTTERM,80) 'Plane 1       :',XX1,YY1,ZZ1
  WRITE (OUTTERM,80) 'Reference axis:',BX,BY,BZ
  WRITE (OUTTERM,80) 'Angle         :',ANGL
  TX=YY1*BZ-ZZ1*BY
  TY=ZZ1*BX-XX1*BZ
  TZ=XX1*BY-YY1*BX
  WRITE (OUTTERM,80) 'Intersection  :',TX+A1X,TY+A1Y,TZ+A1Z
  JRAD=IRAD(IABS(ICLUS(IA1)))
  CALL CTOR (JRAD,IA1,IA2,IA3,TORID,TORID2)
  WRITE (OUTTERM,90) JRAD,'-',IA1,'-',IA2,'-',IA3,'   :',TORID
70 RETURN
80 FORMAT (A,3F9.3)
90 FORMAT (4(I2,A),3F9.3)
END SUBROUTINE PLANE

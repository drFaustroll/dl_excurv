SUBROUTINE BESJNZ (X,JL,NL,LU,*)
  !======================================================================C
  Use Definition
  Use Parameters
  !
  !     purpose:  to calculate the spherical bessel functions jl and nl
  !	         for l=0 to 30 (no offset)
  !
  !     arguments:
  !	 x = argument of jl and nl
  !	 lmax = maximum l value calculated (no offset)
  !	 jl = jl bessel function (abramowitz conventions)
  !	 nl = nl bessel function (abramowitz yl conventions)
  !	 lu = logical unit number for printing out error messages
  !	      (you should set this equal to tape number for tty
  !	      or for tape number where you want to send out error
  !	      messages)
  !
  !     notes:  jl and nl should be calculated at least to 10 place
  !	       accuracy for the range 0<x<100 according to spot
  !	       checks with tables
  !
  !     first coded by r. c. albers on 14 dec 82
  !
  !     version 3
  !
  !     last modified: 27 jan 83 by r. c. albers
  !     dimension of jl,nl changed from 31 to 26  (10 aug 89) j. rehr
  !
  !-----------------------------------------------------------------------
  IMPLICIT real(float) (A-H,O-Z)
  SAVE
  COMPLEX*16 :: X,JL,NL
  COMPLEX*16 :: CJL,SJL,CNL,SNL,XJL,XNL,ASX,ACX
  COMPLEX*16 :: XI,XI2,XI3,XI4,XI5,XI6,XI7,XI8,XI9,XI10,XI11
  DIMENSION JL(27), NL(27)
  DIMENSION CJL(27), SJL(27), CNL(27), SNL(27)
  DATA XCUT /1./, XCUT1 /7.51/, XCUT2 /5.01/
  DATA LMAX /26/
  IF (LMAX.GT.26) GOTO 120
  LMAXP1=LMAX+1
  IF (REAL(X,kind=float).LE.0.)   GOTO 140
  IF (REAL(X,kind=float).GE.XCUT) GOTO 20
  DO IL=1,LMAXP1
     L=IL-1
     IFL=0
     CALL BJNSER (X,L,XJL,XNL,IFL,LU)
     JL(IL)=XJL
     NL(IL)=XNL
  enddo
  RETURN
20 IF (REAL(X,kind=float).GE.XCUT1) GOTO 70
  CALL BJNSER (X,24,XJL,XNL,1,LU)
  JL(25)=XJL
  CALL BJNSER (X,25,XJL,XNL,1,LU)
  JL(26)=XJL
  CALL BJNSER (X,26,XJL,XNL,1,LU)
  JL(27)=XJL
  IF (REAL(X,kind=float).GE.XCUT2) GOTO 30
  CALL BJNSER (X,0,XJL,XNL,2,LU)
  NL(1)=XNL
  CALL BJNSER (X,1,XJL,XNL,2,LU)
  NL(2)=XNL
  GOTO 40
30 ASX=SIN(X)
  ACX=COS(X)
  XI=1./X
  XI2=XI*XI
  NL(1)=-ACX*XI
  NL(2)=-ACX*XI2-ASX*XI
40 DO LP1=3,LMAXP1
     L=LP1-2
     TLXP1=real(2*L+1,kind=dp)
     NL(LP1)=TLXP1*NL(LP1-1)/X-NL(LP1-2)
  enddo
  DO  LX=3,LMAXP1
     LP1=LMAXP1+1-LX
     L=LP1-1
     TLXP3=real(2*L+3,kind=dp)
     JL(LP1)=TLXP3*JL(LP1+1)/X-JL(LP1+2)
  enddo
  RETURN
70 XI=1./X
  XI2=XI*XI
  XI3=XI*XI2
  XI4=XI*XI3
  XI5=XI*XI4
  XI6=XI*XI5
  XI7=XI*XI6
  XI8=XI*XI7
  XI9=XI*XI8
  XI10=XI*XI9
  XI11=XI*XI10
  SJL(1)=XI
  SJL(2)=XI2
  SJL(3)=3*XI3-XI
  SJL(4)=15.*XI4-6.*XI2
  SJL(5)=105.*XI5-45.*XI3+XI
  SJL(6)=945.*XI6-420.*XI4+15.*XI2
  SJL(7)=10395.*XI7-4725.*XI5+210.*XI3-XI
  SJL(8)=135135.*XI8-62370.*XI6+3150.*XI4-28.*XI2
  SJL(9)=2027025.*XI9-945945.*XI7+51975.*XI5-630.*XI3+XI
  SJL(10)=34459425.*XI10-16216200.*XI8+945945.*XI6-13860.*XI4+45.*XI2
  SJL(11)=654729075.*XI11-310134825*XI9+18918900*XI7-315315.*XI5+1485.*XI3-XI
  CJL(1)=0.
  CJL(2)=-XI
  CJL(3)=-3.*XI2
  CJL(4)=-15.*XI3+XI
  CJL(5)=-105.*XI4+10.*XI2
  CJL(6)=-945.*XI5+105.*XI3-XI
  CJL(7)=-10395.*XI6+1260.*XI4-21.*XI2
  CJL(8)=-135135.*XI7+17325.*XI5-378.*XI3+XI
  CJL(9)=-2027025.*XI8+270270.*XI6-6930.*XI4+36.*XI2
  CJL(10)=-34459425.*XI9+4729725.*XI7-135135.*XI5+990.*XI3-XI
  CJL(11)=-654729075.*XI10+91891800.*XI8-2837835.*XI6+25740.*XI4-55.*XI2
  DO IE=1,11
     SNL(IE)=CJL(IE)
     CNL(IE)=-SJL(IE)
  enddo
  IF (LMAXP1.LT.12) GOTO 100
  DO  LP1=12,LMAXP1
     L=LP1-2
     TLXP1=real(2*L+1,kind=dp)
     SJL(LP1)=TLXP1*XI*SJL(LP1-1)-SJL(LP1-2)
     CJL(LP1)=TLXP1*XI*CJL(LP1-1)-CJL(LP1-2)
     SNL(LP1)=TLXP1*XI*SNL(LP1-1)-SNL(LP1-2)
     CNL(LP1)=TLXP1*XI*CNL(LP1-1)-CNL(LP1-2)
  enddo
100 ASX=SIN(X)
  ACX=COS(X)
  DO LP1=1,LMAXP1
     JL(LP1)=ASX*SJL(LP1)+ACX*CJL(LP1)
     NL(LP1)=ASX*SNL(LP1)+ACX*CNL(LP1)
  enddo
  RETURN
120 WRITE (LU,130)
130 FORMAT (/,' lmax in besjn exceeds its maximum allowable value -- execution terminating')
  RETURN 1
140 WRITE (LU,150)
150 FORMAT (/,' x is less than or equal to zero in besjn')
  RETURN 1
END SUBROUTINE BESJNZ

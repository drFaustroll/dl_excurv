SUBROUTINE ITERQ (IOPT,DSTEP,Y,*)
  !======================================================================C
  Use Definition
  Use Common_DLV
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Common_ICF
  Use Common_IP
  Use Index_INDS
  Use Common_LSQ
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  DIMENSION    :: Y(*),NCYCLES(17),ISTARTS(17,3),ILIST(0:IPARNS),LI(IPARNVARS)
  CHARACTER*64 :: TKEY
  CHARACTER*12 :: IP1,IP2,IP3
  CHARACTER*4  :: LL,UL,OLDKEY,ONOFF(0:2)
  DATA LL/'lowe'/,UL/'uppe'/,OLDKEY/'LSR'/,IOLDOPT/9/,ONOFF/'off:','on:','all:'/
  data NCYCLES/3*0,1,2,3,3,2,0,1,2,1,2,1,0,2,0/
  data ISTARTS/3*0,5*14,0,2*11,2*19,13,0,20,0,4*0,2*11,19,13,2*0,13,0,13,2*0,21,0,5*0,2*13,10*0/
  call eqv_convia

10 IMSCB=iset(IMSC)
  IF (IOPT.EQ.18) THEN
     CALL UZERO (REME,IPARNPARAMS)
     CALL WTEXT ('Step parameters set to zero')
     RETURN 1
  ELSEIF (IOPT.LT.3) THEN
     CALL WTEXT('Map calculation with correlation '//ONOFF(iset(ICOR)))
  ELSE
     CALL WTEXT('Parameter refinement with correlation '//ONOFF(iset(ICOR)))
  ENDIF
  CALL UZERO (REME,IPARNPARAMS)
  DO I=1,IPARNVARS
     LI(I)=10
  ENDDO
  IF (INTOPT.EQ.0) INTOPT=100
  IF (INTOPT.GT.0.AND.INTOPT.LE.500) GOTO 30
20 IF (IOPT.LT.3) THEN
     CALL WTEXT ('Enter number of steps')
  ELSE
     CALL WTEXT ('Enter step parameter ( 1 to 500)')
  ENDIF
  CALL CREAD (IP1,IP2,IP3,INTOPT,IC,V,*20,*300)
  GOTO 10
30 IN1=1
  IN2=IPARNVARS
  DSTEP=10.**(-real(INTOPT,kind=dp)/50.)
  !
  !	Generate table from current parameters except for LSR
  !
  GOTO (60,60,50,140,140,140,140,140,60,140,140,140,140,140,60,140,40),IOPT
40 KEYWORD=OLDKEY
  IOPT=IOLDOPT
  IF (NCALL.LE.0) RETURN 1
  DO I=1,NCALL
     J=IVNUMS(I)
     NUMFLAG=2
     IF (ABS(PA1(J)).EQ.0.) CALL PCHANGE (IVCH(I),.001)
     U(I)=PA1(J)
  ENDDO
  GOTO 170
  !
  !	Maximum number of parameters is IPARNVARS
  !
50 IN2=INTOPT*2
60 NCALL=IN2
70 DO I=IN1,IN2
80   IF (LRIS+LMDSN.EQ.0) CALL WTEXT ('Enter parameter name or "=" to skip')
     CALL CREAD (IVCH(I),IP2,IP3,IN,IC,U(I),*80,*160)
     IF (IC.EQ.1.AND.IP2.EQ.' ') CALL ERRS (*80)
     IF (IOPT.LE.3) THEN
        !
        !	If number supplied with the parameter request upper value only.
        !
        LI(I)=MAX(10,NCSTR(IVCH(I)))
        IF (IC.EQ.1) GOTO 100
90      IF (LRIS+LMDSN.EQ.0) WRITE (OUTTERM,310) LL,IVCH(I)(1:LI(I))
        CALL CREAD (IP1,IP2,IP3,IN,IC,U(I),*90,*300)
100     IF (LRIS+LMDSN.EQ.0) WRITE (OUTTERM,310) UL,IVCH(I)(1:LI(I))
        CALL CREAD (IP1,IP2,IP3,IN,IC,Y(I),*100,*300)
     ENDIF
     TKEY=IVCH(I)
     LTKEY=NCSTR(TKEY)
     CALL GETLIST (TKEY,LTKEY,NLIST,ILIST)
     DO J=1,IPARNPARAMS
	IF (IVARS(J).EQ.TKEY) GOTO 120
     enddo
     IF (DVARS(1).EQ.IVCH(I)) THEN
        J=ID1A
     ELSEIF (DVARS(2).EQ.IVCH(I)) THEN
        J=ID2A
     ELSE
        CALL ERRMSG ('Parameter '//IVCH(I)//' not found',*80)
     ENDIF
120  IVNUMS(I)=J
     IF (IOPT.GT.3) THEN
        NUMFLAG=2
        IF (ABS(PA1(J)).EQ.0.) CALL PCHANGE (IVCH(I),.001)
        U(I)=PA1(J)
     ENDIF
  enddo
  GOTO 170
140 NCALL=0
  IF (KEYWORD(LKEYWORD:LKEYWORD).EQ.'E') THEN
     IND1=inds(INDEF0)
     IVCH(1)=IVARS(IND1)
     IVNUMS(1)=IND1
     IF (PA1(IND1).EQ.0) THEN
        NUMFLAG=2
        CALL PCHANGE (IVARS(IND1),.001)
     ENDIF
     U(1)=PA1(IND1)
     NCALL=1
  ENDIF
  DO I=1,NCYCLES(IOPT)
     DO JJ=1,NS
	IF (ISTARTS(IOPT,I).EQ.19.OR.ISTARTS(IOPT,I).EQ.21) THEN
           IF (NU(I).EQ.0.OR.NR(NU(I)).EQ.JJ) cycle
	ENDIF
        !-st	NCALL=MIN0(NCALL+1,IPARNVARS)
	NCALL=MIN(NCALL+1,IPARNVARS)
	JIO=JJ+INDS(ISTARTS(IOPT,I))
	IVNUMS(NCALL)=JIO
	IVCH(NCALL)=IVARS(JIO)
	IF (PA1(JIO).EQ.0.) THEN
           NUMFLAG=2
           CALL PCHANGE (IVCH(NCALL),.001)
	ENDIF
  	U(NCALL)=PA1(JIO)
     enddo
  ENDDO
  GOTO 170
  !
  !	Set NCALL to final parameter number if '=' entered.
  !
160 NCALL=I-1
170 IF (NCALL.LE.0) THEN
     RETURN 1
  ELSEIF (IOPT.EQ.3.AND.MOD(NCALL,2).NE.0) THEN
     CALL ERRMSG ('Maps require two parameters',*300)
  ENDIF
  DO  I=1,NCALL
     J=IVNUMS(I)
     !
     !	Determine magnitude of term to be refined and scaling factors
     !
     !	First case is for CA, CB, AFAC
     !
     XP(I)=1.
     SF(I)=1./2.5
     !
     !	XP is a dummy parameter for MAP
     !
     IF (IOPT.LE.3) cycle
     !
     !	Next case is for ang, th, fi
     !
     IF (ICF(J).EQ.4) THEN
        IF (J.GE.inds(INDPHI)) THEN
           XP(I)=3.*U(I)
        ELSE
           XP(I)=U(I)
        ENDIF
     ENDIF
     !
     !	This case for DISTANCES
     !
     IF (ICF(J).EQ.2) THEN
        XP(I)=5.
        IF (J.GE.inds(INDX)) XP(I)=2.5*U(I)
     ENDIF
     !
     !	Next is for VPI and XE
     !
     IF (IVCH(I)(1:3).EQ.'VPI'.or.IVCH(I).EQ.'VPI'.OR.IVCH(I)(1:2).EQ.'XE') XP(I)=ABS(U(I)*5.)
     IF (IVCH(I).EQ.'V0'.OR.IVCH(I)(1:2).EQ.'EF') XP(I)=U(I)
     IF (IVCH(I)(1:3).EQ.'POS'.OR.IVCH(I)(1:4).EQ.'CELL'.OR.IVCH(I).EQ.'RHO0'.OR.IVCH(I).EQ.'FE0') XP(I)=U(I)
     !
     !	Now the Debye-Waller factors
     !
     IF (ICF(J).EQ.3) XP(I)=.1
     !
     !	Perform unit conversions
     !
     XP(I)=XP(I)*CONVIA(ICF(J))
     IF (REME(J).NE.0.) THEN
        XXP=U(I)/REME(J)*100.*CONVIA(ICF(J))
        IF (ABS(XXP).GT..001) XP(I)=XXP
     ENDIF
     !
     !	SF is the scaling factor used in converting to absolute values
     !
     IF (ABS(XP(I)).GT.1.E-10) SF(I)=U(I)/XP(I)
  enddo
  IF (LRIS.GT.0) GOTO 250
  K=iset(IWEIGHT)
  DO IW=OUTTERM,LOGFILE,OUTDIFF
     IF (IOPT-3) 210,200,190
190  WRITE (IW,320) K
     GOTO 210
200  WRITE (IW,330) INTOPT,K
210  DO L=1,NCALL
	IF (IOPT.GT.3) THEN
           !	  WRITE (IW,340) L,IVCH(L)(1:LI(L)),U(L),SF(L)*DSTEP,U(L)+SF(L)*DSTEP*DFAC
           WRITE (IW,340) L,IVCH(L)(1:LI(L)),U(L),SF(L)*DSTEP,E(L),SF(L),XP(L)
           IF (IW.EQ.7) WRITE (7,*) IVNUMS(L),REME(IVNUMS(L)),E(L)/DSTEP
	ELSE
           WRITE (IW,350) L,IVCH(L)(1:LI(L)),U(L),Y(L)
	ENDIF
     enddo
  enddo
240 IF (IOPT.LE.2) THEN
     CALL WTEXT('Enter parameter number to edit or add, C to continue')
  ELSE
     if(.not.DLV_flag) CALL WTEXT('Enter : Submit to submit a background job')
     if(.not.DLV_flag) CALL WTEXT('        Continue to refine interactively')
     if(.not.DLV_flag) CALL WTEXT('        a number to edit, add to, or delete from the list')
  ENDIF
250 continue
  if(.not.DLV_flag)then
     CALL CREAD (IP1,IP2,IP3,IN1,IC,V,*240,*300)
  else
     IP1(1:1)='C'
     IP2     =' '
     IP3     =' '
     IN1     = 0
     IC      = 0
     V       = 0.0
  endif
  !	IF (IOPT.EQ.3.AND.IN1.LE.0.AND.IP1(1:1).NE.'S') GOTO 240
  IF (IN1.GT.NCALL+1) IN1=NCALL+1
  IF (NCALL.LT.IN1) NCALL=IN1
  IF (IN1.EQ.0) GOTO 260
  IN2=IN1
  GOTO 70
  !
  !	UI is related to the step size in estimating derivatives
  !
260 IF (IOPT.EQ.3) THEN
     DO  I=1,NCALL
        XP(I)=Y(I)
     enddo
     INTOPT=8
     IF (iset(MAPSIZE).EQ.1) THEN
        INTOPT=15
     ENDIF
  ENDIF
  IF (IP1(1:1).EQ.'S') GOTO 270
  IF (IP1(1:1).EQ.'C') RETURN
  CALL ERRS (*240)
  !
  !	Ask if multiple scattering required , if units defined
  !
270 IF (MG.GT.0.AND.IOMAX.GT.1.AND.iset(IMSC).EQ.0) THEN
280  CALL WTEXT ('Do you wish to include multiple scattering ?')
     CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*280,*300)
     IF (IP1(1:1).EQ.'Y') IMSCB=1
  ENDIF
  ! 	IF (IOPT.EQ.3) THEN
  !	  DO 290 I=1,NCALL
  ! 290	  XP(I)=Y(I)
  !	  INTOPT=8
  !	  IF (iset(MAPSIZE).EQ.1) THEN
  !	  INTOPT=15
  !	  ENDIF
  !	ENDIF
  !
  !	ID=1 distinguishes iterative mode, (IT,MAP) from single
  !	calculation mode (MS).
  !
  ID=1
  CALL BSUB (INTOPT,DSTEP,IMSCB)
300 RETURN 1
310 FORMAT (' Enter ',A4,'r limit FOR ',A)
320 FORMAT (/' Least squares refinement using k**',I1,' weighting'///' Initial parameters')
330 FORMAT (/I3,' maps using k**',I1,' weighting'//' Initial parameters')
340 FORMAT (I4,1X,A,2X,F8.3,6X,5F10.5)
350 FORMAT (I4,1X,A,2X,F8.3,' to ',F8.3)
END SUBROUTINE ITERQ

SUBROUTINE SETMTR
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_MTR
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  !
  !	Set the default MTRs according to current atomic number + ionicity
  !
  JSTART=1
  JEND=IPARNP
  GOTO 1
  ENTRY SETMTR1 (JIN,IATIN)
  JSTART=JIN
  JEND=JIN
  ATOM(JIN)=IATIN
  IATOM(JIN)=IATIN
1 DO J=JSTART,JEND
     IF (ATOM(J).GT.0.AND.ATOM(J).LE.103.) THEN
        IDJ=ATOM(J)
     ELSE
        IDJ=0
     ENDIF
     IF (IDJ.LE.0) THEN
        RMTR(J)=.1
     ELSE
        !
        !	Set default MTR according to ION
        !
        IF (RION(J)) 251,261,271
251     RMTR(J)=DEFMTRM1(IDJ)
        GOTO 281
261     RMTR(J)=DEFMTR(IDJ)
        GOTO 281
271     RMTR(J)=DEFMTRP1(IDJ)
     ENDIF
281  RMTR2(J)=RMTR(J)*DC
  ENDDO
  RETURN
END SUBROUTINE SETMTR

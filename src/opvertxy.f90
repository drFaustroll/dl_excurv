SUBROUTINE OPVERTXY (APHI,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - reflection about plane //z at 45 to  x
  !
  Use Common_convia
  APHI=PID2-APHI
  IS=IS+1
  RETURN 1
END SUBROUTINE OPVERTXY

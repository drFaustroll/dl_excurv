Module Common_TXY
  !  real TXY(4)
  !  SAVE TXY
  !  equivalence (TXMIN,TXY(1)),(TXMAX,TXY(2)),(TYMIN,TXY(3)),(TYMAX,TXY(4))
  use definition
  implicit none
  real(dp),dimension(4),public :: TXY
  real(dp),public :: TXMIN,TXMAX,TYMIN,TYMAX
end Module Common_TXY

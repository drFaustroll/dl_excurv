MODULE Common_LSQ

  !      COMMON /LSQ/ RJM(IPARNPOINTS*10*IPARNVARS),GI(IPARNPOINTS*10*IPARN
  !     1VARS+IPARNVARS*IPARNVARS),W(IPARNVARS*IPARNVARS+5*IPARNVARS+2*IPAR
  !     2NPOINTS*10),E(IPARNVARS)

  Use Definition
  Use Parameters
  implicit none
  private
  real(dp), dimension(:), public :: RJM(IPARNPOINTS*10*IPARNVARS)
  real(dp), dimension(:), public ::  GI(IPARNPOINTS*10*IPARNVARS+IPARNVARS*IPARNVARS)
  real(dp), dimension(:), public ::   W(IPARNVARS*IPARNVARS+5*IPARNVARS+2*IPARNPOINTS*10)
  real(dp), dimension(:), public ::   E(IPARNVARS)
end MODULE Common_LSQ

FUNCTION CALCANG (X1,Y1,Z1,X2,Y2,Z2,S)
  !======================================================================C
  Use Common_convia
  !	LOGICAL LERR
  R1=SQRT(X1*X1+Y1*Y1+Z1*Z1)
  R2=SQRT(X2*X2+Y2*Y2+Z2*Z2)
  IF (ABS(R1).LT.1.E-6.OR.ABS(R2).LT.1.E-6) THEN
     CALCANG=0.
  ELSE
     !	S=Y1*Z2-Z1*Y2+Z1*X2-X1*Z2+X1*Y2-Y1*X2
     S=X1*Y2-Y1*X2
     IF (ABS(S).LT.1.E-6) THEN
        S=Z1*X2-X1*Z2
        IF (ABS(S).LT.1.E-6) THEN
           S=Y1*Z2-Z1*Y2
        ENDIF
     ENDIF
     D=(X1*X2+Y1*Y2+Z1*Z2)/(R1*R2)
     IF (ABS(D).GT.1.) D=SIGN(1.,D)
     CALCANG=ACOS(D)
     IF (S.LT.0.) CALCANG=PI2-CALCANG
     IF (CALCANG.GT.PI2) CALCANG=CALCANG-PI2
  ENDIF
  RETURN
END FUNCTION CALCANG

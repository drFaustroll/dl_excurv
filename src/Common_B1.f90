MODULE Common_B1

  ! Module to replace COMMON block /B/
  !      COMMON /B/ ISET(IPARSETOPTS),ID,LC,LF,NPS,IFTSET(IPARFTOPTS),IXA,F
  !     1ITINDEX,RFAC,EINT,BACKGROUND,REXAFS,RDISTANCE,RXRAY,SSQ(0:IPARNS),
  !     2LRIS,LMDSN,IPR,LASTEXTYP,LASTSING,LASTMSC,IPREVEXTYP,IPREVSING,IPR
  !     3EVMSC,NOTREAD,DEBUG,NSAVE,CHISQU,IPTYPE,INFILE,OUTFILE,PLOTFILE,CO
  !     4MFILE,INTERM,OUTTERM,LOGFILE,HELPFILE,OUTDIFF,NPOS,NPOL,IEXITE,IED
  !     5GE,IEDGECODE(IPARNSP),NINITIAL(IPARNSP),LINITIAL(IPARNSP),JINITIAL
  !     5(IPARNSP),NINITIALS(9),LINITIALS(9),
  !     6JINITIALS(9),EDGECODE(IPARNSP),EDGENAME,EDGECODES(9),IZED(IPARNSP)
  !     7,ICOMMON,IEXGROUP(IPARNSP)

  !      EQUIVALENCE (ISET(1),NFIN),(ISET(2),IQUINN),(ISET(3),IUC),(ISET(4)
  !     1,IWEIGHT),(ISET(5),ISING),(ISET(6),IEXTYP),(ISET(7),ICOR),(ISET(8)
  !     2,IATABS),(ISET(9),IAVERAGE),(ISET(10),IREL),(ISET(11),IWAVE),(ISET
  !     3(12),JRCOR),(ISET(13),IMSC),(ISET(14),MAPSIZE),(ISET(15),IEXCH),(I
  !     4SET(16),IDEBUG),(ISET(17),IGROUND),(ISET(18),ITORTOISE),(ISET(19),
  !     5MSSYM),(LUNIT(1),INFILE),(ISET(20),USESIGMA),(ISET(21),IVIEW)

  USE Definition
  USE Parameters
!-st  USE Index_ISET
  implicit none
  private
  integer(i4b),  dimension(IPARSETOPTS), public :: ISET = (/2,1,0,2,0,2,0,0,0,1,2,0,0,1,1,0,1,0,1,0,0/)
  logical     ,  dimension(:), public :: LSET(2)
  logical     ,  public :: MSSYM    = .true.
  logical     ,  public :: USESIGMA = .false.
  integer(I4B),  public :: ID         =  0
  integer(I4B),  public :: LC         = 79
  integer(I4B),  public :: LF         =  2
  integer(I4B),  public :: NPS        =  1
  integer(I4B),  dimension(IPARFTOPTS), public :: IFTSET = (/1,2,2,1,1,1,1,0/)
  integer(I4B),  public :: IXA        = 1 
  real(dp) ,     public :: FITINDEX   = 0.
  real(dp) ,     public :: RFAC       = 0.
  real(dp) ,     public :: EINT       = 0.
  logical  ,     public :: BACKGROUND = .false.
  real(dp) ,     public :: REXAFS     = 0.
  real(dp) ,     public :: RDISTANCE  = 0.
  integer(I4B),  public :: LRIS       = 0
  integer(I4B),  public :: LMDSN      = 0
  integer(I4B),  public :: IPR        = 0
  logical     ,  public :: NOTREAD    =.true.
  logical     ,  public :: DEBUG      =.false.
!  logical     ,  public :: DEBUG      =.true.
  integer(I4B),  public :: IPTYPE     = 1
  integer(I4B),  public :: NSAVE      = 0
  integer(I4B),  public :: LASTEXTYP  = 0
  integer(I4B),  public :: LASTSING   = 0
  integer(I4B),  public :: LASTMSC    = 0
  integer(I4B),  public :: IPREVEXTYP = 0
  integer(I4B),  public :: IPREVSING  = 0
  integer(I4B),  public :: IPREVMSC   = 0
  integer(I4B),  public :: IEXITE     = 1


  integer(I4B),parameter, public :: INFILE   = 1
  integer(I4B),parameter, public :: OUTFILE  = 2
  integer(I4B),parameter, public :: PLOTFILE = 3
  integer(I4B),parameter, public :: COMFILE  = 4
  integer(I4B),parameter, public :: INTERM   = 5
  integer(I4B),parameter, public :: OUTTERM  = 6
  integer(I4B),parameter, public :: LOGFILE  = 7
  integer(I4B), dimension(IPARNSP), public :: IEDGECODE
  data IEDGECODE /IPARNSP*1/
  integer(I4B), dimension(IPARNSP), public :: NINITIAL
  data NINITIAL  /IPARNSP*1/
  integer(I4B), dimension(IPARNSP), public :: LINITIAL
  data LINITIAL  /IPARNSP*0/
  integer(I4B), dimension(IPARNSP), public :: JINITIAL
  data JINITIAL  /IPARNSP*1/
  integer(I4B), dimension(9), public :: NINITIALS = (/1,2,2,2,3,3,3,3,3/)
  integer(I4B), dimension(9), public :: LINITIALS = (/0,0,1,1,0,1,1,2,2/)
  integer(I4B), dimension(9), public :: JINITIALS = (/1,1,1,3,3,1,3,3,5/)
  character*2 , dimension(IPARNSP), public :: EDGECODE
  data EDGECODE/IPARNSP*'K'/
  character*4 ,                     public :: EDGENAME  ='CU'
  character*2 , dimension(9),       public :: EDGECODES = (/'K ','L1','L2','L3','M1','M2','M3','M4','M5'/)
  logical     ,                     public :: ICOMMON = .false.
END MODULE Common_B1


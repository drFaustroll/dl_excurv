SUBROUTINE QUAD (AA,BB,CC,ROOT1,ROOT2,NROOTS,*)
  !======================================================================C
  REAL*8 AA,BB,CC,ROOT,ROOT1,ROOT2,CALCDSQRT
  LOGICAL LERR
  NROOTS=0
  IF (DABS(AA).LT.1.E-10) THEN
     IF (BB.NE.0.) THEN
        ROOT1=-CC/BB
        ROOT2=ROOT1
        NROOTS=1
        RETURN
     ELSE
        RETURN 1
     ENDIF
  ENDIF
  ROOT=BB*BB-4*AA*CC
  IF (DABS(ROOT).LT.1.E-10) THEN
     ROOT=0.
     NROOTS=1
  ELSE
     ROOT=CALCDSQRT(ROOT,'root',LERR)
     IF (LERR) RETURN 1
     NROOTS=2
  ENDIF
  ROOT1=(-BB+ROOT)/(2.*AA)
  ROOT2=(-BB-ROOT)/(2.*AA)
  RETURN
END SUBROUTINE QUAD

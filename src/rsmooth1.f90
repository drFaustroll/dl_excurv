SUBROUTINE RSMOOTH1 (N2,X,Y,A,B,C,D,R,R1,T,T1,U,*)
  !======================================================================C
  Use Definition
  !
  !	Smoothing spline of C.H.Reinsch, Num.Math.,10,177,1967,
  !	with modified smoothing parameter of N.Binsted.
  !
  !	Input: N1,N2      number of first and last data point, N2 > N1
  !	       x,y,dy     arrays with x[i], y[i], dy[i]^(-2) as abscissa
  !	                  , ordinate and relative weight of i-th data
  !	                  point, respectively.
  !            s          a non-negative parameter which controls the
  !	                  extent of smoothing: the spline function f is
  !	                  determined such that
  !	                  INT(n1,n2) ((f(x[i])-y[i])/dy[i])^2<=s
  !
  !	Output: a,b,c,d   arrays, collecting the coefficients of the cubic
  !	                  spline f such that with h=xx-x[i] we have
  !	                  f(xx)=((d[i]*h+c[i])*h+b[i]*h+a[i]
  !	                  if x[i]<=xx<x[i+1]. i=N1,N2-1.
  !	                  Further more, a[N2]=f(x[N2]) and c[N2]=0
  !	                  while b[N2] and d[N2] are left undefined.
  !
  DIMENSION X(*),Y(*),A(*),B(*),C(*),D(*),R(0:*)
  DIMENSION R1(0:*),T(0:*),T1(0:*),U(0:*)
  real(float) :: B,C,D
  M2=N2+1
  R(0)=0
  R(1)=0
  R1(N2)=0
  U(0)=0
  U(1)=0
  U(N2)=0
  U(M2)=0
  M1=2
  M2=N2-1
  H=ABS(X(M1)-X(1))
  F=(Y(M1)-Y(1))/H
  DO I=M1,M2
     G=H
     H=ABS(X(I+1)-X(I))
     E=F
     IF (H.EQ.0.) CALL ERRMSG ('Unable to interpolate spectrum',*1000)
     F=(Y(I+1)-Y(I))/H
     A(I)=F-E
     T(I)=2.*(G+H)/3
     T1(I)=H/3.
     R(I)=1./H
     R1(I)=-1./G-1./H
  enddo
  DO I=M1,M2
     R1(I-1)=F*R(I-1)
     R(I)=1./(T(I)-F*R1(I-1))
     U(I)=A(I)-R1(I-1)*U(I-1)
     F=T1(I)
  enddo
  C(1)=U(1)
  C(N2)=U(N2)
  DO  I=M2,M1,-1
     C(I)=R(I)*U(I)-R1(I)*C(I+1)
  enddo
  CALL UCOPY (Y,A,N2)
  DO  I=1,M2
     H=ABS(X(I+1)-X(I))
     !	CALL TW ('CIDFFFF','I,C(I),H,U,R,R1 ',I,C(I),H,U(I),R(I),R1(I))
     D(I)=(C(I+1)-C(I))/(3*H)
     B(I)=(A(I+1)-A(I))/H-(H*D(I)+C(I))*H
  enddo
  RETURN
1000 RETURN 1
END SUBROUTINE RSMOOTH1

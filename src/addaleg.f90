SUBROUTINE ADDALEG (QK1,QKK,QK2,I,IEP,MEVEN,JLMAX)
  !======================================================================C
  USE Parameters
  USE Common_B2
  Use Common_ATMAT
  parameter(IPART=(IPARTLMAX+1)*(IPARTLMAX+1))
  COMPLEX(imag) :: QKK(IPART,IPART),QK1(IPART,*),QK2(IPART,*)
  LOGICAL       :: MEVEN
  LME=0
  MP=1
  IF (MEVEN) MP=2
  I1=0
  DO M1=-IEDGE,IEDGE
     I1=I1+1
     I2=0
     DO L2=0,JLMAX
        DO  M2=-L2,L2
           I2=I2+1
           QK2(I2,I1)=0.
           IF (MEVEN) LME=MOD(L2+M2,2)
           DO L4=LME,JLMAX
              I4=L4*L4+LME-MP+1
              DO  M4=-L4+LME,L4,MP
                 I4=I4+MP
                 QK2(I2,I1)=QK2(I2,I1)+QKK(I2,I4)*QK1(I4,I1)
              enddo
           enddo
           QK2(I2,I1)=TMAT(IEP,L2,I)*QK2(I2,I1)
        enddo
     enddo
  enddo
  RETURN
END SUBROUTINE ADDALEG

SUBROUTINE EFILE (TITL)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_PRDSN
  Use Common_B2
  !
  !	OUTPUT filename TO TERMINAL AND logfile AND close output file
  !
  !	TITL (I)   CHARACTER VARIABLE HOLDING FILE TYPE STRING USED
  !	           AS PART OF THE OUTPUT MESSAGE: 'TITL' PRINTED IN..
  !
  CHARACTER :: TITL*(*),MSG*80
  !
  !	Output message and close file
  !
  MSG=TITL
  CALL WTEXT (MSG(1:LEN(TITL))//' printed in '//PRDSN)
  CLOSE (OUTFILE)
  RETURN
END SUBROUTINE EFILE

SUBROUTINE WEIGHT (ARRAY,BARRAY,WARRAY,ILEN)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Include_XY
  DIMENSION ARRAY(*),BARRAY(*),WARRAY(*)

  JQ=IGF(5)-1
  PLOTXSCALE=RKC
  IF (IGF(4).EQ.2) PLOTXSCALE=EC
  PLOTYSCALE=1.
  CALL UCOPY (ARRAY,BARRAY,ILEN)
  IF (USESIGMA) THEN
     DO I=1,ILEN
        IF (ABS(SIGMA(I)).LT.1.E-6) THEN
           BARRAY(I)=ARRAY(I)*1.E6
        ELSE
           BARRAY(I)=ARRAY(I)/SIGMA(I)
        ENDIF
     ENDDO
  ELSEIF (JQ.NE.0) THEN
     DO J=1,JQ
        DO I=1,ILEN
           BARRAY(I)=BARRAY(I)*WARRAY(I)
        ENDDO
        PLOTYSCALE=PLOTYSCALE*RKC
     ENDDO
  ENDIF
  CALL ARRMAXA (BARRAY,ILEN,RQ(2))
  IF (iset(IATABS).EQ.0) RQ(1)=-RQ(2)	
  RETURN
END SUBROUTINE WEIGHT

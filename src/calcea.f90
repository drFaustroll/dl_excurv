SUBROUTINE CALCEA (TT1,TT2,DDP,EA)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Include_IPA
  !	Use Common_IPA
  COMPLEX*16  :: EA(-2:2,-2:2),CI
  real :: TT1,TT2,DDP
  real(float) :: ALPHAR,ALPHAI,GAMMAR,GAMMAI,ALPHA,GAMMA,BOT,ARG
  CI=CMPLX(0.,1.)
  ALPHAR=SIN(TT1)*COS(TT2)-COS(TT1)*SIN(TT2)*COS(DDP)
  ALPHAI=-SIN(TT2)*SIN(DDP)
  BOT=ALPHAR**2+ALPHAI**2
  IF (ABS(BOT).GT.1.E-5) THEN
     ARG=ALPHAR/SQRT(BOT)
     IF (ARG.GT.1.) THEN
        ALPHA=0.
     ELSEIF (ARG.LT.-1.) THEN
        ALPHA=PI
     ELSE
        ALPHA=ACOS(ARG)
     ENDIF
  ELSE
     ALPHA=0.
  ENDIF
  GAMMAR=SIN(TT1)*COS(TT2)*COS(DDP)-COS(TT1)*SIN(TT2)
  GAMMAI=SIN(TT1)*SIN(DDP)
  BOT=GAMMAR**2+GAMMAI**2
  IF (ABS(BOT).GT.1.E-5) THEN
     ARG=GAMMAR/SQRT(BOT)
     IF (ARG.GT.1.) THEN
        GAMMA=0.
     ELSEIF (ARG.LT.-1.) THEN
        GAMMA=PI
     ELSE
        GAMMA=ACOS(ARG)
     ENDIF
  ELSE
     GAMMA=0.
  ENDIF
  DO I=-2,2
     DO J=-2,2
	EA(I,J)=EXP(CI*(+ALPHA*real(I,kind=dp)-GAMMA*real(J,kind=dp)))
     ENDDO
  ENDDO
  !	WRITE (7,'(A,3F8.3,1X,2F8.3)') 'EA',T1,T2,DP,ALPHA,GAMMA
  !	DO I=-2,2
  !	WRITE (7,'(10F8.3)') (EA(I,J),J=-2,2)
  !	ENDDO
  RETURN
END SUBROUTINE CALCEA

FUNCTION DLOGIN (E,L,NLOW,NUPP,INT,PIN)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_POT
  !
  !	DLOGIN finds wave-functions and log derivatives (=DLOGIN) by
  !	inward integration of Schrodinger equation.
  !
  DIMENSION RPL(IPARGRID),Y(10),X(10)
  !
  !	Calculate number of inward integration points
  !
10 NSOL=NUPP-NLOW+1
  !
  !	Calculate starting values for inward integration
  !	Following Hartree's book
  !
  DO J=1,5
     X(J)=GRID(NUPP-4+J)
     Y(J)=VMT(NUPP-4+J)
  enddo
  !
  ARG10=0.5*(X(4)+X(3))
  RDEL=0.5*(X(4)-X(3))
  RMID=ARG10
  CALL LARGIT (Y,X,ARG10,5)
  UMID=ARG10
  XL=real(L*(L+1),kind=dp)
  !
  FCN=UMID+XL/(RMID*RMID)-E
  IF (FCN.GT.0.) GOTO 30
  NUPP=NUPP+1
  GOTO 10
30 RDELSQ=RDEL*RDEL
  RT1=0.
40 RTSQ=FCN*RDELSQ*(1.+RT1)
  RT2=SQRT(RTSQ)
  IF (ABS(RT1-RT2).LT.1.0D-5) GOTO 50
  RT1=RT2
  GOTO 40
  !
50 X(1)=X(4)
  X(2)=RMID
  Y(3)=1.+RT2
  Y(2)=1.
  Y(1)=1./Y(3)
  XI=X(1)-0.1*RDEL
  CALL LARGIT (Y,X,XI,3)
  DPL(NSOL)=10.*X(1)*(Y(1)-XI)/RDEL
  WPL(NSOL)=Y(1)
  XI=X(3)+0.1*RDEL
  CALL LARGIT (Y,X,XI,3)
  DPL(NSOL-1)=10.*X(3)*(XI-Y(3))/RDEL
  WPL(NSOL-1)=Y(3)
  !
  !	Using starting values just calculated solve radial schrodinger
  !	equation after Fox and Goodwin
  !
  XL=real(L*(L+1),kind=dp)
  H=-DXLS(INT)
  NSOLM2=NSOL-2
  !
  ALPH2=EXP(2.*XGD(NUPP))*(VMT(NUPP)-E)+XL
  ALPH3=EXP(2.*XGD(NUPP-1))*(VMT(NUPP-1)-E)+XL
  !
  DO J=1,NSOLM2
     JSUB=NSOLM2+1-J
     ALPH1=ALPH2
     ALPH2=ALPH3
     NSUB=NUPP-1-J
     ALPH3=EXP(2.*XGD(NSUB))*(VMT(NSUB)-E)+XL
     BETA=1.-H/3.-H*H*ALPH3/9.
     PLP2=WPL(JSUB+2)
     DPLP2=DPL(JSUB+2)
     !
     WPL(JSUB)=((1.-H/3.+H*H*ALPH1/9.)*PLP2+4.*H*(DPL(JSUB+1)+0.5*DPLP2)/3. &
          +4.*H*H*ALPH2*WPL(JSUB+1)/9.)/BETA
     !
     DPL(JSUB)=((1.+H/3.+H*H*ALPH3/9.)*DPLP2+4.*H*(1.+H*ALPH3/3.)*DPL(JSUB+1)/3. &
          + 4.*H*ALPH2*WPL(JSUB+1)/3.+H*(ALPH3+ALPH1)*PLP2/3.)/BETA
  enddo
  !
  !	Calculate log derivative
  !
  DLOGIN=EXP(-XGD(NSUB))*(DPL(1)/WPL(1)-1.)
  !
  !	Perform integration of radial function from matching radius to infinity(!)
  !
  PNORM=WPL(1)
  DO J=1,NSOL
     N=NLOW-1+J
     RPL(J)=EXP(XGD(N))*WPL(J)*WPL(J)
     PT=WPL(J)
     WPL(J)=EXP(-XGD(N))*WPL(J)
     DPL(J)=EXP(-2.*XGD(N))*(DPL(J)-PT)
  enddo
  !
  ALIM=XGD(NLOW)
  BLIM=XGD(NUPP)
  CALL SMPSN (DXLS(INT),ALIM,ALIM,BLIM,RPL,AREA,NSOL)
  !
  !	Divide by normalisation to get - PIN
  !
  PIN=AREA/(PNORM*PNORM)
  !
  RETURN
END FUNCTION DLOGIN

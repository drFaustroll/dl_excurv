SUBROUTINE RDTRACE (IUN)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_PMTX
  Use Common_MAPS
  !
  !	Read a trace from an iteration map file
  !
  !	    IUN (I)   UNIT NUMBER FOR READING FILE
  !
  !	RETURN 1  ERROR IN READING MAP
  !
  !	The map file may start with alphanumeric records. The first
  !	numeric only record is taken as the descriptor record of the
  !	first map. There are assumed to be 65 numeric records per
  !	map including the descriptor record. further alphanumeric
  !	records may only occur after the end of all the map records.
  !
  DIMENSION NTRACE(2),TRACE(999,2)
  LOGICAL ERR,NUMS
  CHARACTER*80 BUF
  NUMS=.FALSE.
  !
  !	Read header records (until a numeric record has been found) or
  !	read the next descriptor record
  !
  DO  KK=1,2
     NTRACE(KK)=0
     MAXNUM=-1
     REWIND IUN
     DO  IW=OUTTERM,LOGFILE,OUTDIFF
   	WRITE (IW,*) 'In RDTRACE: ',IVARS(K),MAPVARS(KK)
     enddo
20   READ(IUN,110,END=50,ERR=100)BUF
     IF (BUF.EQ.' ') GOTO 20
     IF (BUF(1:12).EQ.'Minimum Pred') GOTO 50
     IF (BUF(1:9).EQ.'    ERROR') GOTO 50
     IL=0
     DO  N=1,10
	CALL NEXFLD(BUF,IL,M1,M2,*40)
	IF (BUF(M1:M1+3).EQ.MAPVARS(KK)(1:4).AND.BUF(M1+4:M1+4).EQ.'=') THEN
           IL=M1+5
           CALL NEXFLD(BUF,IL,MM1,MM2,*40)
           X=FPVAL(BUF(MM1:MM2),ERR)
           IF (.NOT.ERR) THEN
              NTRACE(KK)=NTRACE(KK)+1
              TRACE(NTRACE(KK),KK)=X
           ENDIF
           IF (DEBUG) THEN
              DO  IW=OUTTERM,LOGFILE,OUTDIFF
                 WRITE (IW,*) NTRACE(KK),' ',BUF(M1:M1+3),'=',BUF(MM1:MM2)
              enddo
           ENDIF
	ENDIF
	IF (NTRACE(KK).EQ.MAXNUM) GOTO 50
40	CONTINUE
     enddo
     GOTO 20
50   CONTINUE
  enddo
  DO  IW=OUTTERM,LOGFILE,OUTDIFF
     WRITE (IW,*) 'NTRACE:',NTRACE,' MAXNUM',MAXNUM
  enddo
  IF (NTRACE(1).EQ.NTRACE(2).AND.NTRACE(1).GT.1) THEN
     DO  KK=1,NTRACE(1)
        WRITE (66,*) KK,TRACE(KK,1),TRACE(KK,2)
     enddo
     IF (IGF(2).NE.3) THEN
        CALL MAP (X1,X2,Y1,Y2)
        CALL PTJOIN (TRACE(1,1),TRACE(1,2),1,NTRACE(1),1)
        DO  II=1,NTRACE(1)
           ISYM=MOD(II,10)+48
           CALL PLOTC (TRACE(II,1),TRACE(II,2),CHAR(ISYM))
        enddo
     ENDIF
     IF (IGF(2).NE.1) THEN
        KUN=PLOTFILE
        WRITE (KUN) 'TRAC'
        WRITE (KUN) NTRACE(1),((TRACE(K,KK),K=1,NTRACE(1)),KK=1,2)
     ENDIF
     RETURN
  ENDIF
  !
  !	Error conditions
  !
100 CALL ERRMSG ('Error in trace file: ',*101)
101 CALL WTEXT (BUF)
  RETURN
110 FORMAT(A)
END SUBROUTINE RDTRACE

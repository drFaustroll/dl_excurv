SUBROUTINE GETAXIS (MS,JTN,NF,IG,RX,RY,RZ,FANG,*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Index_INDS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  !
  !	Return axis R and angle FANG to rotate a unique axis so as to be
  !	parallel to z.
  !
  LOGICAL PLANE,LERR
  PLANE=.FALSE.
  JA=IPLA(IG)
  JB=IPLB(IG)
  JRAD=IRAD(IABS(ICLUS(NF)))
  !
  !	Calculate A =    normal to U in horizontal plane
  !	          Z = 1. normal to molecular plane
  !	              2. normal to U in vertical plane
  !	              3. the z-axis
  !	          FANG   angle to rotate A // to z-axis
  !	          R      axis requiring rotation FANG
  !
  ZX=0.
  ZY=0.
  ZZ=1.
  TX=UX(NF)-UX(JRAD)
  TY=UY(NF)-UY(JRAD)
  TZ=UZ(NF)-UZ(JRAD)
  AX=TY
  AY=-TX
  AZ=0.
  !	IF (AY*AY+AX*AX.GT..01) THEN
  IF (JA.NE.JB.AND.NU(JA).EQ.IG.AND.NU(JB).EQ.IG) THEN
     PLANE=.TRUE.
     TPLAX=UX(JA)-UX(NF)
     TPLAY=UY(JA)-UY(NF)
     TPLAZ=UZ(JA)-UZ(NF)
     TPLBX=UX(JB)-UX(NF)
     TPLBY=UY(JB)-UY(NF)
     TPLBZ=UZ(JB)-UZ(NF)
     ZX=TPLAY*TPLBZ-TPLAZ*TPLBY
     ZY=TPLAZ*TPLBX-TPLAX*TPLBZ
     ZZ=TPLAX*TPLBY-TPLAY*TPLBX
  ELSEIF (AY*AY+AX*AX.GT..01) THEN
     !	  ELSE
     ZX=TY*AZ-TZ*AY
     ZY=TZ*AX-TX*AZ
     ZZ=TX*AY-TY*AX
  ENDIF
  !	ENDIF
  IF (JTN.EQ.inds(INDR)) THEN
     !
     !	Here for R
     !	A is normal to U and Z, not neccessarly in horiz plane
     !
     AX=TY*ZZ-TZ*ZY
     AY=TZ*ZX-TX*ZZ
     AZ=TX*ZY-TY*ZX
  ELSEIF (JTN.EQ.inds(INDANG)) THEN
     !
     !	Here for ANG
     !
     !	If possible, A = cross product of two sides including the angle
     !
     AX=TY*(UZ(MS)-UZ(NF))-TZ*(UY(MS)-UY(NF))
     AY=TZ*(UX(MS)-UX(NF))-TX*(UZ(MS)-UZ(NF))
     AZ=TX*(UY(MS)-UY(NF))-TY*(UX(MS)-UX(NF))
     IF (ABS(AX).LT.1.E-6.AND.ABS(AY).LT.1.E-6.AND.ABS(AZ).LT.1.E-6) THEN
        !
        !	If not, A = normal to U and Z - not neccessarily // z
        !
        BX=-(TY*ZZ-TZ*ZY)
        BY=-(TZ*ZX-TX*ZZ)
        BZ=-(TX*ZY-TY*ZX)
        AX=+(TY*BZ-TZ*BY)
        AY=+(TZ*BX-TX*BZ)
        AZ=+(TX*BY-TY*BX)
     ENDIF
     IF (ABS(AX).LT.1.E-6.AND.ABS(AY).LT.1.E-6.AND.ABS(AZ).LT.1.E-6)THEN
        !
        !	Last resort - A // Z
        !
	AX=ZX
	AY=ZY
	AZ=ZZ
     ENDIF
  ELSEIF (JTN.EQ.inds(INDTH)) THEN
     AX=TX
     AY=TY
     AZ=TZ
  ELSEIF (JTN.GE.inds(INDPHI).AND.JTN.LE.inds(INDZ)) THEN
     AX=ZX
     AY=ZY
     AZ=ZZ
  ELSEIF (JTN.EQ.inds(INDPANG)) THEN
     FANG=0.
     RETURN
  ENDIF
  IF (ABS(AX).LT.1.E-6.AND.ABS(AY).LT.1.E-6.AND.ABS(AZ).LT.1.E-6) THEN
     AX=1.
     AY=0.
     AZ=0.
  ENDIF
  IF (DEBUG) WRITE (LOGFILE,*) 'The axis normal to the molecular',' plane is:',ZX,ZY,ZZ
  IF (DEBUG) WRITE (LOGFILE,*) 'The axis in the horizontal',' plane is:',AX,AY,AZ
  IF (JTN.EQ.inds(INDTWST)) THEN
     !
     !	TWST
     !
     RX=ZX
     RY=ZY
     RZ=ZZ
  ELSEIF (JTN.EQ.inds(INDANG).OR.JTN.EQ.inds(INDTILT)) THEN
     !
     !	TILT
     !
     RX=AX
     RY=AY
     RZ=AZ
     IF (JTN.EQ.inds(INDANG)) THEN
        S=RZ
        IF (ABS(S).LT.1.E-6) S=RY
        IF (ABS(S).LT.1.E-6) S=RX
        IF (S.LT.0.) THEN
           RX=-RX
           RY=-RY
           RZ=-RZ
        ENDIF
     ENDIF
  ELSE
     AR=SQRT(AX*AX+AY*AY+AZ*AZ)
     IF (AR.EQ.0.) RETURN 1
     FANG=(AX*0.+AY*0.+AZ*1.)/AR
     FANG=CALCACOS (FANG,'fang',LERR)
     IF (LERR) RETURN 1
     RX=AZ*0.-AY*1.
     RY=AX*1.-AZ*0.
     RZ=AY*0.-AX*0.
  ENDIF
  RETURN
END SUBROUTINE GETAXIS
